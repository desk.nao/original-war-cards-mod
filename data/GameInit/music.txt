GAMEINIT
  MUSIC
    WAV 1 1 Level_Up
    WAV 1 2 Level_Up
    WAV 1 3 Level_Up
    WAV 1 4 Level_Up
    WAV 1 5 Level_Up
    WAV 4 1 Level_Up
    WAV 4 2 Level_Up
    WAV 4 3 Level_Up
    WAV 4 4 Level_Up
    WAV 5 5 Level_Up
    WAV 5 4 Level_Up
    WAV 2 1 Level_Up     
    WAV 2 2 Level_Up
    WAV 2 3 Level_Up
    WAV 2 4 Level_Up
    WAV 2 5 Level_Up
    WAV 3 1 Level_Up
    WAV 3 2 Level_Up
    WAV 3 3 Level_Up
    WAV 3 4 Level_Up
    WAV 3 5 Level_Up 
  END_OF_MUSIC
END_OF_GAMEINIT
{ WAV <side> <class> <name> }
{ side: 0 - any, 1 - Amer, 2 - ArMerc, 3 - Rus, 4 - ArNobl, 5 - other }
{ class: 0 -  auto, 1 - combat, 2 - preparation, 3 - recon, 4 - victory, 5 - menu, 6 - alliance }
{ name: string }
            