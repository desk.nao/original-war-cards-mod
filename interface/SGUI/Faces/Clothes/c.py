import os
from PIL import Image

# Get the directory where the script is located
script_dir = os.path.dirname(os.path.abspath(__file__))

# Loop through all files in the directory
for file_name in os.listdir(script_dir):
    if file_name.endswith(('.bmp', '.png')):
        file_path = os.path.join(script_dir, file_name)
        output_path = os.path.join(script_dir, os.path.splitext(file_name)[0] + '.png')
        
        # Open the image file and process transparency
        with Image.open(file_path) as img:
            img = img.convert('RGBA')
            data = img.getdata()
            new_data = []
            for item in data:
                # Check if the pixel is pure green
                if item[:3] == (0, 255, 0):
                    new_data.append((0, 255, 0, 0))  # Set to transparent
                else:
                    new_data.append(item)
            img.putdata(new_data)
            img.save(output_path, 'PNG')
        
        print(f'Processed: {file_name} -> {os.path.basename(output_path)}')

print('All BMP and PNG files have been processed with green pixels set to full transparency.')
