import os
from PIL import Image

# Function to process each BMP image
def process_image(file_path):
    # Open the image
    img = Image.open(file_path)

    # Get the image size
    width, height = img.size

    # Create a new image with 100 pixels padding around the original image (black padding)
    new_img = Image.new("RGB", (width + 200, height + 200), (0, 255, 0))  # black padding
    new_img.paste(img, (100, 100))  # Paste original image in the middle

    # Convert image to pixels
    pixels = new_img.load()

    # Loop through all pixels and change green pixels to black
    for y in range(new_img.height):
        for x in range(new_img.width):
            r, g, b = pixels[x, y]
            # Check if the pixel is green
            if g > r and g > b:
                pixels[x, y] = (0, 255, 0)  # Change to black

    # Save the new image as a .png file
    new_file_path = os.path.splitext(file_path)[0] + ".png"
    new_img.save(new_file_path)
    print(f"Processed and saved: {new_file_path}")

# Get the current directory
current_dir = os.path.dirname(os.path.realpath(__file__))

# Iterate over all files in the directory
for filename in os.listdir(current_dir):
    if filename.lower().endswith(".bmp"):
        file_path = os.path.join(current_dir, filename)
        process_image(file_path)
