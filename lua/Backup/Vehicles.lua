function getSpriteAnimFrameIndexs(NAME)
    local spr = SPRITE_DATA[NAME];
    
    local r = {};
   
    for i = 1, spr.FRAME_COUNT do
        r[i] = spr.FRAME_INDEXS[NAME..string.format("%04d", i-1)];
    end;
    
    return r;
end;

function PrepareSprite(NAME)
  local sprite = SPRITE_DATA[NAME];
  sprite.TEX_COORDS = {}
  sprite.TRUEINDEXS = getSpriteAnimFrameIndexs(NAME);

  local tw,th = sprite.TEX_WIDTH, sprite.TEX_HEIGHT;
  local fc;

  for i = 1, sprite.FRAME_COUNT do
    fc = sprite.FRAME_COORDS[i];
    sprite.TEX_COORDS[sprite.TRUEINDEXS[i]] = {X=fc[1],Y=fc[2],W=sprite.FRAME_SIZES[i].WIDTH/tw,H=sprite.FRAME_SIZES[i].HEIGHT/th};
  end;
end;

function LoadSprite(NAME)
  include('parts/'..NAME);
  PrepareSprite(NAME);
end;

LoadSprite('wh-a-cylinder');
LoadSprite('w-a-dmg');
LoadSprite('p-r-mw');

local sprite = SPRITE_DATA['wh-a-cylinder'];
local texcoords = sprite.TEX_COORDS[FRAME_ID];


include('_chassis')

local vehicleClass = function()
	local self = {Chassis={},Weapon={},Control={},Engine={},Frame=23}

	self.List = 
	{
	    CONTROL = 
	    {
	        {
	            {
	                {texture='c-u-morphling-man0006', w=60, h=84},
	                {texture='c-u-mediumw-man0006', w=90, h=98},
	                {texture='c-u-mediumt-man0006', w=90, h=98},
	                {texture='c-u-heavyt-man0006', w=136, h=138},
	                {texture='c-u-lw-man0006', w=86, h=92}
	            },
	            {
	                {texture='c-u-morphling-rem0006', w=22, h=36},
	                {texture='c-u-mediumw-rem0006', w=22, h=36},
	                {texture='c-u-mediumt-rem0006', w=22, h=36},
	                {texture='c-u-lw-rem0006', w=22, h=36},
	                {texture='c-u-heavyt-rem0006', w=22, h=36}
	            },
	            {
	                {texture='c-u-morphling-comp0006', w=62, h=86},
	                {texture='c-u-mediumt-comp0006', w=46, h=62},
	                {texture='c-u-mediumw-comp0006', w=46, h=62},
	                {texture='c-u-lw-comp0006', w=40, h=54},
	                {texture='c-u-heavyt-comp0006', w=46, h=62}
	            }
	        },
	        {
	            {
	                {texture='c-a-hovercraft-man0006', w=42, h=66},
	                {texture='c-a-ht-man0006', w=60, h=82},
	                {texture='c-a-lw-man0006', w=50, h=72},
	                {texture='c-a-mw-man0006', w=60, h=80}
	            },
	            {
	                {texture='c-a-mw-rem0006', w=16, h=24},
	                {texture='c-a-lw-rem0006', w=16, h=24},
	                {texture='c-a-ht-rem0006', w=16, h=24},
	                {texture='c-a-hovercraft-rem0006', w=16, h=24}
	            },
	            {
	                {texture='c-a-hovercraft-comp0006', w=54, h=66},
	                {texture='c-a-ht-comp0006', w=60, h=76},
	                {texture='c-a-lw-comp0006', w=50, h=72},
	                {texture='c-a-mw-comp0006', w=68, h=90}
	            }
	        },
	        {
	            {
	                {texture='c-r-mediumt-man0006', w=86, h=102},
	                {texture='c-r-mw-man0006', w=86, h=102},
	                {texture='c-r-heavyw-man0006', w=84, h=98},
	                {texture='c-r-heavyt-man0006', w=84, h=100}
	            },
	            {nil},
	            {
	                {texture='c-r-mw-comp0006', w=86, h=92},
	                {texture='c-r-mediumt-comp0006', w=86, h=92},
	                {texture='c-r-heavyw-comp0006', w=86, h=92},
	                {texture='c-r-heavyt-comp0006', w=86, h=92},
	                {texture='c-r-heavyt-comp0006', w=86, h=92}
	            }
	        }
	    },

	    ENGINE =
	    {
	        {
	            {
	                {texture='e-u-morphling-comb0006', w=132, h=138},
	                {texture='e-u-mediumw-comb0006', w=86, h=92},
	                {texture='e-u-mediumt-comb0006', w=86, h=92},
	                {texture='e-u-heavyt-comb0006', w=134, h=134},
	                {texture='e-u-lw-comb0006', w=90, h=96}
	            },
	            {
	                {texture='e-u-mediumw-solar0006', w=92, h=106},
	                {texture='e-u-lw-solar0006', w=92, h=104},
	                {texture='e-u-mediumt-solar0006', w=92, h=106}
	            },
	            {
	                {texture='e-u-morphling-siber0006', w=118, h=128},
	                {texture='e-u-mediumw-siber0006', w=84, h=94},
	                {texture='e-u-heavyt-siber0006', w=132, h=140},
	                {texture='e-u-mediumt-siber0006', w=84, h=94}
	            }
	        },
	        {
	            {
	                {texture='e-a-hovercraft-comb0006', w=82, h=88},
	                {texture='e-a-ht-comb0006', w=72, h=98},
	                {texture='e-a-lw-comb0006', w=84, h=96},
	                {texture='e-a-mw-comb0006', w=80, h=106}
	            },
	            {
	                {texture='e-a-hovercraft-solar0006', w=82, h=88},
	                {texture='e-a-ht-solar0006', w=86, h=112},
	                {texture='e-a-lw-solar0006', w=82, h=100},
	                {texture='e-a-mw-solar0006', w=84, h=108}
	            },
	            {
	                {texture='e-a-ht-siber0006', w=78, h=102},
	                {texture='e-a-mw-siber0006', w=70, h=100}
	            }
	        },
	        {
	            {
	                {texture='e-r-mw-comb0006', w=94, h=128},
	                {texture='e-r-heavyt-comb0006', w=148, h=160},
	                {texture='e-r-heavyw-comb0006', w=148, h=160},
	                {texture='e-r-mediumt-comb0006', w=94, h=128}
	            },
	            {nil},
	            {
	                {texture='e-r-mw-siber0006', w=88, h=102},
	                {texture='e-r-heavyt-siber0006', w=144, h=146},
	                {texture='e-r-heavyw-siber0006', w=144, h=146},
	                {texture='e-r-mediumt-siber0006', w=88, h=102}
	            }
	        }
	    },

		WEAPONS =
		{
		    { -- US
		        {texture='w-u-mg0006', w=62, h=60},
		        {texture='w-u-lightgun0006', w=70, h=64},
		        {texture='w-u-gg0006', w=48, h=48},
		        {texture='w-u-double0006', w=80, h=78},
		        {texture='w-u-heavy0006', w=96, h=92},
		        {texture='w-u-rocket0006', w=46, h=64},
		        {texture='w-u-srock0006', w=106, h=96},
		        {texture='w-u-las0006', w=46, h=62},
		        {texture='w-u-dlas0006', w=40, h=46},
		        {texture='n-u-radar0006', w=36, h=32},
		        {{texture='n-cargo-u-mediumw0006', w=100, h=112}, {texture='n-cargo-u-morphling0006', w=132, h=140}, {texture='n-cargo-u-heavyt0006', w=154, h=160}, {texture='n-cargo-u-mediumt0006', w=100, h=112}},
		        {texture='n-u-crane0006', w=88, h=156},
		        {{texture='n-doze-u-heavyt0006', w=98, h=96}, {texture='n-doze-u-morphling0006', w=96, h=94}},
		        {texture='w-u-srockrem0006', w=76, h=88}
		    },
		    { -- AR (22)
		        {texture='w-a-mmb0006', w=40, h=50},
		        {texture='w-a-lg0006', w=66, h=66},
		        {texture='w-a-dmg0006', w=60, h=60},
		        {texture='w-a-gg0006', w=64, h=66},
		        {texture='w-a-flame0006', w=60, h=60},
		        {texture='w-a-gun0006', w=74, h=74},
		        {texture='w-a-rocket0006', w=38, h=54},
		        {{texture='w-a-spb-a-hovercraft0006', w=98, h=120}, {texture='w-a-spb-a-ht0006', w=98, h=120}, {texture='w-a-spb-a-lw0006', w=98, h=120}, {texture='w-a-spb-a-mw0006', w=98, h=120}},
		        {texture='n-a-radar0006', w=38, h=64},
		        {texture='n-a-control0006', w=62, h=102},
		        {{texture='n-cargo-a-ht0006', w=100, h=114}, {texture='n-cargo-a-mw0006', w=98, h=116}}
		    },
		    { -- RU (42)
		        {texture='w-r-hmg0006', w=86, h=94},
		        {texture='w-r-gg0006', w=106, h=100},
		        {texture='w-r-gun0006', w=86, h=100},
		        {texture='w-r-rlaunch0006', w=58, h=78},
		        {texture='w-r-hg0006', w=108, h=102},
		        {texture='w-r-rocket0006', w=84, h=102},
		        {texture='w-r-srocket0006', w=170, h=162},
		        {texture='w-r-srockrem0006', w=168, h=176},
		        {texture='w-r-lapser0006', w=74, h=90},
		        {{texture='n-cargo-r-heavyt0006', w=128, h=150}, {texture='n-cargo-r-heavyw0006', w=128, h=150}, {texture='n-cargo-r-mediumt0006', w=80, h=114}, {texture='n-cargo-r-mw0006', w=80, h=114}},
		        {texture='n-r-crane0006', w=110, h=136},
		        {{texture='n-doze-r-heavyt0006', w=114, h=148}, {texture='n-doze-r-heavyw0006', w=114, h=148}},
		        {texture='w-r-behemoth-flame0006', w=66, h=60},
		        {texture='w-r-behemoth-gun0006', w=58, h=78},
		        {texture='w-r-behemoth-mgun0006', w=60, h=88},
		        {texture='w-r-behemoth-turret0006', w=70, h=92}
		    }
		}
	}

	self.List.Chassis = {}

	self.SCREEN_FBO = fboclass.make(200,200,true,false,0,false)

	self.GMZ_Shader = loadGLSL('gmztest')
	self.GMZ_Shader.zpos = self.GMZ_Shader:getLocation('zpos')
	self.GMZ_Shader.TO = self.GMZ_Shader:getLocation('TO')
	self.GMZ_Shader.QS = self.GMZ_Shader:getLocation('QS')

	self.Picture = getElementEX(nil,anchorLT,XYWH(200,400,200,200),true,{nomouseevent=true})

	self.MakePartTexture = function(TEXTURE, W, H, X, Y, Z)
	  local r = {}
	  r.COLOUR_TEXTURE = loadOGLTexture("SGUI/Cards/Vehicles/"..TEXTURE..'.png',true,false,false,false)
	  r.HEIGHT_TEXTURE = loadOGLTexture("SGUI/Cards/Vehicles/"..TEXTURE..'z.png',true,false,false,false)
	  r.W = W
	  r.H = H
	  r.X = X or nil
	  r.Y = Y or nil
	  r.Z = Z or nil
	  return r
	end

	self.renderPart = function(SHADER,PART,X,Y,Z)
        OGL_ACTIVE_TEXTURE(GL_TEXTURE1)
        PART.HEIGHT_TEXTURE:bind()
        OGL_ACTIVE_TEXTURE(GL_TEXTURE0)
        PART.COLOUR_TEXTURE:bind()

        local W,H = PART.COLOUR_TEXTURE.W, PART.COLOUR_TEXTURE.H

        SHADER:use()
        SHADER:setUniform4F(SHADER.QS,X-math.floor(W/2),Y-math.floor(H/2),W,H)
        SHADER:setUniform4F(SHADER.TO,0,0,1,1)
        SHADER:setUniform1I(SHADER.zpos,Z)
        OGL_DRAW_ARRAYS(GL_TRIANGLES,0,6)
    end

	self.Render = function()
		self.SCREEN_FBO:setClearColour(20,20,20,100,false);
        self.SCREEN_FBO:doBegin()
        OGL_BEGIN()
            OGL_GLENABLE(GL_BLEND)
            OGL_QUAD_BEGIN2D(self.SCREEN_FBO.W,self.SCREEN_FBO.H)  
                OGL_GLENABLE(GL_DEPTH_TEST)

                self.renderPart(self.GMZ_Shader, self.Chassis, 100, 100, 10)

				for k, v in ipairs(self.Wheels) do
					self.renderPart(self.GMZ_Shader, v, 100+v.X+0, 100+v.Y+0, v.Z)
				end

                --local W = self.Chassis.Weapon
				--self.renderPart(self.GMZ_Shader, self.Weapon, 100+W.x-self.Chassis.W/2, 100+W.y, 50)

            OGL_QUAD_END2D()
            OGL_GLDISABLE(GL_BLEND)
        OGL_END()
        self.SCREEN_FBO:doEnd()
        SGUI_settextureid(self.Picture.ID,self.SCREEN_FBO:getTextureID(),200,200,200,200)
	end

	self.getSeqFrame = function(FRAME)
	    return string.format("%04d", FRAME)
	end

	self.getFrame = function()
		return self.Frame
	end

	self.setChassis = function(Nation, ID)
		local Chassis
		if Nation == 1 then
			Chassis = us_Chassis[ID]
		end
		if Nation == 2 then
			Chassis = ar_Chassis[ID]
		end
		if Nation == 3 then
			Chassis = ru_Chassis[ID]
		end
		self.Chassis = self.MakePartTexture(Chassis.Texture..self.getSeqFrame(1 + self.getFrame()), Chassis.W, Chassis.H, nil, nil, 0)
		self.Chassis.Weapon = Chassis[1][3]
		self.Wheels = {}
		local newX, newY
		for k, v in ipairs(Chassis.Wheels) do

			--if k == 1 then
				newX = Chassis[1 + self.getFrame()][2+k].x -- -4
				newY = -Chassis[1 + self.getFrame()][2+k].y -- -6
				self.Wheels[k] = self.MakePartTexture(v.Texture..self.getSeqFrame(1 + self.getFrame()), v.W, v.H, newX, newY, Chassis[1+ self.getFrame()][2+k].z)
			--end
		end
		setText(shader.LABEL, 'Wheels: ' .. 1 + self.getFrame() .. ' and Chassis: ' ..Chassis.Texture..self.getSeqFrame(1 + self.getFrame()))
	end

	self.setWeapon = function(Nation, ID)
		local Weapon = self.List.WEAPONS[Nation][ID]
		self.Weapon = self.MakePartTexture(Weapon.texture, Weapon.w, Weapon.h, 0, 0, 0)
	end

	return self
end


Vehicles = vehicleClass()




SHADER:setUniform4F(SHADER.TO,texcoords.X,texcoords.Y,texcoords.W,texcoords.H)

Vehicles.Render()