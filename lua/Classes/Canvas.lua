LuaEvent = {}

function LuaEvent:callback(UNIT, HEALTHDIFF)
	local POS = OW_HEXTOSCREEN(UNIT.XS,UNIT.YS)
	local COLOUR

	if -HEALTHDIFF > 75 then
	    COLOUR = RGB(255, 0, 0)
	elseif -HEALTHDIFF > 50 then
	    COLOUR = RGB(255, 75, 75)
	elseif -HEALTHDIFF > 25 then
	    COLOUR = RGB(255, 100, 100)
	elseif -HEALTHDIFF > 0 then
	    COLOUR = RGB(255, 150, 150)
	end

	if HEALTHDIFF > -1 then
	    COLOUR = RGB(75, 255, 75)
	end

	local T = getLabelEX(Canvas.canvas,anchorNone,XYWH(math.random(POS.X-10,POS.X+10), math.random(POS.Y-10,POS.Y+10), 0, 0),SmallFonts_10,HEALTHDIFF,{shadowtext=true,font_colour=COLOUR,text_valign=ALIGN_TOP,text_halign=ALIGN_MIDDLE})
	AddSingleUseTimer(0.15,'sgui_delete('..T.ID..')')
end

OW_LFC_ADD(LFC_UNIT_HEALTH_CHANGE, LuaEvent.callback, LuaEvent)

createCanvas = function ()
    local self = {MVR=nil}

    self.canvas = getElementEX(nil,anchorLTRB,XYWH(0,0,ScrWidth,ScrHeight),true,{nomouseevent=true,colour1=BLACKA(0)})

    self.tick = function (FRAMETIME)
        self.MVR = OW_GETMVRXY()                
        setXY(self.canvas,-self.MVR.X,-self.MVR.Y)
    end

    return self
end

Canvas = createCanvas()

regTickCallback('Canvas.tick(%frametime)')