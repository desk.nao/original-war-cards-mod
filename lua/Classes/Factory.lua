makeFactory = function()
	local self = {toggled=false,}
	
	self.list = {{ElementID=0,UID=0}, {ElementID=0,UID=0}, {ElementID=0,UID=0}, {ElementID=0,UID=0}, {ElementID=0,UID=0}, {ElementID=0,UID=0},}

	self.Coords = 
		{
			XYWH(10+74, 5, 58, 72),XYWH(10+74+60, 5, 58, 72),
			XYWH(10+74, 7+72, 58, 72),XYWH(10+74+60, 7+72, 58, 72),
			XYWH(10+74, 9+144, 58, 72),XYWH(10+74+60, 9+144, 58, 72)
		}

	self.board = getElementEX(nil,anchorNone,XYWH(0,1080-275,1920,275), false, {colour1=BLACKA(255),scissor=false,nomouseeventthis=true,})

	self.Workshop =
	{
		Image = getElementEX(self.board, anchorNone, XYWH(5,25,74,68), true, {UNIT_ID=nil}),
		Title = getLabelEX(self.board, anchorNone, XYWH(5,5,74,20),Tahoma_14,'Workshop Name',{colour1=BLACKA(125),text_halign=ALIGN_MIDDLE,font_colour=RGB(100,100,255)}),
	}

	self.InfoUnit = getElementEX(self.board, anchorNone, XYWH(10+74+60+60, 5, 190, 220), false, {colour1=BLACKA(175)})
	self.InfoUnit.Name = getLabelEX(self.InfoUnit,anchorNone,XYWH(0,5,getWidth(self.InfoUnit),20),Tahoma_14,'Name',{text_halign=ALIGN_MIDDLE,font_colour=RGB(100,100,255)})
	self.Selector = nil

	self.toggle = function()
		self.toggled = not self.toggled

		setVisible(cards.board, not self.toggled)
		setVisible(self.board, self.toggled)
	end

	self.selectUnit = function(ID, Click)
		local Element, Temporary
		if Click == 1 then
			self.attached = ID
			Element = eReg:get(ID)
			setNoMouseEvent(Element, true)
		end

		if Click == 0 then
			Element = eReg:get(ID)
			local Unit = OW_GET_UNIT_INFO(Element.UNIT_ID)
			setText(self.InfoUnit.Name, Unit.INFO[1])
		end

		if self.Selector ~= nil then
			sgui_delete(self.Selector.ID)
		end

		self.Selector = getElementEX(Element, anchorNone, XYWH(0, 0, getWidth(Element), getHeight(Element)), true, {texture='SGUI/Cards/FactorySelection.png'})

		for k, v in ipairs(self.list) do
			if v.ElementID ~= 0 then
				Temporary = eReg:get(v.ElementID)
				if v.ElementID == ID then
					setAlpha(Temporary, 255)
				else
					setAlpha(Temporary, 150)
				end
			end
		end
	end

	self.define = function(FACTORY_ID)
		local Data = OW_GET_UNIT_INFO(FACTORY_ID)

		self.Workshop.Image.Unit_ID = Data.ID

		setTextureFromID(self.Workshop.Image, getUnitTexture(self.Workshop.Image.Unit_ID), 74, 68, 74, 68)

		setText(self.Workshop.Title, Data.INFO[1])
		setFontColour(self.Workshop.Title, SIDE_COLOURS[Data.SIDE+1])
	end

	self.addUnit = function(UNIT_ID)
		for k, v in ipairs(self.list) do
			if v.UID == UNIT_ID then
				return
			end
		end

		local getEmpty = function()
			for k, v in ipairs(self.list) do
				if v.UID == 0 then
					return k
				end
			end
		end

		local Count = getEmpty()
		local Element = getElementEX(self.board, anchorNone, self.Coords[Count], true, {alpha=150, UNIT_ID=UNIT_ID})
		self.list[Count] = {ElementID=Element.ID,UID=UNIT_ID}
		setTextureFromID(Element, getUnitTexture(UNIT_ID), 58, 72, 58, 72)
		set_Callback(Element.ID, CALLBACK_MOUSECLICK, 'Factory.selectUnit('..Element.ID..',%b)')

		setHint(self.Workshop.Image, serializeTable(self.list,'Factory List'))
	end

	self.removeUnit = function(UNIT_ID)
		for k, v in ipairs(self.list) do
			if v.UID == UNIT_ID then
				sgui_delete(v.ElementID)
				self.list[k].ElementID = 0
				self.list[k].UID = 0
			end
		end
		setHint(self.Workshop.Image, serializeTable(self.list,'Factory List'))
	end

	self.createCard = function(Slot)

	end

	self.TaskMouseOver = function(ID)
		self.isOver = ID
	end

	self.TaskMouseClick = function(ID)
		if self.attached ~= nil then
			local Unit = eReg:get(self.attached)
			local TaskElement = eReg:get(ID)

			self.attached = nil
			self.canUpdatePhysics = false

			setWH(Unit, 29, 36)
			setWH(self.Selector, 29, 36)
			setXY(self.Selector, 0, 0)

			setRotation(Unit, SetRotate(2, 360, false))

			local function getSlot(ID)
				local TaskElement = eReg:get(ID)
				local found = false
				for i = 1, #TaskElement.slots do
					if TaskElement.slots[i] == 0 then
						found = i
						break
					end
				end
				return found
			end

			local Slot = getSlot(ID)

			if Slot == false then
				return
			end

			TaskElement.slots[Slot] = 1

			AddEventSlideX(Unit.ID, getX(self.board)+getX(TaskElement)+getX(TaskElement.Slots[Slot]), 0.15)
			AddEventSlideY(Unit.ID, getY(self.board)+getY(TaskElement)+getY(TaskElement.Slots[Slot]), 0.15)
			--AddSingleUseTimer(1, 'setParent({ID='..Unit.ID..'}, {ID='..TaskElement.ID..'})')
			setVisible(TaskElement.Slots[Slot], false)
			setNoMouseEvent(Unit, false)
		end
	end

	self.createTask = function()
		local Element = getElementEX(self.board, anchorNone, XYWH(10+74+60+60, 5, 200, 220), true, {texture='SGUI/Cards/FactoryTask.png',slots={0,0,0,0,0,0},nomouseeventthis=false})
		Element.Title = getLabelEX(Element,anchorNone,XYWH(0,5,getWidth(Element),20),Tahoma_14,'Vehicle Construction',{nomouseeventthis=true,text_halign=ALIGN_MIDDLE,font_colour=RGB(200,200,200)})
		Element.Image = getElementEX(Element, anchorNone, XYWH(getWidth(Element)/2-75/2, 35, 75, 75), true, {nomouseeventthis=true,colour1=BLACKA(175)})
		Element.Slots =
		{
			getElementEX(Element, anchorNone, XYWH(54.5, getHeight(Element)-72-8, 29, 36), true, {nomouseeventthis=true,colour1=BLACKA(175)}),
			getElementEX(Element, anchorNone, XYWH(85.5, getHeight(Element)-72-8, 29, 36), true, {nomouseeventthis=true,colour1=BLACKA(175)}),
			getElementEX(Element, anchorNone, XYWH(116.5, getHeight(Element)-72-8, 29, 36), true, {nomouseeventthis=true,colour1=BLACKA(175)}),

			getElementEX(Element, anchorNone, XYWH(54.5, getHeight(Element)-36-6, 29, 36), true, {nomouseeventthis=true,colour1=BLACKA(175)}),
			getElementEX(Element, anchorNone, XYWH(85.5, getHeight(Element)-36-6, 29, 36), true, {nomouseeventthis=true,colour1=BLACKA(175)}),
			getElementEX(Element, anchorNone, XYWH(116.5, getHeight(Element)-36-6, 29, 36), true, {nomouseeventthis=true,colour1=BLACKA(175)}),
		}
		set_Callback(Element.ID, CALLBACK_MOUSEOVER, 'Factory.TaskMouseOver('..Element.ID..')')
		set_Callback(Element.ID, CALLBACK_MOUSELEAVE, 'Factory.isOver = nil')
		set_Callback(Element.ID, CALLBACK_MOUSECLICK, 'Factory.TaskMouseClick('..Element.ID..')')
	end

	self.createTask()

	self.assignTask = function(Unit, Task)
		self.attached = nil
	end

	self.tempX = 0
	self.MousePos = {X=0, Y=0}
	self.canUpdatePhysics = true

	self.dragCard = function()
		if self.attached ~= nil then
			local Element, Pointer = eReg:get(self.attached), eReg:get(1)
			setParent(Element, nil)

			if self.isOver == nil then
				if self.canUpdatePhysics then
					self.MousePos = {X=getX(Pointer), Y=getY(Pointer)}
					self.canUpdatePhysics = false
					AddSingleUseTimer(0.05, 'Factory.canUpdatePhysics = true')
				end
				setX(Element, getX(Pointer)-getWidth(Element)+18, 0.1)
				setY(Element, getY(Pointer)-getHeight(Element)+41-22, 0.1)
			end

			if self.isOver ~= nil then
				local TaskElement = eReg:get(self.isOver)
				AddEventSlideX(Element.ID, getX(self.board)+getX(TaskElement)+getWidth(TaskElement)/2-getWidth(Element)/2, 0.1)
				AddEventSlideY(Element.ID, getY(self.board)-getHeight(cards.header)-getHeight(Element)-5, 0.1)
			end
		end
	end

	self.physicsCard = function()
		if self.attached ~= nil then
			local Element, Pointer = eReg:get(self.attached), eReg:get(1)
			local X = getX(Pointer)
			
			local deltaX = X - self.MousePos.X
			
			self.tempX = (self.tempX + (deltaX * 0.5)) * 0.9
			self.tempX = math.max(math.min(self.tempX, 30), -30)
			
			if math.abs(self.tempX) < 0.1 then
				self.tempX = self.tempX > 0 and 0.1 or -0.1
			end
			
			self.MousePos.X = X

			setRotation(Element, SetRotate(2, 360 - self.tempX, false))
		end
	end

	return self
end

Factory = makeFactory()

regTickCallback('Factory.dragCard()')
regTickCallback('Factory.physicsCard()')

local Visibility = getImageButtonEX(nil, anchorNone, XYWH(100,200,75,15), 'Factory', '', 'Factory.toggle()', SKINTYPE_BUTTON, {})

ApplySkinToElement(Visibility)
setFontColour(Visibility,RGB(255,230,0))