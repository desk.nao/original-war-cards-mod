makeInfoProgress = function()
	local self = {}

	self.makeUI = function()
		local x, y = getX(Minimap), getY(Minimap) + getHeight(Minimap) + 40
		self.panel = getElementEX(nil, anchorNone, XYWH(x,y,getWidth(Minimap),60), true, {nomouseevent=true,colour1=BLACKA(100)})


			getLabelEX(self.panel,anchorNone,XYWH(2.5, 2.5, getWidth(self.panel)-5, 15),Tahoma_10,'Soldiers',{shadowtext=true,text_valign=ALIGN_TOP,text_halign=ALIGN_MIDDLE})
			getLabelEX(self.panel,anchorNone,XYWH(0, 5, getWidth(self.panel), 0),SmallFonts_10,'x1 Units Lvl. 1',{font_colour=SIDE_COLOURS[cards.PlayerLeft+1],text_valign=ALIGN_TOP,text_halign=ALIGN_LEFT})
			getLabelEX(self.panel,anchorNone,XYWH(0, 5, getWidth(self.panel), 0),SmallFonts_10,'x1 Units Lvl. 1',{font_colour=SIDE_COLOURS[cards.PlayerRight+1],text_valign=ALIGN_TOP,text_halign=ALIGN_RIGHT})

			getLabelEX(self.panel,anchorNone,XYWH(2.5, 22.5, getWidth(self.panel)-5, 15),Tahoma_10,'Scientists',{shadowtext=true,text_valign=ALIGN_TOP,text_halign=ALIGN_MIDDLE})
			getLabelEX(self.panel,anchorNone,XYWH(0, 25, getWidth(self.panel), 0),SmallFonts_10,'x1 Units Lvl. 1',{font_colour=SIDE_COLOURS[cards.PlayerLeft+1],text_valign=ALIGN_TOP,text_halign=ALIGN_LEFT})
			getLabelEX(self.panel,anchorNone,XYWH(0, 25, getWidth(self.panel), 0),SmallFonts_10,'x1 Units Lvl. 1',{font_colour=SIDE_COLOURS[cards.PlayerRight+1],text_valign=ALIGN_TOP,text_halign=ALIGN_RIGHT})
			
			getLabelEX(self.panel,anchorNone,XYWH(2.5, 42.5, getWidth(self.panel)-5, 15),Tahoma_10,'Specialists',{shadowtext=true,text_valign=ALIGN_TOP,text_halign=ALIGN_MIDDLE})
			getLabelEX(self.panel,anchorNone,XYWH(0, 45, getWidth(self.panel), 0),SmallFonts_10,'x1 Units Lvl. 1',{font_colour=SIDE_COLOURS[cards.PlayerLeft+1],text_valign=ALIGN_TOP,text_halign=ALIGN_LEFT})
			getLabelEX(self.panel,anchorNone,XYWH(0, 45, getWidth(self.panel), 0),SmallFonts_10,'x1 Units Lvl. 1',{font_colour=SIDE_COLOURS[cards.PlayerRight+1],text_valign=ALIGN_TOP,text_halign=ALIGN_RIGHT})

			--getLabelEX(self.panel,anchorNone,XYWH(2.5, 2.5+15, getWidth(self.panel)-5, 15),Tahoma_10,'Engineers Lvl. '..'1',{text_valign=ALIGN_TOP,text_halign=Align})
			--getLabelEX(self.panel,anchorNone,XYWH(2.5, 2.5+30, getWidth(self.panel)-5, 15),Tahoma_10,'Mechanics Lvl. '..'1',{text_valign=ALIGN_TOP,text_halign=Align})
			--getLabelEX(self.panel,anchorNone,XYWH(2.5, 2.5+45, getWidth(self.panel)-5, 15),Tahoma_10,'Scientists Lvl. '..'1',{text_valign=ALIGN_TOP,text_halign=Align})
	end

	return self
end

InfoProgress = makeInfoProgress()

AddSingleUseTimer(5,'InfoProgress.makeUI()')