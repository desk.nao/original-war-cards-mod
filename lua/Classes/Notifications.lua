createNotification = function()
	local self = {processed={}}

	self.add = function(Item, String, Colour)
		self.Create(Item, String, Colour)
	end

	self.Remove = function(Element)
		local found
		for k, v in ipairs(self.processed) do
			if v == Element then
				AddEventFade(v, 5, 1, 'sgui_delete('..v..')')
				found = k
				break
			end
		end

		for k, v in ipairs(self.processed) do 
			AddEventSlideY(v, 300+((k-1)*77.5), 0.25)
			if k < 4 then
				AddEventFade(v, 255, 2, nil)
				AddEventSlideX(v, 25, 0.5)
			end
		end

		table.remove(self.processed, found)

	end

	self.Create = function(Item, String, Colour)
		local function ScalePicture(W, H)
		    local scale = math.min((75-5) / W, (75-5) / H)
		    return {W=math.floor(W * scale), H=math.floor(H * scale)}
		end

		local Element = getElementEX(nil, anchorNone, XYWH(-100, 300+((#self.processed)*77.5), 175, 75), true, {nomouseevent=true, alpha=0, colour1=BLACKA(100)})
		table.insert(self.processed, Element.ID)

		local Dimensions = ScalePicture(Item.Dimensions.Width, Item.Dimensions.Height)
		Element.Image = getElementEX(Element, anchorNone, XYWH(2.5, 2.5, Dimensions.W, Dimensions.H), true, {texture='SGUI/Cards/top_entities/'..Item.Texture})
	    Element.Title = getLabelEX(Element,anchorNone,XYWH(getWidth(Element.Image) + 5, 2.5, getWidth(Element) - getWidth(Element.Image) - 5, 0),Tahoma_16,String,{text_halign = ALIGN_LEFT})
	    Element.Label = getLabelEX(Element,anchorNone,XYWH(getWidth(Element.Image) + 7.5, 25, getWidth(Element) - getWidth(Element.Image) - 7.5, 0),Tahoma_12,Item.Title,{text_halign = ALIGN_LEFT})
	    setFontColour(Element.Title, Colour)

	    if #self.processed < 4 then
		    AddEventFade(Element.ID, 255, 2, nil)
		    AddEventSlideX(Element.ID, 25, 0.5)
		end

	    AddSingleUseTimer(5+(#self.processed), 'Notify.Remove('..Element.ID..')')
	end

	return self
end

Notify = createNotification()