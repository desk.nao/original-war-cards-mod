local shaderClass = function()
	local self = {THETIME=0,FRAME_PROG=0,DIRECTION=0,canPlace=false}

	self.init = function()
		self.TVLINES_GLSL = loadGLSL('tvlines')
		self.TVLINES_GLSL.TIME = self.TVLINES_GLSL:getLocation('time')
		self.PREVIEW = loadOGLTexture('SGUI/Cards/entities/b-a-armoury0000.png',true,false,false,false)
		self.MOUSEOVER = getElementEX(nil,anchorLT,XYWH(0,0,0,0),false,{nomouseevent=true,subtexture=true,subcoords=SUBCOORD(0,0,0,0)})
		self.LABEL = getLabelEX(nil,anchorTR,XYWH(600-32,50,32,32),Tahoma_20,'HEX',{special=true,text_halign=ALIGN_MIDDLE,font_colour=RGB(255,255,255)})
		self.FBO = fboclass.make(640,640,true,false,0,false)
		SGUI_settextureid(self.MOUSEOVER.ID,self.FBO:getTextureID(),640,640,640,640)
		setWH(self.MOUSEOVER,self.PREVIEW.W,self.PREVIEW.H)
	end

	self.RenderMouseElement = function(FRAMETIME)
		self.FRAME_PROG = self.FRAME_PROG + FRAMETIME

		local needupdate = false
	
		while self.FRAME_PROG >= 1/30 do
			needupdate = true
			self.THETIME = self.THETIME + 1/30
			self.FRAME_PROG = self.FRAME_PROG - 1/30
			if self.FRAME_PROG > 1/2 then
				self.FRAME_PROG = 0
			end
		end
	
		if (not needupdate) then
			return
		end

		self.FBO:setClearColour(0,0,0,0,false)
		
		self.FBO:doBegin()
			OGL_BEGIN()
				OGL_GLENABLE(GL_BLEND)
				OGL_QUAD_BEGIN2D(self.FBO.W,self.FBO.H)
				self.TVLINES_GLSL:use()
				self.TVLINES_GLSL:setMatrixs()
				self.TVLINES_GLSL:setUniform1F(self.TVLINES_GLSL.TIME, self.THETIME)
				OGL_TEXTURE_BINDID(self.PREVIEW:getTextureID())
				OGL_QUAD_RENDER(0,0,self.PREVIEW.W,self.PREVIEW.H,0,0,1,1)
				OGL_QUAD_END2D()
			OGL_END()
		self.FBO:doEnd()
	end

	self.MoveMouseElement = function(a, b)
		local FHA = OW_FINDHEX(a, b)

		local function recenterElement()
			AddEventSlideX(self.MOUSEOVER.ID,1920/2 - getWidth(self.MOUSEOVER)/2, 0.1)
			AddEventSlideY(self.MOUSEOVER.ID,1080/2.5 - getHeight(self.MOUSEOVER)/2, 0.1)
		end
		
		if FHA.FOUND == true and getY({ID=1}) < 755 then
			local HA = OW_HEXTOSCREEN(FHA.X,FHA.Y)
			AddEventSlideX(self.MOUSEOVER.ID,HA.X-OW_GETMVRXY(HA.X,HA.Y).X-getWidth(self.MOUSEOVER)/2,0.1)
			AddEventSlideY(self.MOUSEOVER.ID,HA.Y-OW_GETMVRXY(HA.X,HA.Y).Y-getHeight(self.MOUSEOVER)/2,0.1)
		else
			recenterElement()
		end

		if getY({ID=1}) > 755 then
			recenterElement()
		end
	end

	self.Reload = function()
		self.PREVIEW = loadOGLTexture('SGUI/Cards/entities/' .. self.TEXTURE .. self.DIRECTION .. '.png',true,false,false,false)
	end

	self.Change = function(ID)

	    local v = eReg:get(ID)

		self.TEXTURE = string.sub(v.imgtex, 1, -6)

		set_Callback(gamewindow.ID, CALLBACK_MOUSECLICK, 'shader.Click(%b, %x, %y, '..v.Sail.Nation..', '..v.Sail.Ident..')')
		set_Callback(gamewindow.ID, CALLBACK_MOUSEMOVE, 'shader.MoveMouseElement(%x, %y)')

		setVisible(self.MOUSEOVER, true)
		self.Reload()
		setSubCoords(self.MOUSEOVER,SUBCOORD(0,640-self.PREVIEW.H,self.PREVIEW.W,self.PREVIEW.H))
		setWH(self.MOUSEOVER,self.PREVIEW.W,self.PREVIEW.H)
		if cards.state == 100 then
			setVisible(cards.cancel, true)
		end
	end

	self.getHexagons = function(posX, posY)
		local FHA = OW_FINDHEX(posX, posY)
		if FHA.FOUND == true then
			return {X=FHA.X, Y=FHA.Y}
		end
	end

	self.canPlaceBuilding = function(BOOLEAN)
		self.canPlace = BOOLEAN
		if self.canPlace then
			OW_CUSTOM_COMMAND(100, 103)
			set_Callback(gamewindow.ID, CALLBACK_MOUSECLICK, '')
			set_Callback(gamewindow.ID, CALLBACK_MOUSEMOVE, '')
			setVisible(self.MOUSEOVER, false)
			self.canPlace = false
			setVisible(cards.cancel, false)
		end
	end

	self.Click = function(BUTTON, posX, posY, Nation, BcType)
		if BUTTON == 0 then
			OW_CUSTOM_COMMAND(51, self.DIRECTION)
			OW_CUSTOM_COMMAND(53, Nation)
			OW_CUSTOM_COMMAND(54, BcType)
			OW_CUSTOM_COMMAND(55, self.getHexagons(posX, posY).X, self.getHexagons(posX, posY).Y)
			OW_CUSTOM_COMMAND(1)
		end
		if BUTTON == 1 then
			self.DIRECTION = self.DIRECTION +1
			if self.DIRECTION > 5 then
				self.DIRECTION = 0
			end
			OW_CUSTOM_COMMAND(51, self.DIRECTION)
		end
		self.Reload()
	end

	self.init()

	return self
end

shader = shaderClass()

SGUI_register_tick_callback('shader.RenderMouseElement(%frametime)')