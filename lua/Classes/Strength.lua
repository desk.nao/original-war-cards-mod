createStrength = function()
    local self = {V = {50, 50}}

    self.Left = nil
    self.Right = nil

    self.setProgress = function()
	    if self.Left == nil and self.Right == nil then
		    self.Left = getElementEX(Minimap, anchorLTRB, XYWH(0, -25, 0, 20), true, {nomouseevent = true, colour1 = SIDE_COLOURS[cards.PlayerLeft + 1]})
		    self.Right = getElementEX(Minimap, anchorLTRB, XYWH(getWidth(Minimap), -25, 0, 20), true, {nomouseevent = true, colour1 = SIDE_COLOURS[cards.PlayerRight + 1]})
		end
        local total = self.V[1] + self.V[2]
        if total == 0 then
            setWidth(self.Left, 0)
            setWidth(self.Right, 0)
        else
            local totalWidth = getWidth(Minimap)
            setWidth(self.Left, totalWidth * (self.V[2] / total))
            setWidth(self.Right, -totalWidth * (self.V[1] / total))
        end
    end

    self.updateRegistry = function(Position, Count, Value)
		self.V[Position] = Value
		self.setProgress()
	end

    return self
end

Strength = createStrength()