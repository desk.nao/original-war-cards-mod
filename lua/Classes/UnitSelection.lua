makeUnitSelection = function()
	local self = {devMode=false,panel=nil,list={},listsub={},canUpdate=true,saved_ID=0}

	self.Description =
	{
		[1] = 'Provides a passive income of crates. If destroyed, you lose the game!',
		[2] = 'Provides a passive income of crates. If destroyed, you lose the game!',
		[3] = 'Recruits Mechanics who work on simple vehicles.',
		[4] = 'Recruits Mechanics who work on complex weaponry and vehicles.',
		[7] = 'Recruits Scientists who occupy this building to work on technologies.',
		[5] = 'Allows recruitment of Soldiers.',
		[6] = 'Allows recruitment of Soldiers and Heroes.',
		[30] = 'Provides a passive income of oil that is used to discard cards primarily.',
		[31] = 'Provides a passive income of siberite that is used to pay for advanced cards.',
		[32] = 'Defensive positions manned by either a Soldier or a Specialist.',
		[33] = 'Advanced defensive positions that offer diversity based on the weapon installed atop.',
		[34] = 'Complex defensive positions that differ in terms of diversity based on the weapon installed atop.',
	}

	self.makeUI = function()
		self.panel = getScrollboxEX(nil,anchorNone,XYWH(5, getY(cards.header)-255, 250, (95+42*6)),{scissor=true,nomouseeventthis=true,image=nil,colour1=BLACKA(0)})
		--self.panel.scrollbar = getScrollBarEX(nil,anchorNone,XYWH(getX(self.panel)+getWidth(self.panel),getY(self.panel),12,250),'scrollbar.png',self.panel,{colour1=Scrollbar_Colour1,colour2=Scrollbar_Colour2,},{})
		--sgui_set(self.panel.ID, PROP_SCROLLBAR, self.panel.scrollbar.ID)
		self.panel.block = getLabelEX(self.panel,anchorNone,XYWH(15, 15, 1, 1),Tahoma_12,'',{text_halign=ALIGN_LEFT})
		if self.devMode then
			self.devPanel = getScrollboxEX(nil,anchorNone,XYWH(getX(self.panel)+getWidth(self.panel)+25, getY(cards.header)-605, 275, 600),{scissor=true,nomouseeventthis=false,colour1=BLACKA(100)})
			setVisible(self.devPanel, false)
			self.devPanel.info = getLabelEX(self.devPanel,anchorNone,XYWH(12+2.5, 2.5, 1, 1),Tahoma_12,'...',{text_valign=ALIGN_TOP,text_halign=ALIGN_LEFT,wordrap=true,autosize=true,automaxwidth=265})
			self.devPanel.scrollbar = getScrollBarEX(nil,anchorNone,XYWH(getX(self.devPanel),5+getY(self.devPanel),12,595),'scrollbar.png',self.devPanel,{colour1=Scrollbar_Colour1,colour2=Scrollbar_Colour2,},{})
			setVisible(self.devPanel, false)
			setVisible(self.devPanel.scrollbar, false)
			sgui_set(self.devPanel.ID, PROP_SCROLLBAR, self.devPanel.scrollbar.ID)
		end
	end

	self.makeUI()

	self.clean = function()
		if #self.list > 0 then
			for k, v in ipairs(self.list) do
				sgui_delete(v)
				table.remove(self.list, k)
			end
		end
		if #self.listsub > 0 then
		    for i = #self.listsub, 1, -1 do
		        sgui_delete(self.listsub[i])
		        table.remove(self.listsub, i)
		    end
		end
	end

	self.update = function(Data)
		if not self.canUpdate or Data.ID == self.saved_ID then
			return
		end

		self.saved_ID = Data.ID

		if Data["TYP"] == 1 and Data.CLASSTYPE < 6 then
			self.clean()

			local Element = getElementEX(self.panel, anchorNone, XYWH(-300, 5, 240, 82), true, {nomouseeventthis=true,alpha=0,colour1=BLACKA(100)})
			Element.Image = getElementEX(Element, anchorNone, XYWH(5, 5, 58, 72), true, {nomouseeventthis=true,})
			setTextureFromID(Element.Image, getUnitTexture(Data.ID), 58, 72, 58, 72)
			Element.Name = getLabelEX(Element,anchorNone,XYWH(58+10, 5, 240-10-63, 12),Tahoma_12,Data.INFO[1],{nomouseeventthis=true,colour1=BLACKA(200),text_halign=ALIGN_MIDDLE})
			setFontColour(Element.Name, SIDE_COLOURS[Data.SIDE+1])
			Element.Level = getLabelEX(Element,anchorNone,XYWH(58+10, 20, 240-10-63, 10),SmallFonts_10,Data.SKILLSTEXT[Data.CLASS][1]["TEXT"] .. ': ' .. Data.SKILLSTEXT[Data.CLASS][1]["VALUE"],{colour1=BLACKA(200),nomouseeventthis=true,text_halign=ALIGN_LEFT})
			Element.Speed = getLabelEX(Element,anchorNone,XYWH(getWidth(Element.Image)+10, 35, 250-getWidth(Element.Image)-25, 10),SmallFonts_10,'Speed: '..Data.VALUESTEXT[1][4]["VALUE"],{nomouseeventthis=true,colour1=BLACKA(200),text_halign = ALIGN_LEFT})
			Element.Dexterity = getLabelEX(Element,anchorNone,XYWH(getWidth(Element.Image)+10, 35, 250-getWidth(Element.Image)-25, 10),SmallFonts_10,'Armour: '..Data.VALUESTEXT[2][4]["VALUE"],{nomouseeventthis=true,colour1=BLACKA(0),text_halign = ALIGN_RIGHT})
			Element.VsBuildings = getLabelEX(Element,anchorNone,XYWH(getWidth(Element.Image)+10, 50, 250-getWidth(Element.Image)-25, 10),SmallFonts_10,'Vs. Buildings: '..Data.VALUESTEXT[5][4]["VALUE"],{nomouseeventthis=true,colour1=BLACKA(200),text_halign = ALIGN_LEFT})
			Element.VsVehicles = getLabelEX(Element,anchorNone,XYWH(getWidth(Element.Image)+10, 65, 250-getWidth(Element.Image)-25, 10),SmallFonts_10,'Vs. Vehicles: '..Data.VALUESTEXT[4][4]["VALUE"],{nomouseeventthis=true,colour1=BLACKA(200),text_halign = ALIGN_LEFT})
			Element.VsHumans = getLabelEX(Element,anchorNone,XYWH(getWidth(Element.Image)+10, 50, 250-getWidth(Element.Image)-25, 10),SmallFonts_10,'Vs. Humans: '..Data.VALUESTEXT[3][4]["VALUE"],{nomouseeventthis=true,colour1=BLACKA(0),text_halign = ALIGN_RIGHT})

			table.insert(self.list, Element.ID)
			AddEventSlideX(Element.ID, 5, 0.5)
			AddEventFade(Element.ID, 255, 0.5, '')
			self.canUpdate = false
			AddSingleUseTimer(1,'UnitSelection.canUpdate = true')

			if self.devMode then
				self.updateDevInfo(Data)
			end

			setY(self.panel, getY(cards.header)-95)
		end

		if Data["TYP"] == 3 then
			self.clean()

			local Element = getElementEX(self.panel, anchorNone, XYWH(-300, 5, 240, 78), true, {nomouseeventthis=true,alpha=0,colour1=BLACKA(100)})
			Element.Image = getElementEX(Element, anchorNone, XYWH(5, 5, 74, 68), true, {nomouseeventthis=true,})
			setTextureFromID(Element.Image, getUnitTexture(Data.ID), 74, 68, 74, 68)
			Element.Name = getLabelEX(Element,anchorNone,XYWH(84, 5, 250-74-25, 12),Tahoma_12,Data.INFO[1],{nomouseeventthis=true,colour1=BLACKA(200),text_halign=ALIGN_MIDDLE})
			setFontColour(Element.Name, SIDE_COLOURS[Data.SIDE+1])
			Element.Description = getLabelEX(Element,anchorNone,XYWH(84, 15, 151, 15),Tahoma_10,self.Description[Data.KIND+1],{nomouseeventthis=true,text_halign=ALIGN_LEFT,wordwrap=true,autosize=true,automaxwidth=151})
			sgui_autosizecheck(Element.Description.ID)
			Element.Level = getLabelEX(Element,anchorNone,XYWH(3, 3, 74, 15),SmallFonts_10,'Lvl. '..Data.BUDINFO[1]..'/10',{nomouseeventthis=true,text_halign=ALIGN_LEFT})
			table.insert(self.list, Element.ID)
			AddEventSlideX(Element.ID, 5, 0.5)
			AddEventFade(Element.ID, 255, 0.5, '')
			self.canUpdate = false
			AddSingleUseTimer(1,'UnitSelection.canUpdate = true')
			
			if self.devMode then
				self.updateDevInfo(Data)
			end

			setY(self.panel, getY(cards.header)-95)

			for k, v in ipairs(Data.UNITSINSIDE) do
				local Element = getElementEX(self.panel, anchorNone, XYWH(-300, 7.5+78+45*(k-1), 240, 42), true, {nomouseeventthis=true,alpha=0,colour1=BLACKA(100)})
				Element.Image = getElementEX(Element, anchorNone, XYWH(2.5, 2.5, 58/2, 72/2), true, {})
				Element.Name = getLabelEX(Element,anchorNone,XYWH(getWidth(Element.Image)+10, 5, 250-getWidth(Element.Image)-25, 12),Tahoma_12,v.INFO[1],{nomouseeventthis=true,colour1=BLACKA(200),text_halign = ALIGN_MIDDLE})
				Element.Skill = getLabelEX(Element,anchorNone,XYWH(getWidth(Element.Image)+10, 20, 250-getWidth(Element.Image)-25, 12),SmallFonts_10,v.SKILLSTEXT[v.CLASS][1]["TEXT"] .. ': ' .. v.SKILLSTEXT[v.CLASS][1]["VALUE"],{nomouseeventthis=true,colour1=BLACKA(200),text_halign = ALIGN_LEFT})
				setFontColour(Element.Name, SIDE_COLOURS[v.SIDE+1])
				setTextureFromID(Element.Image, getUnitTexture(v.ID), 58/2, 72/2, 58/2, 72/2)
				AddEventSlideX(Element.ID, 5, 0.5+k/10)
				AddEventFade(Element.ID, 255, 1.5+k/10, '')
				table.insert(self.listsub, Element.ID)
				setY(self.panel.block, getY(Element)+10)
				setY(self.panel, getY(cards.header)-(95+45*k))
			end

			return
		end
	end

	self.updateDevInfo = function(Data)
		setVisible(self.devPanel, true)
		setVisible(self.devPanel.info, true)
		setVisible(self.devPanel.scrollbar, true)
		setText(self.devPanel.info, serializeTable(Data,"FROMOW_INFOPANEL_UPDATE"))
		sgui_autosizecheck(self.devPanel.info.ID)
		setHeight(self.devPanel.info, 2675)
	end

	return self
end

UnitSelection = makeUnitSelection()