createXicht = function()
  local self = {}

  self.dev = getLabelEX(nil,anchorTR,XYWH(500,600,315,0),Tahoma_12,'Xichted',{colour1=BLACKA(100),nomouseevent=true,text_halign=ALIGN_LEFT,font_colour=RGB(255,255,255)})

  self.Unit = getElementEX(nil, anchorNone, XYWH(190, 200, 320, 400), true, {scissor=false,nomouseevent=true})

  self.Unit.Parts =
  {
    Background = getElementEX(self.Unit, anchorNone, XYWH(0, 0, 0, 0), true, {}),
    Neck = getElementEX(self.Unit, anchorNone, XYWH(0, 0, 0, 0), true, {}),
    Face = getElementEX(nil, anchorNone, XYWH(0, 0, 0, 0), true, {}),
    Mouth = getElementEX(nil, anchorNone, XYWH(0, 0, 0, 0), true, {}),
    Clothing_Top = getElementEX(nil, anchorNone, XYWH(0, 0, 0, 0), true, {}),
  }

  self.processUnit = function(Data)
    if Data == nil then
      return
    end

    local function getDirectory(Ident)
      local List = {'Backgrounds','Necks','Faces','Cheeks','Eyes','Noses','Mouths','Ears','Eyebrows','Hair','Beards','Clothes','Glasses','Foreground'}
      return 'SGUI/Faces/'..List[Ident]..'/'
    end

    setText(self.dev, serializeTable(Data,"XICHTED PARTS"))

    local _Background, _Neck, _Face, _Mouth, _ClothTop

    _Background = self.Parts[1][Data.PARTS[1]]
    _Neck = self.Parts[2][Data.PARTS[2]]
    _Face = self.Parts[3][Data.PARTS[3]]
    _Mouth = self.Parts[7][Data.PARTS[7]]
    _ClothTop = self.Parts[12][Data.PARTS[12]]

    setWH(self.Unit.Parts.Background, _Background.W, _Background.H)
    setWH(self.Unit.Parts.Neck, _Neck.W, _Neck.H)
    setWH(self.Unit.Parts.Face, _Face.W, _Face.H)
    setWH(self.Unit.Parts.Mouth, _Mouth.W, _Mouth.H)
    setWH(self.Unit.Parts.Clothing_Top, _ClothTop.W, _ClothTop.H)

    setTexture(self.Unit.Parts.Background, getDirectory(1).._Background.T..'.png')
    setTexture(self.Unit.Parts.Neck, getDirectory(2).._Neck.T..'.png')
    setTexture(self.Unit.Parts.Face, getDirectory(3).._Face.T..'.png')
    setTexture(self.Unit.Parts.Mouth, getDirectory(7).._Mouth.T..'.png')
    setTexture(self.Unit.Parts.Clothing_Top, getDirectory(12).._ClothTop.T..'.png')

    setXY(self.Unit.Parts.Neck, _Background.W - _Background.NECK_SNAP.X - _Neck.W/2, _Background.H - _Background.NECK_SNAP.Y + _Neck.H/2)
    
    setParent(self.Unit.Parts.Face, self.Unit.Parts.Neck)
    setXY(self.Unit.Parts.Face, (_Neck.W/2) - (_Face.W/2) + _Neck.FACE_SNAP.X, -(_Face.H/2) )-- - (_Face.H/2) + )-- (_Neck.W/2) - (_Face.W/2) + _Neck.FACE_SNAP.X, - (_Face.H/2) + _Neck.FACE_SNAP.Y)

    setParent(self.Unit.Parts.Mouth, self.Unit.Parts.Face)
    setXY(self.Unit.Parts.Mouth, (_Face.W/2) - _Face.MOUTH_SNAP.X, (_Face.H) - (_Mouth.H/2) + _Face.MOUTH_SNAP.Y) -- getX(self.Unit.Parts.Neck) + getX(self.Unit.Parts.Face) + (_Face.W/2) + (_Mouth.W/2) + _Face.MOUTH_SNAP.X, getY(self.Unit.Parts.Neck) + getY(self.Unit.Parts.Face) + (_Mouth.H/2) + _Face.MOUTH_SNAP.Y)

    setParent(self.Unit.Parts.Clothing_Top, self.Unit.Parts.Neck)
    setXY(self.Unit.Parts.Clothing_Top, 0, _Neck.H - _ClothTop.H + _Neck.CLOTH_SNAP.Y ) -- getX(self.Unit.Parts.Neck) + getX(self.Unit.Parts.Face) + (_Face.W/2) + (_Mouth.W/2) + _Face.MOUTH_SNAP.X, getY(self.Unit.Parts.Neck) + getY(self.Unit.Parts.Face) + (_Mouth.H/2) + _Face.MOUTH_SNAP.Y)

    --setVisible(self.Unit.Parts.Face, false)

    -- TESTING CODE, DIRTY ASS CODE BELOW _Neck.FACE_SNAP.X
    --setParent(self.Unit.Parts.Eye_Left, self.Unit.Parts.Face)
    --setTexture(self.Unit.Parts.Eye_Left, 'SGUI/Faces/eyes/002_p.png')
    --setWH(self.Unit.Parts.Eye_Left, 71, 65)
    --setXY(self.Unit.Parts.Eye_Left, _Face.W/2 - 3, _Face.H - 132)

    
    -- setXY(self.Unit.Parts[2], 320 - 177 - 357/2, 400 - 362 + 220/2)

    -- 177 362, Background.W - Neck

    -- X: Background.W - NeckSnapPoint.X - Neck.W/2
    -- X: Background.H - NeckSnapPoint.Y + Neck.H/2
  end

  self.Parts =
  {
    -- Backgrounds
    {
      {T='normalni', W=320, H=400, NECK_SNAP={X=177, Y=362}},
      {T='pozadi1', W=320, H=400, NECK_SNAP={X=177, Y=362}},
      {T='pozadi2', W=320, H=400, NECK_SNAP={X=177, Y=362}},
      {T='pozadi3', W=320, H=400, NECK_SNAP={X=177, Y=362}},
      {T='pozadi4', W=320, H=400, NECK_SNAP={X=177, Y=362}},
      {T='pozadi5', W=320, H=400, NECK_SNAP={X=177, Y=362}},
      {T='pozadi6', W=320, H=400, NECK_SNAP={X=177, Y=362}},
      {T='pozadi7', W=320, H=400, NECK_SNAP={X=177, Y=362}},
      {T='pozadi8', W=320, H=400, NECK_SNAP={X=177, Y=362}},
      {T='pozadi9', W=320, H=400, NECK_SNAP={X=177, Y=362}},
      {T='pozadi10', W=320, H=400, NECK_SNAP={X=177, Y=362}},
      {T='pokus 2 copy', W=320, H=400, NECK_SNAP={X=177, Y=362}},
      {T='pokus2-3', W=320, H=400, NECK_SNAP={X=177, Y=362}},
      {T='us-pozadi', W=320, H=400, NECK_SNAP={X=177, Y=362}},
      {T='pokus2-3br', W=320, H=400, NECK_SNAP={X=177, Y=362}},
      {T='us-pozadibr', W=320, H=400, NECK_SNAP={X=177, Y=362}}
    },

    -- Neck
    {
      {T='rek', W=357, H=220, FACE_SNAP={X=16,Y=-67}, CLOTH_SNAP={X=-8,Y=0}},
      {T='samaris', W=414, H=271, FACE_SNAP={X=19,Y=-63}, CLOTH_SNAP={X=-5,Y=6}},
      {T='1krk', W=420, H=285, FACE_SNAP={X=15,Y=-58}, CLOTH_SNAP={X=-15,Y=-1}},
      {T='grg1', W=353, H=211, FACE_SNAP={X=20,Y=-70}, CLOTH_SNAP={X=-4,Y=-5}},
      {T='faldy', W=396, H=258, FACE_SNAP={X=17,Y=-70}, CLOTH_SNAP={X=-4,Y=-9}},
      {T='new', W=421, H=294, FACE_SNAP={X=21,Y=-61}, CLOTH_SNAP={X=-3,Y=12}},
    },

    -- Faces
    {
      {T='rek', W=205, H=250, MOUTH_SNAP={X=3,Y=-45}},
      {T='thorgal', W=199, H=253, MOUTH_SNAP={X=3,Y=-45}},
      {T='samaris', W=204, H=248, MOUTH_SNAP={X=3,Y=-45}},
      {T='lebza2', W=201, H=244, MOUTH_SNAP={X=3,Y=-45}},
      {T='lebza3', W=201, H=238, MOUTH_SNAP={X=3,Y=-45}},
      {T='1hlava', W=197, H=248, MOUTH_SNAP={X=3,Y=-45}},
      {T='pacino', W=199, H=251, MOUTH_SNAP={X=3,Y=-45}},
      {T='2hlava', W=197, H=248, MOUTH_SNAP={X=3,Y=-45}},
      {T='aqua', W=197, H=253, MOUTH_SNAP={X=3,Y=-45}},
      {T='new', W=197, H=253, MOUTH_SNAP={X=3,Y=-45}},
      {T='general', W=182, H=260, MOUTH_SNAP={X=3,Y=-45}},
      {T='gosudraov', W=178, H=258, MOUTH_SNAP={X=3,Y=-45}},
    },
    -- Cheeks

    {},
    -- Eyes
    {
      {T='001', W=80, H=74},
      {T='002', W=57, H=66},
      --[[{T='003', W=, H=},
      {T='004', W=, H=},
      {T='kingdom', W=, H=},
      {T='samaris', W=, H=},
      {T='rekb', W=, H=},
      {T='reka', W=, H=},
      {T='pacino', W=, H=},
      {T='thorgal', W=, H=},
      {T='1oko', W=, H=},
      {T='new', W=, H=},
      {T='general', W=, H=},
      {T='gosudarov', W=, H=},
      {T='thorgalbum', W=, H=},--]]
    },
    -- Noses
    {},
    -- Mouths
    {
      {T='rek', W=65, H=46},
      {T='thorgal', W=66, H=36},
      {T='kingdom', W=75, H=46},
      {T='001', W=84, H=60},
      {T='002', W=82, H=45},
      {T='003', W=83, H=53},
      {T='004', W=78, H=54},
      {T='005', W=73, H=41},
      {T='batman', W=92, H=89},
      {T='dredd', W=80, H=81},
      {T='samaris', W=62, H=37},
      {T='pusa8', W=82, H=51},
      {T='pusa9', W=87, H=51},
      {T='pusa10', W=75, H=58},
      {T='pusa11', W=84, H=60},
      {T='anim2', W=78, H=43},
      {T='1usta', W=75, H=70},
      {T='tlama', W=999, H=999},
      {T='pacino', W=62, H=56},
      {T='samaris1', W=60, H=39},
      {T='new', W=64, H=60},
      {T='john', W=65, H=62},
      {T='general', W=89, H=79},
      {T='gosudarov', W=79, H=69},
      {T='thorgalbum', W=66, H=39},
    },
    -- Ears
    {},
    -- Eyebrows
    {},
    -- Hair
    {},
    -- Beards
    {},
    -- Clothing
    {
      {T='Adel2_s', W=302, H=137},
      {T='USdel1_s', W=467, H=145},
      {T='Adoc1_s', W=440, H=199},
      {T='USmech1_s', W=472, H=213},
      {T='USdoc1_s', W=445, H=181},
      {T='USsol1_s', W=438, H=216},
      {T='USsnip1_s', W=362, H=233},
      {T='Anoble1_s', W=443, H=257},
      {T='Asol_s', W=440, H=166},
      {T='Asol2_s', W=440, H=175},
      {T='Amort_s', W=442, H=188},
      {T='Amech_s', W=444, H=196},
      {T='Rumech_s', W=499, H=193},
      {T='Rudoc_s', W=431, H=180},
      {T='Rusol_s', W=431, H=165},
      {T='Rudel_s', W=462, H=132},
      {T='Rubaz_s', W=597, H=258},
     },
    -- Glasses
    {},
    -- Foreground
  }

  return self
end

Xichted = createXicht() 


    --framework.Xichted_list = string.sub(Xichted.TYP,1,4)

    --for i = 1, #Xichted.PARTS do
    --  framework.Xichted_list = framework.Xichted_list..'|'..Xichted.PARTS[i]
    --end