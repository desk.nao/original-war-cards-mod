local self = cards

self.toggleRotate = function(ELEMENT)
	sgui_setcallback(ELEMENT, CALLBACK_MOUSECLICK, '')
    table.insert(self.ANIM_LIST, ELEMENT)
end

self.callbackBuilding = function(ID)
	local Element = eReg:get(ID)
	if not getVisible(shader.MOUSEOVER) then
		if self.state == 100 then
			OW_CUSTOM_COMMAND(2, 2, Element.Cost.Amount, Element.Cost.Type, Element.Slot)
		else
			self.freeSlot(Element.Slot) 
		end
		self.setCancelValue(Element.Cost.Amount)
		self.click(ID)
	end
end

self.callbackHuman = function(ID)
	local function returnIdent(Ident)
		local List = {[1]=1,[2]=2,[3]=3,[4]=4,[5]=5,[8]=5,[9]=5,[11]=7,[12]=6,[15]=8,[17]=9}
		return List[Ident]
	end

	local Element = eReg:get(ID)

	if not getVisible(shader.MOUSEOVER) then
		if self.attached == nil then
			if self.Currency[Element.Cost.Type] >= Element.Cost.Type then
				OW_CUSTOM_COMMAND(3, Element.Cost.Type, Element.Cost.Amount)
				if #Element.Sail == 2 then
					for Amount_Count = 1, Element.Sail[1].Amount do
						AddSingleUseTimer(Amount_Count/10, string.format('cards.spawnHuman(%s, %s)', Element.Sail[1].Ident, Element.Sail[1].Nation))
					end
					for Amount_Count = 1, Element.Sail[2].Amount do
						AddSingleUseTimer(Amount_Count/10, string.format('cards.spawnHuman(%s, %s)', Element.Sail[2].Ident, Element.Sail[2].Nation))
					end
					self.freeSlot(Element.Slot)
				else
					for Amount_Count = 1, Element.Sail.Amount do -- self.Squads[returnIdent(Element.Sail.Ident)].Amount do
						AddSingleUseTimer(Amount_Count/10, string.format('cards.spawnHuman(%s, %s)', Element.Sail.Ident, Element.Sail.Nation))
					end
					self.freeSlot(Element.Slot)
				end
			end
		else
			if self.isOver ~= Element.ID and self.isOver ~= self.attached then
				if Element.hasMerged ~= nil then
					if Element.hasMerged == true then
						return
					end
				end
				if Element.hasMerged == nil then
					Element.hasMerged = true
				end
				self.isOver = Element.ID
				local Over = eReg:get(self.attached)

				Element.Cost = {Type=Element.Cost.Type, Amount=Element.Cost.Amount + Over.Cost.Amount}

				setText(Element.Price, 'x'..Element.Cost.Amount)
				setText(Element.Title, 'Custom Squad')
				setText(Element.Description, self.createNewDescription(Element.Sail, Over.Sail))

				sgui_delete(Element.Image.ID)

				local getSquadInfo = function(Ident)
					local Sail_Classes = {1, 2, 3, 4, 5, 0, 0, 5, 5, 0, 7, 6, 0, -1, 8, -1, 9}
					return self.Squads[Sail_Classes[Ident]]
				end

				-- Element
				if self.state == 100 then
			    	table.insert(Element, getElementEX(Element.Front, anchorNone, XYWH(30, 32.5, 13, 12), true, {texture='SGUI/Cards/Level.png', nomouseevent=true}))
					table.insert(Element, getLabelEX(Element.Front, anchorNone, XYWH(30, 42.5, 13, 12), Tahoma_10, getText(Element.levelLabel), {nomouseevent=true, font_colour=sgui_get(Element.Title.ID, PROP_FONT_COL), shadowtext=true, text_halign=ALIGN_MIDDLE}))

					-- Over
					table.insert(Element, getElementEX(Element.Front, anchorNone, XYWH(160-43, 32.5, 13, 12), true, {texture='SGUI/Cards/Level.png', nomouseevent=true}))
					table.insert(Element, getLabelEX(Element.Front, anchorNone, XYWH(160-43, 42.5, 13, 12), Tahoma_10, getText(Over.levelLabel), {nomouseevent=true, font_colour=sgui_get(Element.Title.ID, PROP_FONT_COL), shadowtext=true, text_halign=ALIGN_MIDDLE}))

					sgui_delete(Element.levelLabel.ID)
					sgui_delete(Element.levelIcon.ID)
					sgui_delete(Element.amountLabel.ID)
					sgui_delete(Element.amountIcon.ID)
				end

				local Squad = getSquadInfo(Element.Sail.Ident)
				local Possize = XYWH((getWidth(Element)/2 - getWidth(Element)/8) - Squad.Dimensions.Width/2, 32.5, Squad.Dimensions.Width, Squad.Dimensions.Height)
				table.insert(Element, getElementEX(Element, anchorNone, Possize, true, {texture='SGUI/Cards/top_entities/'..Squad.Texture, nomouseevent=true}))

				Squad = getSquadInfo(Over.Sail.Ident)
				Possize = XYWH((getWidth(Element)/2 + getWidth(Element)/8) - Squad.Dimensions.Width/2, 32.5, Squad.Dimensions.Width, Squad.Dimensions.Height)
				table.insert(Element, getElementEX(Element, anchorNone, Possize, true, {texture='SGUI/Cards/top_entities/'..Squad.Texture, nomouseevent=true}))
				
				Element.Sail = 
				{
				    [1] = deepCopy(Element.Sail),
				    [2] = deepCopy(Over.Sail)
				}

				local Slot = Over.Slot
				self.freeSlot(Slot)
				self.attached = nil
				self.isOver = nil

				self.initCallback(Element.ID)
				--AddSingleUseTimer(math.random(0.1,0.9),'cards.createCard('..Slot..', nil)')
			end
		end
	end
end

self.callbackHero = function(ID)
	local Element = eReg:get(ID)
	if not getVisible(shader.MOUSEOVER) then
		OW_CUSTOM_COMMAND(2, 4, Element.Cost.Amount, Element.Cost.Type, Element.Slot)
		self.hero = Element.Sail.Ident
	end
end

self.callbackTechnology = function(ID)
	local Element = eReg:get(ID)
	if not getVisible(shader.MOUSEOVER) then
		OW_CUSTOM_COMMAND(Element.Sail.Category, Element.Sail.Ident, Element.Slot, Element.Cost.Amount, Element.Cost.Type)
	end
end

self.initCallback = function(ID)
	local Element = eReg:get(ID)

	if Element.Sail.Category == 0 then
		sgui_setcallback(ID, CALLBACK_MOUSECLICK, 'cards.callbackBuilding('..ID..')')
	end

	if Element.Sail.Category == 1 then
		sgui_setcallback(ID, CALLBACK_MOUSECLICK, 'if %b == 0 then cards.callbackHuman('..ID..') elseif %b == 1 then cards.callbackMerge('..ID..') end')
	end

	if Element.Sail.Category == 2 then
		sgui_setcallback(ID, CALLBACK_MOUSECLICK, 'cards.callbackHero('..ID..')')
	end

	if Element.Sail.Category == 3 then
		sgui_setcallback(ID, CALLBACK_MOUSECLICK, 'cards.callbackMultipleHuman('..ID..')')
	end

	if Element.Sail.Category == 10 then
		sgui_setcallback(ID, CALLBACK_MOUSECLICK, 'cards.callbackTechnology('..ID..')')
	end
end

self.animate = function()
	self.updateDevInfo()

	if #self.ANIM_LIST > 0 then
		for k, v in ipairs(self.ANIM_LIST) do
			local Element = eReg:get(v)
			setRotation(Element, SetRotate(1, Element.Angle * 2, false))
			setRotation(Element.Front, SetRotate(1, 180 + Element.Angle * 2, false))
			Element.Angle = (Element.Angle +2) % 360
			if Element.Angle > 90 then
				Element.Angle = 0
				table.remove(self.ANIM_LIST, k)
				self.initCallback(v)
			end
		end
	end
end

self.tempX = 0
self.MousePos = {X=0, Y=0}
self.canUpdatePhysics = true

self.dragCard = function()
	if self.attached ~= nil then
		local Element, Pointer = eReg:get(self.attached), eReg:get(1)

		if self.canUpdatePhysics then
			self.MousePos = {X=getX(Pointer), Y=getY(Pointer)}
			self.canUpdatePhysics = false
			AddSingleUseTimer(0.05, 'cards.canUpdatePhysics = true')
		end

		setParent(Element, nil)
		--AddEventSlideX(self.attached, getX(Pointer)-getWidth(Element)+18, 0.1)
		--AddEventSlideY(self.attached, getY(Pointer)-getHeight(Element)+41-22, 0.1)

		setX(Element, getX(Pointer)-getWidth(Element)+18, 0.1)
		setY(Element, getY(Pointer)-getHeight(Element)+41-22, 0.1)
	end
end

self.physicsCard = function()
	if self.attached ~= nil then
		local Element, Pointer = eReg:get(self.attached), eReg:get(1)
		local X = getX(Pointer)
		
		local deltaX = X - self.MousePos.X
		
		self.tempX = (self.tempX + (deltaX * 0.5)) * 0.9
		self.tempX = math.max(math.min(self.tempX, 30), -30)
		
		if math.abs(self.tempX) < 0.1 then
			self.tempX = self.tempX > 0 and 0.1 or -0.1
		end
		
		self.MousePos.X = X

		setRotation(Element.Front, SetRotate(2, 360 - self.tempX, false))
	end
end

regTickCallback('cards.dragCard()')
regTickCallback('cards.physicsCard()')

self.createNewDescription = function(Element, Over)
	local getTitle = function(Ident)
		local List = {[1]='Soldier(s)',[2]='Engineer(s)',[3]='Mechanic(s)',[4]='Scientist(s)',[5]='Sniper(s)',[8]='Mortarmen(s)',[9]='Bazooker(s)',[11]='Sheikh(s)',[12]='Apemen',[14]='Tiger(s)',[15]='Ap. Sol.',[17]='Ap. Kam.'}
		return List[Ident]
	end

	local getSquadInfo = function(Ident)
		local Sail_Classes = {1, 2, 3, 4, 5, 0, 0, 5, 5, 0, 7, 6, 0, -1, 8, -1, 9}
		return self.Squads[Sail_Classes[Ident]]
	end

	local values = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} 

	values[Element.Ident] = values[Element.Ident] +1
	values[Over.Ident] = values[Over.Ident] +1

	local Label = ''
	for k, v in ipairs(values) do
		if v > 0 then
			Label = Label .. 'x'..((getSquadInfo(k).Amount)*v)..' ' .. getTitle(k)..'\n'
		end
	end
	return Label
end

self.isOver = nil

self.MouseOver = function(ID)
	local Element = eReg:get(ID)
	AddEventSlideY(ID, 0, 0.25, '') 
	setText(cards.header.label, getText(Element.Description)) 
	setFontColour(cards.header.label, sgui_get(Element.Description.ID, PROP_FONT_COL))
end

self.attached = nil

self.callbackMerge = function(ID)
	if not getVisible(shader.MOUSEOVER) then
		local Element = eReg:get(ID)
		if Element.hasMerged then
			return
		end
		setRotation(Element.Front, SetRotate(2, self.tempX, false))
		self.attached = ID

		setWH(Element, 160*0.75, 192*0.75)
		setWH(Element.Front, 160*0.75, 192*0.75)

		setVisible(Element.FrontAnim, false)
		setWH(Element.FrontAnim, 160*0.75, 192*0.75)

		setNoMouseEvent(Element, true)

		setWH(Element.Image, getWidth(Element.Image)*0.75, getHeight(Element.Image)*0.75)
		setXY(Element.Image, getWidth(Element)/2-getWidth(Element.Image)/2, getY(Element.Image)+20)

		setFontName(Element.Description, Tahoma_10)
		setFontName(Element.Title, Tahoma_12B)
		setFontName(Element.Price, Tahoma_10)

		setWH(Element.Description, 94, 44)
		setXY(Element.Description, 13, 90)

		setY(Element.Title, getY(Element.Title) + 20)
		setY(Element.Price, getY(Element.Price) - 2.5)

		if self.state == 100 then
			setXY(Element.levelIcon, 15, 30)
			setXY(Element.levelLabel, 16.5, 0)
			setXY(Element.amountIcon, 15, 47.5)
			setXY(Element.amountLabel, 18, 0)
		end

		setX(Element.PriceIcon, 160*0.75-45*0.75)
		setWH(Element.PriceIcon, 25*0.75, 25*0.75)
		setWidth(Element.Price, getWidth(Element.Price)-7.5)
	end
end