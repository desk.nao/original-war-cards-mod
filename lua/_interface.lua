local self = cards
self.PlayerLeft = nil
self.PlayerRight = nil

self.SetGeneral = function(SAIL_ID, POSITION, SIDE)
	if POSITION == 1 then
		self.PlayerLeft = SIDE
	else
		self.PlayerRight = SIDE
	end
	local Offset, Width = getWidth(Minimap)+15, -58
	if POSITION == 1 then
		Offset, Width = -72, 58
	end
	self.InfoPlayers.GENERAL[POSITION] = getElementEX(Minimap, anchorNone, XYWH(Offset, getHeight(Minimap)/2, 58, 72), true, {})
	setTextureFromID(self.InfoPlayers.GENERAL[POSITION], getUnitTexture(SAIL_ID), 58, 72, Width, 72)
	setRotation(self.InfoPlayers.GENERAL[POSITION], SetRotate(1, 0, false))
	setVisible(self.InfoPlayers.GENERAL[POSITION], true)
end

self.SetDepot = function(SAIL_ID, POSITION)
	local Parent = self.InfoPlayers.GENERAL[POSITION]
	local Offset, Width = 66, 74
	if POSITION == 1 then
		Offset, Width = -82, -74
	end
	self.InfoPlayers.DEPOT[POSITION] = getElementEX(Parent, anchorNone, XYWH(Offset, 2, 74, 68), true, {})
	setTextureFromID(self.InfoPlayers.DEPOT[POSITION], getUnitTexture(SAIL_ID), 74, 68, Width, 68)
end

self.SetUnitHealth = function(SAIL_ID, HEALTH)
	local ele = getElementEX(nil, anchorNone, XYWH(500,200,58,72), true, {})
	setTextureFromID(ele, getUnitTexture(SAIL_ID), 58, 72, 58, 72)
end

self.createBoard = function()
	self.board = getScrollboxEX(nil,anchorNone,XYWH(0,1080-275,1920,275),{tile=true,texture='SGUI/Cards/FacePanel.png',scissor=false,nomouseeventthis=true,})
	if self.CurrentNation == 2 or self.CurrentNation == 3 then
		setColour1(self.board, BLACK(0))
	end
	self.board_bar = getScrollBarEX(nil,anchorNone,XYWH(0,1080-285,1920,10),'scrollbar.png',nil,{nomouseeventthis=true,colour1=Scrollbar_Colour1,colour2=Scrollbar_Colour2,},{vertical=false,})
	sgui_set(self.board.ID, PROP_SCROLLBAR, self.board_bar.ID)
	self.header = getElementEX(nil, anchorNone, XYWH(0, 1080-325, 1920, 50), true, {nomouseeventthis=true,colour1=BLACKA(200)})
	self.header.label = getLabelEX(self.header, anchorNone, XYWH(0, 0, 1920, 50), Tahoma_14B, 'This is the header label.', {nomouseeventthis=true,font_colour=SIDE_COLOURS[2],shadowtext=true,text_halign=ALIGN_MIDDLE})
end

self.createBoard()

self.scaleDimensions = function(w, h, maxWidth, maxHeight)
    if w > maxWidth then
        local aspectRatio = w / h
        w = maxWidth
        h = maxWidth / aspectRatio
    end
    if h > maxHeight then
        local aspectRatio = w / h
        h = maxHeight
        w = maxHeight * aspectRatio
    end
    return w, h
end

self.info = getElementEX(nil, anchorNone, XYWH(15, LayoutHeight-120, 170, 100), true, {labels={},nomouseeventthis=true,colour1=BLACKA(100)})

self.createInfoLabel = function()
	local Labels = {'Breastworks', 'Bunkers', 'Laboratory', 'Workshop', 'Siberite mines', 'Derricks'}
	local Textures = {'breastworks', 'bunker', 'basiclab', 'workshop', 'siberiummine', 'oilmine'}
	local temp
	for k, v in ipairs(Labels) do
		table.insert(self.info.labels, getLabelEX(self.info,anchorTR,XYWH(25,5+(15*(k-1)),315,0),Tahoma_12,v,{text_halign=ALIGN_LEFT,font_colour=RGB(255,255,255)}))
	end
	for k, v in ipairs(Textures) do
		getElementEX(self.info, anchorNone, XYWH(5,4+(15*(k-1)),15,15), true, {texture='SGUI/Cards/top_entities/b-u-'..v..'0001.png'})
		temp = getImageButtonEX(self.info, anchorNone, XYWH(145,0+(15*(k-1)),25,10), '?', '', '', SKINTYPE_BUTTON, {})
		ApplySkinToElement(temp)
		setFontColour(temp,RGB(255,230,0))
	end
end

self.createInfoLabel()

self.updateLabels = function(Type, Amount)
	local list = {'Breastworks', 'Bunkers', 'Laboratory', 'Workshop', 'Siberite mines', 'Derricks'}
	local limit = {2, 2, 1, 1, 2, 2}
	setText(self.info.labels[Type], list[Type]..' '..Amount..'/'..limit[Type])

	setText(self.Info.Slots.Label,'')
	local element, sail, category
	for k, v in ipairs(self.slots) do
		if v ~= 0 then
			element = eReg:get(v)
			sail = element.Sail.Ident or 'nil'
			category = element.Sail.Category or 'nil'
			setText(self.Info.Slots.Label, getText(self.Info.Slots.Label) .. '<'..k..'> '..v..' (C:'..category..', S:'..sail..')\n\n')
			setY(self.Info.Slots.Label, getHeight(self.Info.Slots)/2+getHeight(self.Info.Slots.Label)/2)
		end
	end
end

self.createDevInfo = function()
	self.devInfo = getElementEX(nil, anchorNone, XYWH(190, LayoutHeight-180, 195, 160), true, {labels={},nomouseeventthis=true,colour1=BLACKA(100)})
	for k = 1, 12 do
		table.insert(self.devInfo.labels, getLabelEX(self.devInfo,anchorTR,XYWH(5,5+(15*(k-1)),315,0),Tahoma_12,'',{text_halign=ALIGN_LEFT,font_colour=RGB(255,255,255)}))
	end
	self.devInfo2 = getElementEX(nil, anchorNone, XYWH(390, LayoutHeight-180, 195, 160), true, {labels={},nomouseeventthis=true,colour1=BLACKA(100)})
	for k = 1, 9 do
		table.insert(self.devInfo2.labels, getLabelEX(self.devInfo2,anchorTR,XYWH(5,5+(15*(k-1)),315,0),Tahoma_12,'',{text_halign=ALIGN_LEFT,font_colour=RGB(255,255,255)}))
		setFontColour(self.devInfo2.labels[k],RGB(255,230,0))
	end

	self.Info = {}

	self.Info.Slots = getElementEX(nil, anchorNone, XYWH(190, LayoutHeight-180-5-120, 195, 120), true, {nomouseeventthis=true,colour1=BLACKA(100)})
	self.Info.Slots.Label = getLabelEX(self.Info.Slots,anchorTR,XYWH(5,5,185,0),Tahoma_12,'',{text_halign=ALIGN_LEFT,font_colour=RGB(255,255,255)})

	self.Info.Cards = getElementEX(nil, anchorNone, XYWH(15, LayoutHeight-285, 170, 160), true, {nomouseeventthis=true,colour1=BLACKA(0)})
	self.Info.Cards.Element = getScrollboxEX(self.Info.Cards,anchorNone,XYWH(5,35,160,120),{nomouseeventthis=false,colour1=BLACKA(100)})
	getScrollBarEX(self.Info.Cards,anchorTRB,XYWH(153,35,12,120),'scrollbar.png',self.Info.Cards.Element,{colour1=Scrollbar_Colour1,colour2=Scrollbar_Colour2,},{});

	local List = 
	{
		getImageButtonEX(self.Info.Cards, anchorNone, XYWH(2.5,5,25,15), 'Hu', '', 'cards.ShowCards(1)', SKINTYPE_BUTTON, {}),
		getImageButtonEX(self.Info.Cards, anchorNone, XYWH(27.5,5,25,15), 'Bu', '', 'cards.ShowCards(2)', SKINTYPE_BUTTON, {}),
		getImageButtonEX(self.Info.Cards, anchorNone, XYWH(52.5,5,25,15), 'He', '', 'cards.ShowCards(3)', SKINTYPE_BUTTON, {}),
		getImageButtonEX(self.Info.Cards, anchorNone, XYWH(77.5,5,25,15), 'Te', '', 'cards.ShowCards(4)', SKINTYPE_BUTTON, {}),
	}
	for k, v in ipairs(List) do
		ApplySkinToElement(v)
		setFontColour(v,RGB(255,230,0))
	end
end

self.createDevInfo()

self.updateDevInfo = function()
    local labels = {
		'hasBarracks',
		'hasArmoury',
        'hasWorkshop', 
        'hasLaboratory',
        'hasFactory',
        'hasWarehouse',
        'canUnlockSlots', 
        'hasTechnologicalCard'
    }

    for k, label in ipairs(labels) do
        local value = self[label]
        local statusText = label .. " -> " .. tostring(value)
        local fontColor = value and RGB(0, 230, 0) or RGB(230, 0, 0)

        setText(self.devInfo.labels[k], statusText)
        setFontColour(self.devInfo.labels[k], fontColor)
    end

    labels = {'Crates', 'Oil', 'Siberite'}

    for k, label in ipairs(labels) do
        setText(self.devInfo2.labels[k], label..' -> x'..self.Currency[k])
    end

    labels = {'Soldiers', 'Engineers', 'Mechanics', 'Scientists', 'Specialists'}

    for k, label in ipairs(labels) do
        setText(self.devInfo2.labels[k+3], 'x'..self.Squads[k].Amount..' '..self.Squads[k].Title)
    end

    setText(self.devInfo2.labels[9], 'Curr. Res. -> '..self.currentResearch)
end

self.ShowCards = function(Category)
	sgui_deletechildren(self.Info.Cards.Element.ID)
	local Deck = self.POOL[self.CurrentNation]
	local List = {Deck.HUMAN, Deck.BUILDING, Deck.HERO, Deck.TECHNOLOGY}
	for k, v in ipairs(List[Category]) do
		self.Info.Cards.Element[k] = getLabelEX(self.Info.Cards.Element,anchorTR,XYWH(5,5+(15*(k-1)),315,0),Tahoma_12,v.Title,{nomouseevent=true,text_halign=ALIGN_LEFT,font_colour=RGB(255,255,255)})
	end
end

self.ShowDev = getImageButtonEX(nil, anchorNone, XYWH(25,75,125,15), 'Show Dev. Panels', '', 'cards.showDevPanels()', SKINTYPE_BUTTON, {})
ApplySkinToElement(self.ShowDev)

self.showDevPanels = function()
	setVisible(self.info, not getVisible(self.info))
	setVisible(self.devInfo, not getVisible(self.devInfo))
	setVisible(self.devInfo2, not getVisible(self.devInfo2))
	setVisible(self.Info.Slots, not getVisible(self.Info.Slots))
	setVisible(self.Info.Cards, not getVisible(self.Info.Cards))
end

self.showDevPanels()

function FROMOW_INFOPANEL_UPDATE(DATA)
  game_info.Update(DATA)
  UnitSelection.update(DATA)
  --Xichted.processUnit(OW_GET_UNIT_PORTRAIT_PARTS(DATA.ID))
end