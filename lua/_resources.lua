local self = cards

self.setResource = function(Cans, Oil, Siberite)
	self.Currency = {Cans, Oil, Siberite}
end

self.TooPoor = function()
	set_Callback(gamewindow.ID, CALLBACK_MOUSECLICK, '')
	set_Callback(gamewindow.ID, CALLBACK_MOUSEMOVE, '')
	setVisible(shader.MOUSEOVER, false)
	setVisible(self.cancel, false)
	shader.canPlace = false
	self.cancel.refund = 0
end

self.doCancel = function()
	set_Callback(gamewindow.ID, CALLBACK_MOUSECLICK, '')
	set_Callback(gamewindow.ID, CALLBACK_MOUSEMOVE, '')
	setVisible(shader.MOUSEOVER, false)
	setVisible(self.cancel, false)
	shader.canPlace = false
	OW_CUSTOM_COMMAND(200, 1, self.cancel.refund)
	self.cancel.refund = 0
end

self.cancel = getImageButtonEX(nil, anchorNone, XYWH(0, 0, 75, 25), 'Cancel', '', 'cards.doCancel()', SKINTYPE_BUTTON, {hint='You will obtain a full refund of the cost of the card.', refund=0, font_colour_disabled=GRAY(127)})
ApplySkinToElement(self.cancel)
setVisible(self.cancel, false)

self.setCancelValue = function(Value)
	self.cancel.refund = Value
end
