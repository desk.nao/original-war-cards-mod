local self = cards

self.KilledCount = 0
self.Reward = nil
self.RewardStep = 0
self.KillCount = {}

self.setCountLabel = function(Side, Position)
	local Parent = self.InfoPlayers.GENERAL[Position]
	local Possize = XYWH(0,getHeight(Parent)+5,getWidth(Parent),12.5)
	if Position == 1 then
		self.KillCount.Left = getLabelEX(Parent,anchorTR,Possize,Tahoma_12,'0',{shadowtext=true,text_halign=ALIGN_MIDDLE,font_colour=RGB(255,255,255)})
	end
	if Position == 2 then
		self.KillCount.Right = getLabelEX(Parent,anchorTR,Possize,Tahoma_12,'0',{shadowtext=true,text_halign=ALIGN_MIDDLE,font_colour=RGB(255,255,255)})
	end
end

self.setKilledCount = function(Count, Position)
	if Position == 1 then
		setText(self.KillCount.Left, Count..' kills.')
	else
		setText(self.KillCount.Right, Count..' kills.')
	end
	
end

self.RewardList =
{
	-- American
	{
		Chassis =
		{
			{Texture='wh-u-small', W=22, H=36, Title='Light Wheels', Coefficient=1, Type=1},
			{Texture='wh-u-big', W=26, H=38, Title='Medium Wheels', Coefficient=2, Type=1},
			{Texture='wh-u-medtrack', W=64, H=64, Title='Medium Tracks', Coefficient=3, Type=1},
			{Texture='wh-u-heavytrack', W=80, H=76, Title='Heavy Tracks', Coefficient=4, Type=1},
			{Texture='wh-u-morphling', W=46, H=46, Title='Morphling', Coefficient=5, Type=1}
		},
		Weapons = 
		{
			{Texture='w-u-mg', W=62, H=60, Title='Machine Gun', Coefficient=1, Type=1},
			{Texture='w-u-lightgun', W=70, H=64, Title='Light Gun', Coefficient=2, Type=1},
			{Texture='w-u-gg', W=48, H=48, Title='Gatling Gun', Coefficient=3, Type=1},
			{Texture='w-u-double', W=80, H=78, Title='Double Gun', Coefficient=4, Type=1},
			{Texture='w-u-heavy', W=96, H=92, Title='Heavy Gun', Coefficient=5, Type=1},
			{Texture='w-u-rocket', W=46, H=64, Title='Rocket Launcher', Coefficient=2, Type=1},
			{Texture='w-u-las', W=46, H=62, Title='Laser', Coefficient=3, Type=3},
			{Texture='w-u-dlas', W=40, H=46, Title='Double Laser', Coefficient=4, Type=3}
		},

	},

	-- Arabian
	{},

	-- Russian
	{}
}

self.returnRewards = function(i)
	local Parent = self.RewardList[self.CurrentNation]

	if #Parent.Weapons < 1 and #Parent.Chassis < 1 then
		return
	end

	local R, Deck = math.random(1,2)

	if R == 1 then
		Deck = Parent.Weapons
		if #Deck < 1 and #Parent.Chassis > 0 then
			Deck = Parent.Chassis
		end
	else
		Deck = Parent.Chassis
		if #Deck < 1 and #Parent.Weapons > 0 then
			Deck = Parent.Weapons
		end
	end

	local Results = {}

	table.insert(Results, Deck[math.random(1,#Deck)])
	table.insert(Results, Deck[math.random(1,#Deck)])

	if Results[2] == Results[1] then
		repeat
			table.remove(Results, 2)
			table.insert(Results, Deck[math.random(1,#Deck)])
		until Results[2] ~= Results[1]
	end

	return Results
end

self.createRewardButton = function()
	local Background = getElementEX(nil, anchorNone, XYWH(getX(Minimap),getY(Minimap)+getHeight(Minimap)+2.5,getWidth(Minimap),25), true, {colour1=BLACKA(175)})
	local Button = getImageButtonEX(Background, anchorNone, XYWH(0,0,getWidth(Minimap),75), 'REWARD AVAILABLE', '', 'cards.createRewardWindow()', SKINTYPE_BUTTON, {})
	ApplySkinToElement(Button)
	setFontColour(Button,RGB(255,230,0))
end

AddSingleUseTimer(2,'cards.createRewardButton()')

self.queueAnimReward = {}

self.createRewardWindow = function()
	if self.Reward ~= nil then
		sgui_delete(self.Reward.ID)
	end
	self.Reward = getElementEX(nil, anchorNone, XYWH(LayoutWidth/2+150,225,600,375), true, {nomouseeventthis=true,colour1=BLACKA(0)})

	self.Reward.Warning = getLabelEX(self.Reward,anchorTR,XYWH(150,170,300,25),Tahoma_12,'Warning\n\nBe mindful... an advanced component will be very expensive and might NOT be a good idea early-on!',{alpha=0,wordwrap=true,shadowtext=true,text_halign=ALIGN_MIDDLE,font_colour=RGB(225,20,20)})

	self.Reward.Choices = 
	{
		getElementEX(self.Reward, anchorNone, XYWH(-150,45,125,125), true, {scissor=true,alpha=0,colour1=BLACKA(125)}),
		getElementEX(self.Reward, anchorNone, XYWH(getWidth(self.Reward)+25,45,125,125), true, {cissor=true,alpha=0,colour1=BLACKA(125)})
	}

	self.accumulatedTime = 0

	local Data = self.returnRewards(i)

	self.RewardStep = self.RewardStep +1

	local ResType = {'crates','oil','siberite'}
	local CostLabel = ''

	for i = 1, 2 do
		CostLabel = 'x'..Data[i].Coefficient*(4-Data[i].Type)..' '..ResType[Data[i].Type]..'.'

		self.Reward.Choices[i].Elements =
		{
			Label = getImageButtonEX(self.Reward.Choices[i], anchorNone, XYWH(5,125,115,15), '', '', 'cards.ShowCards(1)', SKINTYPE_BUTTON, {typewriterIndex=1,loc=i,currentText='',targetText=Data[i].Title,font_style_outline=true,wordwrap=true,autosize=true,automaxwidth=210,shadowtext=true,text_halign=ALIGN_MIDDLE,font_colour=RGB(255,255,255)}),
			Image = getElementEX(self.Reward.Choices[i], anchorNone, XYWH(62.5-Data[i].W/2,62.5-Data[i].H/2,Data[i].W,Data[i].H), true, {texture='SGUI/Cards/top_entities/'..Data[i].Texture..'.png'}),
			Cost = getLabelEX(self.Reward.Choices[i],anchorNone,XYWH(2.5,2.5,120,10),Tahoma_10,CostLabel,{wordwrap=true,shadowtext=true,text_halign=ALIGN_LEFT,font_colour=RGB(200,200,200)})
		}
		ApplySkinToElement(self.Reward.Choices[i].Elements.Label)
		AddSingleUseTimer(1.5,'table.insert(cards.queueAnimReward, cards.Reward.Choices['..i..'].Elements.Label.ID)')
	end

	AddEventFade(self.Reward.Warning.ID, 255, 1.25, nil)
	AddEventFade(self.Reward.Choices[1].ID, 255, 1.75, nil)
	AddEventFade(self.Reward.Choices[2].ID, 255, 1.75, nil)
	AddEventSlideX(self.Reward.Choices[1].ID, 125, 1, nil)
	AddEventSlideX(self.Reward.Choices[2].ID, getWidth(self.Reward)-125-125, 1, nil)
end

self.accumulatedTime = 0

self.animateRewardLabel = function(frametime)
    if self.Reward == nil or #self.queueAnimReward == 0 then
        return
    end

    self.accumulatedTime = self.accumulatedTime + frametime

    if self.accumulatedTime >= 0.04 then
        self.accumulatedTime = self.accumulatedTime - 0.04

        for k, reward in ipairs(self.queueAnimReward) do
            local v = eReg:get(reward)
            if v.typewriterIndex <= #v.targetText then
                v.currentText = v.currentText .. v.targetText:sub(v.typewriterIndex, v.typewriterIndex)
                setText(v, v.currentText)
                v.typewriterIndex = v.typewriterIndex + 1
                sgui_autosizecheck(v.ID)
                if self.CurrentNation == 3 then
                	setY(v, getHeight(self.Reward.Choices[v.loc]) - getHeight(v) - 5)
                else
                	setY(v, getHeight(self.Reward.Choices[v.loc]) - getHeight(v) - 10)
                end
            else
                table.remove(self.queueAnimReward, k)
            end
        end
    end
end

regTickCallback('cards.animateRewardLabel(%frametime)')