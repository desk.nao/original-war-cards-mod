local self = cards

self.SetCardProgress = function(Slot, Progress)
	if self.slots[Slot] == nil then
		OW_CUSTOM_COMMAND(11)
		return
	end
	local Element = eReg:get(self.slots[Slot])
	setColour1(Element.FrontAnim, WHITEA((1 - Progress / 100) * 360))
	setText(Element.Description, 'Researching... ' .. Progress .. '% / 100%')
end

self.cancelResearch = function()
    for k, v in ipairs(self.slots) do
    	if v ~= 0 then 
    		v = eReg:get(v)
    		if v.Sail.Category == 10 then
    			self.freeSlot(k)
    		end
    	end
    end
end

self.completeResearch = function(Ident)
    local function RemoveByName(Table, Title)
        for k, v in ipairs(Table) do
            if v.Title == Title then
                table.remove(Table, k)
                break
            end
        end
    end

    local Checker

	for k, v in ipairs(self.Technologies) do
		Checker = false
		if v.Sail.Ident == Ident then
			RemoveByName(self.POOL[self.CurrentNation].TECHNOLOGY, v.Title)
			AddSingleUseTimer(k, 'Notify.add(cards.Technologies['..k..'], "Complete: ", RGB(100,255,100))')
			if v.Title == 'Upgr. Soldiers' or v.Title == 'Upgr. Engineers' or v.Title == 'Upgr. Mechanics' or v.Title == 'Upgr. Scientists' or v.Title == 'Upgr. Snipers' or v.Title == 'Upgr. Mortars' or v.Title == 'Upgr. Bazookers' then
				if v.Level < 10 then
					Checker = true
					self.Technologies[k].Level = self.Technologies[k].Level +1
					table.insert(self.Technologies, v)
					break
				end
			end
			if v.Title == 'Add Soldier' or v.Title == 'Add Mechanic' or v.Title == 'Add Scientist' or v.Title == 'Add Sniper' or v.Title == 'Add Mortar' or v.Title == 'Add Bazooker' then
				if v.Amount < 3 then
					Checker = true
					self.Technologies[k].Amount = self.Technologies[k].Amount +1
					table.insert(self.Technologies, v)
					break
				end
			end
			if Checker == false then
				table.remove(self.Technologies, k)
				break
			end
		end
	end
end