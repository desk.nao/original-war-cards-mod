local self = cards

self.hasBarracks = false
self.hasArmoury = false
self.hasWorkshop = false
self.hasLaboratory = false
self.hasFactory = false
self.hasWarehouse = false

self.canSpawnBunker = false

self.I = {}

if self.CurrentNation == 1 then
	self.I = {'u','US','Sniper',5}
end

if self.CurrentNation == 2 then
	self.I = {'a','AR','Mortar',8}
end

if self.CurrentNation == 3 then
	self.I = {'r','RU','Bazooker',9}
end

self.Squads = 
{ 	

	{
		Title		= 'Soldier',
		Level 		= 1,
		Comment		= 'Gets into defenses & attacks.',
		Amount		= 1,
		Cost		= {Amount=5, Type=1},
		Dimensions	= {Width=447, Height=708},
		Sail 		= {Category=1, Ident=1, Nation=self.CurrentNation, Amount=1, Level=1},
		Texture 	= self.I[2]..'#Male#Soldier.png'
	},

	{
		Title		= 'Engineer',
		Level 		= 1,
		Comment		= 'Upgrades & repairs buildings, then attacks.',
		Amount		= 1,
		Cost		= {Amount=5, Type=1},
		Dimensions	= {Width=335, Height=708},
		Sail 		= {Category=1, Ident=2, Nation=self.CurrentNation, Amount=1, Level=1},
		Texture 	= self.I[2]..'#Male#Engineer.png'
	},

	{
		Title		= 'Mechanic',
		Level 		= 1,
		Comment		= 'Creates vehicles & weapons, then attacks.',
		Amount		= 1,
		Cost		= {Amount=5, Type=1},
		Dimensions	= {Width=335, Height=708},
		Sail 		= {Category=1, Ident=3, Nation=self.CurrentNation, Amount=1, Level=1},
		Texture 	= self.I[2]..'#Male#Mechanic.png'
	},

	{
		Title		= 'Scientist',
		Level 		= 1,
		Comment		= 'Researches, then heals and attacks.',
	 	Amount		= 1,
	 	Cost		= {Amount=15, Type=1},
	 	Dimensions	= {Width=34, Height=718},
	 	Sail 		= {Category=1, Ident=4, Nation=self.CurrentNation, Amount=1, Level=1},
	 	Texture 	= self.I[2]..'#Male#Scientist.png'
	},

	{
		Title		= self.I[3],
		Level 		= 1,
		Comment		= 'Attacks.',
	 	Amount		= 1,
	 	Cost		= {Amount=15, Type=1},
	 	Dimensions	= {Width=34, Height=718},
	 	Sail 		= {Category=1, Ident=self.I[4], Nation=self.CurrentNation, Amount=1, Level=1},
	 	Texture 	= self.I[2]..'#Male#'..self.I[3]..'.png'
	},

	{
		Title		= 'Apeman',
		Level 		= 1,
		Comment		= 'Attacks.',
	 	Amount		= 1,
	 	Cost		= {Amount=1, Type=1},
	 	Dimensions	= {Width=317, Height=623},
	 	Sail 		= {Category=1, Ident=12, Nation=0, Amount=1, Level=1},
	 	Texture 	= 'NA#Ape#Regular.png'
	},

	{
		Title		= 'Sheikh',
		Level 		= 1,
		Comment		= 'Attacks.',
	 	Amount		= 1,
	 	Cost		= {Amount=10, Type=1},
	 	Dimensions	= {Width=29, Height=55},
	 	Sail 		= {Category=1, Ident=11, Nation=2, Amount=1, Level=1},
	 	Texture 	= 'AR#Male#Sheik.png'
	},

	{
		Title		= 'Ap. Sol.',
		Level 		= 1,
		Comment		= 'Attacks.',
	 	Amount		= 1,
	 	Cost		= {Amount=3, Type=1},
	 	Dimensions	= {Width=24, Height=48},
	 	Sail 		= {Category=1, Ident=15, Nation=self.CurrentNation, Amount=1, Level=1},
	 	Texture 	= 'NA#Ape#Soldier.png'
	},

	{
		Title		= 'Ap. Kam.',
		Level 		= 1,
		Comment		= 'Explodes.',
	 	Amount		= 1,
	 	Cost		= {Amount=3, Type=1},
	 	Dimensions	= {Width=24, Height=48},
	 	Sail 		= {Category=1, Ident=17, Nation=2, Amount=1, Level=1},
	 	Texture 	= 'NA#Ape#Kamikaze.png'
	},
}

if self.CurrentNation == 3 then
	local list = {{37,52}, {21,55}, {25,52}, {21,53}, {37,60}, {24,48}, {28,57}, {40,49}, {35,51}}
	for k, v in ipairs(self.Squads) do
			self.Squads[k].Dimensions = {Width=list[k][1], Height=list[k][2]}
	end
end

if self.CurrentNation == 2 then
	local list = {{37,53}, {21,53}, {25,52}, {21,54}, {22,55}, {24,48}, {28,57}, {40,49}, {35,51}}
	for k, v in ipairs(self.Squads) do
			self.Squads[k].Dimensions = {Width=list[k][1], Height=list[k][2]}
	end
end

if self.CurrentNation == 1 then
	local list = {{34,54}, {21,55}, {25,54}, {25,55}, {29,55}, {24,48}, {28,57}, {40,49}, {35,51}}
	for k, v in ipairs(self.Squads) do
			self.Squads[k].Dimensions = {Width=list[k][1], Height=list[k][2]}
	end
end

 --{Title='Lvl. 1 Machair.', Comment='Attacks.', Cost={Amount=0, Type=1}, Texture='NA#Machairodus.png', Dimensions={Width=24, Height=48}, Sail={Category=1, Ident=14, Nation=0}}

self.Buildings =
{
	Workshop = {Title='Workshop', Comment='Unlocks vehicle cards.', Cost={Amount=35, Type=1}, Texture='b-'..self.I[1]..'-workshop0000.png', Dimensions={Width=159, Height=164}, Sail={Category=0, Ident=2, Nation=self.CurrentNation}},
	Laboratory = {Title='Laboratory', Comment='Unlocks upgrades for most cards.', Cost={Amount=20, Type=1}, Texture='b-'..self.I[1]..'-basiclab0000.png', Dimensions={Width=118, Height=129}, Sail={Category=0, Ident=6, Nation=self.CurrentNation}},
	Armoury = {Title='Armoury', Comment='Unlocks powerful attacking cards.', Cost={Amount=25, Type=1}, Texture='b-'..self.I[1]..'-armoury0000.png', Dimensions={Width=131, Height=108}, Sail={Category=0, Ident=4, Nation=self.CurrentNation}},
	SiberiumMine = {Title='Siberite Mine', Comment='Allows the placement of a siberite mine.', Cost={Amount=50, Type=1}, Texture='b-'..self.I[1]..'-siberiummine0000.png', Dimensions={Width=74, Height=63}, Sail={Category=0, Ident=30, Nation=self.CurrentNation}},
	Derrick = {Title='Derrick', Comment='Allows the placement of an oil mine.', Cost={Amount=40, Type=1}, Texture='b-'..self.I[1]..'-oilmine0000.png', Dimensions={Width=84, Height=69}, Sail={Category=0, Ident=29, Nation=self.CurrentNation}},
	Bunker = {Title='Bunker', Comment='Allows the placement of a basic bunker.', Cost={Amount=60, Type=1}, Texture='b-'..self.I[1]..'-bunker0000.png', Dimensions={Width=76, Height=65}, Sail={Category=0, Ident=32, Nation=self.CurrentNation}},
	Breastwork = {Title='Breastwork', Comment='Allows the placement of breastworks.', Cost={Amount=20, Type=1}, Texture='b-'..self.I[1]..'-breastworks0000.png', Dimensions={Width=91,	Height=87}, Sail={Category=0, Ident=31, Nation=self.CurrentNation}},
}

if self.CurrentNation == 3 then
	self.Buildings.Workshop.Dimensions = {Width=161, Height=157}
	self.Buildings.Laboratory.Dimensions = {Width=117, Height=120}
	self.Buildings.Armoury.Dimensions = {Width=125, Height=107}
	self.Buildings.SiberiumMine.Dimensions = {Width=81, Height=68}
	self.Buildings.Derrick.Dimensions = {Width=87, Height=91}
	self.Buildings.Bunker.Dimensions = {Width=78, Height=58}
	self.Buildings.Breastwork.Dimensions = {Width=84, Height=81}
end

if self.CurrentNation == 2 then
	self.Buildings.Workshop.Dimensions = {Width=161, Height=155}
	self.Buildings.Laboratory.Dimensions = {Width=116, Height=146}
	self.Buildings.Armoury.Dimensions = {Width=147, Height=135}
	self.Buildings.SiberiumMine.Dimensions = {Width=86, Height=91}
	self.Buildings.Derrick.Dimensions = {Width=79, Height=96}
	self.Buildings.Bunker.Dimensions = {Width=82, Height=66}
	self.Buildings.Breastwork.Dimensions = {Width=74, Height=86}
end

self.unlockDismantle = function()
	self.dismantle = getImageButtonEX(nil, anchorNone, XYWH(20,getY(self.header)-25,160,15), 'Dismantle a building', '', 'cards.dismantle()', SKINTYPE_BUTTON, {})
	ApplySkinToElement(self.dismantle)
end

self.dismantle = function()

end

self.Technologies =
{
	--[[ Upgrade Soldiers    ]] {Condition=function() if self.hasArmoury or self.hasBarracks then return true else return false end end, Level=1, Title='Upgr. Soldiers', Comment='Upgrades the Level of Soldiers.', Cost={Amount=15, Type=1}, Texture=self.I[2]..'#Male#Soldier#Improve.png', Dimensions={Width=34, Height=54}, Sail={Category=10, Ident=1, Nation=self.CurrentNation}},
	--[[ Upgrade Mechanics   ]] {Condition=function() return self.hasWorkshop end, Level=1, Title='Upgr. Mechanics', Comment='Upgrades the Level of Mechanics.', Cost={Amount=15, Type=1}, Texture=self.I[2]..'#Male#Mechanic#Improve.png', Dimensions={Width=25, Height=54}, Sail={Category=10, Ident=3, Nation=self.CurrentNation}},
	--[[ Upgrade Scientists  ]] {Condition=function() return true end, Level=1, Title='Upgr. Scientists', Comment='Upgrades the Level of Scientists.', Cost={Amount=15, Type=1}, Texture=self.I[2]..'#Male#Scientist#Improve.png', Dimensions={Width=25, Height=55}, Sail={Category=10, Ident=4, Nation=self.CurrentNation}},
	--[[ Upgrade Specialists ]] {Condition=function() return self.hasBarracks end, Level=1, Title='Upgr. '..self.I[3], Comment='Upgrades the Level of '..self.I[3]..'.', Cost={Amount=15, Type=1}, Texture=self.I[2]..'#Male#'..self.I[3]..'#Improve.png', Dimensions={Width=29, Height=55}, Sail={Category=10, Ident=self.I[4], Nation=self.CurrentNation}},

	--[[ Upgrade Slots  ]] {Condition=function() return true end, Title='Upgr. Slots', Comment='Grants x1 extra card slot.', Cost={Amount=35, Type=1}, Texture='Slots#Improve.png', Dimensions={Width=135, Height=103}, Sail={Category=10, Ident=20, Nation=self.CurrentNation}},
	--[[ Unlock Bunkers ]] {Condition=function() return self.hasWarehouse end, Title='Unlc. Bunkers', Comment='Unlocks basic bunkers.', Cost={Amount=50, Type=1}, Texture=self.I[2]..'#Bunker#Unlock.png', Dimensions={Width=76, Height=65}, Sail={Category=10, Ident=21, Nation=self.CurrentNation}},

	--[[ Add Soldiers     ]] {Condition=function() if self.hasArmoury or self.hasBarracks then return true else return false end end, Amount=1, Title='Add Soldier', Comment='Recruits x1 more Soldier.', Cost={Amount=30, Type=1}, Texture=self.I[2]..'#Male#Soldier#Add.png', Dimensions={Width=34, Height=54}, Sail={Category=10, Ident=22, Nation=self.CurrentNation}},
	--[[ Add Scientists   ]] {Condition=function() return true end, Amount=1, Title='Add Scientist', Comment='Recruits x1 more Scientist.', Cost={Amount=30, Type=1}, Texture=self.I[2]..'#Male#Scientist#Add.png', Dimensions={Width=25, Height=55}, Sail={Category=10, Ident=25, Nation=self.CurrentNation}},
	--[[ Add Specialists  ]] {Condition=function() return self.hasBarracks end, Amount=1, Title='Add '..self.I[3], Comment='Recruits x1 more '..self.I[3]..'.', Cost={Amount=30, Type=1}, Texture=self.I[2]..'#Male#'..self.I[3]..'#Add.png', Dimensions={Width=29, Height=55}, Sail={Category=10, Ident=26, Nation=self.CurrentNation}},

	--[[ Upgrade Depot      ]] {Condition=function() return true end, Title='Upgr. Depot', Comment='Increases resources and buildings limits & unlocks new researches.', Cost={Amount=25, Type=3}, Texture='b-'..self.I[1]..'-warehouse0000.png', Dimensions={Width=91, Height=87}, Sail={Category=10, Ident=27, Nation=self.CurrentNation}},
	--[[ Upgrade Workshop   ]] {Condition=function() return self.hasWorkshop end, Title='Upgr. Workshop', Comment='Unlocks vehicles construction.', Cost={Amount=15, Type=3}, Texture='UnlockVehicles.png', Dimensions={Width=1, Height=1}, Sail={Category=10, Ident=29, Nation=self.CurrentNation}},
	--[[ Upgrade B. Level   ]] {Condition=function() return self.hasWarehouse end, Title='Upgr. Bld. Level', Comment='Upgrades the level of all buildings.', Cost={Amount=5, Type=3}, Texture='UpgrBuildingLevel.png', Dimensions={Width=1, Height=1}, Sail={Category=10, Ident=30, Nation=self.CurrentNation}},
	--[[ Dismantle Building ]] {Condition=function() return true end, Title='Dismantle Building', Comment='Allows you to dismantle buildings of your choice.', Cost={Amount=50, Type=2}, Texture='Dismantle.png', Dimensions={Width=1, Height=1}, Sail={Category=10, Ident=31, Nation=self.CurrentNation}},
	--[[ Upgrade Armoury    ]] {Condition=function() if self.hasArmoury and self.hasWarehouse then return true else return false end end, Title='Upgr. Armoury', Comment='Unlocks new powerful offensive cards.', Cost={Amount=15, Type=3}, Texture='b-'..self.I[1]..'-barracks0000.png', Dimensions={Width=127, Height=118}, Sail={Category=10, Ident=32, Nation=self.CurrentNation}},
	--[[ Passive U. Gen.    ]] {Condition=function() return self.hasWarehouse end, Title='Passive Unit Gen.', Comment='Your buildings create units every few seconds.', Cost={Amount=50, Type=3}, Texture='PassiveGen.png', Dimensions={Width=1, Height=1}, Sail={Category=10, Ident=33, Nation=self.CurrentNation}},
	--[[ Power Lab. Ext.    ]] {Condition=function() return self.hasWarehouse end, Title='Power Lab.', Comment='Unlocks new Point & Click-type cards.', Cost={Amount=20, Type=3}, Texture='PassiveGen.png', Dimensions={Width=1, Height=1}, Sail={Category=10, Ident=34, Nation=self.CurrentNation}},
}

self.unlock = function(ID, Boolean)

    local function RemoveByTexture(Table, Texture)
        for k, v in ipairs(Table) do
            if v.Texture == Texture then
                table.remove(Table, k)
                break
            end
        end
    end

    local function ContainsTexture(Table, Texture)
        for _, v in ipairs(Table) do
            if v.Texture == Texture then
                return true
            end
        end
        return false
    end

    local function ClearCards(Texture)
    	if self.state == 1 then
    		return
    	end
	    for k, v in ipairs(self.slots) do
	    	if v ~= 0 then
	    		v = eReg:get(v)
	    		if v.imgtex == Texture then
	    			self.freeSlot(k)
	    		end
	    	end
	    end
	end

    local Data = self.POOL[self.CurrentNation]

    local is = {Depot=0, Warehouse=1, Workshop=2, Factory=3, Armoury=4, Barracks=5, Laboratory=6, SibMine=30, OilMine=29, Breastwork=31, Bunker=32}

	-- Limit of 1

	if self.state == 1 then
		if ID == 0 then
			if Boolean then
		        if not ContainsTexture(Data.HUMAN, self.Squads[6].Texture) then
					Notify.add(self.Squads[6], 'Unlocked: ', RGB(255,230,0))
					table.insert(Data.HUMAN, self.Squads[6])
				end
				if self.CurrentNation == 2 then
			        if not ContainsTexture(Data.HUMAN, self.Squads[7].Texture) then
						Notify.add(self.Squads[7], 'Unlocked: ', RGB(255,230,0))
						table.insert(Data.HUMAN, self.Squads[7])
					end
				end
			end
		end
		return
	end

	if ID == is.Factory then
		self.hasFactory = Boolean
	end

    if ID == is.Workshop then
    	self.toggleVehicleConstruction(Boolean)
        if Boolean then
            if not ContainsTexture(Data.HUMAN, self.Squads[3].Texture) then
            	Notify.add(self.Squads[3], 'Unlocked: ', RGB(255,230,0))
                table.insert(Data.HUMAN, self.Squads[3])
            end
            if ContainsTexture(Data.BUILDING, 'b-'..self.I[1]..'-workshop0000.png') then
                RemoveByTexture(Data.BUILDING, 'b-'..self.I[1]..'-workshop0000.png')
            end
            ClearCards('b-'..self.I[1]..'-workshop0000.png')
        else
            if not self.hasFactory then
	            if not ContainsTexture(Data.BUILDING, 'b-'..self.I[1]..'-workshop0000.png') then
	            	Notify.add(self.Buildings.Workshop, 'Unlocked: ', RGB(255,230,0))
	                table.insert(Data.BUILDING, self.Buildings.Workshop)
	            end
            	if ContainsTexture(Data.HUMAN, self.Squads[3].Texture) then
	                RemoveByTexture(Data.HUMAN, self.Squads[3].Texture)
	                Notify.add(self.Squads[3], 'Lost: ', RGB(255,100,100))
	            end
            ClearCards(self.Squads[3].Texture)
            end
        end
    end

    if ID == is.Laboratory then
    	self.hasLaboratory = Boolean
        if Boolean then
        	self.checkTechnologies()
            if not ContainsTexture(Data.HUMAN, self.Squads[4].Texture) then
                table.insert(Data.HUMAN, self.Squads[4])
                Notify.add(self.Squads[4], 'Unlocked: ', RGB(255,230,0))
            end
            if ContainsTexture(Data.BUILDING, 'b-'..self.I[1]..'-basiclab0000.png') then
                RemoveByTexture(Data.BUILDING, 'b-'..self.I[1]..'-basiclab0000.png')
            end
            ClearCards('b-'..self.I[1]..'-basiclab0000.png')
        else
            if not ContainsTexture(Data.BUILDING, 'b-'..self.I[1]..'-basiclab0000.png') then
            	Notify.add(self.Buildings.Laboratory, 'Unlocked: ', RGB(255,230,0))
                table.insert(Data.BUILDING, self.Buildings.Laboratory)
            end
            if ContainsTexture(Data.HUMAN, self.Squads[4].Texture) then
            	Notify.add(self.Squads[4], 'Lost: ', RGB(255,100,100))
                RemoveByTexture(Data.HUMAN, self.Squads[4].Texture)
            end
            ClearCards(self.Squads[4].Texture)
		    if self.hasTechnologicalCard then
		    	for k, v in ipairs(self.slots) do
			    	if v ~= 0 then
			    		v = eReg:get(v)
			    		if v.Sail.Category == 10 then
			    			self.freeSlot(k)
			    			self.hasTechnologicalCard = false
			    			break
			    		end
			    	end
			    end
			end
        end
    end

    if ID == is.Armoury then
    	self.hasArmoury = Boolean
        if Boolean then
        	if self.CurrentNation ~= 1 then
	            if not ContainsTexture(Data.HUMAN, self.Squads[8].Texture) then
	                table.insert(Data.HUMAN, self.Squads[8])
	                Notify.add(self.Squads[8], 'Unlocked: ', RGB(255,230,0))
	            end
        	end
            if not ContainsTexture(Data.HUMAN, self.Squads[1].Texture) then
                table.insert(Data.HUMAN, self.Squads[1])
                Notify.add(self.Squads[1], 'Unlocked: ', RGB(255,230,0))
            end
            if ContainsTexture(Data.BUILDING, 'b-'..self.I[1]..'-armoury0000.png') then
                RemoveByTexture(Data.BUILDING, 'b-'..self.I[1]..'-armoury0000.png')
            end
            ClearCards('b-'..self.I[1]..'-armoury0000.png')
        else
        	if not self.hasBarracks then
	            if not ContainsTexture(Data.BUILDING, 'b-'..self.I[1]..'-armoury0000.png') then
	                table.insert(Data.BUILDING, self.Buildings.Armoury)
	            	Notify.add(self.Buildings.Armoury, 'Unlocked: ', RGB(255,230,0))
	            end
	            if self.CurrentNation ~= 1 then
		            if ContainsTexture(Data.HUMAN, self.Squads[8].Texture) then
		                RemoveByTexture(Data.HUMAN, self.Squads[8].Texture)
		                Notify.add(self.Squads[8], 'Lost: ', RGB(255,100,100))
					end
					ClearCards(self.Squads[8].Texture)
				end
	            if ContainsTexture(Data.HUMAN, self.Squads[1].Texture) then
	                RemoveByTexture(Data.HUMAN, self.Squads[1].Texture)
	                Notify.add(self.Squads[1], 'Lost: ', RGB(255,100,100))
				end
				ClearCards('b-'..self.I[1]..'-barracks0000.png')
				ClearCards(self.Squads[1].Texture)
			end
        end
    end

--{Title='Arthur Powell', Comment='Spawns a team of soldiers before comitting to a heroic attack.', Cost={Amount=0, Type=1}, Texture='ArthurPowellXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=1}},
--{Title='Hugh Stevens', Comment='', Cost={Amount=0, Type=1}, Texture='HuckStevensXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=1}},
--{Title='Ron Harrison', Comment='', Cost={Amount=0, Type=1}, Texture='RonHarrisonXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=1}},
--{Title='John Macmillan', Comment='', Cost={Amount=0, Type=1}, Texture='JohnMacmillanXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=1}},
--{Title='Jeff Brown', Comment='', Cost={Amount=0, Type=1}, Texture='JeffBrownXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=1}},
--{Title='Tim Gladstone', Comment='Spawns a group of incredible apemen.', Cost={Amount=0, Type=1}, Texture='TimGladstoneXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=1}},
--{Title='Lisa Lawson', Comment='Spawns on map with an upgraded machine gun.', Cost={Amount=0, Type=1}, Texture='LisaLawsonXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=1}}

--{Title='Abdul Shariff', Comment='Calls in a swarm of troops.', Cost={Amount=0, Type=1}, Texture='AbdulShariffXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=2}},
--{Title='Dietrich Gensher', Comment='', Cost={Amount=0, Type=1}, Texture='DietrichGensherXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=2}},
--{Title='Robert Farmer', Comment='', Cost={Amount=0, Type=1}, Texture='RobertFarmerXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=2}},
--{Title='Heike Steyer', Comment='', Cost={Amount=0, Type=1}, Texture='HeikeSteyerXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=2}},
--{Title='Kurt Schmidt', Comment='', Cost={Amount=0, Type=1}, Texture='KurtSchmidtXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=2}},
--{Title='Sheikh Omar', Comment='', Cost={Amount=0, Type=1}, Texture='SheikhOmarXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=2}},
--{Title='Sheherezade', Comment='Sheherezade provides passive healing to nearby allies.', Cost={Amount=0, Type=1}, Texture='SheherezadeXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=2}},
--{Title='Raul Xavier', Comment='', Cost={Amount=0, Type=1}, Texture='RaulXavierXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=2}}

--{Title='Burlak Gorky', Comment='', Cost={Amount=0, Type=1}, Texture='BurlakGorkyXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=3}},
--{Title='Major Platonov', Comment='', Cost={Amount=0, Type=1}, Texture='MajorPlatonovXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=3}},
--{Title='Sergey I. Popov', Comment='', Cost={Amount=0, Type=1}, Texture='SergeyPopovXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=3}},
--{Title='Marshal Yashin', Comment='', Cost={Amount=0, Type=1}, Texture='MarshalYashinXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=3}},
--{Title='Ivan Kurin', Comment='', Cost={Amount=0, Type=1}, Texture='IvanKurinXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=3}},
--{Title='Grishko', Comment='', Cost={Amount=0, Type=1}, Texture='GrishkoXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=3}},
--{Title='P. M. Belkov', Comment='', Cost={Amount=0, Type=1}, Texture='BelkovXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=3}},
--{Title='Petrovova', Comment='', Cost={Amount=0, Type=1}, Texture='PetrovovaXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=3}},
--{Title='Eisenstein', Comment='', Cost={Amount=0, Type=1}, Texture='EisensteinXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=3}},
--{Title='Tsarytsyn', Comment='', Cost={Amount=0, Type=1}, Texture='TsarytsynXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=0, Nation=3}}

    if ID == is.Barracks then
    	self.hasBarracks = Boolean
        if Boolean then
        	if self.CurrentNation == 2 then
	            if not ContainsTexture(Data.HUMAN, self.Squads[9].Texture) then
	            	table.insert(Data.HUMAN, self.Squads[9])
	            end
        	end
            if not ContainsTexture(Data.HUMAN, self.Squads[5].Texture) then
                table.insert(Data.HUMAN, self.Squads[5])
                Notify.add(self.Squads[5], 'Unlocked: ', RGB(255,230,0))
            end
            if ContainsTexture(Data.BUILDING, 'b-'..self.I[1]..'-barracks0000.png') then
                RemoveByTexture(Data.BUILDING, 'b-'..self.I[1]..'-barracks0000.png')
            end
            ClearCards('b-'..self.I[1]..'-barracks0000.png')
            if self.CurrentNation == 1 then
	            if not ContainsTexture(Data.HERO, 'PeterRothXicht.png') then
	            	table.insert(Data.HERO, {Title='Peter Roth', Comment='Converts enemies to your side.', Cost={Amount=0, Type=1}, Texture='PeterRothXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=1, Nation=1}})
	            end
	            if not ContainsTexture(Data.HERO, 'FrankForsythXicht.png') then
	            	table.insert(Data.HERO, {Title='Frank Forsyth', Comment='Spawns on map with an upgraded sniper rifle.', Cost={Amount=0, Type=1}, Texture='FrankForsythXicht.png', Dimensions={Width=120, Height=150}, Sail={Category=2, Ident=2, Nation=1}})
	            end
	        end
        else
        	if self.CurrentNation == 1 then
				if ContainsTexture(Data.HERO, 'FrankForsythXicht.png') then
					RemoveByTexture(Data.HERO, 'FrankForsythXicht.png')
				end
				ClearCards('FrankForsythXicht.png')
				if ContainsTexture(Data.HERO, 'PeterRothXicht.png') then
					RemoveByTexture(Data.HERO, 'PeterRothXicht.png')
				end
				ClearCards('PeterRothXicht.png')
			end
			if self.CurrentNation == 2 then
				if ContainsTexture(Data.HERO, self.Squads[9].Texture) then
					RemoveByTexture(Data.HERO, self.Squads[9].Texture)
				end
				ClearCards(self.Squads[9].Texture)
			end
	        if ContainsTexture(Data.HUMAN, self.Squads[5].Texture) then
	            RemoveByTexture(Data.HUMAN, self.Squads[5].Texture)
	            Notify.add(self.Squads[5], 'Lost: ', RGB(255,100,100))
			end
			ClearCards(self.Squads[5].Texture)
        	--[[if self.hasArmoury then
	            if not ContainsTexture(Data.BUILDING, 'b-'..self.I[1]..'-barracks0000.png') then
	                table.insert(Data.BUILDING, {Title='Barracks', Comment='Unlocks powerful Heroes cards.', Cost={Amount=75, Type=1}, Texture='b-'..self.I[1]..'-barracks0000.png', Dimensions={Width=76, Height=65}, Sail={Category=0, Ident=5, Nation=self.CurrentNation}})
	            end
	        else
	            if ContainsTexture(Data.BUILDING, 'b-'..self.I[1]..'-barracks0000.png') then
	                RemoveByTexture(Data.BUILDING, 'b-'..self.I[1]..'-barracks0000.png')
				end
	        end--]]
        end
    end

-- {'Power Plant', 35, 'b-u-oilpower0000.png', 76, 92, {Category=1, ListPOS=6, Nation=1, Btype=26}, 'Allows the placement of an oil-powered generator.'},

	if ID == is.Warehouse then
		self.hasWarehouse = Boolean
		if Boolean then
            if ContainsTexture(Data.TECHNOLOGY, 'b-'..self.I[1]..'-warehouse0000.png') then
                RemoveByTexture(Data.TECHNOLOGY, 'b-'..self.I[1]..'-warehouse0000.png')
            end
            ClearCards('b-'..self.I[1]..'-warehouse0000.png')
        else
            if not ContainsTexture(Data.TECHNOLOGY, 'b-'..self.I[1]..'-warehouse0000.png') then
                --table.insert(Data.TECHNOLOGY, self.Technologies.UnlockWarehouse)
            	--Notify.add(self.Technologies.UnlockWarehouse, 'Unlocked: ', RGB(255,230,0))
            end
        end
	end

    -- Limit of 2

    if ID == is.Breastwork then
        if Boolean then
            if ContainsTexture(Data.BUILDING, 'b-'..self.I[1]..'-breastworks0000.png') then
                RemoveByTexture(Data.BUILDING, 'b-'..self.I[1]..'-breastworks0000.png')
            end
            ClearCards('b-'..self.I[1]..'-breastworks0000.png')
        else
            if not ContainsTexture(Data.BUILDING, 'b-'..self.I[1]..'-breastworks0000.png') then
                table.insert(Data.BUILDING, self.Buildings.Breastwork)
            	Notify.add(self.Buildings.Breastwork, 'Unlocked: ', RGB(255,230,0))
            end
        end
    end

    if ID == is.Bunker then
        if Boolean then
            if ContainsTexture(Data.BUILDING, 'b-'..self.I[1]..'-bunker0000.png') then
                RemoveByTexture(Data.BUILDING, 'b-'..self.I[1]..'-bunker0000.png')
            end
            ClearCards('b-'..self.I[1]..'-bunker0000.png')
        else
        	if self.canSpawnBunker then
	            if not ContainsTexture(Data.BUILDING, 'b-'..self.I[1]..'-bunker0000.png') then
	                table.insert(Data.BUILDING, self.Buildings.Bunker)
	            	Notify.add(self.Buildings.Bunker, 'Unlocked: ', RGB(255,230,0))
	            end
	        end
        end
    end

    if ID == is.OilMine then
        if Boolean then
            if ContainsTexture(Data.BUILDING, 'b-'..self.I[1]..'-oilmine0000.png') then
                RemoveByTexture(Data.BUILDING, 'b-'..self.I[1]..'-oilmine0000.png')
            end
            ClearCards('b-'..self.I[1]..'-oilmine0000.png')
        else
            if not ContainsTexture(Data.BUILDING, 'b-'..self.I[1]..'-oilmine0000.png') then
                table.insert(Data.BUILDING, self.Buildings.Derrick)
            	Notify.add(self.Buildings.Derrick, 'Unlocked: ', RGB(255,230,0))
            end
        end
    end

    if ID == is.SibMine then
        if Boolean then
            if ContainsTexture(Data.BUILDING, 'b-'..self.I[1]..'-siberiummine0000.png') then
                RemoveByTexture(Data.BUILDING, 'b-'..self.I[1]..'-siberiummine0000.png')
            end
            ClearCards('b-'..self.I[1]..'-siberiummine0000.png')
        else
            if not ContainsTexture(Data.BUILDING, 'b-'..self.I[1]..'-siberiummine0000.png') then
                table.insert(Data.BUILDING, self.Buildings.SiberiumMine)
            	Notify.add(self.Buildings.SiberiumMine, 'Unlocked: ', RGB(255,230,0))
            end
        end
    end
end

self.UpgradeSquads = function(Class, Slot)
	
	local function GetData(Class)
		local Sail_Classes = {1, 2, 3, 4, 5, 0, 0, 5, 5, 0, 7, 6, 0, -1, 8, -1, 9}
		local Cost = {5, 5, 5, 15, 15, 1, 10, 3, 3}
		return {Class=Sail_Classes[Class], Cost=Cost[Sail_Classes[Class]]}
	end

	local Data = GetData(Class)

	self.Squads[Data.Class].Amount = self.Squads[Data.Class].Amount +1
	self.Squads[Data.Class].Sail.Amount = self.Squads[Data.Class].Amount
	self.Squads[Data.Class].Cost.Amount = self.Squads[Data.Class].Cost.Amount + Data.Cost

    for k, v in ipairs(self.slots) do
    	if v ~= 0 then 
    		v = eReg:get(v)
    		if #v.Sail == 2 then
    			-- TODO
    		else
	    		if v.Sail.Category == 1 and v.Sail.Ident == Class then
	    			v.Cost.Amount = self.Squads[Data.Class].Cost.Amount
	    			v.Sail.Amount = self.Squads[Data.Class].Sail.Amount
	    			setText(v.amountLabel, 'x'..v.Sail.Amount)
	    			setText(v.Price, 'x'..v.Cost.Amount)
	    		end
	    	end
    	end
    end

	self.freeSlot(Slot)
	self.hasTechnologicalCard = false
end

self.UpgradeLevel = function(Slot)
	local function returnInfo()
	    for k, v in ipairs(self.slots) do
	    	if v ~= 0 then 
	    		v = eReg:get(v)
	    		if v.Sail.Category == 10 then
	    			return v.Sail.Ident
	    		end
	    	end
	    end
	end

	local Unit = returnInfo()

	if Unit > 4 then
		Unit = 5
	end

    self.Squads[Unit].Level = self.Squads[Unit].Level +1
    self.Squads[Unit].Sail.Level = self.Squads[Unit].Level
    self.Squads[Unit].Cost.Amount = self.Squads[Unit].Cost.Amount + 2

    for k, v in ipairs(self.POOL[self.CurrentNation].TECHNOLOGY) do
    	if v.Sail.Ident == returnInfo() then
    		v.Cost.Amount = v.Cost.Amount +10
    		break
    	end
    end

    for k, v in ipairs(self.slots) do
    	if self.slots ~= 0 then
    		v = eReg:get(v)
    		if #v.Sail == 2 then
    			-- TODO
    		else
	    		if v.Sail.Category == 1 then
	    			if v.Sail.Ident == returnInfo() then
	    				v.Sail.Level = self.Squads[Unit].Sail.Level
	    				setText(v.levelLabel, v.Sail.Level)
	    				v.Cost.Amount = self.Squads[Unit].Cost.Amount
	    				setText(v.Price, 'x'..v.Cost.Amount)
	    			end
	    		end
    		end
    	end
    end

    self.freeSlot(Slot)
    self.hasTechnologicalCard = false
end

self.addSlot = function(Slot)

	table.insert(self.slots, 0)

	local Deck = self.POOL[self.CurrentNation].TECHNOLOGY

    for k, v in ipairs(Deck) do
    	if v.Texture == 'Slots#Improve.png' then
    		if #self.slots > 7 then
		    	self.canUnlockSlots = false
				table.remove(Deck, k)
				break
			else
				v.Cost.Amount = v.Cost.Amount *2
				break
			end
    	end
    end

	self.deleteCards[Slot+1] = getImageButtonEX(nil, anchorNone, XYWH(80+160*(Slot), getY(self.board)-getHeight(self.header)+15, 125, 10), 'Discard', '', 'if getVisible(shader.MOUSEOVER) == false then OW_CUSTOM_COMMAND(2, 1, 10, 2, '..(Slot+1)..') end', SKINTYPE_BUTTON, {hint = 'Discard card (x10 oil)'})
	ApplySkinToElement(self.deleteCards[Slot+1])
	setFontColour(self.deleteCards[Slot+1],RGB(255,125,125))
	self.hasTechnologicalCard = false
	
	self.freeSlot(Slot)
	self.createCard(#self.slots, nil)
end

self.unlockBunkers = function(Slot)
	self.canSpawnBunker = true
	for k, v in ipairs(self.POOL[self.CurrentNation].TECHNOLOGY) do
		if v.Sail.Ident == 21 then
			table.remove(self.POOL[self.CurrentNation].TECHNOLOGY, k)
		end
	end
	for k, v in ipairs(self.Technologies) do
		if v.Title == 'Unlc. Bunker' then
			Notify.add(v, 'Complete: ', RGB(100,255,100))
		end
	end
	self.freeSlot(Slot)
end

self.upgradeDepot = function(Slot)
	self.freeSlot(Slot)
end

self.checkTechnologies = function()
    local function ContainsTexture(Table, Texture)
        for _, v in ipairs(Table) do
            if v.Texture == Texture then
                return true
            end
        end
        return false
    end

	for k, v in ipairs(self.Technologies) do
	    local Checker = false
	    if not ContainsTexture(self.POOL[self.CurrentNation].TECHNOLOGY, v.Texture) then
	        if v.Condition() then
	            AddSingleUseTimer(1+k/2, string.format("cards.NotifyTechnology(%d)", k))
	            table.insert(self.POOL[self.CurrentNation].TECHNOLOGY, v)
	        end
	    end
	end
end

self.NotifyTechnology = function(index)
    local v = self.Technologies[index]
    Notify.add(v, 'Unlocked: ', RGB(255, 230, 0))
end

self.Vehicles = 
{
	{
		-- Tier 1
		{
			Chassis = 
			{
				{ID='01', Title='Light Wheels'},
				{ID='02', Title='Medium Wheels'}
			},
			Weapon =
			{
				{ID='03', Title='Light Gun'},
				{ID='02', Title='Machine Gun'}
			}
		},

		-- Tier 2
		{
			Chassis =
			{
				{Title='Medium Tracks',Dimensions={Width=94,Height=100},Texture='p-u-mediumt'},
				{Title='Heavy Tracks',Dimensions={Width=148,Height=142},Texture='p-u-heavyt',Weapon={X=44,Y=42},Wheels={{T='wh-u-heavytrack',X=57,Y=44,W=80,H=76},{T='wh-u-heavytrack',X=57,Y=81,W=80,H=76}}},
			},
			Weapon =
			{
				{Title='Rocket Launcher',Dimensions={Width=46,Height=64},Texture='w-u-rocket'},
				{Title='Laser',Dimensions={Width=46,Height=62},Texture='w-u-las'},
				{Title='Gatling Gun',Dimensions={Width=48,Height=48},Texture='w-u-gg'},
				{Title='Double Canon',Dimensions={Width=80,Height=78},Texture='w-u-double'},
			}
		},

		-- Tier 3
		{
			Chassis =
			{
				{Title='Morphling',Dimensions={Width=142,Height=138},Texture='p-u-morphling'},
			},
			Weapon =
			{
				{Title='Double Laser',Dimensions={Width=40,Height=46},Texture='w-u-dlas'},
				{Title='Heavy Gun',Dimensions={Width=96,Height=92},Texture='w-u-heavy'},
				{Title='Siberite Rocket',Dimensions={Width=106,Height=96},Texture='w-u-srock'},
			}
		}

	}

}

self.canConstructVehicles = false
self.tierVehicles = 1

self.toggleVehicleConstruction = function(Boolean)
	if Boolean ~= self.canConstructVehicles then
		self.canConstructVehicles = Boolean
	else
		return
	end

	if not self.hasFactory then
		Notify.add({Dimensions={Width=1,Height=1}, Title='Simple vehicles', Texture='_'}, 'Unlocked: ', RGB(255,230,0))
	else
		Notify.add({Dimensions={Width=1,Height=1}, Title='Complex vehicles', Texture='_'}, 'Unlocked: ', RGB(255,230,0))
	end

	local getPart = function(Type)
		local Value = self.Vehicles[self.CurrentNation][self.tierVehicles]
		local Lists = {Value.Chassis, Value.Weapon}
		return Lists[Type][math.random(1,#Lists[Type])]
	end

	local CardSlot = #self.slots +1

	local Element = getElementEX(self.board, anchorNone, XYWH((-1 + CardSlot) * 160, 300, 160, 192), true, {Cost={Crates=0,Oil=0,Siberite=0}, texture='SGUI/Cards/skins/Cardfront_'..self.CurrentNation..'_0.png'})
	Element.Title = getLabelEX(Element, anchorNone, XYWH(0, 15, 160, 0), Tahoma_14B, 'Workshop', {nomouseevent=true, font_colour=SIDE_COLOURS[2], shadowtext=true, text_halign=ALIGN_MIDDLE})
	Element.Description = getLabelEX(Element, anchorNone, XYWH(17.5, 118, 130, 55), Tahoma_12B, 'Design & construct your vehicles here.', {nomouseevent=true, font_colour=SIDE_COLOURS[2], shadowtext=true, font_style_outline=true, wordwrap=true, text_halign=ALIGN_MIDDLE})

	local Chassis = getPart(1)
	local Engine = '01'
	local Control = '01'
	local Weapon = getPart(2)

	getElementEX(Element, anchorNone, XYWH(getWidth(Element)/2-75, 0, 150, 120), true, {texture='Faces/vehimg-u-'..Chassis.ID..'-'..Engine..'-'..Control..'-'..Weapon.ID..'.png'})

	--"if getVisible(shader.MOUSEOVER) == false then cards.toggleRotate(" .. card.BACK.ID .. ", true) end")
	--sgui_setcallback(card.BACK.ID, CALLBACK_MOUSELEAVE, "AddEventSlideY(" .. card.BACK.ID .. ", 15, 0.25, '')")
	--sgui_setcallback(card.BACK.ID, CALLBACK_MOUSEOVER, "AddEventSlideY(" .. card.BACK.ID .. ", -50, 0.25, '') setText(cards.header.label, '" .. getText(card.DES) .. "') setFontColour(cards.header.label, sgui_get(" .. card.DES.ID .. ", PROP_FONT_COL))")

	AddEventSlideY(Element.ID, 15, math.random(1, 2))
end

cards.toggleVehicleConstruction(false)