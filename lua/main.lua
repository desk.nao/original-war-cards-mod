local cardsClass = function()
	local self = 
	{
		ANIM_LIST={},
		POOL={},
		DECK={},

		X=0,
		CurrentNation=1,
		state=1,

		Currency={0,0,0},
		
		InfoPlayers={GENERAL={},DEPOT={}},

		canUnlockSlots=true,

		hasTechnologicalCard=false,
		currentResearch=0,

		slots={0},
		deleteCards={0,0,0,0,0,0,0,0,0,0,0,0,0}
	}

	self.maxDimensions = {
	    [1] = {width = 110, height = 66},
	    [2] = {width = 85, height = 51},
	    [3] = {width = 85, height = 51}
	}

	self.categorySettings = {
	    [1] = {key = "AM", colorIndex = 2, yOffset = {title = 15, comment = 97.5}},
	    [2] = {key = "AR", colorIndex = 3, yOffset = {title = 22.5, comment = 88.5}},
	    [3] = {key = "RU", colorIndex = 4, yOffset = {title = 15, comment = 90}}
	}

	self.effects = {
		{
			[1] = {
				{14, -11, 32, 35, 'lasseg_end.png'},
				{113, -11, 32, 35, 'lasseg_end.png'},
				{3, 105, 19, 57, 'a-u-trackext_1.png'},
				{138, 105, 19, 57, 'a-u-trackext_1.png'}
			},
			[2] = {
				{108, 79, 44, 48, 'lasseg_end_green.png'},
				{108, 79, 44, 48, 'lasseg_end_green.png'}
			},
			[3] = {
				{147, 15, 20, 48, 'US_LaserExt_0d.png'},
				{-6, 15, 20, 48, 'US_LaserExt_2d.png'}
			}
		},
		{
			[1] = {
				{139, 74, 26, 43, 'n-a-radar.png'}
			},
			[2] = {
				{-3, 90, 36, 56, 'a-a-solarpower.png'}
			},
			[3] = {
				{139, 29, 16, 24, 'c-a-hovercraft-rem.png'},
				{-1, 22, 29, 72, 'n-a-control.png'}
			}
		},
		{
			[1] = {
				{3, 41, 58, 95, 'RU_SiberExt_4_20000.png'}
			},
			[2] = {
				{3, 41, 58, 95, 'RU_SiberExt_4_20000.png'},
				{18, 57, 27, 86, 'Kour_siber.png'},
				{18, 169, 22, 106, 'a-r-trackext_1.png'},
				{116, 169, 22, 106, 'a-r-trackext_1.png'}
			},
			[3] = {
				{121, -14, 52, 100, 'RU_SiberExt_1d_2.png'}
			}
		}
	}

	self.initCards = function()
		self.POOL = { 

			{
				START =
				{
					{Title='Depot', Comment='Place your depot, and defend it at all costs!', Cost={Amount=0, Type=1}, Texture='b-u-depot0001.png', Dimensions={Width=164, Height=92}, Sail={Category=0, Ident=0, Nation=1}	},
					{Title='Breastwork', Comment='Breastworks will be occupied by idle soldiers.', Cost={Amount=0, Type=1}, Texture='b-u-breastworks0000.png', Dimensions={Width=91,	Height=87},		Sail={Category=0, Ident=31, Nation=1, Amount=1, Level=1}	},
					{Title='Lvl. 1 Soldier', Comment='Soldiers defend the base and perform assaults.', Cost={Amount=0, Type=1}, Texture='US#Male#Soldier.png', Dimensions={Width=34,	Height=54},		Sail={Category=1, Ident=1, Nation=1, Amount=1, Level=1}	},
					{Title='Derrick', Comment='Oil is used to discard unwanted cards.', Cost={Amount=0, Type=1}, Texture='b-u-oilmine0000.png', Dimensions={Width=84, Height=69},		Sail={Category=0, Ident=29, Nation=1}	}
				},
				BUILDING = {},
				HUMAN = {},
				HERO = {},
				TECHNOLOGY = {}
			},

			{
				START = 
				{
					{Title='Depot', Comment='Place your depot, and defend it at all costs!', Cost={Amount=0, Type=1}, Texture='b-a-depot0001.png', Dimensions={Width=133, Height=124}, Sail={Category=0, Ident=0, Nation=2}	},
					{Title='Breastwork', Comment='Breastworks will be occupied by idle soldiers.', Cost={Amount=0, Type=1}, Texture='b-a-breastworks0000.png', Dimensions={Width=91, Height=87}, Sail={Category=0, Ident=31, Nation=2, Amount=1, Level=1} },
					{Title='Lvl. 1 Soldier', Comment='Soldiers defend the base and perform assaults.', Cost={Amount=0, Type=1}, Texture='AR#Male#Soldier.png', Dimensions={Width=34, Height=54}, Sail={Category=1, Ident=1, Nation=2, Amount=1, Level=1} },
					{Title='Derrick', Comment='Oil is used to discard unwanted cards.', Cost={Amount=0, Type=1}, Texture='b-a-oilmine0000.png', Dimensions={Width=84, Height=69}, Sail={Category=0, Ident=29, Nation=2}	}
				},
				BUILDING = {},
				HUMAN = {},
				HERO = {},
				TECHNOLOGY = {}
			},

			{
				START = 
				{
					{Title='Depot', Comment='Place your depot, and defend it at all costs!', Cost={Amount=0, Type=1}, Texture='b-r-depot0001.png', Dimensions={Width=143, Height=120}, Sail={Category=0, Ident=0, Nation=3}	},
					{Title='Breastwork', Comment='Breastworks will be occupied by idle soldiers.', Cost={Amount=0, Type=1}, Texture='b-r-breastworks0000.png', Dimensions={Width=84,	Height=81},		Sail={Category=0, Ident=31, Nation=3, Amount=1, Level=1}	},
					{Title='Lvl. 1 Soldier', Comment='Soldiers defend the base and perform assaults.', Cost={Amount=0, Type=1}, Texture='RU#Male#Soldier.png', Dimensions={Width=34,	Height=54},		Sail={Category=1, Ident=1, Nation=3, Amount=1, Level=1}	},
					{Title='Derrick', Comment='Oil is used to discard unwanted cards.', Cost={Amount=0, Type=1}, Texture='b-r-oilmine0000.png', Dimensions={Width=87,	Height=91},		Sail={Category=0, Ident=29, Nation=3}	}
				},
				BUILDING = {},
				HUMAN = {},
				HERO = {},
				TECHNOLOGY = {}
			}
		}
	end

	self.click = function(ID)
		shader.Change(ID)
	end

	self.removeCallback = function(Slot)
		local elem = eReg:get(self.slots[Slot])
		sgui_setcallback(elem.ID, CALLBACK_MOUSECLICK, '')
	end

	self.spawnHuman = function(Class, Nation)
	 	OW_CUSTOM_COMMAND(100, 101, Nation, Class)
	end

	self.spawnHero = function(Slot)
	 	OW_CUSTOM_COMMAND(100, 104, 110 + self.hero)
	 	self.freeSlot(Slot)
	end

	self.exitStarter = function()
		local suffix
		self.state = 100
		if interface.current.side == interface.amer.side then
			suffix = 'u'
		elseif interface.current.side == interface.rus.side then
			suffix = 'r'
		elseif interface.current.side == interface.arab.side then
			suffix = 'a'
		end
		for Slot = 1, 4 do 
			self.deleteCards[Slot] = getImageButtonEX(nil, anchorNone, XYWH(-50+80+160*(Slot-1), getY(self.board)-getHeight(self.header)+15, 100, 10), 'Discard', '', 'if getVisible(shader.MOUSEOVER) == false then OW_CUSTOM_COMMAND(2, 1, 10, 2, '..(Slot)..') end', SKINTYPE_BUTTON, {hint = 'Discard card (x10 oil)'})
			ApplySkinToElement(self.deleteCards[Slot])
			setFontColour(self.deleteCards[Slot],RGB(255,125,125))
			self.createCard(Slot, nil)
		end
	end

	self.freeSlot = function(Slot)
		if self.slots[Slot] == 0 then
			return
		end

		local ELEMENT = eReg:get(self.slots[Slot])

		if ELEMENT.isResearch then
			OW_CUSTOM_COMMAND(11)
			self.hasTechnologicalCard = false
		end

		table.remove(self.ANIM_LIST, ELEMENT.ID)
		sgui_delete(ELEMENT.ID)
		self.slots[Slot] = 0
		if self.state == 100 then
			AddSingleUseTimer(math.random(0.1,0.9),'cards.createCard('..Slot..', nil)')
		end
	end

	self.createCard = function(Slot, Card)
	    local DECK, DATA
	    local nationPool = self.POOL[self.CurrentNation]

	    -- Handle technological cards
	    if self.hasLaboratory and not self.hasTechnologicalCard then
	        if math.random(3) == 1 then
	            self.hasTechnologicalCard = true
	            DECK = nationPool.TECHNOLOGY
	            Card = math.random(1, #DECK)
	            DATA = DECK[Card]
	            goto Proceed
	        end
	    end

	    -- Handle start and general cards
	    if self.state == 1 then
	        DATA = nationPool.START[Card]
	    else
	        local list = {}
	        if #nationPool.HUMAN > 0 then
	        	table.insert(list, nationPool.HUMAN)
	        end
	        if #nationPool.BUILDING > 0 then
	        	table.insert(list, nationPool.BUILDING)
	        end
	        if #nationPool.HERO > 0 then
	        	table.insert(list, nationPool.HERO)
	        end
	        DECK = list[math.random(1, #list)]
	        Card = math.random(1, #DECK)
	        DATA = DECK[Card]
	    end

	    ::Proceed::

	    -- Card dimensions and rendering
	    local S, D = self.categorySettings[self.CurrentNation], self.maxDimensions[self.CurrentNation]
	    local w, h = self.scaleDimensions(DATA.Dimensions.Width, DATA.Dimensions.Height, D.width, D.height)
	    w, h = math.min(w, DATA.Dimensions.Width), math.min(h, DATA.Dimensions.Height)

	    -- Create and set up card elements

	    local _PriceTexture = {'crate', 'oil', 'sib'}
	    local Cost = DATA.Cost

	    local Element = getElementEX(self.board, anchorNone, XYWH((Slot-1)*160, 300, 160, 192), true, {Slot=Slot, Cost=Cost, Sail=DATA.Sail, Angle=0, texture='SGUI/Cards/skins/Cardback_'..self.CurrentNation..'.png', imgtex=DATA.Texture})
	    Element.Front = getElementEX(Element, anchorNone, XYWH(0, 0, 160, 192), true, {Angle=0, texture='SGUI/Cards/skins/Cardfront_'..self.CurrentNation..'_'..DATA.Sail.Category..'.png', nomouseevent=true})
	    Element.FrontAnim = getElementEX(Element.Front, anchorNone, XYWH(0, 0, 160, 192), true, {texture='SGUI/Cards/skins/Cardfront_'..self.CurrentNation..'_anim.png', nomouseevent=true})
	    Element.Image = getElementEX(Element.Front, anchorNone, XYWH(160/2-w/2, 32.5, w, h), true, {texture='SGUI/Cards/top_entities/'..DATA.Texture, nomouseevent=true})
	    Element.Title = getLabelEX(Element.Front, anchorNone, XYWH(0, S.yOffset.title, 160, 0), Tahoma_14B, DATA.Title, {nomouseevent=true, font_colour=SIDE_COLOURS[S.colorIndex], shadowtext=true, text_halign=ALIGN_MIDDLE})
	    Element.Price = getLabelEX(Element.Front, anchorNone, XYWH(30, S.yOffset.comment, 92.5, 0), Tahoma_12B, 'x'..DATA.Cost.Amount, {nomouseevent=true, font_colour=SIDE_COLOURS[S.colorIndex], shadowtext=true, font_style_outline=true, text_halign=ALIGN_RIGHT})
	    Element.Description = getLabelEX(Element.Front, anchorNone, XYWH(17.5, 118, 130, 55), Tahoma_12B, DATA.Comment, {colour1=BLACKA(125), nomouseevent=true, font_colour=SIDE_COLOURS[S.colorIndex], shadowtext=true, font_style_outline=true, wordwrap=true, text_halign=ALIGN_MIDDLE})
	    Element.PriceIcon = getElementEX(Element.Front, anchorNone, XYWH(160-45, getY(Element.Price)-5, 25, 25), true, {texture='SGUI/Cards/'.._PriceTexture[DATA.Cost.Type]..'.png', nomouseevent=true})

	    if self.state == 100 and DATA.Sail.Category == 1 then
	    	Element.levelIcon = getElementEX(Element.Front, anchorNone, XYWH(20, 20+25, 13, 12), true, {texture='SGUI/Cards/Level.png', nomouseevent=true})
			Element.levelLabel = getLabelEX(Element.levelIcon, anchorLT, XYWH(13+2.5, 0, 0, 12), Tahoma_10, (DATA.Level or '1'), {nomouseevent=true, font_colour=SIDE_COLOURS[S.colorIndex], shadowtext=true, text_valign=ALIGN_MIDDLE, text_halign=ALIGN_LEFT})
	    	Element.amountIcon = getElementEX(Element.Front, anchorNone, XYWH(20, 20+40, 18, 17), true, {texture='SGUI/Cards/Units.png', nomouseevent=true})
	    	Element.amountLabel = getLabelEX(Element.amountIcon, anchorNone, XYWH(18+2.5, 0, 0, 17), Tahoma_10, 'x' .. (DATA.Amount or '1'), {nomouseevent=true, font_colour=SIDE_COLOURS[S.colorIndex], shadowtext=true, text_valign=ALIGN_MIDDLE, text_halign=ALIGN_LEFT})
		end

	    if DATA.Sail.Category == 10 then
	    	Element.isResearch = true
	    else
	    	Element.isResearch = false
	    end

		if DATA.Sail.Category ~= 1 or self.CurrentNation == 2 then
			setColour1(Element.FrontAnim, WHITEA(0))
	    end

	    setRotation(Element.Front, SetRotate(1, 180, false))

	    self.slots[Slot] = Element.ID

	    -- Callbacks and animations
	    sgui_setcallback(Element.ID, CALLBACK_MOUSECLICK, "if getVisible(shader.MOUSEOVER) == false then cards.toggleRotate(" .. Element.ID .. ") end")
	    sgui_setcallback(Element.ID, CALLBACK_MOUSELEAVE, "AddEventSlideY(" .. Element.ID .. ", 15, 0.25, '')")
	    sgui_setcallback(Element.ID, CALLBACK_MOUSEOVER, 'cards.MouseOver('..Element.ID..')')

	    AddEventSlideY(Element.ID, 15, math.random(0.5, 0.95))

	    return Element
	end

	return self
end

Minimap = getElementEX(nil, anchorNone, XYWH(0, 0, 0, 0), true, {Notifications={},colour1=BLACKA(100)})
Minimap.Map = getElementEX(Minimap, anchorNone, XYWH(0, 0, 0, 0), true, {})

function FROMOW_MINIMAP_REFRESH(DATA)
	SGUI_SETTEXTUREID(Minimap.Map.ID, DATA.OGLID, DATA.TEX_WIDTH, DATA.TEX_HEIGHT, DATA.WIDTH, DATA.HEIGHT)
	setWH(Minimap.Map, DATA.MW, DATA.MH)
	setWH(Minimap, DATA.MW + 10, DATA.MH + 10)
	setXY(Minimap, 1920/2-DATA.MW/2, 75)
	setXY(Minimap.Map, 5, 5)
	set_Callback(Minimap.Map.ID,CALLBACK_MOUSEDOWN,'MinimapDown(%x,%y,%b)');
	set_Callback(Minimap.Map.ID,CALLBACK_MOUSEMOVE,'MinimapMove(%x,%y)');
	set_Property(Minimap.Map.ID,PROP_BORDER_TYPE,BORDER_TYPE_OUTER);
	for i = 1, 4 do
		setParent(game.ui.minimap.map.img.corners[i], Minimap.Map)
	end
	setXY(cards.cancel, 1920 / 2 - getWidth(cards.cancel) / 2 + 5, 75 + DATA.MH + 10 + 5, 75, 25)
end

cards = cardsClass()
cards.initCards()

include('_interface')
include('_resources')
include('_technologies')
include('_upgrades')
include('_animation')
include('_rewards')

include('Classes/Factory','Classes/Shader','Classes/Canvas','Classes/InfoProgress','Classes/Notifications','Classes/Strength','Classes/UnitSelection')
-- 'Classes/Xichted',
regTickCallback('cards.animate()')

AddSingleUseTimer(5, 'cards.createCard(1, 1)')

function deepCopy(orig)
    local copy = {}
    for k, v in pairs(orig) do
        if type(v) == 'table' then
            copy[k] = deepCopy(v)
        else
            copy[k] = v
        end
    end
    return copy
end