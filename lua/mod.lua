function Init()
	include('utils/fast2D','utils/OpenGL')
    setText(logotext, "Who thinks outside the box\npacks the best packages")
    setY(logotext, getY(logotext) + 25)
	include("devmode")
end

function removeFromName(tbl, value)
    for i = #tbl, 1, -1 do
        if tbl[i] == value then
            table.remove(tbl, i)
            return true
        end
    end
    return false
end

function removeFromNameAndDestroy(tbl, tbl2, value)
    for i = #tbl, 1, -1 do
        if tbl[i] == value then
            table.remove(tbl, i)
            local destroy = tbl2[i]
            sgui_delete(destroy.ID)
            table.remove(tbl2, i)
            return true
        end
    end
    return false
end

function containsValue(array, value)
    for i = 1, #array do
        if array[i] == value then
            return true
        end
    end
    return false
end

function getUnitTexture(ID)
  local r = OW_GET_UNIT_TEXTURE(ID)
  if r ~= nil then
    return r.TEXTURE
  else
    return 0
  end
end