--GENERATED ON: 05/12/2024 06:59:00


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['c-a-ht-manz'] = {
	TEX_WIDTH=1020,
	TEX_HEIGHT=410,
	FRAME_COUNT=72,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0588235294117647,0),
		[3]=v2fM(0,117647058823529,0),
		[4]=v2fM(0,176470588235294,0),
		[5]=v2fM(0,235294117647059,0),
		[6]=v2fM(0,294117647058824,0),
		[7]=v2fM(0,352941176470588,0),
		[8]=v2fM(0,411764705882353,0),
		[9]=v2fM(0,470588235294118,0),
		[10]=v2fM(0,529411764705882,0),
		[11]=v2fM(0,588235294117647,0),
		[12]=v2fM(0,647058823529412,0),
		[13]=v2fM(0,705882352941176,0),
		[14]=v2fM(0,764705882352941,0),
		[15]=v2fM(0,823529411764706,0),
		[16]=v2fM(0,882352941176471,0),
		[17]=v2fM(0,941176470588235,0),
		[18]=v2fM(0,0,2),
		[19]=v2fM(0,0588235294117647,0,2),
		[20]=v2fM(0,117647058823529,0,2),
		[21]=v2fM(0,176470588235294,0,2),
		[22]=v2fM(0,235294117647059,0,2),
		[23]=v2fM(0,294117647058824,0,2),
		[24]=v2fM(0,352941176470588,0,2),
		[25]=v2fM(0,411764705882353,0,2),
		[26]=v2fM(0,470588235294118,0,2),
		[27]=v2fM(0,529411764705882,0,2),
		[28]=v2fM(0,588235294117647,0,2),
		[29]=v2fM(0,647058823529412,0,2),
		[30]=v2fM(0,705882352941176,0,2),
		[31]=v2fM(0,764705882352941,0,2),
		[32]=v2fM(0,823529411764706,0,2),
		[33]=v2fM(0,882352941176471,0,2),
		[34]=v2fM(0,941176470588235,0,2),
		[35]=v2fM(0,0,4),
		[36]=v2fM(0,0588235294117647,0,4),
		[37]=v2fM(0,117647058823529,0,4),
		[38]=v2fM(0,176470588235294,0,4),
		[39]=v2fM(0,235294117647059,0,4),
		[40]=v2fM(0,294117647058824,0,4),
		[41]=v2fM(0,352941176470588,0,4),
		[42]=v2fM(0,411764705882353,0,4),
		[43]=v2fM(0,470588235294118,0,4),
		[44]=v2fM(0,529411764705882,0,4),
		[45]=v2fM(0,588235294117647,0,4),
		[46]=v2fM(0,647058823529412,0,4),
		[47]=v2fM(0,705882352941176,0,4),
		[48]=v2fM(0,764705882352941,0,4),
		[49]=v2fM(0,823529411764706,0,4),
		[50]=v2fM(0,882352941176471,0,4),
		[51]=v2fM(0,941176470588235,0,4),
		[52]=v2fM(0,0,6),
		[53]=v2fM(0,0588235294117647,0,6),
		[54]=v2fM(0,117647058823529,0,6),
		[55]=v2fM(0,176470588235294,0,6),
		[56]=v2fM(0,235294117647059,0,6),
		[57]=v2fM(0,294117647058824,0,6),
		[58]=v2fM(0,352941176470588,0,6),
		[59]=v2fM(0,411764705882353,0,6),
		[60]=v2fM(0,470588235294118,0,6),
		[61]=v2fM(0,529411764705882,0,6),
		[62]=v2fM(0,588235294117647,0,6),
		[63]=v2fM(0,647058823529412,0,6),
		[64]=v2fM(0,705882352941176,0,6),
		[65]=v2fM(0,764705882352941,0,6),
		[66]=v2fM(0,823529411764706,0,6),
		[67]=v2fM(0,882352941176471,0,6),
		[68]=v2fM(0,941176470588235,0,6),
		[69]=v2fM(0,0,8),
		[70]=v2fM(0,0588235294117647,0,8),
		[71]=v2fM(0,117647058823529,0,8),
		[72]=v2fM(0,176470588235294,0,8),
	},
	FRAME_SIZES={
		[1]={WIDTH=60,HEIGHT=82},
		[2]={WIDTH=60,HEIGHT=82},
		[3]={WIDTH=60,HEIGHT=82},
		[4]={WIDTH=60,HEIGHT=82},
		[5]={WIDTH=60,HEIGHT=82},
		[6]={WIDTH=60,HEIGHT=82},
		[7]={WIDTH=60,HEIGHT=82},
		[8]={WIDTH=60,HEIGHT=82},
		[9]={WIDTH=60,HEIGHT=82},
		[10]={WIDTH=60,HEIGHT=82},
		[11]={WIDTH=60,HEIGHT=82},
		[12]={WIDTH=60,HEIGHT=82},
		[13]={WIDTH=60,HEIGHT=82},
		[14]={WIDTH=60,HEIGHT=82},
		[15]={WIDTH=60,HEIGHT=82},
		[16]={WIDTH=60,HEIGHT=82},
		[17]={WIDTH=60,HEIGHT=82},
		[18]={WIDTH=60,HEIGHT=82},
		[19]={WIDTH=60,HEIGHT=82},
		[20]={WIDTH=60,HEIGHT=82},
		[21]={WIDTH=60,HEIGHT=82},
		[22]={WIDTH=60,HEIGHT=82},
		[23]={WIDTH=60,HEIGHT=82},
		[24]={WIDTH=60,HEIGHT=82},
		[25]={WIDTH=60,HEIGHT=82},
		[26]={WIDTH=60,HEIGHT=82},
		[27]={WIDTH=60,HEIGHT=82},
		[28]={WIDTH=60,HEIGHT=82},
		[29]={WIDTH=60,HEIGHT=82},
		[30]={WIDTH=60,HEIGHT=82},
		[31]={WIDTH=60,HEIGHT=82},
		[32]={WIDTH=60,HEIGHT=82},
		[33]={WIDTH=60,HEIGHT=82},
		[34]={WIDTH=60,HEIGHT=82},
		[35]={WIDTH=60,HEIGHT=82},
		[36]={WIDTH=60,HEIGHT=82},
		[37]={WIDTH=60,HEIGHT=82},
		[38]={WIDTH=60,HEIGHT=82},
		[39]={WIDTH=60,HEIGHT=82},
		[40]={WIDTH=60,HEIGHT=82},
		[41]={WIDTH=60,HEIGHT=82},
		[42]={WIDTH=60,HEIGHT=82},
		[43]={WIDTH=60,HEIGHT=82},
		[44]={WIDTH=60,HEIGHT=82},
		[45]={WIDTH=60,HEIGHT=82},
		[46]={WIDTH=60,HEIGHT=82},
		[47]={WIDTH=60,HEIGHT=82},
		[48]={WIDTH=60,HEIGHT=82},
		[49]={WIDTH=60,HEIGHT=82},
		[50]={WIDTH=60,HEIGHT=82},
		[51]={WIDTH=60,HEIGHT=82},
		[52]={WIDTH=60,HEIGHT=82},
		[53]={WIDTH=60,HEIGHT=82},
		[54]={WIDTH=60,HEIGHT=82},
		[55]={WIDTH=60,HEIGHT=82},
		[56]={WIDTH=60,HEIGHT=82},
		[57]={WIDTH=60,HEIGHT=82},
		[58]={WIDTH=60,HEIGHT=82},
		[59]={WIDTH=60,HEIGHT=82},
		[60]={WIDTH=60,HEIGHT=82},
		[61]={WIDTH=60,HEIGHT=82},
		[62]={WIDTH=60,HEIGHT=82},
		[63]={WIDTH=60,HEIGHT=82},
		[64]={WIDTH=60,HEIGHT=82},
		[65]={WIDTH=60,HEIGHT=82},
		[66]={WIDTH=60,HEIGHT=82},
		[67]={WIDTH=60,HEIGHT=82},
		[68]={WIDTH=60,HEIGHT=82},
		[69]={WIDTH=60,HEIGHT=82},
		[70]={WIDTH=60,HEIGHT=82},
		[71]={WIDTH=60,HEIGHT=82},
		[72]={WIDTH=60,HEIGHT=82},
	},
	FRAME_INDEXS={
		['c-a-ht-man0047z']=1,
		['c-a-ht-man0048z']=2,
		['c-a-ht-man0045z']=3,
		['c-a-ht-man0046z']=4,
		['c-a-ht-man0049z']=5,
		['c-a-ht-man0052z']=6,
		['c-a-ht-man0053z']=7,
		['c-a-ht-man0050z']=8,
		['c-a-ht-man0051z']=9,
		['c-a-ht-man0038z']=10,
		['c-a-ht-man0039z']=11,
		['c-a-ht-man0036z']=12,
		['c-a-ht-man0037z']=13,
		['c-a-ht-man0040z']=14,
		['c-a-ht-man0043z']=15,
		['c-a-ht-man0044z']=16,
		['c-a-ht-man0041z']=17,
		['c-a-ht-man0042z']=18,
		['c-a-ht-man0065z']=19,
		['c-a-ht-man0066z']=20,
		['c-a-ht-man0063z']=21,
		['c-a-ht-man0064z']=22,
		['c-a-ht-man0067z']=23,
		['c-a-ht-man0070z']=24,
		['c-a-ht-man0071z']=25,
		['c-a-ht-man0068z']=26,
		['c-a-ht-man0069z']=27,
		['c-a-ht-man0056z']=28,
		['c-a-ht-man0057z']=29,
		['c-a-ht-man0054z']=30,
		['c-a-ht-man0055z']=31,
		['c-a-ht-man0058z']=32,
		['c-a-ht-man0061z']=33,
		['c-a-ht-man0062z']=34,
		['c-a-ht-man0059z']=35,
		['c-a-ht-man0060z']=36,
		['c-a-ht-man0011z']=37,
		['c-a-ht-man0012z']=38,
		['c-a-ht-man0009z']=39,
		['c-a-ht-man0010z']=40,
		['c-a-ht-man0013z']=41,
		['c-a-ht-man0016z']=42,
		['c-a-ht-man0017z']=43,
		['c-a-ht-man0014z']=44,
		['c-a-ht-man0015z']=45,
		['c-a-ht-man0002z']=46,
		['c-a-ht-man0003z']=47,
		['c-a-ht-man0000z']=48,
		['c-a-ht-man0001z']=49,
		['c-a-ht-man0004z']=50,
		['c-a-ht-man0007z']=51,
		['c-a-ht-man0008z']=52,
		['c-a-ht-man0005z']=53,
		['c-a-ht-man0006z']=54,
		['c-a-ht-man0029z']=55,
		['c-a-ht-man0030z']=56,
		['c-a-ht-man0027z']=57,
		['c-a-ht-man0028z']=58,
		['c-a-ht-man0031z']=59,
		['c-a-ht-man0034z']=60,
		['c-a-ht-man0035z']=61,
		['c-a-ht-man0032z']=62,
		['c-a-ht-man0033z']=63,
		['c-a-ht-man0020z']=64,
		['c-a-ht-man0021z']=65,
		['c-a-ht-man0018z']=66,
		['c-a-ht-man0019z']=67,
		['c-a-ht-man0022z']=68,
		['c-a-ht-man0025z']=69,
		['c-a-ht-man0026z']=70,
		['c-a-ht-man0023z']=71,
		['c-a-ht-man0024z']=72,
	},
};
