--GENERATED ON: 05/12/2024 06:59:08


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['c-a-mw-rem'] = {
	TEX_WIDTH=576,
	TEX_HEIGHT=24,
	FRAME_COUNT=36,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0277777777777778,0),
		[3]=v2fM(0,0555555555555556,0),
		[4]=v2fM(0,0833333333333333,0),
		[5]=v2fM(0,111111111111111,0),
		[6]=v2fM(0,138888888888889,0),
		[7]=v2fM(0,166666666666667,0),
		[8]=v2fM(0,194444444444444,0),
		[9]=v2fM(0,222222222222222,0),
		[10]=v2fM(0,25,0),
		[11]=v2fM(0,277777777777778,0),
		[12]=v2fM(0,305555555555556,0),
		[13]=v2fM(0,333333333333333,0),
		[14]=v2fM(0,361111111111111,0),
		[15]=v2fM(0,388888888888889,0),
		[16]=v2fM(0,416666666666667,0),
		[17]=v2fM(0,444444444444444,0),
		[18]=v2fM(0,472222222222222,0),
		[19]=v2fM(0,5,0),
		[20]=v2fM(0,527777777777778,0),
		[21]=v2fM(0,555555555555556,0),
		[22]=v2fM(0,583333333333333,0),
		[23]=v2fM(0,611111111111111,0),
		[24]=v2fM(0,638888888888889,0),
		[25]=v2fM(0,666666666666667,0),
		[26]=v2fM(0,694444444444444,0),
		[27]=v2fM(0,722222222222222,0),
		[28]=v2fM(0,75,0),
		[29]=v2fM(0,777777777777778,0),
		[30]=v2fM(0,805555555555556,0),
		[31]=v2fM(0,833333333333333,0),
		[32]=v2fM(0,861111111111111,0),
		[33]=v2fM(0,888888888888889,0),
		[34]=v2fM(0,916666666666667,0),
		[35]=v2fM(0,944444444444444,0),
		[36]=v2fM(0,972222222222222,0),
	},
	FRAME_SIZES={
		[1]={WIDTH=16,HEIGHT=24},
		[2]={WIDTH=16,HEIGHT=24},
		[3]={WIDTH=16,HEIGHT=24},
		[4]={WIDTH=16,HEIGHT=24},
		[5]={WIDTH=16,HEIGHT=24},
		[6]={WIDTH=16,HEIGHT=24},
		[7]={WIDTH=16,HEIGHT=24},
		[8]={WIDTH=16,HEIGHT=24},
		[9]={WIDTH=16,HEIGHT=24},
		[10]={WIDTH=16,HEIGHT=24},
		[11]={WIDTH=16,HEIGHT=24},
		[12]={WIDTH=16,HEIGHT=24},
		[13]={WIDTH=16,HEIGHT=24},
		[14]={WIDTH=16,HEIGHT=24},
		[15]={WIDTH=16,HEIGHT=24},
		[16]={WIDTH=16,HEIGHT=24},
		[17]={WIDTH=16,HEIGHT=24},
		[18]={WIDTH=16,HEIGHT=24},
		[19]={WIDTH=16,HEIGHT=24},
		[20]={WIDTH=16,HEIGHT=24},
		[21]={WIDTH=16,HEIGHT=24},
		[22]={WIDTH=16,HEIGHT=24},
		[23]={WIDTH=16,HEIGHT=24},
		[24]={WIDTH=16,HEIGHT=24},
		[25]={WIDTH=16,HEIGHT=24},
		[26]={WIDTH=16,HEIGHT=24},
		[27]={WIDTH=16,HEIGHT=24},
		[28]={WIDTH=16,HEIGHT=24},
		[29]={WIDTH=16,HEIGHT=24},
		[30]={WIDTH=16,HEIGHT=24},
		[31]={WIDTH=16,HEIGHT=24},
		[32]={WIDTH=16,HEIGHT=24},
		[33]={WIDTH=16,HEIGHT=24},
		[34]={WIDTH=16,HEIGHT=24},
		[35]={WIDTH=16,HEIGHT=24},
		[36]={WIDTH=16,HEIGHT=24},
	},
	FRAME_INDEXS={
		['c-a-mw-rem0024']=1,
		['c-a-mw-rem0023']=2,
		['c-a-mw-rem0026']=3,
		['c-a-mw-rem0025']=4,
		['c-a-mw-rem0022']=5,
		['c-a-mw-rem0019']=6,
		['c-a-mw-rem0018']=7,
		['c-a-mw-rem0021']=8,
		['c-a-mw-rem0020']=9,
		['c-a-mw-rem0033']=10,
		['c-a-mw-rem0032']=11,
		['c-a-mw-rem0035']=12,
		['c-a-mw-rem0034']=13,
		['c-a-mw-rem0031']=14,
		['c-a-mw-rem0028']=15,
		['c-a-mw-rem0027']=16,
		['c-a-mw-rem0030']=17,
		['c-a-mw-rem0029']=18,
		['c-a-mw-rem0006']=19,
		['c-a-mw-rem0005']=20,
		['c-a-mw-rem0008']=21,
		['c-a-mw-rem0007']=22,
		['c-a-mw-rem0004']=23,
		['c-a-mw-rem0001']=24,
		['c-a-mw-rem0000']=25,
		['c-a-mw-rem0003']=26,
		['c-a-mw-rem0002']=27,
		['c-a-mw-rem0015']=28,
		['c-a-mw-rem0014']=29,
		['c-a-mw-rem0017']=30,
		['c-a-mw-rem0016']=31,
		['c-a-mw-rem0013']=32,
		['c-a-mw-rem0010']=33,
		['c-a-mw-rem0009']=34,
		['c-a-mw-rem0012']=35,
		['c-a-mw-rem0011']=36,
	},
};
