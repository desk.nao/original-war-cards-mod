--GENERATED ON: 05/12/2024 06:59:19


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['c-u-morphling-comp'] = {
	TEX_WIDTH=992,
	TEX_HEIGHT=774,
	FRAME_COUNT=144,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0625,0),
		[3]=v2fM(0,125,0),
		[4]=v2fM(0,1875,0),
		[5]=v2fM(0,25,0),
		[6]=v2fM(0,3125,0),
		[7]=v2fM(0,375,0),
		[8]=v2fM(0,4375,0),
		[9]=v2fM(0,5,0),
		[10]=v2fM(0,5625,0),
		[11]=v2fM(0,625,0),
		[12]=v2fM(0,6875,0),
		[13]=v2fM(0,75,0),
		[14]=v2fM(0,8125,0),
		[15]=v2fM(0,875,0),
		[16]=v2fM(0,9375,0),
		[17]=v2fM(0,0,111111111111111),
		[18]=v2fM(0,0625,0,111111111111111),
		[19]=v2fM(0,125,0,111111111111111),
		[20]=v2fM(0,1875,0,111111111111111),
		[21]=v2fM(0,25,0,111111111111111),
		[22]=v2fM(0,3125,0,111111111111111),
		[23]=v2fM(0,375,0,111111111111111),
		[24]=v2fM(0,4375,0,111111111111111),
		[25]=v2fM(0,5,0,111111111111111),
		[26]=v2fM(0,5625,0,111111111111111),
		[27]=v2fM(0,625,0,111111111111111),
		[28]=v2fM(0,6875,0,111111111111111),
		[29]=v2fM(0,75,0,111111111111111),
		[30]=v2fM(0,8125,0,111111111111111),
		[31]=v2fM(0,875,0,111111111111111),
		[32]=v2fM(0,9375,0,111111111111111),
		[33]=v2fM(0,0,222222222222222),
		[34]=v2fM(0,0625,0,222222222222222),
		[35]=v2fM(0,125,0,222222222222222),
		[36]=v2fM(0,1875,0,222222222222222),
		[37]=v2fM(0,25,0,222222222222222),
		[38]=v2fM(0,3125,0,222222222222222),
		[39]=v2fM(0,375,0,222222222222222),
		[40]=v2fM(0,4375,0,222222222222222),
		[41]=v2fM(0,5,0,222222222222222),
		[42]=v2fM(0,5625,0,222222222222222),
		[43]=v2fM(0,625,0,222222222222222),
		[44]=v2fM(0,6875,0,222222222222222),
		[45]=v2fM(0,75,0,222222222222222),
		[46]=v2fM(0,8125,0,222222222222222),
		[47]=v2fM(0,875,0,222222222222222),
		[48]=v2fM(0,9375,0,222222222222222),
		[49]=v2fM(0,0,333333333333333),
		[50]=v2fM(0,0625,0,333333333333333),
		[51]=v2fM(0,125,0,333333333333333),
		[52]=v2fM(0,1875,0,333333333333333),
		[53]=v2fM(0,25,0,333333333333333),
		[54]=v2fM(0,3125,0,333333333333333),
		[55]=v2fM(0,375,0,333333333333333),
		[56]=v2fM(0,4375,0,333333333333333),
		[57]=v2fM(0,5,0,333333333333333),
		[58]=v2fM(0,5625,0,333333333333333),
		[59]=v2fM(0,625,0,333333333333333),
		[60]=v2fM(0,6875,0,333333333333333),
		[61]=v2fM(0,75,0,333333333333333),
		[62]=v2fM(0,8125,0,333333333333333),
		[63]=v2fM(0,875,0,333333333333333),
		[64]=v2fM(0,9375,0,333333333333333),
		[65]=v2fM(0,0,444444444444444),
		[66]=v2fM(0,0625,0,444444444444444),
		[67]=v2fM(0,125,0,444444444444444),
		[68]=v2fM(0,1875,0,444444444444444),
		[69]=v2fM(0,25,0,444444444444444),
		[70]=v2fM(0,3125,0,444444444444444),
		[71]=v2fM(0,375,0,444444444444444),
		[72]=v2fM(0,4375,0,444444444444444),
		[73]=v2fM(0,5,0,444444444444444),
		[74]=v2fM(0,5625,0,444444444444444),
		[75]=v2fM(0,625,0,444444444444444),
		[76]=v2fM(0,6875,0,444444444444444),
		[77]=v2fM(0,75,0,444444444444444),
		[78]=v2fM(0,8125,0,444444444444444),
		[79]=v2fM(0,875,0,444444444444444),
		[80]=v2fM(0,9375,0,444444444444444),
		[81]=v2fM(0,0,555555555555556),
		[82]=v2fM(0,0625,0,555555555555556),
		[83]=v2fM(0,125,0,555555555555556),
		[84]=v2fM(0,1875,0,555555555555556),
		[85]=v2fM(0,25,0,555555555555556),
		[86]=v2fM(0,3125,0,555555555555556),
		[87]=v2fM(0,375,0,555555555555556),
		[88]=v2fM(0,4375,0,555555555555556),
		[89]=v2fM(0,5,0,555555555555556),
		[90]=v2fM(0,5625,0,555555555555556),
		[91]=v2fM(0,625,0,555555555555556),
		[92]=v2fM(0,6875,0,555555555555556),
		[93]=v2fM(0,75,0,555555555555556),
		[94]=v2fM(0,8125,0,555555555555556),
		[95]=v2fM(0,875,0,555555555555556),
		[96]=v2fM(0,9375,0,555555555555556),
		[97]=v2fM(0,0,666666666666667),
		[98]=v2fM(0,0625,0,666666666666667),
		[99]=v2fM(0,125,0,666666666666667),
		[100]=v2fM(0,1875,0,666666666666667),
		[101]=v2fM(0,25,0,666666666666667),
		[102]=v2fM(0,3125,0,666666666666667),
		[103]=v2fM(0,375,0,666666666666667),
		[104]=v2fM(0,4375,0,666666666666667),
		[105]=v2fM(0,5,0,666666666666667),
		[106]=v2fM(0,5625,0,666666666666667),
		[107]=v2fM(0,625,0,666666666666667),
		[108]=v2fM(0,6875,0,666666666666667),
		[109]=v2fM(0,75,0,666666666666667),
		[110]=v2fM(0,8125,0,666666666666667),
		[111]=v2fM(0,875,0,666666666666667),
		[112]=v2fM(0,9375,0,666666666666667),
		[113]=v2fM(0,0,777777777777778),
		[114]=v2fM(0,0625,0,777777777777778),
		[115]=v2fM(0,125,0,777777777777778),
		[116]=v2fM(0,1875,0,777777777777778),
		[117]=v2fM(0,25,0,777777777777778),
		[118]=v2fM(0,3125,0,777777777777778),
		[119]=v2fM(0,375,0,777777777777778),
		[120]=v2fM(0,4375,0,777777777777778),
		[121]=v2fM(0,5,0,777777777777778),
		[122]=v2fM(0,5625,0,777777777777778),
		[123]=v2fM(0,625,0,777777777777778),
		[124]=v2fM(0,6875,0,777777777777778),
		[125]=v2fM(0,75,0,777777777777778),
		[126]=v2fM(0,8125,0,777777777777778),
		[127]=v2fM(0,875,0,777777777777778),
		[128]=v2fM(0,9375,0,777777777777778),
		[129]=v2fM(0,0,888888888888889),
		[130]=v2fM(0,0625,0,888888888888889),
		[131]=v2fM(0,125,0,888888888888889),
		[132]=v2fM(0,1875,0,888888888888889),
		[133]=v2fM(0,25,0,888888888888889),
		[134]=v2fM(0,3125,0,888888888888889),
		[135]=v2fM(0,375,0,888888888888889),
		[136]=v2fM(0,4375,0,888888888888889),
		[137]=v2fM(0,5,0,888888888888889),
		[138]=v2fM(0,5625,0,888888888888889),
		[139]=v2fM(0,625,0,888888888888889),
		[140]=v2fM(0,6875,0,888888888888889),
		[141]=v2fM(0,75,0,888888888888889),
		[142]=v2fM(0,8125,0,888888888888889),
		[143]=v2fM(0,875,0,888888888888889),
		[144]=v2fM(0,9375,0,888888888888889),
	},
	FRAME_SIZES={
		[1]={WIDTH=62,HEIGHT=86},
		[2]={WIDTH=62,HEIGHT=86},
		[3]={WIDTH=62,HEIGHT=86},
		[4]={WIDTH=62,HEIGHT=86},
		[5]={WIDTH=62,HEIGHT=86},
		[6]={WIDTH=62,HEIGHT=86},
		[7]={WIDTH=62,HEIGHT=86},
		[8]={WIDTH=62,HEIGHT=86},
		[9]={WIDTH=62,HEIGHT=86},
		[10]={WIDTH=62,HEIGHT=86},
		[11]={WIDTH=62,HEIGHT=86},
		[12]={WIDTH=62,HEIGHT=86},
		[13]={WIDTH=62,HEIGHT=86},
		[14]={WIDTH=62,HEIGHT=86},
		[15]={WIDTH=62,HEIGHT=86},
		[16]={WIDTH=62,HEIGHT=86},
		[17]={WIDTH=62,HEIGHT=86},
		[18]={WIDTH=62,HEIGHT=86},
		[19]={WIDTH=62,HEIGHT=86},
		[20]={WIDTH=62,HEIGHT=86},
		[21]={WIDTH=62,HEIGHT=86},
		[22]={WIDTH=62,HEIGHT=86},
		[23]={WIDTH=62,HEIGHT=86},
		[24]={WIDTH=62,HEIGHT=86},
		[25]={WIDTH=62,HEIGHT=86},
		[26]={WIDTH=62,HEIGHT=86},
		[27]={WIDTH=62,HEIGHT=86},
		[28]={WIDTH=62,HEIGHT=86},
		[29]={WIDTH=62,HEIGHT=86},
		[30]={WIDTH=62,HEIGHT=86},
		[31]={WIDTH=62,HEIGHT=86},
		[32]={WIDTH=62,HEIGHT=86},
		[33]={WIDTH=62,HEIGHT=86},
		[34]={WIDTH=62,HEIGHT=86},
		[35]={WIDTH=62,HEIGHT=86},
		[36]={WIDTH=62,HEIGHT=86},
		[37]={WIDTH=62,HEIGHT=86},
		[38]={WIDTH=62,HEIGHT=86},
		[39]={WIDTH=62,HEIGHT=86},
		[40]={WIDTH=62,HEIGHT=86},
		[41]={WIDTH=62,HEIGHT=86},
		[42]={WIDTH=62,HEIGHT=86},
		[43]={WIDTH=62,HEIGHT=86},
		[44]={WIDTH=62,HEIGHT=86},
		[45]={WIDTH=62,HEIGHT=86},
		[46]={WIDTH=62,HEIGHT=86},
		[47]={WIDTH=62,HEIGHT=86},
		[48]={WIDTH=62,HEIGHT=86},
		[49]={WIDTH=62,HEIGHT=86},
		[50]={WIDTH=62,HEIGHT=86},
		[51]={WIDTH=62,HEIGHT=86},
		[52]={WIDTH=62,HEIGHT=86},
		[53]={WIDTH=62,HEIGHT=86},
		[54]={WIDTH=62,HEIGHT=86},
		[55]={WIDTH=62,HEIGHT=86},
		[56]={WIDTH=62,HEIGHT=86},
		[57]={WIDTH=62,HEIGHT=86},
		[58]={WIDTH=62,HEIGHT=86},
		[59]={WIDTH=62,HEIGHT=86},
		[60]={WIDTH=62,HEIGHT=86},
		[61]={WIDTH=62,HEIGHT=86},
		[62]={WIDTH=62,HEIGHT=86},
		[63]={WIDTH=62,HEIGHT=86},
		[64]={WIDTH=62,HEIGHT=86},
		[65]={WIDTH=62,HEIGHT=86},
		[66]={WIDTH=62,HEIGHT=86},
		[67]={WIDTH=62,HEIGHT=86},
		[68]={WIDTH=62,HEIGHT=86},
		[69]={WIDTH=62,HEIGHT=86},
		[70]={WIDTH=62,HEIGHT=86},
		[71]={WIDTH=62,HEIGHT=86},
		[72]={WIDTH=62,HEIGHT=86},
		[73]={WIDTH=62,HEIGHT=86},
		[74]={WIDTH=62,HEIGHT=86},
		[75]={WIDTH=62,HEIGHT=86},
		[76]={WIDTH=62,HEIGHT=86},
		[77]={WIDTH=62,HEIGHT=86},
		[78]={WIDTH=62,HEIGHT=86},
		[79]={WIDTH=62,HEIGHT=86},
		[80]={WIDTH=62,HEIGHT=86},
		[81]={WIDTH=62,HEIGHT=86},
		[82]={WIDTH=62,HEIGHT=86},
		[83]={WIDTH=62,HEIGHT=86},
		[84]={WIDTH=62,HEIGHT=86},
		[85]={WIDTH=62,HEIGHT=86},
		[86]={WIDTH=62,HEIGHT=86},
		[87]={WIDTH=62,HEIGHT=86},
		[88]={WIDTH=62,HEIGHT=86},
		[89]={WIDTH=62,HEIGHT=86},
		[90]={WIDTH=62,HEIGHT=86},
		[91]={WIDTH=62,HEIGHT=86},
		[92]={WIDTH=62,HEIGHT=86},
		[93]={WIDTH=62,HEIGHT=86},
		[94]={WIDTH=62,HEIGHT=86},
		[95]={WIDTH=62,HEIGHT=86},
		[96]={WIDTH=62,HEIGHT=86},
		[97]={WIDTH=62,HEIGHT=86},
		[98]={WIDTH=62,HEIGHT=86},
		[99]={WIDTH=62,HEIGHT=86},
		[100]={WIDTH=62,HEIGHT=86},
		[101]={WIDTH=62,HEIGHT=86},
		[102]={WIDTH=62,HEIGHT=86},
		[103]={WIDTH=62,HEIGHT=86},
		[104]={WIDTH=62,HEIGHT=86},
		[105]={WIDTH=62,HEIGHT=86},
		[106]={WIDTH=62,HEIGHT=86},
		[107]={WIDTH=62,HEIGHT=86},
		[108]={WIDTH=62,HEIGHT=86},
		[109]={WIDTH=62,HEIGHT=86},
		[110]={WIDTH=62,HEIGHT=86},
		[111]={WIDTH=62,HEIGHT=86},
		[112]={WIDTH=62,HEIGHT=86},
		[113]={WIDTH=62,HEIGHT=86},
		[114]={WIDTH=62,HEIGHT=86},
		[115]={WIDTH=62,HEIGHT=86},
		[116]={WIDTH=62,HEIGHT=86},
		[117]={WIDTH=62,HEIGHT=86},
		[118]={WIDTH=62,HEIGHT=86},
		[119]={WIDTH=62,HEIGHT=86},
		[120]={WIDTH=62,HEIGHT=86},
		[121]={WIDTH=62,HEIGHT=86},
		[122]={WIDTH=62,HEIGHT=86},
		[123]={WIDTH=62,HEIGHT=86},
		[124]={WIDTH=62,HEIGHT=86},
		[125]={WIDTH=62,HEIGHT=86},
		[126]={WIDTH=62,HEIGHT=86},
		[127]={WIDTH=62,HEIGHT=86},
		[128]={WIDTH=62,HEIGHT=86},
		[129]={WIDTH=62,HEIGHT=86},
		[130]={WIDTH=62,HEIGHT=86},
		[131]={WIDTH=62,HEIGHT=86},
		[132]={WIDTH=62,HEIGHT=86},
		[133]={WIDTH=62,HEIGHT=86},
		[134]={WIDTH=62,HEIGHT=86},
		[135]={WIDTH=62,HEIGHT=86},
		[136]={WIDTH=62,HEIGHT=86},
		[137]={WIDTH=62,HEIGHT=86},
		[138]={WIDTH=62,HEIGHT=86},
		[139]={WIDTH=62,HEIGHT=86},
		[140]={WIDTH=62,HEIGHT=86},
		[141]={WIDTH=62,HEIGHT=86},
		[142]={WIDTH=62,HEIGHT=86},
		[143]={WIDTH=62,HEIGHT=86},
		[144]={WIDTH=62,HEIGHT=86},
	},
	FRAME_INDEXS={
		['c-u-morphling-comp0096']=1,
		['c-u-morphling-comp0095']=2,
		['c-u-morphling-comp0098']=3,
		['c-u-morphling-comp0097']=4,
		['c-u-morphling-comp0094']=5,
		['c-u-morphling-comp0091']=6,
		['c-u-morphling-comp0090']=7,
		['c-u-morphling-comp0093']=8,
		['c-u-morphling-comp0092']=9,
		['c-u-morphling-comp0105']=10,
		['c-u-morphling-comp0104']=11,
		['c-u-morphling-comp0107']=12,
		['c-u-morphling-comp0106']=13,
		['c-u-morphling-comp0103']=14,
		['c-u-morphling-comp0100']=15,
		['c-u-morphling-comp0099']=16,
		['c-u-morphling-comp0102']=17,
		['c-u-morphling-comp0101']=18,
		['c-u-morphling-comp0078']=19,
		['c-u-morphling-comp0077']=20,
		['c-u-morphling-comp0080']=21,
		['c-u-morphling-comp0079']=22,
		['c-u-morphling-comp0076']=23,
		['c-u-morphling-comp0073']=24,
		['c-u-morphling-comp0072']=25,
		['c-u-morphling-comp0075']=26,
		['c-u-morphling-comp0074']=27,
		['c-u-morphling-comp0087']=28,
		['c-u-morphling-comp0086']=29,
		['c-u-morphling-comp0089']=30,
		['c-u-morphling-comp0088']=31,
		['c-u-morphling-comp0085']=32,
		['c-u-morphling-comp0082']=33,
		['c-u-morphling-comp0081']=34,
		['c-u-morphling-comp0084']=35,
		['c-u-morphling-comp0083']=36,
		['c-u-morphling-comp0132']=37,
		['c-u-morphling-comp0131']=38,
		['c-u-morphling-comp0134']=39,
		['c-u-morphling-comp0133']=40,
		['c-u-morphling-comp0130']=41,
		['c-u-morphling-comp0127']=42,
		['c-u-morphling-comp0126']=43,
		['c-u-morphling-comp0129']=44,
		['c-u-morphling-comp0128']=45,
		['c-u-morphling-comp0141']=46,
		['c-u-morphling-comp0140']=47,
		['c-u-morphling-comp0143']=48,
		['c-u-morphling-comp0142']=49,
		['c-u-morphling-comp0139']=50,
		['c-u-morphling-comp0136']=51,
		['c-u-morphling-comp0135']=52,
		['c-u-morphling-comp0138']=53,
		['c-u-morphling-comp0137']=54,
		['c-u-morphling-comp0114']=55,
		['c-u-morphling-comp0113']=56,
		['c-u-morphling-comp0116']=57,
		['c-u-morphling-comp0115']=58,
		['c-u-morphling-comp0112']=59,
		['c-u-morphling-comp0109']=60,
		['c-u-morphling-comp0108']=61,
		['c-u-morphling-comp0111']=62,
		['c-u-morphling-comp0110']=63,
		['c-u-morphling-comp0123']=64,
		['c-u-morphling-comp0122']=65,
		['c-u-morphling-comp0125']=66,
		['c-u-morphling-comp0124']=67,
		['c-u-morphling-comp0121']=68,
		['c-u-morphling-comp0118']=69,
		['c-u-morphling-comp0117']=70,
		['c-u-morphling-comp0120']=71,
		['c-u-morphling-comp0119']=72,
		['c-u-morphling-comp0024']=73,
		['c-u-morphling-comp0023']=74,
		['c-u-morphling-comp0026']=75,
		['c-u-morphling-comp0025']=76,
		['c-u-morphling-comp0022']=77,
		['c-u-morphling-comp0019']=78,
		['c-u-morphling-comp0018']=79,
		['c-u-morphling-comp0021']=80,
		['c-u-morphling-comp0020']=81,
		['c-u-morphling-comp0033']=82,
		['c-u-morphling-comp0032']=83,
		['c-u-morphling-comp0035']=84,
		['c-u-morphling-comp0034']=85,
		['c-u-morphling-comp0031']=86,
		['c-u-morphling-comp0028']=87,
		['c-u-morphling-comp0027']=88,
		['c-u-morphling-comp0030']=89,
		['c-u-morphling-comp0029']=90,
		['c-u-morphling-comp0006']=91,
		['c-u-morphling-comp0005']=92,
		['c-u-morphling-comp0008']=93,
		['c-u-morphling-comp0007']=94,
		['c-u-morphling-comp0004']=95,
		['c-u-morphling-comp0001']=96,
		['c-u-morphling-comp0000']=97,
		['c-u-morphling-comp0003']=98,
		['c-u-morphling-comp0002']=99,
		['c-u-morphling-comp0015']=100,
		['c-u-morphling-comp0014']=101,
		['c-u-morphling-comp0017']=102,
		['c-u-morphling-comp0016']=103,
		['c-u-morphling-comp0013']=104,
		['c-u-morphling-comp0010']=105,
		['c-u-morphling-comp0009']=106,
		['c-u-morphling-comp0012']=107,
		['c-u-morphling-comp0011']=108,
		['c-u-morphling-comp0060']=109,
		['c-u-morphling-comp0059']=110,
		['c-u-morphling-comp0062']=111,
		['c-u-morphling-comp0061']=112,
		['c-u-morphling-comp0058']=113,
		['c-u-morphling-comp0055']=114,
		['c-u-morphling-comp0054']=115,
		['c-u-morphling-comp0057']=116,
		['c-u-morphling-comp0056']=117,
		['c-u-morphling-comp0069']=118,
		['c-u-morphling-comp0068']=119,
		['c-u-morphling-comp0071']=120,
		['c-u-morphling-comp0070']=121,
		['c-u-morphling-comp0067']=122,
		['c-u-morphling-comp0064']=123,
		['c-u-morphling-comp0063']=124,
		['c-u-morphling-comp0066']=125,
		['c-u-morphling-comp0065']=126,
		['c-u-morphling-comp0042']=127,
		['c-u-morphling-comp0041']=128,
		['c-u-morphling-comp0044']=129,
		['c-u-morphling-comp0043']=130,
		['c-u-morphling-comp0040']=131,
		['c-u-morphling-comp0037']=132,
		['c-u-morphling-comp0036']=133,
		['c-u-morphling-comp0039']=134,
		['c-u-morphling-comp0038']=135,
		['c-u-morphling-comp0051']=136,
		['c-u-morphling-comp0050']=137,
		['c-u-morphling-comp0053']=138,
		['c-u-morphling-comp0052']=139,
		['c-u-morphling-comp0049']=140,
		['c-u-morphling-comp0046']=141,
		['c-u-morphling-comp0045']=142,
		['c-u-morphling-comp0048']=143,
		['c-u-morphling-comp0047']=144,
	},
};
