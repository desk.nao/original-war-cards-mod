--GENERATED ON: 05/12/2024 06:59:19


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['c-u-morphling-compz'] = {
	TEX_WIDTH=992,
	TEX_HEIGHT=774,
	FRAME_COUNT=144,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0625,0),
		[3]=v2fM(0,125,0),
		[4]=v2fM(0,1875,0),
		[5]=v2fM(0,25,0),
		[6]=v2fM(0,3125,0),
		[7]=v2fM(0,375,0),
		[8]=v2fM(0,4375,0),
		[9]=v2fM(0,5,0),
		[10]=v2fM(0,5625,0),
		[11]=v2fM(0,625,0),
		[12]=v2fM(0,6875,0),
		[13]=v2fM(0,75,0),
		[14]=v2fM(0,8125,0),
		[15]=v2fM(0,875,0),
		[16]=v2fM(0,9375,0),
		[17]=v2fM(0,0,111111111111111),
		[18]=v2fM(0,0625,0,111111111111111),
		[19]=v2fM(0,125,0,111111111111111),
		[20]=v2fM(0,1875,0,111111111111111),
		[21]=v2fM(0,25,0,111111111111111),
		[22]=v2fM(0,3125,0,111111111111111),
		[23]=v2fM(0,375,0,111111111111111),
		[24]=v2fM(0,4375,0,111111111111111),
		[25]=v2fM(0,5,0,111111111111111),
		[26]=v2fM(0,5625,0,111111111111111),
		[27]=v2fM(0,625,0,111111111111111),
		[28]=v2fM(0,6875,0,111111111111111),
		[29]=v2fM(0,75,0,111111111111111),
		[30]=v2fM(0,8125,0,111111111111111),
		[31]=v2fM(0,875,0,111111111111111),
		[32]=v2fM(0,9375,0,111111111111111),
		[33]=v2fM(0,0,222222222222222),
		[34]=v2fM(0,0625,0,222222222222222),
		[35]=v2fM(0,125,0,222222222222222),
		[36]=v2fM(0,1875,0,222222222222222),
		[37]=v2fM(0,25,0,222222222222222),
		[38]=v2fM(0,3125,0,222222222222222),
		[39]=v2fM(0,375,0,222222222222222),
		[40]=v2fM(0,4375,0,222222222222222),
		[41]=v2fM(0,5,0,222222222222222),
		[42]=v2fM(0,5625,0,222222222222222),
		[43]=v2fM(0,625,0,222222222222222),
		[44]=v2fM(0,6875,0,222222222222222),
		[45]=v2fM(0,75,0,222222222222222),
		[46]=v2fM(0,8125,0,222222222222222),
		[47]=v2fM(0,875,0,222222222222222),
		[48]=v2fM(0,9375,0,222222222222222),
		[49]=v2fM(0,0,333333333333333),
		[50]=v2fM(0,0625,0,333333333333333),
		[51]=v2fM(0,125,0,333333333333333),
		[52]=v2fM(0,1875,0,333333333333333),
		[53]=v2fM(0,25,0,333333333333333),
		[54]=v2fM(0,3125,0,333333333333333),
		[55]=v2fM(0,375,0,333333333333333),
		[56]=v2fM(0,4375,0,333333333333333),
		[57]=v2fM(0,5,0,333333333333333),
		[58]=v2fM(0,5625,0,333333333333333),
		[59]=v2fM(0,625,0,333333333333333),
		[60]=v2fM(0,6875,0,333333333333333),
		[61]=v2fM(0,75,0,333333333333333),
		[62]=v2fM(0,8125,0,333333333333333),
		[63]=v2fM(0,875,0,333333333333333),
		[64]=v2fM(0,9375,0,333333333333333),
		[65]=v2fM(0,0,444444444444444),
		[66]=v2fM(0,0625,0,444444444444444),
		[67]=v2fM(0,125,0,444444444444444),
		[68]=v2fM(0,1875,0,444444444444444),
		[69]=v2fM(0,25,0,444444444444444),
		[70]=v2fM(0,3125,0,444444444444444),
		[71]=v2fM(0,375,0,444444444444444),
		[72]=v2fM(0,4375,0,444444444444444),
		[73]=v2fM(0,5,0,444444444444444),
		[74]=v2fM(0,5625,0,444444444444444),
		[75]=v2fM(0,625,0,444444444444444),
		[76]=v2fM(0,6875,0,444444444444444),
		[77]=v2fM(0,75,0,444444444444444),
		[78]=v2fM(0,8125,0,444444444444444),
		[79]=v2fM(0,875,0,444444444444444),
		[80]=v2fM(0,9375,0,444444444444444),
		[81]=v2fM(0,0,555555555555556),
		[82]=v2fM(0,0625,0,555555555555556),
		[83]=v2fM(0,125,0,555555555555556),
		[84]=v2fM(0,1875,0,555555555555556),
		[85]=v2fM(0,25,0,555555555555556),
		[86]=v2fM(0,3125,0,555555555555556),
		[87]=v2fM(0,375,0,555555555555556),
		[88]=v2fM(0,4375,0,555555555555556),
		[89]=v2fM(0,5,0,555555555555556),
		[90]=v2fM(0,5625,0,555555555555556),
		[91]=v2fM(0,625,0,555555555555556),
		[92]=v2fM(0,6875,0,555555555555556),
		[93]=v2fM(0,75,0,555555555555556),
		[94]=v2fM(0,8125,0,555555555555556),
		[95]=v2fM(0,875,0,555555555555556),
		[96]=v2fM(0,9375,0,555555555555556),
		[97]=v2fM(0,0,666666666666667),
		[98]=v2fM(0,0625,0,666666666666667),
		[99]=v2fM(0,125,0,666666666666667),
		[100]=v2fM(0,1875,0,666666666666667),
		[101]=v2fM(0,25,0,666666666666667),
		[102]=v2fM(0,3125,0,666666666666667),
		[103]=v2fM(0,375,0,666666666666667),
		[104]=v2fM(0,4375,0,666666666666667),
		[105]=v2fM(0,5,0,666666666666667),
		[106]=v2fM(0,5625,0,666666666666667),
		[107]=v2fM(0,625,0,666666666666667),
		[108]=v2fM(0,6875,0,666666666666667),
		[109]=v2fM(0,75,0,666666666666667),
		[110]=v2fM(0,8125,0,666666666666667),
		[111]=v2fM(0,875,0,666666666666667),
		[112]=v2fM(0,9375,0,666666666666667),
		[113]=v2fM(0,0,777777777777778),
		[114]=v2fM(0,0625,0,777777777777778),
		[115]=v2fM(0,125,0,777777777777778),
		[116]=v2fM(0,1875,0,777777777777778),
		[117]=v2fM(0,25,0,777777777777778),
		[118]=v2fM(0,3125,0,777777777777778),
		[119]=v2fM(0,375,0,777777777777778),
		[120]=v2fM(0,4375,0,777777777777778),
		[121]=v2fM(0,5,0,777777777777778),
		[122]=v2fM(0,5625,0,777777777777778),
		[123]=v2fM(0,625,0,777777777777778),
		[124]=v2fM(0,6875,0,777777777777778),
		[125]=v2fM(0,75,0,777777777777778),
		[126]=v2fM(0,8125,0,777777777777778),
		[127]=v2fM(0,875,0,777777777777778),
		[128]=v2fM(0,9375,0,777777777777778),
		[129]=v2fM(0,0,888888888888889),
		[130]=v2fM(0,0625,0,888888888888889),
		[131]=v2fM(0,125,0,888888888888889),
		[132]=v2fM(0,1875,0,888888888888889),
		[133]=v2fM(0,25,0,888888888888889),
		[134]=v2fM(0,3125,0,888888888888889),
		[135]=v2fM(0,375,0,888888888888889),
		[136]=v2fM(0,4375,0,888888888888889),
		[137]=v2fM(0,5,0,888888888888889),
		[138]=v2fM(0,5625,0,888888888888889),
		[139]=v2fM(0,625,0,888888888888889),
		[140]=v2fM(0,6875,0,888888888888889),
		[141]=v2fM(0,75,0,888888888888889),
		[142]=v2fM(0,8125,0,888888888888889),
		[143]=v2fM(0,875,0,888888888888889),
		[144]=v2fM(0,9375,0,888888888888889),
	},
	FRAME_SIZES={
		[1]={WIDTH=62,HEIGHT=86},
		[2]={WIDTH=62,HEIGHT=86},
		[3]={WIDTH=62,HEIGHT=86},
		[4]={WIDTH=62,HEIGHT=86},
		[5]={WIDTH=62,HEIGHT=86},
		[6]={WIDTH=62,HEIGHT=86},
		[7]={WIDTH=62,HEIGHT=86},
		[8]={WIDTH=62,HEIGHT=86},
		[9]={WIDTH=62,HEIGHT=86},
		[10]={WIDTH=62,HEIGHT=86},
		[11]={WIDTH=62,HEIGHT=86},
		[12]={WIDTH=62,HEIGHT=86},
		[13]={WIDTH=62,HEIGHT=86},
		[14]={WIDTH=62,HEIGHT=86},
		[15]={WIDTH=62,HEIGHT=86},
		[16]={WIDTH=62,HEIGHT=86},
		[17]={WIDTH=62,HEIGHT=86},
		[18]={WIDTH=62,HEIGHT=86},
		[19]={WIDTH=62,HEIGHT=86},
		[20]={WIDTH=62,HEIGHT=86},
		[21]={WIDTH=62,HEIGHT=86},
		[22]={WIDTH=62,HEIGHT=86},
		[23]={WIDTH=62,HEIGHT=86},
		[24]={WIDTH=62,HEIGHT=86},
		[25]={WIDTH=62,HEIGHT=86},
		[26]={WIDTH=62,HEIGHT=86},
		[27]={WIDTH=62,HEIGHT=86},
		[28]={WIDTH=62,HEIGHT=86},
		[29]={WIDTH=62,HEIGHT=86},
		[30]={WIDTH=62,HEIGHT=86},
		[31]={WIDTH=62,HEIGHT=86},
		[32]={WIDTH=62,HEIGHT=86},
		[33]={WIDTH=62,HEIGHT=86},
		[34]={WIDTH=62,HEIGHT=86},
		[35]={WIDTH=62,HEIGHT=86},
		[36]={WIDTH=62,HEIGHT=86},
		[37]={WIDTH=62,HEIGHT=86},
		[38]={WIDTH=62,HEIGHT=86},
		[39]={WIDTH=62,HEIGHT=86},
		[40]={WIDTH=62,HEIGHT=86},
		[41]={WIDTH=62,HEIGHT=86},
		[42]={WIDTH=62,HEIGHT=86},
		[43]={WIDTH=62,HEIGHT=86},
		[44]={WIDTH=62,HEIGHT=86},
		[45]={WIDTH=62,HEIGHT=86},
		[46]={WIDTH=62,HEIGHT=86},
		[47]={WIDTH=62,HEIGHT=86},
		[48]={WIDTH=62,HEIGHT=86},
		[49]={WIDTH=62,HEIGHT=86},
		[50]={WIDTH=62,HEIGHT=86},
		[51]={WIDTH=62,HEIGHT=86},
		[52]={WIDTH=62,HEIGHT=86},
		[53]={WIDTH=62,HEIGHT=86},
		[54]={WIDTH=62,HEIGHT=86},
		[55]={WIDTH=62,HEIGHT=86},
		[56]={WIDTH=62,HEIGHT=86},
		[57]={WIDTH=62,HEIGHT=86},
		[58]={WIDTH=62,HEIGHT=86},
		[59]={WIDTH=62,HEIGHT=86},
		[60]={WIDTH=62,HEIGHT=86},
		[61]={WIDTH=62,HEIGHT=86},
		[62]={WIDTH=62,HEIGHT=86},
		[63]={WIDTH=62,HEIGHT=86},
		[64]={WIDTH=62,HEIGHT=86},
		[65]={WIDTH=62,HEIGHT=86},
		[66]={WIDTH=62,HEIGHT=86},
		[67]={WIDTH=62,HEIGHT=86},
		[68]={WIDTH=62,HEIGHT=86},
		[69]={WIDTH=62,HEIGHT=86},
		[70]={WIDTH=62,HEIGHT=86},
		[71]={WIDTH=62,HEIGHT=86},
		[72]={WIDTH=62,HEIGHT=86},
		[73]={WIDTH=62,HEIGHT=86},
		[74]={WIDTH=62,HEIGHT=86},
		[75]={WIDTH=62,HEIGHT=86},
		[76]={WIDTH=62,HEIGHT=86},
		[77]={WIDTH=62,HEIGHT=86},
		[78]={WIDTH=62,HEIGHT=86},
		[79]={WIDTH=62,HEIGHT=86},
		[80]={WIDTH=62,HEIGHT=86},
		[81]={WIDTH=62,HEIGHT=86},
		[82]={WIDTH=62,HEIGHT=86},
		[83]={WIDTH=62,HEIGHT=86},
		[84]={WIDTH=62,HEIGHT=86},
		[85]={WIDTH=62,HEIGHT=86},
		[86]={WIDTH=62,HEIGHT=86},
		[87]={WIDTH=62,HEIGHT=86},
		[88]={WIDTH=62,HEIGHT=86},
		[89]={WIDTH=62,HEIGHT=86},
		[90]={WIDTH=62,HEIGHT=86},
		[91]={WIDTH=62,HEIGHT=86},
		[92]={WIDTH=62,HEIGHT=86},
		[93]={WIDTH=62,HEIGHT=86},
		[94]={WIDTH=62,HEIGHT=86},
		[95]={WIDTH=62,HEIGHT=86},
		[96]={WIDTH=62,HEIGHT=86},
		[97]={WIDTH=62,HEIGHT=86},
		[98]={WIDTH=62,HEIGHT=86},
		[99]={WIDTH=62,HEIGHT=86},
		[100]={WIDTH=62,HEIGHT=86},
		[101]={WIDTH=62,HEIGHT=86},
		[102]={WIDTH=62,HEIGHT=86},
		[103]={WIDTH=62,HEIGHT=86},
		[104]={WIDTH=62,HEIGHT=86},
		[105]={WIDTH=62,HEIGHT=86},
		[106]={WIDTH=62,HEIGHT=86},
		[107]={WIDTH=62,HEIGHT=86},
		[108]={WIDTH=62,HEIGHT=86},
		[109]={WIDTH=62,HEIGHT=86},
		[110]={WIDTH=62,HEIGHT=86},
		[111]={WIDTH=62,HEIGHT=86},
		[112]={WIDTH=62,HEIGHT=86},
		[113]={WIDTH=62,HEIGHT=86},
		[114]={WIDTH=62,HEIGHT=86},
		[115]={WIDTH=62,HEIGHT=86},
		[116]={WIDTH=62,HEIGHT=86},
		[117]={WIDTH=62,HEIGHT=86},
		[118]={WIDTH=62,HEIGHT=86},
		[119]={WIDTH=62,HEIGHT=86},
		[120]={WIDTH=62,HEIGHT=86},
		[121]={WIDTH=62,HEIGHT=86},
		[122]={WIDTH=62,HEIGHT=86},
		[123]={WIDTH=62,HEIGHT=86},
		[124]={WIDTH=62,HEIGHT=86},
		[125]={WIDTH=62,HEIGHT=86},
		[126]={WIDTH=62,HEIGHT=86},
		[127]={WIDTH=62,HEIGHT=86},
		[128]={WIDTH=62,HEIGHT=86},
		[129]={WIDTH=62,HEIGHT=86},
		[130]={WIDTH=62,HEIGHT=86},
		[131]={WIDTH=62,HEIGHT=86},
		[132]={WIDTH=62,HEIGHT=86},
		[133]={WIDTH=62,HEIGHT=86},
		[134]={WIDTH=62,HEIGHT=86},
		[135]={WIDTH=62,HEIGHT=86},
		[136]={WIDTH=62,HEIGHT=86},
		[137]={WIDTH=62,HEIGHT=86},
		[138]={WIDTH=62,HEIGHT=86},
		[139]={WIDTH=62,HEIGHT=86},
		[140]={WIDTH=62,HEIGHT=86},
		[141]={WIDTH=62,HEIGHT=86},
		[142]={WIDTH=62,HEIGHT=86},
		[143]={WIDTH=62,HEIGHT=86},
		[144]={WIDTH=62,HEIGHT=86},
	},
	FRAME_INDEXS={
		['c-u-morphling-comp0096z']=1,
		['c-u-morphling-comp0095z']=2,
		['c-u-morphling-comp0098z']=3,
		['c-u-morphling-comp0097z']=4,
		['c-u-morphling-comp0094z']=5,
		['c-u-morphling-comp0091z']=6,
		['c-u-morphling-comp0090z']=7,
		['c-u-morphling-comp0093z']=8,
		['c-u-morphling-comp0092z']=9,
		['c-u-morphling-comp0105z']=10,
		['c-u-morphling-comp0104z']=11,
		['c-u-morphling-comp0107z']=12,
		['c-u-morphling-comp0106z']=13,
		['c-u-morphling-comp0103z']=14,
		['c-u-morphling-comp0100z']=15,
		['c-u-morphling-comp0099z']=16,
		['c-u-morphling-comp0102z']=17,
		['c-u-morphling-comp0101z']=18,
		['c-u-morphling-comp0078z']=19,
		['c-u-morphling-comp0077z']=20,
		['c-u-morphling-comp0080z']=21,
		['c-u-morphling-comp0079z']=22,
		['c-u-morphling-comp0076z']=23,
		['c-u-morphling-comp0073z']=24,
		['c-u-morphling-comp0072z']=25,
		['c-u-morphling-comp0075z']=26,
		['c-u-morphling-comp0074z']=27,
		['c-u-morphling-comp0087z']=28,
		['c-u-morphling-comp0086z']=29,
		['c-u-morphling-comp0089z']=30,
		['c-u-morphling-comp0088z']=31,
		['c-u-morphling-comp0085z']=32,
		['c-u-morphling-comp0082z']=33,
		['c-u-morphling-comp0081z']=34,
		['c-u-morphling-comp0084z']=35,
		['c-u-morphling-comp0083z']=36,
		['c-u-morphling-comp0132z']=37,
		['c-u-morphling-comp0131z']=38,
		['c-u-morphling-comp0134z']=39,
		['c-u-morphling-comp0133z']=40,
		['c-u-morphling-comp0130z']=41,
		['c-u-morphling-comp0127z']=42,
		['c-u-morphling-comp0126z']=43,
		['c-u-morphling-comp0129z']=44,
		['c-u-morphling-comp0128z']=45,
		['c-u-morphling-comp0141z']=46,
		['c-u-morphling-comp0140z']=47,
		['c-u-morphling-comp0143z']=48,
		['c-u-morphling-comp0142z']=49,
		['c-u-morphling-comp0139z']=50,
		['c-u-morphling-comp0136z']=51,
		['c-u-morphling-comp0135z']=52,
		['c-u-morphling-comp0138z']=53,
		['c-u-morphling-comp0137z']=54,
		['c-u-morphling-comp0114z']=55,
		['c-u-morphling-comp0113z']=56,
		['c-u-morphling-comp0116z']=57,
		['c-u-morphling-comp0115z']=58,
		['c-u-morphling-comp0112z']=59,
		['c-u-morphling-comp0109z']=60,
		['c-u-morphling-comp0108z']=61,
		['c-u-morphling-comp0111z']=62,
		['c-u-morphling-comp0110z']=63,
		['c-u-morphling-comp0123z']=64,
		['c-u-morphling-comp0122z']=65,
		['c-u-morphling-comp0125z']=66,
		['c-u-morphling-comp0124z']=67,
		['c-u-morphling-comp0121z']=68,
		['c-u-morphling-comp0118z']=69,
		['c-u-morphling-comp0117z']=70,
		['c-u-morphling-comp0120z']=71,
		['c-u-morphling-comp0119z']=72,
		['c-u-morphling-comp0024z']=73,
		['c-u-morphling-comp0023z']=74,
		['c-u-morphling-comp0026z']=75,
		['c-u-morphling-comp0025z']=76,
		['c-u-morphling-comp0022z']=77,
		['c-u-morphling-comp0019z']=78,
		['c-u-morphling-comp0018z']=79,
		['c-u-morphling-comp0021z']=80,
		['c-u-morphling-comp0020z']=81,
		['c-u-morphling-comp0033z']=82,
		['c-u-morphling-comp0032z']=83,
		['c-u-morphling-comp0035z']=84,
		['c-u-morphling-comp0034z']=85,
		['c-u-morphling-comp0031z']=86,
		['c-u-morphling-comp0028z']=87,
		['c-u-morphling-comp0027z']=88,
		['c-u-morphling-comp0030z']=89,
		['c-u-morphling-comp0029z']=90,
		['c-u-morphling-comp0006z']=91,
		['c-u-morphling-comp0005z']=92,
		['c-u-morphling-comp0008z']=93,
		['c-u-morphling-comp0007z']=94,
		['c-u-morphling-comp0004z']=95,
		['c-u-morphling-comp0001z']=96,
		['c-u-morphling-comp0000z']=97,
		['c-u-morphling-comp0003z']=98,
		['c-u-morphling-comp0002z']=99,
		['c-u-morphling-comp0015z']=100,
		['c-u-morphling-comp0014z']=101,
		['c-u-morphling-comp0017z']=102,
		['c-u-morphling-comp0016z']=103,
		['c-u-morphling-comp0013z']=104,
		['c-u-morphling-comp0010z']=105,
		['c-u-morphling-comp0009z']=106,
		['c-u-morphling-comp0012z']=107,
		['c-u-morphling-comp0011z']=108,
		['c-u-morphling-comp0060z']=109,
		['c-u-morphling-comp0059z']=110,
		['c-u-morphling-comp0062z']=111,
		['c-u-morphling-comp0061z']=112,
		['c-u-morphling-comp0058z']=113,
		['c-u-morphling-comp0055z']=114,
		['c-u-morphling-comp0054z']=115,
		['c-u-morphling-comp0057z']=116,
		['c-u-morphling-comp0056z']=117,
		['c-u-morphling-comp0069z']=118,
		['c-u-morphling-comp0068z']=119,
		['c-u-morphling-comp0071z']=120,
		['c-u-morphling-comp0070z']=121,
		['c-u-morphling-comp0067z']=122,
		['c-u-morphling-comp0064z']=123,
		['c-u-morphling-comp0063z']=124,
		['c-u-morphling-comp0066z']=125,
		['c-u-morphling-comp0065z']=126,
		['c-u-morphling-comp0042z']=127,
		['c-u-morphling-comp0041z']=128,
		['c-u-morphling-comp0044z']=129,
		['c-u-morphling-comp0043z']=130,
		['c-u-morphling-comp0040z']=131,
		['c-u-morphling-comp0037z']=132,
		['c-u-morphling-comp0036z']=133,
		['c-u-morphling-comp0039z']=134,
		['c-u-morphling-comp0038z']=135,
		['c-u-morphling-comp0051z']=136,
		['c-u-morphling-comp0050z']=137,
		['c-u-morphling-comp0053z']=138,
		['c-u-morphling-comp0052z']=139,
		['c-u-morphling-comp0049z']=140,
		['c-u-morphling-comp0046z']=141,
		['c-u-morphling-comp0045z']=142,
		['c-u-morphling-comp0048z']=143,
		['c-u-morphling-comp0047z']=144,
	},
};
