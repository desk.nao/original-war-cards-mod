--GENERATED ON: 05/12/2024 06:59:19


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['c-u-morphling-manz'] = {
	TEX_WIDTH=1020,
	TEX_HEIGHT=420,
	FRAME_COUNT=72,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0588235294117647,0),
		[3]=v2fM(0,117647058823529,0),
		[4]=v2fM(0,176470588235294,0),
		[5]=v2fM(0,235294117647059,0),
		[6]=v2fM(0,294117647058824,0),
		[7]=v2fM(0,352941176470588,0),
		[8]=v2fM(0,411764705882353,0),
		[9]=v2fM(0,470588235294118,0),
		[10]=v2fM(0,529411764705882,0),
		[11]=v2fM(0,588235294117647,0),
		[12]=v2fM(0,647058823529412,0),
		[13]=v2fM(0,705882352941176,0),
		[14]=v2fM(0,764705882352941,0),
		[15]=v2fM(0,823529411764706,0),
		[16]=v2fM(0,882352941176471,0),
		[17]=v2fM(0,941176470588235,0),
		[18]=v2fM(0,0,2),
		[19]=v2fM(0,0588235294117647,0,2),
		[20]=v2fM(0,117647058823529,0,2),
		[21]=v2fM(0,176470588235294,0,2),
		[22]=v2fM(0,235294117647059,0,2),
		[23]=v2fM(0,294117647058824,0,2),
		[24]=v2fM(0,352941176470588,0,2),
		[25]=v2fM(0,411764705882353,0,2),
		[26]=v2fM(0,470588235294118,0,2),
		[27]=v2fM(0,529411764705882,0,2),
		[28]=v2fM(0,588235294117647,0,2),
		[29]=v2fM(0,647058823529412,0,2),
		[30]=v2fM(0,705882352941176,0,2),
		[31]=v2fM(0,764705882352941,0,2),
		[32]=v2fM(0,823529411764706,0,2),
		[33]=v2fM(0,882352941176471,0,2),
		[34]=v2fM(0,941176470588235,0,2),
		[35]=v2fM(0,0,4),
		[36]=v2fM(0,0588235294117647,0,4),
		[37]=v2fM(0,117647058823529,0,4),
		[38]=v2fM(0,176470588235294,0,4),
		[39]=v2fM(0,235294117647059,0,4),
		[40]=v2fM(0,294117647058824,0,4),
		[41]=v2fM(0,352941176470588,0,4),
		[42]=v2fM(0,411764705882353,0,4),
		[43]=v2fM(0,470588235294118,0,4),
		[44]=v2fM(0,529411764705882,0,4),
		[45]=v2fM(0,588235294117647,0,4),
		[46]=v2fM(0,647058823529412,0,4),
		[47]=v2fM(0,705882352941176,0,4),
		[48]=v2fM(0,764705882352941,0,4),
		[49]=v2fM(0,823529411764706,0,4),
		[50]=v2fM(0,882352941176471,0,4),
		[51]=v2fM(0,941176470588235,0,4),
		[52]=v2fM(0,0,6),
		[53]=v2fM(0,0588235294117647,0,6),
		[54]=v2fM(0,117647058823529,0,6),
		[55]=v2fM(0,176470588235294,0,6),
		[56]=v2fM(0,235294117647059,0,6),
		[57]=v2fM(0,294117647058824,0,6),
		[58]=v2fM(0,352941176470588,0,6),
		[59]=v2fM(0,411764705882353,0,6),
		[60]=v2fM(0,470588235294118,0,6),
		[61]=v2fM(0,529411764705882,0,6),
		[62]=v2fM(0,588235294117647,0,6),
		[63]=v2fM(0,647058823529412,0,6),
		[64]=v2fM(0,705882352941176,0,6),
		[65]=v2fM(0,764705882352941,0,6),
		[66]=v2fM(0,823529411764706,0,6),
		[67]=v2fM(0,882352941176471,0,6),
		[68]=v2fM(0,941176470588235,0,6),
		[69]=v2fM(0,0,8),
		[70]=v2fM(0,0588235294117647,0,8),
		[71]=v2fM(0,117647058823529,0,8),
		[72]=v2fM(0,176470588235294,0,8),
	},
	FRAME_SIZES={
		[1]={WIDTH=60,HEIGHT=84},
		[2]={WIDTH=60,HEIGHT=84},
		[3]={WIDTH=60,HEIGHT=84},
		[4]={WIDTH=60,HEIGHT=84},
		[5]={WIDTH=60,HEIGHT=84},
		[6]={WIDTH=60,HEIGHT=84},
		[7]={WIDTH=60,HEIGHT=84},
		[8]={WIDTH=60,HEIGHT=84},
		[9]={WIDTH=60,HEIGHT=84},
		[10]={WIDTH=60,HEIGHT=84},
		[11]={WIDTH=60,HEIGHT=84},
		[12]={WIDTH=60,HEIGHT=84},
		[13]={WIDTH=60,HEIGHT=84},
		[14]={WIDTH=60,HEIGHT=84},
		[15]={WIDTH=60,HEIGHT=84},
		[16]={WIDTH=60,HEIGHT=84},
		[17]={WIDTH=60,HEIGHT=84},
		[18]={WIDTH=60,HEIGHT=84},
		[19]={WIDTH=60,HEIGHT=84},
		[20]={WIDTH=60,HEIGHT=84},
		[21]={WIDTH=60,HEIGHT=84},
		[22]={WIDTH=60,HEIGHT=84},
		[23]={WIDTH=60,HEIGHT=84},
		[24]={WIDTH=60,HEIGHT=84},
		[25]={WIDTH=60,HEIGHT=84},
		[26]={WIDTH=60,HEIGHT=84},
		[27]={WIDTH=60,HEIGHT=84},
		[28]={WIDTH=60,HEIGHT=84},
		[29]={WIDTH=60,HEIGHT=84},
		[30]={WIDTH=60,HEIGHT=84},
		[31]={WIDTH=60,HEIGHT=84},
		[32]={WIDTH=60,HEIGHT=84},
		[33]={WIDTH=60,HEIGHT=84},
		[34]={WIDTH=60,HEIGHT=84},
		[35]={WIDTH=60,HEIGHT=84},
		[36]={WIDTH=60,HEIGHT=84},
		[37]={WIDTH=60,HEIGHT=84},
		[38]={WIDTH=60,HEIGHT=84},
		[39]={WIDTH=60,HEIGHT=84},
		[40]={WIDTH=60,HEIGHT=84},
		[41]={WIDTH=60,HEIGHT=84},
		[42]={WIDTH=60,HEIGHT=84},
		[43]={WIDTH=60,HEIGHT=84},
		[44]={WIDTH=60,HEIGHT=84},
		[45]={WIDTH=60,HEIGHT=84},
		[46]={WIDTH=60,HEIGHT=84},
		[47]={WIDTH=60,HEIGHT=84},
		[48]={WIDTH=60,HEIGHT=84},
		[49]={WIDTH=60,HEIGHT=84},
		[50]={WIDTH=60,HEIGHT=84},
		[51]={WIDTH=60,HEIGHT=84},
		[52]={WIDTH=60,HEIGHT=84},
		[53]={WIDTH=60,HEIGHT=84},
		[54]={WIDTH=60,HEIGHT=84},
		[55]={WIDTH=60,HEIGHT=84},
		[56]={WIDTH=60,HEIGHT=84},
		[57]={WIDTH=60,HEIGHT=84},
		[58]={WIDTH=60,HEIGHT=84},
		[59]={WIDTH=60,HEIGHT=84},
		[60]={WIDTH=60,HEIGHT=84},
		[61]={WIDTH=60,HEIGHT=84},
		[62]={WIDTH=60,HEIGHT=84},
		[63]={WIDTH=60,HEIGHT=84},
		[64]={WIDTH=60,HEIGHT=84},
		[65]={WIDTH=60,HEIGHT=84},
		[66]={WIDTH=60,HEIGHT=84},
		[67]={WIDTH=60,HEIGHT=84},
		[68]={WIDTH=60,HEIGHT=84},
		[69]={WIDTH=60,HEIGHT=84},
		[70]={WIDTH=60,HEIGHT=84},
		[71]={WIDTH=60,HEIGHT=84},
		[72]={WIDTH=60,HEIGHT=84},
	},
	FRAME_INDEXS={
		['c-u-morphling-man0047z']=1,
		['c-u-morphling-man0048z']=2,
		['c-u-morphling-man0045z']=3,
		['c-u-morphling-man0046z']=4,
		['c-u-morphling-man0049z']=5,
		['c-u-morphling-man0052z']=6,
		['c-u-morphling-man0053z']=7,
		['c-u-morphling-man0050z']=8,
		['c-u-morphling-man0051z']=9,
		['c-u-morphling-man0038z']=10,
		['c-u-morphling-man0039z']=11,
		['c-u-morphling-man0036z']=12,
		['c-u-morphling-man0037z']=13,
		['c-u-morphling-man0040z']=14,
		['c-u-morphling-man0043z']=15,
		['c-u-morphling-man0044z']=16,
		['c-u-morphling-man0041z']=17,
		['c-u-morphling-man0042z']=18,
		['c-u-morphling-man0065z']=19,
		['c-u-morphling-man0066z']=20,
		['c-u-morphling-man0063z']=21,
		['c-u-morphling-man0064z']=22,
		['c-u-morphling-man0067z']=23,
		['c-u-morphling-man0070z']=24,
		['c-u-morphling-man0071z']=25,
		['c-u-morphling-man0068z']=26,
		['c-u-morphling-man0069z']=27,
		['c-u-morphling-man0056z']=28,
		['c-u-morphling-man0057z']=29,
		['c-u-morphling-man0054z']=30,
		['c-u-morphling-man0055z']=31,
		['c-u-morphling-man0058z']=32,
		['c-u-morphling-man0061z']=33,
		['c-u-morphling-man0062z']=34,
		['c-u-morphling-man0059z']=35,
		['c-u-morphling-man0060z']=36,
		['c-u-morphling-man0011z']=37,
		['c-u-morphling-man0012z']=38,
		['c-u-morphling-man0009z']=39,
		['c-u-morphling-man0010z']=40,
		['c-u-morphling-man0013z']=41,
		['c-u-morphling-man0016z']=42,
		['c-u-morphling-man0017z']=43,
		['c-u-morphling-man0014z']=44,
		['c-u-morphling-man0015z']=45,
		['c-u-morphling-man0002z']=46,
		['c-u-morphling-man0003z']=47,
		['c-u-morphling-man0000z']=48,
		['c-u-morphling-man0001z']=49,
		['c-u-morphling-man0004z']=50,
		['c-u-morphling-man0007z']=51,
		['c-u-morphling-man0008z']=52,
		['c-u-morphling-man0005z']=53,
		['c-u-morphling-man0006z']=54,
		['c-u-morphling-man0029z']=55,
		['c-u-morphling-man0030z']=56,
		['c-u-morphling-man0027z']=57,
		['c-u-morphling-man0028z']=58,
		['c-u-morphling-man0031z']=59,
		['c-u-morphling-man0034z']=60,
		['c-u-morphling-man0035z']=61,
		['c-u-morphling-man0032z']=62,
		['c-u-morphling-man0033z']=63,
		['c-u-morphling-man0020z']=64,
		['c-u-morphling-man0021z']=65,
		['c-u-morphling-man0018z']=66,
		['c-u-morphling-man0019z']=67,
		['c-u-morphling-man0022z']=68,
		['c-u-morphling-man0025z']=69,
		['c-u-morphling-man0026z']=70,
		['c-u-morphling-man0023z']=71,
		['c-u-morphling-man0024z']=72,
	},
};
