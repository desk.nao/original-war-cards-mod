--GENERATED ON: 05/12/2024 06:59:21


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['e-a-ht-combz'] = {
	TEX_WIDTH=1008,
	TEX_HEIGHT=588,
	FRAME_COUNT=72,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0714285714285714,0),
		[3]=v2fM(0,142857142857143,0),
		[4]=v2fM(0,214285714285714,0),
		[5]=v2fM(0,285714285714286,0),
		[6]=v2fM(0,357142857142857,0),
		[7]=v2fM(0,428571428571429,0),
		[8]=v2fM(0,5,0),
		[9]=v2fM(0,571428571428571,0),
		[10]=v2fM(0,642857142857143,0),
		[11]=v2fM(0,714285714285714,0),
		[12]=v2fM(0,785714285714286,0),
		[13]=v2fM(0,857142857142857,0),
		[14]=v2fM(0,928571428571429,0),
		[15]=v2fM(0,0,166666666666667),
		[16]=v2fM(0,0714285714285714,0,166666666666667),
		[17]=v2fM(0,142857142857143,0,166666666666667),
		[18]=v2fM(0,214285714285714,0,166666666666667),
		[19]=v2fM(0,285714285714286,0,166666666666667),
		[20]=v2fM(0,357142857142857,0,166666666666667),
		[21]=v2fM(0,428571428571429,0,166666666666667),
		[22]=v2fM(0,5,0,166666666666667),
		[23]=v2fM(0,571428571428571,0,166666666666667),
		[24]=v2fM(0,642857142857143,0,166666666666667),
		[25]=v2fM(0,714285714285714,0,166666666666667),
		[26]=v2fM(0,785714285714286,0,166666666666667),
		[27]=v2fM(0,857142857142857,0,166666666666667),
		[28]=v2fM(0,928571428571429,0,166666666666667),
		[29]=v2fM(0,0,333333333333333),
		[30]=v2fM(0,0714285714285714,0,333333333333333),
		[31]=v2fM(0,142857142857143,0,333333333333333),
		[32]=v2fM(0,214285714285714,0,333333333333333),
		[33]=v2fM(0,285714285714286,0,333333333333333),
		[34]=v2fM(0,357142857142857,0,333333333333333),
		[35]=v2fM(0,428571428571429,0,333333333333333),
		[36]=v2fM(0,5,0,333333333333333),
		[37]=v2fM(0,571428571428571,0,333333333333333),
		[38]=v2fM(0,642857142857143,0,333333333333333),
		[39]=v2fM(0,714285714285714,0,333333333333333),
		[40]=v2fM(0,785714285714286,0,333333333333333),
		[41]=v2fM(0,857142857142857,0,333333333333333),
		[42]=v2fM(0,928571428571429,0,333333333333333),
		[43]=v2fM(0,0,5),
		[44]=v2fM(0,0714285714285714,0,5),
		[45]=v2fM(0,142857142857143,0,5),
		[46]=v2fM(0,214285714285714,0,5),
		[47]=v2fM(0,285714285714286,0,5),
		[48]=v2fM(0,357142857142857,0,5),
		[49]=v2fM(0,428571428571429,0,5),
		[50]=v2fM(0,5,0,5),
		[51]=v2fM(0,571428571428571,0,5),
		[52]=v2fM(0,642857142857143,0,5),
		[53]=v2fM(0,714285714285714,0,5),
		[54]=v2fM(0,785714285714286,0,5),
		[55]=v2fM(0,857142857142857,0,5),
		[56]=v2fM(0,928571428571429,0,5),
		[57]=v2fM(0,0,666666666666667),
		[58]=v2fM(0,0714285714285714,0,666666666666667),
		[59]=v2fM(0,142857142857143,0,666666666666667),
		[60]=v2fM(0,214285714285714,0,666666666666667),
		[61]=v2fM(0,285714285714286,0,666666666666667),
		[62]=v2fM(0,357142857142857,0,666666666666667),
		[63]=v2fM(0,428571428571429,0,666666666666667),
		[64]=v2fM(0,5,0,666666666666667),
		[65]=v2fM(0,571428571428571,0,666666666666667),
		[66]=v2fM(0,642857142857143,0,666666666666667),
		[67]=v2fM(0,714285714285714,0,666666666666667),
		[68]=v2fM(0,785714285714286,0,666666666666667),
		[69]=v2fM(0,857142857142857,0,666666666666667),
		[70]=v2fM(0,928571428571429,0,666666666666667),
		[71]=v2fM(0,0,833333333333333),
		[72]=v2fM(0,0714285714285714,0,833333333333333),
	},
	FRAME_SIZES={
		[1]={WIDTH=72,HEIGHT=98},
		[2]={WIDTH=72,HEIGHT=98},
		[3]={WIDTH=72,HEIGHT=98},
		[4]={WIDTH=72,HEIGHT=98},
		[5]={WIDTH=72,HEIGHT=98},
		[6]={WIDTH=72,HEIGHT=98},
		[7]={WIDTH=72,HEIGHT=98},
		[8]={WIDTH=72,HEIGHT=98},
		[9]={WIDTH=72,HEIGHT=98},
		[10]={WIDTH=72,HEIGHT=98},
		[11]={WIDTH=72,HEIGHT=98},
		[12]={WIDTH=72,HEIGHT=98},
		[13]={WIDTH=72,HEIGHT=98},
		[14]={WIDTH=72,HEIGHT=98},
		[15]={WIDTH=72,HEIGHT=98},
		[16]={WIDTH=72,HEIGHT=98},
		[17]={WIDTH=72,HEIGHT=98},
		[18]={WIDTH=72,HEIGHT=98},
		[19]={WIDTH=72,HEIGHT=98},
		[20]={WIDTH=72,HEIGHT=98},
		[21]={WIDTH=72,HEIGHT=98},
		[22]={WIDTH=72,HEIGHT=98},
		[23]={WIDTH=72,HEIGHT=98},
		[24]={WIDTH=72,HEIGHT=98},
		[25]={WIDTH=72,HEIGHT=98},
		[26]={WIDTH=72,HEIGHT=98},
		[27]={WIDTH=72,HEIGHT=98},
		[28]={WIDTH=72,HEIGHT=98},
		[29]={WIDTH=72,HEIGHT=98},
		[30]={WIDTH=72,HEIGHT=98},
		[31]={WIDTH=72,HEIGHT=98},
		[32]={WIDTH=72,HEIGHT=98},
		[33]={WIDTH=72,HEIGHT=98},
		[34]={WIDTH=72,HEIGHT=98},
		[35]={WIDTH=72,HEIGHT=98},
		[36]={WIDTH=72,HEIGHT=98},
		[37]={WIDTH=72,HEIGHT=98},
		[38]={WIDTH=72,HEIGHT=98},
		[39]={WIDTH=72,HEIGHT=98},
		[40]={WIDTH=72,HEIGHT=98},
		[41]={WIDTH=72,HEIGHT=98},
		[42]={WIDTH=72,HEIGHT=98},
		[43]={WIDTH=72,HEIGHT=98},
		[44]={WIDTH=72,HEIGHT=98},
		[45]={WIDTH=72,HEIGHT=98},
		[46]={WIDTH=72,HEIGHT=98},
		[47]={WIDTH=72,HEIGHT=98},
		[48]={WIDTH=72,HEIGHT=98},
		[49]={WIDTH=72,HEIGHT=98},
		[50]={WIDTH=72,HEIGHT=98},
		[51]={WIDTH=72,HEIGHT=98},
		[52]={WIDTH=72,HEIGHT=98},
		[53]={WIDTH=72,HEIGHT=98},
		[54]={WIDTH=72,HEIGHT=98},
		[55]={WIDTH=72,HEIGHT=98},
		[56]={WIDTH=72,HEIGHT=98},
		[57]={WIDTH=72,HEIGHT=98},
		[58]={WIDTH=72,HEIGHT=98},
		[59]={WIDTH=72,HEIGHT=98},
		[60]={WIDTH=72,HEIGHT=98},
		[61]={WIDTH=72,HEIGHT=98},
		[62]={WIDTH=72,HEIGHT=98},
		[63]={WIDTH=72,HEIGHT=98},
		[64]={WIDTH=72,HEIGHT=98},
		[65]={WIDTH=72,HEIGHT=98},
		[66]={WIDTH=72,HEIGHT=98},
		[67]={WIDTH=72,HEIGHT=98},
		[68]={WIDTH=72,HEIGHT=98},
		[69]={WIDTH=72,HEIGHT=98},
		[70]={WIDTH=72,HEIGHT=98},
		[71]={WIDTH=72,HEIGHT=98},
		[72]={WIDTH=72,HEIGHT=98},
	},
	FRAME_INDEXS={
		['e-a-ht-comb0047z']=1,
		['e-a-ht-comb0048z']=2,
		['e-a-ht-comb0045z']=3,
		['e-a-ht-comb0046z']=4,
		['e-a-ht-comb0049z']=5,
		['e-a-ht-comb0052z']=6,
		['e-a-ht-comb0053z']=7,
		['e-a-ht-comb0050z']=8,
		['e-a-ht-comb0051z']=9,
		['e-a-ht-comb0038z']=10,
		['e-a-ht-comb0039z']=11,
		['e-a-ht-comb0036z']=12,
		['e-a-ht-comb0037z']=13,
		['e-a-ht-comb0040z']=14,
		['e-a-ht-comb0043z']=15,
		['e-a-ht-comb0044z']=16,
		['e-a-ht-comb0041z']=17,
		['e-a-ht-comb0042z']=18,
		['e-a-ht-comb0065z']=19,
		['e-a-ht-comb0066z']=20,
		['e-a-ht-comb0063z']=21,
		['e-a-ht-comb0064z']=22,
		['e-a-ht-comb0067z']=23,
		['e-a-ht-comb0070z']=24,
		['e-a-ht-comb0071z']=25,
		['e-a-ht-comb0068z']=26,
		['e-a-ht-comb0069z']=27,
		['e-a-ht-comb0056z']=28,
		['e-a-ht-comb0057z']=29,
		['e-a-ht-comb0054z']=30,
		['e-a-ht-comb0055z']=31,
		['e-a-ht-comb0058z']=32,
		['e-a-ht-comb0061z']=33,
		['e-a-ht-comb0062z']=34,
		['e-a-ht-comb0059z']=35,
		['e-a-ht-comb0060z']=36,
		['e-a-ht-comb0011z']=37,
		['e-a-ht-comb0012z']=38,
		['e-a-ht-comb0009z']=39,
		['e-a-ht-comb0010z']=40,
		['e-a-ht-comb0013z']=41,
		['e-a-ht-comb0016z']=42,
		['e-a-ht-comb0017z']=43,
		['e-a-ht-comb0014z']=44,
		['e-a-ht-comb0015z']=45,
		['e-a-ht-comb0002z']=46,
		['e-a-ht-comb0003z']=47,
		['e-a-ht-comb0000z']=48,
		['e-a-ht-comb0001z']=49,
		['e-a-ht-comb0004z']=50,
		['e-a-ht-comb0007z']=51,
		['e-a-ht-comb0008z']=52,
		['e-a-ht-comb0005z']=53,
		['e-a-ht-comb0006z']=54,
		['e-a-ht-comb0029z']=55,
		['e-a-ht-comb0030z']=56,
		['e-a-ht-comb0027z']=57,
		['e-a-ht-comb0028z']=58,
		['e-a-ht-comb0031z']=59,
		['e-a-ht-comb0034z']=60,
		['e-a-ht-comb0035z']=61,
		['e-a-ht-comb0032z']=62,
		['e-a-ht-comb0033z']=63,
		['e-a-ht-comb0020z']=64,
		['e-a-ht-comb0021z']=65,
		['e-a-ht-comb0018z']=66,
		['e-a-ht-comb0019z']=67,
		['e-a-ht-comb0022z']=68,
		['e-a-ht-comb0025z']=69,
		['e-a-ht-comb0026z']=70,
		['e-a-ht-comb0023z']=71,
		['e-a-ht-comb0024z']=72,
	},
};
