--GENERATED ON: 05/12/2024 06:59:23


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['e-a-ht-solarz'] = {
	TEX_WIDTH=946,
	TEX_HEIGHT=784,
	FRAME_COUNT=72,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0909090909090909,0),
		[3]=v2fM(0,181818181818182,0),
		[4]=v2fM(0,272727272727273,0),
		[5]=v2fM(0,363636363636364,0),
		[6]=v2fM(0,454545454545455,0),
		[7]=v2fM(0,545454545454545,0),
		[8]=v2fM(0,636363636363636,0),
		[9]=v2fM(0,727272727272727,0),
		[10]=v2fM(0,818181818181818,0),
		[11]=v2fM(0,909090909090909,0),
		[12]=v2fM(0,0,142857142857143),
		[13]=v2fM(0,0909090909090909,0,142857142857143),
		[14]=v2fM(0,181818181818182,0,142857142857143),
		[15]=v2fM(0,272727272727273,0,142857142857143),
		[16]=v2fM(0,363636363636364,0,142857142857143),
		[17]=v2fM(0,454545454545455,0,142857142857143),
		[18]=v2fM(0,545454545454545,0,142857142857143),
		[19]=v2fM(0,636363636363636,0,142857142857143),
		[20]=v2fM(0,727272727272727,0,142857142857143),
		[21]=v2fM(0,818181818181818,0,142857142857143),
		[22]=v2fM(0,909090909090909,0,142857142857143),
		[23]=v2fM(0,0,285714285714286),
		[24]=v2fM(0,0909090909090909,0,285714285714286),
		[25]=v2fM(0,181818181818182,0,285714285714286),
		[26]=v2fM(0,272727272727273,0,285714285714286),
		[27]=v2fM(0,363636363636364,0,285714285714286),
		[28]=v2fM(0,454545454545455,0,285714285714286),
		[29]=v2fM(0,545454545454545,0,285714285714286),
		[30]=v2fM(0,636363636363636,0,285714285714286),
		[31]=v2fM(0,727272727272727,0,285714285714286),
		[32]=v2fM(0,818181818181818,0,285714285714286),
		[33]=v2fM(0,909090909090909,0,285714285714286),
		[34]=v2fM(0,0,428571428571429),
		[35]=v2fM(0,0909090909090909,0,428571428571429),
		[36]=v2fM(0,181818181818182,0,428571428571429),
		[37]=v2fM(0,272727272727273,0,428571428571429),
		[38]=v2fM(0,363636363636364,0,428571428571429),
		[39]=v2fM(0,454545454545455,0,428571428571429),
		[40]=v2fM(0,545454545454545,0,428571428571429),
		[41]=v2fM(0,636363636363636,0,428571428571429),
		[42]=v2fM(0,727272727272727,0,428571428571429),
		[43]=v2fM(0,818181818181818,0,428571428571429),
		[44]=v2fM(0,909090909090909,0,428571428571429),
		[45]=v2fM(0,0,571428571428571),
		[46]=v2fM(0,0909090909090909,0,571428571428571),
		[47]=v2fM(0,181818181818182,0,571428571428571),
		[48]=v2fM(0,272727272727273,0,571428571428571),
		[49]=v2fM(0,363636363636364,0,571428571428571),
		[50]=v2fM(0,454545454545455,0,571428571428571),
		[51]=v2fM(0,545454545454545,0,571428571428571),
		[52]=v2fM(0,636363636363636,0,571428571428571),
		[53]=v2fM(0,727272727272727,0,571428571428571),
		[54]=v2fM(0,818181818181818,0,571428571428571),
		[55]=v2fM(0,909090909090909,0,571428571428571),
		[56]=v2fM(0,0,714285714285714),
		[57]=v2fM(0,0909090909090909,0,714285714285714),
		[58]=v2fM(0,181818181818182,0,714285714285714),
		[59]=v2fM(0,272727272727273,0,714285714285714),
		[60]=v2fM(0,363636363636364,0,714285714285714),
		[61]=v2fM(0,454545454545455,0,714285714285714),
		[62]=v2fM(0,545454545454545,0,714285714285714),
		[63]=v2fM(0,636363636363636,0,714285714285714),
		[64]=v2fM(0,727272727272727,0,714285714285714),
		[65]=v2fM(0,818181818181818,0,714285714285714),
		[66]=v2fM(0,909090909090909,0,714285714285714),
		[67]=v2fM(0,0,857142857142857),
		[68]=v2fM(0,0909090909090909,0,857142857142857),
		[69]=v2fM(0,181818181818182,0,857142857142857),
		[70]=v2fM(0,272727272727273,0,857142857142857),
		[71]=v2fM(0,363636363636364,0,857142857142857),
		[72]=v2fM(0,454545454545455,0,857142857142857),
	},
	FRAME_SIZES={
		[1]={WIDTH=86,HEIGHT=112},
		[2]={WIDTH=86,HEIGHT=112},
		[3]={WIDTH=86,HEIGHT=112},
		[4]={WIDTH=86,HEIGHT=112},
		[5]={WIDTH=86,HEIGHT=112},
		[6]={WIDTH=86,HEIGHT=112},
		[7]={WIDTH=86,HEIGHT=112},
		[8]={WIDTH=86,HEIGHT=112},
		[9]={WIDTH=86,HEIGHT=112},
		[10]={WIDTH=86,HEIGHT=112},
		[11]={WIDTH=86,HEIGHT=112},
		[12]={WIDTH=86,HEIGHT=112},
		[13]={WIDTH=86,HEIGHT=112},
		[14]={WIDTH=86,HEIGHT=112},
		[15]={WIDTH=86,HEIGHT=112},
		[16]={WIDTH=86,HEIGHT=112},
		[17]={WIDTH=86,HEIGHT=112},
		[18]={WIDTH=86,HEIGHT=112},
		[19]={WIDTH=86,HEIGHT=112},
		[20]={WIDTH=86,HEIGHT=112},
		[21]={WIDTH=86,HEIGHT=112},
		[22]={WIDTH=86,HEIGHT=112},
		[23]={WIDTH=86,HEIGHT=112},
		[24]={WIDTH=86,HEIGHT=112},
		[25]={WIDTH=86,HEIGHT=112},
		[26]={WIDTH=86,HEIGHT=112},
		[27]={WIDTH=86,HEIGHT=112},
		[28]={WIDTH=86,HEIGHT=112},
		[29]={WIDTH=86,HEIGHT=112},
		[30]={WIDTH=86,HEIGHT=112},
		[31]={WIDTH=86,HEIGHT=112},
		[32]={WIDTH=86,HEIGHT=112},
		[33]={WIDTH=86,HEIGHT=112},
		[34]={WIDTH=86,HEIGHT=112},
		[35]={WIDTH=86,HEIGHT=112},
		[36]={WIDTH=86,HEIGHT=112},
		[37]={WIDTH=86,HEIGHT=112},
		[38]={WIDTH=86,HEIGHT=112},
		[39]={WIDTH=86,HEIGHT=112},
		[40]={WIDTH=86,HEIGHT=112},
		[41]={WIDTH=86,HEIGHT=112},
		[42]={WIDTH=86,HEIGHT=112},
		[43]={WIDTH=86,HEIGHT=112},
		[44]={WIDTH=86,HEIGHT=112},
		[45]={WIDTH=86,HEIGHT=112},
		[46]={WIDTH=86,HEIGHT=112},
		[47]={WIDTH=86,HEIGHT=112},
		[48]={WIDTH=86,HEIGHT=112},
		[49]={WIDTH=86,HEIGHT=112},
		[50]={WIDTH=86,HEIGHT=112},
		[51]={WIDTH=86,HEIGHT=112},
		[52]={WIDTH=86,HEIGHT=112},
		[53]={WIDTH=86,HEIGHT=112},
		[54]={WIDTH=86,HEIGHT=112},
		[55]={WIDTH=86,HEIGHT=112},
		[56]={WIDTH=86,HEIGHT=112},
		[57]={WIDTH=86,HEIGHT=112},
		[58]={WIDTH=86,HEIGHT=112},
		[59]={WIDTH=86,HEIGHT=112},
		[60]={WIDTH=86,HEIGHT=112},
		[61]={WIDTH=86,HEIGHT=112},
		[62]={WIDTH=86,HEIGHT=112},
		[63]={WIDTH=86,HEIGHT=112},
		[64]={WIDTH=86,HEIGHT=112},
		[65]={WIDTH=86,HEIGHT=112},
		[66]={WIDTH=86,HEIGHT=112},
		[67]={WIDTH=86,HEIGHT=112},
		[68]={WIDTH=86,HEIGHT=112},
		[69]={WIDTH=86,HEIGHT=112},
		[70]={WIDTH=86,HEIGHT=112},
		[71]={WIDTH=86,HEIGHT=112},
		[72]={WIDTH=86,HEIGHT=112},
	},
	FRAME_INDEXS={
		['e-a-ht-solar0047z']=1,
		['e-a-ht-solar0048z']=2,
		['e-a-ht-solar0045z']=3,
		['e-a-ht-solar0046z']=4,
		['e-a-ht-solar0049z']=5,
		['e-a-ht-solar0052z']=6,
		['e-a-ht-solar0053z']=7,
		['e-a-ht-solar0050z']=8,
		['e-a-ht-solar0051z']=9,
		['e-a-ht-solar0038z']=10,
		['e-a-ht-solar0039z']=11,
		['e-a-ht-solar0036z']=12,
		['e-a-ht-solar0037z']=13,
		['e-a-ht-solar0040z']=14,
		['e-a-ht-solar0043z']=15,
		['e-a-ht-solar0044z']=16,
		['e-a-ht-solar0041z']=17,
		['e-a-ht-solar0042z']=18,
		['e-a-ht-solar0065z']=19,
		['e-a-ht-solar0066z']=20,
		['e-a-ht-solar0063z']=21,
		['e-a-ht-solar0064z']=22,
		['e-a-ht-solar0067z']=23,
		['e-a-ht-solar0070z']=24,
		['e-a-ht-solar0071z']=25,
		['e-a-ht-solar0068z']=26,
		['e-a-ht-solar0069z']=27,
		['e-a-ht-solar0056z']=28,
		['e-a-ht-solar0057z']=29,
		['e-a-ht-solar0054z']=30,
		['e-a-ht-solar0055z']=31,
		['e-a-ht-solar0058z']=32,
		['e-a-ht-solar0061z']=33,
		['e-a-ht-solar0062z']=34,
		['e-a-ht-solar0059z']=35,
		['e-a-ht-solar0060z']=36,
		['e-a-ht-solar0011z']=37,
		['e-a-ht-solar0012z']=38,
		['e-a-ht-solar0009z']=39,
		['e-a-ht-solar0010z']=40,
		['e-a-ht-solar0013z']=41,
		['e-a-ht-solar0016z']=42,
		['e-a-ht-solar0017z']=43,
		['e-a-ht-solar0014z']=44,
		['e-a-ht-solar0015z']=45,
		['e-a-ht-solar0002z']=46,
		['e-a-ht-solar0003z']=47,
		['e-a-ht-solar0000z']=48,
		['e-a-ht-solar0001z']=49,
		['e-a-ht-solar0004z']=50,
		['e-a-ht-solar0007z']=51,
		['e-a-ht-solar0008z']=52,
		['e-a-ht-solar0005z']=53,
		['e-a-ht-solar0006z']=54,
		['e-a-ht-solar0029z']=55,
		['e-a-ht-solar0030z']=56,
		['e-a-ht-solar0027z']=57,
		['e-a-ht-solar0028z']=58,
		['e-a-ht-solar0031z']=59,
		['e-a-ht-solar0034z']=60,
		['e-a-ht-solar0035z']=61,
		['e-a-ht-solar0032z']=62,
		['e-a-ht-solar0033z']=63,
		['e-a-ht-solar0020z']=64,
		['e-a-ht-solar0021z']=65,
		['e-a-ht-solar0018z']=66,
		['e-a-ht-solar0019z']=67,
		['e-a-ht-solar0022z']=68,
		['e-a-ht-solar0025z']=69,
		['e-a-ht-solar0026z']=70,
		['e-a-ht-solar0023z']=71,
		['e-a-ht-solar0024z']=72,
	},
};
