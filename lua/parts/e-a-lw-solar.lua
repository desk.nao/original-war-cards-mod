--GENERATED ON: 05/12/2024 06:59:24


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['e-a-lw-solar'] = {
	TEX_WIDTH=984,
	TEX_HEIGHT=600,
	FRAME_COUNT=72,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0833333333333333,0),
		[3]=v2fM(0,166666666666667,0),
		[4]=v2fM(0,25,0),
		[5]=v2fM(0,333333333333333,0),
		[6]=v2fM(0,416666666666667,0),
		[7]=v2fM(0,5,0),
		[8]=v2fM(0,583333333333333,0),
		[9]=v2fM(0,666666666666667,0),
		[10]=v2fM(0,75,0),
		[11]=v2fM(0,833333333333333,0),
		[12]=v2fM(0,916666666666667,0),
		[13]=v2fM(0,0,166666666666667),
		[14]=v2fM(0,0833333333333333,0,166666666666667),
		[15]=v2fM(0,166666666666667,0,166666666666667),
		[16]=v2fM(0,25,0,166666666666667),
		[17]=v2fM(0,333333333333333,0,166666666666667),
		[18]=v2fM(0,416666666666667,0,166666666666667),
		[19]=v2fM(0,5,0,166666666666667),
		[20]=v2fM(0,583333333333333,0,166666666666667),
		[21]=v2fM(0,666666666666667,0,166666666666667),
		[22]=v2fM(0,75,0,166666666666667),
		[23]=v2fM(0,833333333333333,0,166666666666667),
		[24]=v2fM(0,916666666666667,0,166666666666667),
		[25]=v2fM(0,0,333333333333333),
		[26]=v2fM(0,0833333333333333,0,333333333333333),
		[27]=v2fM(0,166666666666667,0,333333333333333),
		[28]=v2fM(0,25,0,333333333333333),
		[29]=v2fM(0,333333333333333,0,333333333333333),
		[30]=v2fM(0,416666666666667,0,333333333333333),
		[31]=v2fM(0,5,0,333333333333333),
		[32]=v2fM(0,583333333333333,0,333333333333333),
		[33]=v2fM(0,666666666666667,0,333333333333333),
		[34]=v2fM(0,75,0,333333333333333),
		[35]=v2fM(0,833333333333333,0,333333333333333),
		[36]=v2fM(0,916666666666667,0,333333333333333),
		[37]=v2fM(0,0,5),
		[38]=v2fM(0,0833333333333333,0,5),
		[39]=v2fM(0,166666666666667,0,5),
		[40]=v2fM(0,25,0,5),
		[41]=v2fM(0,333333333333333,0,5),
		[42]=v2fM(0,416666666666667,0,5),
		[43]=v2fM(0,5,0,5),
		[44]=v2fM(0,583333333333333,0,5),
		[45]=v2fM(0,666666666666667,0,5),
		[46]=v2fM(0,75,0,5),
		[47]=v2fM(0,833333333333333,0,5),
		[48]=v2fM(0,916666666666667,0,5),
		[49]=v2fM(0,0,666666666666667),
		[50]=v2fM(0,0833333333333333,0,666666666666667),
		[51]=v2fM(0,166666666666667,0,666666666666667),
		[52]=v2fM(0,25,0,666666666666667),
		[53]=v2fM(0,333333333333333,0,666666666666667),
		[54]=v2fM(0,416666666666667,0,666666666666667),
		[55]=v2fM(0,5,0,666666666666667),
		[56]=v2fM(0,583333333333333,0,666666666666667),
		[57]=v2fM(0,666666666666667,0,666666666666667),
		[58]=v2fM(0,75,0,666666666666667),
		[59]=v2fM(0,833333333333333,0,666666666666667),
		[60]=v2fM(0,916666666666667,0,666666666666667),
		[61]=v2fM(0,0,833333333333333),
		[62]=v2fM(0,0833333333333333,0,833333333333333),
		[63]=v2fM(0,166666666666667,0,833333333333333),
		[64]=v2fM(0,25,0,833333333333333),
		[65]=v2fM(0,333333333333333,0,833333333333333),
		[66]=v2fM(0,416666666666667,0,833333333333333),
		[67]=v2fM(0,5,0,833333333333333),
		[68]=v2fM(0,583333333333333,0,833333333333333),
		[69]=v2fM(0,666666666666667,0,833333333333333),
		[70]=v2fM(0,75,0,833333333333333),
		[71]=v2fM(0,833333333333333,0,833333333333333),
		[72]=v2fM(0,916666666666667,0,833333333333333),
	},
	FRAME_SIZES={
		[1]={WIDTH=82,HEIGHT=100},
		[2]={WIDTH=82,HEIGHT=100},
		[3]={WIDTH=82,HEIGHT=100},
		[4]={WIDTH=82,HEIGHT=100},
		[5]={WIDTH=82,HEIGHT=100},
		[6]={WIDTH=82,HEIGHT=100},
		[7]={WIDTH=82,HEIGHT=100},
		[8]={WIDTH=82,HEIGHT=100},
		[9]={WIDTH=82,HEIGHT=100},
		[10]={WIDTH=82,HEIGHT=100},
		[11]={WIDTH=82,HEIGHT=100},
		[12]={WIDTH=82,HEIGHT=100},
		[13]={WIDTH=82,HEIGHT=100},
		[14]={WIDTH=82,HEIGHT=100},
		[15]={WIDTH=82,HEIGHT=100},
		[16]={WIDTH=82,HEIGHT=100},
		[17]={WIDTH=82,HEIGHT=100},
		[18]={WIDTH=82,HEIGHT=100},
		[19]={WIDTH=82,HEIGHT=100},
		[20]={WIDTH=82,HEIGHT=100},
		[21]={WIDTH=82,HEIGHT=100},
		[22]={WIDTH=82,HEIGHT=100},
		[23]={WIDTH=82,HEIGHT=100},
		[24]={WIDTH=82,HEIGHT=100},
		[25]={WIDTH=82,HEIGHT=100},
		[26]={WIDTH=82,HEIGHT=100},
		[27]={WIDTH=82,HEIGHT=100},
		[28]={WIDTH=82,HEIGHT=100},
		[29]={WIDTH=82,HEIGHT=100},
		[30]={WIDTH=82,HEIGHT=100},
		[31]={WIDTH=82,HEIGHT=100},
		[32]={WIDTH=82,HEIGHT=100},
		[33]={WIDTH=82,HEIGHT=100},
		[34]={WIDTH=82,HEIGHT=100},
		[35]={WIDTH=82,HEIGHT=100},
		[36]={WIDTH=82,HEIGHT=100},
		[37]={WIDTH=82,HEIGHT=100},
		[38]={WIDTH=82,HEIGHT=100},
		[39]={WIDTH=82,HEIGHT=100},
		[40]={WIDTH=82,HEIGHT=100},
		[41]={WIDTH=82,HEIGHT=100},
		[42]={WIDTH=82,HEIGHT=100},
		[43]={WIDTH=82,HEIGHT=100},
		[44]={WIDTH=82,HEIGHT=100},
		[45]={WIDTH=82,HEIGHT=100},
		[46]={WIDTH=82,HEIGHT=100},
		[47]={WIDTH=82,HEIGHT=100},
		[48]={WIDTH=82,HEIGHT=100},
		[49]={WIDTH=82,HEIGHT=100},
		[50]={WIDTH=82,HEIGHT=100},
		[51]={WIDTH=82,HEIGHT=100},
		[52]={WIDTH=82,HEIGHT=100},
		[53]={WIDTH=82,HEIGHT=100},
		[54]={WIDTH=82,HEIGHT=100},
		[55]={WIDTH=82,HEIGHT=100},
		[56]={WIDTH=82,HEIGHT=100},
		[57]={WIDTH=82,HEIGHT=100},
		[58]={WIDTH=82,HEIGHT=100},
		[59]={WIDTH=82,HEIGHT=100},
		[60]={WIDTH=82,HEIGHT=100},
		[61]={WIDTH=82,HEIGHT=100},
		[62]={WIDTH=82,HEIGHT=100},
		[63]={WIDTH=82,HEIGHT=100},
		[64]={WIDTH=82,HEIGHT=100},
		[65]={WIDTH=82,HEIGHT=100},
		[66]={WIDTH=82,HEIGHT=100},
		[67]={WIDTH=82,HEIGHT=100},
		[68]={WIDTH=82,HEIGHT=100},
		[69]={WIDTH=82,HEIGHT=100},
		[70]={WIDTH=82,HEIGHT=100},
		[71]={WIDTH=82,HEIGHT=100},
		[72]={WIDTH=82,HEIGHT=100},
	},
	FRAME_INDEXS={
		['e-a-lw-solar0047']=1,
		['e-a-lw-solar0048']=2,
		['e-a-lw-solar0045']=3,
		['e-a-lw-solar0046']=4,
		['e-a-lw-solar0049']=5,
		['e-a-lw-solar0052']=6,
		['e-a-lw-solar0053']=7,
		['e-a-lw-solar0050']=8,
		['e-a-lw-solar0051']=9,
		['e-a-lw-solar0038']=10,
		['e-a-lw-solar0039']=11,
		['e-a-lw-solar0036']=12,
		['e-a-lw-solar0037']=13,
		['e-a-lw-solar0040']=14,
		['e-a-lw-solar0043']=15,
		['e-a-lw-solar0044']=16,
		['e-a-lw-solar0041']=17,
		['e-a-lw-solar0042']=18,
		['e-a-lw-solar0065']=19,
		['e-a-lw-solar0066']=20,
		['e-a-lw-solar0063']=21,
		['e-a-lw-solar0064']=22,
		['e-a-lw-solar0067']=23,
		['e-a-lw-solar0070']=24,
		['e-a-lw-solar0071']=25,
		['e-a-lw-solar0068']=26,
		['e-a-lw-solar0069']=27,
		['e-a-lw-solar0056']=28,
		['e-a-lw-solar0057']=29,
		['e-a-lw-solar0054']=30,
		['e-a-lw-solar0055']=31,
		['e-a-lw-solar0058']=32,
		['e-a-lw-solar0061']=33,
		['e-a-lw-solar0062']=34,
		['e-a-lw-solar0059']=35,
		['e-a-lw-solar0060']=36,
		['e-a-lw-solar0011']=37,
		['e-a-lw-solar0012']=38,
		['e-a-lw-solar0009']=39,
		['e-a-lw-solar0010']=40,
		['e-a-lw-solar0013']=41,
		['e-a-lw-solar0016']=42,
		['e-a-lw-solar0017']=43,
		['e-a-lw-solar0014']=44,
		['e-a-lw-solar0015']=45,
		['e-a-lw-solar0002']=46,
		['e-a-lw-solar0003']=47,
		['e-a-lw-solar0000']=48,
		['e-a-lw-solar0001']=49,
		['e-a-lw-solar0004']=50,
		['e-a-lw-solar0007']=51,
		['e-a-lw-solar0008']=52,
		['e-a-lw-solar0005']=53,
		['e-a-lw-solar0006']=54,
		['e-a-lw-solar0029']=55,
		['e-a-lw-solar0030']=56,
		['e-a-lw-solar0027']=57,
		['e-a-lw-solar0028']=58,
		['e-a-lw-solar0031']=59,
		['e-a-lw-solar0034']=60,
		['e-a-lw-solar0035']=61,
		['e-a-lw-solar0032']=62,
		['e-a-lw-solar0033']=63,
		['e-a-lw-solar0020']=64,
		['e-a-lw-solar0021']=65,
		['e-a-lw-solar0018']=66,
		['e-a-lw-solar0019']=67,
		['e-a-lw-solar0022']=68,
		['e-a-lw-solar0025']=69,
		['e-a-lw-solar0026']=70,
		['e-a-lw-solar0023']=71,
		['e-a-lw-solar0024']=72,
	},
};
