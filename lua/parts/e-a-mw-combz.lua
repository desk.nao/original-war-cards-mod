--GENERATED ON: 05/12/2024 06:59:25


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['e-a-mw-combz'] = {
	TEX_WIDTH=960,
	TEX_HEIGHT=636,
	FRAME_COUNT=72,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0833333333333333,0),
		[3]=v2fM(0,166666666666667,0),
		[4]=v2fM(0,25,0),
		[5]=v2fM(0,333333333333333,0),
		[6]=v2fM(0,416666666666667,0),
		[7]=v2fM(0,5,0),
		[8]=v2fM(0,583333333333333,0),
		[9]=v2fM(0,666666666666667,0),
		[10]=v2fM(0,75,0),
		[11]=v2fM(0,833333333333333,0),
		[12]=v2fM(0,916666666666667,0),
		[13]=v2fM(0,0,166666666666667),
		[14]=v2fM(0,0833333333333333,0,166666666666667),
		[15]=v2fM(0,166666666666667,0,166666666666667),
		[16]=v2fM(0,25,0,166666666666667),
		[17]=v2fM(0,333333333333333,0,166666666666667),
		[18]=v2fM(0,416666666666667,0,166666666666667),
		[19]=v2fM(0,5,0,166666666666667),
		[20]=v2fM(0,583333333333333,0,166666666666667),
		[21]=v2fM(0,666666666666667,0,166666666666667),
		[22]=v2fM(0,75,0,166666666666667),
		[23]=v2fM(0,833333333333333,0,166666666666667),
		[24]=v2fM(0,916666666666667,0,166666666666667),
		[25]=v2fM(0,0,333333333333333),
		[26]=v2fM(0,0833333333333333,0,333333333333333),
		[27]=v2fM(0,166666666666667,0,333333333333333),
		[28]=v2fM(0,25,0,333333333333333),
		[29]=v2fM(0,333333333333333,0,333333333333333),
		[30]=v2fM(0,416666666666667,0,333333333333333),
		[31]=v2fM(0,5,0,333333333333333),
		[32]=v2fM(0,583333333333333,0,333333333333333),
		[33]=v2fM(0,666666666666667,0,333333333333333),
		[34]=v2fM(0,75,0,333333333333333),
		[35]=v2fM(0,833333333333333,0,333333333333333),
		[36]=v2fM(0,916666666666667,0,333333333333333),
		[37]=v2fM(0,0,5),
		[38]=v2fM(0,0833333333333333,0,5),
		[39]=v2fM(0,166666666666667,0,5),
		[40]=v2fM(0,25,0,5),
		[41]=v2fM(0,333333333333333,0,5),
		[42]=v2fM(0,416666666666667,0,5),
		[43]=v2fM(0,5,0,5),
		[44]=v2fM(0,583333333333333,0,5),
		[45]=v2fM(0,666666666666667,0,5),
		[46]=v2fM(0,75,0,5),
		[47]=v2fM(0,833333333333333,0,5),
		[48]=v2fM(0,916666666666667,0,5),
		[49]=v2fM(0,0,666666666666667),
		[50]=v2fM(0,0833333333333333,0,666666666666667),
		[51]=v2fM(0,166666666666667,0,666666666666667),
		[52]=v2fM(0,25,0,666666666666667),
		[53]=v2fM(0,333333333333333,0,666666666666667),
		[54]=v2fM(0,416666666666667,0,666666666666667),
		[55]=v2fM(0,5,0,666666666666667),
		[56]=v2fM(0,583333333333333,0,666666666666667),
		[57]=v2fM(0,666666666666667,0,666666666666667),
		[58]=v2fM(0,75,0,666666666666667),
		[59]=v2fM(0,833333333333333,0,666666666666667),
		[60]=v2fM(0,916666666666667,0,666666666666667),
		[61]=v2fM(0,0,833333333333333),
		[62]=v2fM(0,0833333333333333,0,833333333333333),
		[63]=v2fM(0,166666666666667,0,833333333333333),
		[64]=v2fM(0,25,0,833333333333333),
		[65]=v2fM(0,333333333333333,0,833333333333333),
		[66]=v2fM(0,416666666666667,0,833333333333333),
		[67]=v2fM(0,5,0,833333333333333),
		[68]=v2fM(0,583333333333333,0,833333333333333),
		[69]=v2fM(0,666666666666667,0,833333333333333),
		[70]=v2fM(0,75,0,833333333333333),
		[71]=v2fM(0,833333333333333,0,833333333333333),
		[72]=v2fM(0,916666666666667,0,833333333333333),
	},
	FRAME_SIZES={
		[1]={WIDTH=80,HEIGHT=106},
		[2]={WIDTH=80,HEIGHT=106},
		[3]={WIDTH=80,HEIGHT=106},
		[4]={WIDTH=80,HEIGHT=106},
		[5]={WIDTH=80,HEIGHT=106},
		[6]={WIDTH=80,HEIGHT=106},
		[7]={WIDTH=80,HEIGHT=106},
		[8]={WIDTH=80,HEIGHT=106},
		[9]={WIDTH=80,HEIGHT=106},
		[10]={WIDTH=80,HEIGHT=106},
		[11]={WIDTH=80,HEIGHT=106},
		[12]={WIDTH=80,HEIGHT=106},
		[13]={WIDTH=80,HEIGHT=106},
		[14]={WIDTH=80,HEIGHT=106},
		[15]={WIDTH=80,HEIGHT=106},
		[16]={WIDTH=80,HEIGHT=106},
		[17]={WIDTH=80,HEIGHT=106},
		[18]={WIDTH=80,HEIGHT=106},
		[19]={WIDTH=80,HEIGHT=106},
		[20]={WIDTH=80,HEIGHT=106},
		[21]={WIDTH=80,HEIGHT=106},
		[22]={WIDTH=80,HEIGHT=106},
		[23]={WIDTH=80,HEIGHT=106},
		[24]={WIDTH=80,HEIGHT=106},
		[25]={WIDTH=80,HEIGHT=106},
		[26]={WIDTH=80,HEIGHT=106},
		[27]={WIDTH=80,HEIGHT=106},
		[28]={WIDTH=80,HEIGHT=106},
		[29]={WIDTH=80,HEIGHT=106},
		[30]={WIDTH=80,HEIGHT=106},
		[31]={WIDTH=80,HEIGHT=106},
		[32]={WIDTH=80,HEIGHT=106},
		[33]={WIDTH=80,HEIGHT=106},
		[34]={WIDTH=80,HEIGHT=106},
		[35]={WIDTH=80,HEIGHT=106},
		[36]={WIDTH=80,HEIGHT=106},
		[37]={WIDTH=80,HEIGHT=106},
		[38]={WIDTH=80,HEIGHT=106},
		[39]={WIDTH=80,HEIGHT=106},
		[40]={WIDTH=80,HEIGHT=106},
		[41]={WIDTH=80,HEIGHT=106},
		[42]={WIDTH=80,HEIGHT=106},
		[43]={WIDTH=80,HEIGHT=106},
		[44]={WIDTH=80,HEIGHT=106},
		[45]={WIDTH=80,HEIGHT=106},
		[46]={WIDTH=80,HEIGHT=106},
		[47]={WIDTH=80,HEIGHT=106},
		[48]={WIDTH=80,HEIGHT=106},
		[49]={WIDTH=80,HEIGHT=106},
		[50]={WIDTH=80,HEIGHT=106},
		[51]={WIDTH=80,HEIGHT=106},
		[52]={WIDTH=80,HEIGHT=106},
		[53]={WIDTH=80,HEIGHT=106},
		[54]={WIDTH=80,HEIGHT=106},
		[55]={WIDTH=80,HEIGHT=106},
		[56]={WIDTH=80,HEIGHT=106},
		[57]={WIDTH=80,HEIGHT=106},
		[58]={WIDTH=80,HEIGHT=106},
		[59]={WIDTH=80,HEIGHT=106},
		[60]={WIDTH=80,HEIGHT=106},
		[61]={WIDTH=80,HEIGHT=106},
		[62]={WIDTH=80,HEIGHT=106},
		[63]={WIDTH=80,HEIGHT=106},
		[64]={WIDTH=80,HEIGHT=106},
		[65]={WIDTH=80,HEIGHT=106},
		[66]={WIDTH=80,HEIGHT=106},
		[67]={WIDTH=80,HEIGHT=106},
		[68]={WIDTH=80,HEIGHT=106},
		[69]={WIDTH=80,HEIGHT=106},
		[70]={WIDTH=80,HEIGHT=106},
		[71]={WIDTH=80,HEIGHT=106},
		[72]={WIDTH=80,HEIGHT=106},
	},
	FRAME_INDEXS={
		['e-a-mw-comb0047z']=1,
		['e-a-mw-comb0048z']=2,
		['e-a-mw-comb0045z']=3,
		['e-a-mw-comb0046z']=4,
		['e-a-mw-comb0049z']=5,
		['e-a-mw-comb0052z']=6,
		['e-a-mw-comb0053z']=7,
		['e-a-mw-comb0050z']=8,
		['e-a-mw-comb0051z']=9,
		['e-a-mw-comb0038z']=10,
		['e-a-mw-comb0039z']=11,
		['e-a-mw-comb0036z']=12,
		['e-a-mw-comb0037z']=13,
		['e-a-mw-comb0040z']=14,
		['e-a-mw-comb0043z']=15,
		['e-a-mw-comb0044z']=16,
		['e-a-mw-comb0041z']=17,
		['e-a-mw-comb0042z']=18,
		['e-a-mw-comb0065z']=19,
		['e-a-mw-comb0066z']=20,
		['e-a-mw-comb0063z']=21,
		['e-a-mw-comb0064z']=22,
		['e-a-mw-comb0067z']=23,
		['e-a-mw-comb0070z']=24,
		['e-a-mw-comb0071z']=25,
		['e-a-mw-comb0068z']=26,
		['e-a-mw-comb0069z']=27,
		['e-a-mw-comb0056z']=28,
		['e-a-mw-comb0057z']=29,
		['e-a-mw-comb0054z']=30,
		['e-a-mw-comb0055z']=31,
		['e-a-mw-comb0058z']=32,
		['e-a-mw-comb0061z']=33,
		['e-a-mw-comb0062z']=34,
		['e-a-mw-comb0059z']=35,
		['e-a-mw-comb0060z']=36,
		['e-a-mw-comb0011z']=37,
		['e-a-mw-comb0012z']=38,
		['e-a-mw-comb0009z']=39,
		['e-a-mw-comb0010z']=40,
		['e-a-mw-comb0013z']=41,
		['e-a-mw-comb0016z']=42,
		['e-a-mw-comb0017z']=43,
		['e-a-mw-comb0014z']=44,
		['e-a-mw-comb0015z']=45,
		['e-a-mw-comb0002z']=46,
		['e-a-mw-comb0003z']=47,
		['e-a-mw-comb0000z']=48,
		['e-a-mw-comb0001z']=49,
		['e-a-mw-comb0004z']=50,
		['e-a-mw-comb0007z']=51,
		['e-a-mw-comb0008z']=52,
		['e-a-mw-comb0005z']=53,
		['e-a-mw-comb0006z']=54,
		['e-a-mw-comb0029z']=55,
		['e-a-mw-comb0030z']=56,
		['e-a-mw-comb0027z']=57,
		['e-a-mw-comb0028z']=58,
		['e-a-mw-comb0031z']=59,
		['e-a-mw-comb0034z']=60,
		['e-a-mw-comb0035z']=61,
		['e-a-mw-comb0032z']=62,
		['e-a-mw-comb0033z']=63,
		['e-a-mw-comb0020z']=64,
		['e-a-mw-comb0021z']=65,
		['e-a-mw-comb0018z']=66,
		['e-a-mw-comb0019z']=67,
		['e-a-mw-comb0022z']=68,
		['e-a-mw-comb0025z']=69,
		['e-a-mw-comb0026z']=70,
		['e-a-mw-comb0023z']=71,
		['e-a-mw-comb0024z']=72,
	},
};
