--GENERATED ON: 05/12/2024 06:59:36


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['e-r-heavyw-siberz'] = {
	TEX_WIDTH=4064,
	TEX_HEIGHT=2044,
	FRAME_COUNT=216,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0354330708661417,0),
		[3]=v2fM(0,0708661417322835,0),
		[4]=v2fM(0,106299212598425,0),
		[5]=v2fM(0,141732283464567,0),
		[6]=v2fM(0,177165354330709,0),
		[7]=v2fM(0,21259842519685,0),
		[8]=v2fM(0,0,0714285714285714),
		[9]=v2fM(0,0354330708661417,0,0714285714285714),
		[10]=v2fM(0,0708661417322835,0,0714285714285714),
		[11]=v2fM(0,106299212598425,0,0714285714285714),
		[12]=v2fM(0,141732283464567,0,0714285714285714),
		[13]=v2fM(0,177165354330709,0,0714285714285714),
		[14]=v2fM(0,21259842519685,0,0714285714285714),
		[15]=v2fM(0,0,142857142857143),
		[16]=v2fM(0,0354330708661417,0,142857142857143),
		[17]=v2fM(0,0708661417322835,0,142857142857143),
		[18]=v2fM(0,106299212598425,0,142857142857143),
		[19]=v2fM(0,141732283464567,0,142857142857143),
		[20]=v2fM(0,177165354330709,0,142857142857143),
		[21]=v2fM(0,21259842519685,0,142857142857143),
		[22]=v2fM(0,0,214285714285714),
		[23]=v2fM(0,0354330708661417,0,214285714285714),
		[24]=v2fM(0,0708661417322835,0,214285714285714),
		[25]=v2fM(0,106299212598425,0,214285714285714),
		[26]=v2fM(0,141732283464567,0,214285714285714),
		[27]=v2fM(0,177165354330709,0,214285714285714),
		[28]=v2fM(0,21259842519685,0,214285714285714),
		[29]=v2fM(0,0,285714285714286),
		[30]=v2fM(0,0354330708661417,0,285714285714286),
		[31]=v2fM(0,0708661417322835,0,285714285714286),
		[32]=v2fM(0,106299212598425,0,285714285714286),
		[33]=v2fM(0,141732283464567,0,285714285714286),
		[34]=v2fM(0,177165354330709,0,285714285714286),
		[35]=v2fM(0,21259842519685,0,285714285714286),
		[36]=v2fM(0,0,357142857142857),
		[37]=v2fM(0,0354330708661417,0,357142857142857),
		[38]=v2fM(0,0708661417322835,0,357142857142857),
		[39]=v2fM(0,106299212598425,0,357142857142857),
		[40]=v2fM(0,141732283464567,0,357142857142857),
		[41]=v2fM(0,177165354330709,0,357142857142857),
		[42]=v2fM(0,21259842519685,0,357142857142857),
		[43]=v2fM(0,0,428571428571429),
		[44]=v2fM(0,0354330708661417,0,428571428571429),
		[45]=v2fM(0,0708661417322835,0,428571428571429),
		[46]=v2fM(0,106299212598425,0,428571428571429),
		[47]=v2fM(0,141732283464567,0,428571428571429),
		[48]=v2fM(0,177165354330709,0,428571428571429),
		[49]=v2fM(0,21259842519685,0,428571428571429),
		[50]=v2fM(0,251968503937008,0),
		[51]=v2fM(0,28740157480315,0),
		[52]=v2fM(0,322834645669291,0),
		[53]=v2fM(0,358267716535433,0),
		[54]=v2fM(0,393700787401575,0),
		[55]=v2fM(0,429133858267717,0),
		[56]=v2fM(0,464566929133858,0),
		[57]=v2fM(0,251968503937008,0,0714285714285714),
		[58]=v2fM(0,28740157480315,0,0714285714285714),
		[59]=v2fM(0,322834645669291,0,0714285714285714),
		[60]=v2fM(0,358267716535433,0,0714285714285714),
		[61]=v2fM(0,393700787401575,0,0714285714285714),
		[62]=v2fM(0,429133858267717,0,0714285714285714),
		[63]=v2fM(0,464566929133858,0,0714285714285714),
		[64]=v2fM(0,251968503937008,0,142857142857143),
		[65]=v2fM(0,28740157480315,0,142857142857143),
		[66]=v2fM(0,322834645669291,0,142857142857143),
		[67]=v2fM(0,358267716535433,0,142857142857143),
		[68]=v2fM(0,393700787401575,0,142857142857143),
		[69]=v2fM(0,429133858267717,0,142857142857143),
		[70]=v2fM(0,464566929133858,0,142857142857143),
		[71]=v2fM(0,251968503937008,0,214285714285714),
		[72]=v2fM(0,28740157480315,0,214285714285714),
		[73]=v2fM(0,322834645669291,0,214285714285714),
		[74]=v2fM(0,358267716535433,0,214285714285714),
		[75]=v2fM(0,393700787401575,0,214285714285714),
		[76]=v2fM(0,429133858267717,0,214285714285714),
		[77]=v2fM(0,464566929133858,0,214285714285714),
		[78]=v2fM(0,251968503937008,0,285714285714286),
		[79]=v2fM(0,28740157480315,0,285714285714286),
		[80]=v2fM(0,322834645669291,0,285714285714286),
		[81]=v2fM(0,358267716535433,0,285714285714286),
		[82]=v2fM(0,393700787401575,0,285714285714286),
		[83]=v2fM(0,429133858267717,0,285714285714286),
		[84]=v2fM(0,464566929133858,0,285714285714286),
		[85]=v2fM(0,251968503937008,0,357142857142857),
		[86]=v2fM(0,28740157480315,0,357142857142857),
		[87]=v2fM(0,322834645669291,0,357142857142857),
		[88]=v2fM(0,358267716535433,0,357142857142857),
		[89]=v2fM(0,393700787401575,0,357142857142857),
		[90]=v2fM(0,429133858267717,0,357142857142857),
		[91]=v2fM(0,464566929133858,0,357142857142857),
		[92]=v2fM(0,251968503937008,0,428571428571429),
		[93]=v2fM(0,28740157480315,0,428571428571429),
		[94]=v2fM(0,322834645669291,0,428571428571429),
		[95]=v2fM(0,358267716535433,0,428571428571429),
		[96]=v2fM(0,393700787401575,0,428571428571429),
		[97]=v2fM(0,429133858267717,0,428571428571429),
		[98]=v2fM(0,464566929133858,0,428571428571429),
		[99]=v2fM(0,0,5),
		[100]=v2fM(0,0354330708661417,0,5),
		[101]=v2fM(0,0708661417322835,0,5),
		[102]=v2fM(0,106299212598425,0,5),
		[103]=v2fM(0,141732283464567,0,5),
		[104]=v2fM(0,177165354330709,0,5),
		[105]=v2fM(0,21259842519685,0,5),
		[106]=v2fM(0,248031496062992,0,5),
		[107]=v2fM(0,283464566929134,0,5),
		[108]=v2fM(0,318897637795276,0,5),
		[109]=v2fM(0,354330708661417,0,5),
		[110]=v2fM(0,389763779527559,0,5),
		[111]=v2fM(0,425196850393701,0,5),
		[112]=v2fM(0,460629921259843,0,5),
		[113]=v2fM(0,0,571428571428571),
		[114]=v2fM(0,0354330708661417,0,571428571428571),
		[115]=v2fM(0,0708661417322835,0,571428571428571),
		[116]=v2fM(0,106299212598425,0,571428571428571),
		[117]=v2fM(0,141732283464567,0,571428571428571),
		[118]=v2fM(0,177165354330709,0,571428571428571),
		[119]=v2fM(0,21259842519685,0,571428571428571),
		[120]=v2fM(0,248031496062992,0,571428571428571),
		[121]=v2fM(0,283464566929134,0,571428571428571),
		[122]=v2fM(0,318897637795276,0,571428571428571),
		[123]=v2fM(0,354330708661417,0,571428571428571),
		[124]=v2fM(0,389763779527559,0,571428571428571),
		[125]=v2fM(0,425196850393701,0,571428571428571),
		[126]=v2fM(0,460629921259843,0,571428571428571),
		[127]=v2fM(0,0,642857142857143),
		[128]=v2fM(0,0354330708661417,0,642857142857143),
		[129]=v2fM(0,0708661417322835,0,642857142857143),
		[130]=v2fM(0,106299212598425,0,642857142857143),
		[131]=v2fM(0,141732283464567,0,642857142857143),
		[132]=v2fM(0,177165354330709,0,642857142857143),
		[133]=v2fM(0,21259842519685,0,642857142857143),
		[134]=v2fM(0,248031496062992,0,642857142857143),
		[135]=v2fM(0,283464566929134,0,642857142857143),
		[136]=v2fM(0,318897637795276,0,642857142857143),
		[137]=v2fM(0,354330708661417,0,642857142857143),
		[138]=v2fM(0,389763779527559,0,642857142857143),
		[139]=v2fM(0,425196850393701,0,642857142857143),
		[140]=v2fM(0,460629921259843,0,642857142857143),
		[141]=v2fM(0,0,714285714285714),
		[142]=v2fM(0,0354330708661417,0,714285714285714),
		[143]=v2fM(0,0708661417322835,0,714285714285714),
		[144]=v2fM(0,106299212598425,0,714285714285714),
		[145]=v2fM(0,141732283464567,0,714285714285714),
		[146]=v2fM(0,177165354330709,0,714285714285714),
		[147]=v2fM(0,21259842519685,0,714285714285714),
		[148]=v2fM(0,248031496062992,0,714285714285714),
		[149]=v2fM(0,283464566929134,0,714285714285714),
		[150]=v2fM(0,318897637795276,0,714285714285714),
		[151]=v2fM(0,354330708661417,0,714285714285714),
		[152]=v2fM(0,389763779527559,0,714285714285714),
		[153]=v2fM(0,425196850393701,0,714285714285714),
		[154]=v2fM(0,460629921259843,0,714285714285714),
		[155]=v2fM(0,0,785714285714286),
		[156]=v2fM(0,0354330708661417,0,785714285714286),
		[157]=v2fM(0,0708661417322835,0,785714285714286),
		[158]=v2fM(0,106299212598425,0,785714285714286),
		[159]=v2fM(0,141732283464567,0,785714285714286),
		[160]=v2fM(0,177165354330709,0,785714285714286),
		[161]=v2fM(0,21259842519685,0,785714285714286),
		[162]=v2fM(0,248031496062992,0,785714285714286),
		[163]=v2fM(0,283464566929134,0,785714285714286),
		[164]=v2fM(0,318897637795276,0,785714285714286),
		[165]=v2fM(0,354330708661417,0,785714285714286),
		[166]=v2fM(0,389763779527559,0,785714285714286),
		[167]=v2fM(0,425196850393701,0,785714285714286),
		[168]=v2fM(0,460629921259843,0,785714285714286),
		[169]=v2fM(0,0,857142857142857),
		[170]=v2fM(0,0354330708661417,0,857142857142857),
		[171]=v2fM(0,0708661417322835,0,857142857142857),
		[172]=v2fM(0,106299212598425,0,857142857142857),
		[173]=v2fM(0,141732283464567,0,857142857142857),
		[174]=v2fM(0,177165354330709,0,857142857142857),
		[175]=v2fM(0,21259842519685,0,857142857142857),
		[176]=v2fM(0,248031496062992,0,857142857142857),
		[177]=v2fM(0,283464566929134,0,857142857142857),
		[178]=v2fM(0,318897637795276,0,857142857142857),
		[179]=v2fM(0,354330708661417,0,857142857142857),
		[180]=v2fM(0,389763779527559,0,857142857142857),
		[181]=v2fM(0,425196850393701,0,857142857142857),
		[182]=v2fM(0,460629921259843,0,857142857142857),
		[183]=v2fM(0,0,928571428571429),
		[184]=v2fM(0,0354330708661417,0,928571428571429),
		[185]=v2fM(0,0708661417322835,0,928571428571429),
		[186]=v2fM(0,106299212598425,0,928571428571429),
		[187]=v2fM(0,141732283464567,0,928571428571429),
		[188]=v2fM(0,177165354330709,0,928571428571429),
		[189]=v2fM(0,21259842519685,0,928571428571429),
		[190]=v2fM(0,248031496062992,0,928571428571429),
		[191]=v2fM(0,283464566929134,0,928571428571429),
		[192]=v2fM(0,318897637795276,0,928571428571429),
		[193]=v2fM(0,354330708661417,0,928571428571429),
		[194]=v2fM(0,389763779527559,0,928571428571429),
		[195]=v2fM(0,425196850393701,0,928571428571429),
		[196]=v2fM(0,460629921259843,0,928571428571429),
		[197]=v2fM(0,503937007874016,0),
		[198]=v2fM(0,539370078740157,0),
		[199]=v2fM(0,574803149606299,0),
		[200]=v2fM(0,610236220472441,0),
		[201]=v2fM(0,645669291338583,0),
		[202]=v2fM(0,681102362204724,0),
		[203]=v2fM(0,716535433070866,0),
		[204]=v2fM(0,751968503937008,0),
		[205]=v2fM(0,78740157480315,0),
		[206]=v2fM(0,822834645669291,0),
		[207]=v2fM(0,858267716535433,0),
		[208]=v2fM(0,893700787401575,0),
		[209]=v2fM(0,929133858267717,0),
		[210]=v2fM(0,964566929133858,0),
		[211]=v2fM(0,503937007874016,0,0714285714285714),
		[212]=v2fM(0,539370078740157,0,0714285714285714),
		[213]=v2fM(0,574803149606299,0,0714285714285714),
		[214]=v2fM(0,610236220472441,0,0714285714285714),
		[215]=v2fM(0,645669291338583,0,0714285714285714),
		[216]=v2fM(0,681102362204724,0,0714285714285714),
	},
	FRAME_SIZES={
		[1]={WIDTH=144,HEIGHT=146},
		[2]={WIDTH=144,HEIGHT=146},
		[3]={WIDTH=144,HEIGHT=146},
		[4]={WIDTH=144,HEIGHT=146},
		[5]={WIDTH=144,HEIGHT=146},
		[6]={WIDTH=144,HEIGHT=146},
		[7]={WIDTH=144,HEIGHT=146},
		[8]={WIDTH=144,HEIGHT=146},
		[9]={WIDTH=144,HEIGHT=146},
		[10]={WIDTH=144,HEIGHT=146},
		[11]={WIDTH=144,HEIGHT=146},
		[12]={WIDTH=144,HEIGHT=146},
		[13]={WIDTH=144,HEIGHT=146},
		[14]={WIDTH=144,HEIGHT=146},
		[15]={WIDTH=144,HEIGHT=146},
		[16]={WIDTH=144,HEIGHT=146},
		[17]={WIDTH=144,HEIGHT=146},
		[18]={WIDTH=144,HEIGHT=146},
		[19]={WIDTH=144,HEIGHT=146},
		[20]={WIDTH=144,HEIGHT=146},
		[21]={WIDTH=144,HEIGHT=146},
		[22]={WIDTH=144,HEIGHT=146},
		[23]={WIDTH=144,HEIGHT=146},
		[24]={WIDTH=144,HEIGHT=146},
		[25]={WIDTH=144,HEIGHT=146},
		[26]={WIDTH=144,HEIGHT=146},
		[27]={WIDTH=144,HEIGHT=146},
		[28]={WIDTH=144,HEIGHT=146},
		[29]={WIDTH=144,HEIGHT=146},
		[30]={WIDTH=144,HEIGHT=146},
		[31]={WIDTH=144,HEIGHT=146},
		[32]={WIDTH=144,HEIGHT=146},
		[33]={WIDTH=144,HEIGHT=146},
		[34]={WIDTH=144,HEIGHT=146},
		[35]={WIDTH=144,HEIGHT=146},
		[36]={WIDTH=144,HEIGHT=146},
		[37]={WIDTH=144,HEIGHT=146},
		[38]={WIDTH=144,HEIGHT=146},
		[39]={WIDTH=144,HEIGHT=146},
		[40]={WIDTH=144,HEIGHT=146},
		[41]={WIDTH=144,HEIGHT=146},
		[42]={WIDTH=144,HEIGHT=146},
		[43]={WIDTH=144,HEIGHT=146},
		[44]={WIDTH=144,HEIGHT=146},
		[45]={WIDTH=144,HEIGHT=146},
		[46]={WIDTH=144,HEIGHT=146},
		[47]={WIDTH=144,HEIGHT=146},
		[48]={WIDTH=144,HEIGHT=146},
		[49]={WIDTH=144,HEIGHT=146},
		[50]={WIDTH=144,HEIGHT=146},
		[51]={WIDTH=144,HEIGHT=146},
		[52]={WIDTH=144,HEIGHT=146},
		[53]={WIDTH=144,HEIGHT=146},
		[54]={WIDTH=144,HEIGHT=146},
		[55]={WIDTH=144,HEIGHT=146},
		[56]={WIDTH=144,HEIGHT=146},
		[57]={WIDTH=144,HEIGHT=146},
		[58]={WIDTH=144,HEIGHT=146},
		[59]={WIDTH=144,HEIGHT=146},
		[60]={WIDTH=144,HEIGHT=146},
		[61]={WIDTH=144,HEIGHT=146},
		[62]={WIDTH=144,HEIGHT=146},
		[63]={WIDTH=144,HEIGHT=146},
		[64]={WIDTH=144,HEIGHT=146},
		[65]={WIDTH=144,HEIGHT=146},
		[66]={WIDTH=144,HEIGHT=146},
		[67]={WIDTH=144,HEIGHT=146},
		[68]={WIDTH=144,HEIGHT=146},
		[69]={WIDTH=144,HEIGHT=146},
		[70]={WIDTH=144,HEIGHT=146},
		[71]={WIDTH=144,HEIGHT=146},
		[72]={WIDTH=144,HEIGHT=146},
		[73]={WIDTH=144,HEIGHT=146},
		[74]={WIDTH=144,HEIGHT=146},
		[75]={WIDTH=144,HEIGHT=146},
		[76]={WIDTH=144,HEIGHT=146},
		[77]={WIDTH=144,HEIGHT=146},
		[78]={WIDTH=144,HEIGHT=146},
		[79]={WIDTH=144,HEIGHT=146},
		[80]={WIDTH=144,HEIGHT=146},
		[81]={WIDTH=144,HEIGHT=146},
		[82]={WIDTH=144,HEIGHT=146},
		[83]={WIDTH=144,HEIGHT=146},
		[84]={WIDTH=144,HEIGHT=146},
		[85]={WIDTH=144,HEIGHT=146},
		[86]={WIDTH=144,HEIGHT=146},
		[87]={WIDTH=144,HEIGHT=146},
		[88]={WIDTH=144,HEIGHT=146},
		[89]={WIDTH=144,HEIGHT=146},
		[90]={WIDTH=144,HEIGHT=146},
		[91]={WIDTH=144,HEIGHT=146},
		[92]={WIDTH=144,HEIGHT=146},
		[93]={WIDTH=144,HEIGHT=146},
		[94]={WIDTH=144,HEIGHT=146},
		[95]={WIDTH=144,HEIGHT=146},
		[96]={WIDTH=144,HEIGHT=146},
		[97]={WIDTH=144,HEIGHT=146},
		[98]={WIDTH=144,HEIGHT=146},
		[99]={WIDTH=144,HEIGHT=146},
		[100]={WIDTH=144,HEIGHT=146},
		[101]={WIDTH=144,HEIGHT=146},
		[102]={WIDTH=144,HEIGHT=146},
		[103]={WIDTH=144,HEIGHT=146},
		[104]={WIDTH=144,HEIGHT=146},
		[105]={WIDTH=144,HEIGHT=146},
		[106]={WIDTH=144,HEIGHT=146},
		[107]={WIDTH=144,HEIGHT=146},
		[108]={WIDTH=144,HEIGHT=146},
		[109]={WIDTH=144,HEIGHT=146},
		[110]={WIDTH=144,HEIGHT=146},
		[111]={WIDTH=144,HEIGHT=146},
		[112]={WIDTH=144,HEIGHT=146},
		[113]={WIDTH=144,HEIGHT=146},
		[114]={WIDTH=144,HEIGHT=146},
		[115]={WIDTH=144,HEIGHT=146},
		[116]={WIDTH=144,HEIGHT=146},
		[117]={WIDTH=144,HEIGHT=146},
		[118]={WIDTH=144,HEIGHT=146},
		[119]={WIDTH=144,HEIGHT=146},
		[120]={WIDTH=144,HEIGHT=146},
		[121]={WIDTH=144,HEIGHT=146},
		[122]={WIDTH=144,HEIGHT=146},
		[123]={WIDTH=144,HEIGHT=146},
		[124]={WIDTH=144,HEIGHT=146},
		[125]={WIDTH=144,HEIGHT=146},
		[126]={WIDTH=144,HEIGHT=146},
		[127]={WIDTH=144,HEIGHT=146},
		[128]={WIDTH=144,HEIGHT=146},
		[129]={WIDTH=144,HEIGHT=146},
		[130]={WIDTH=144,HEIGHT=146},
		[131]={WIDTH=144,HEIGHT=146},
		[132]={WIDTH=144,HEIGHT=146},
		[133]={WIDTH=144,HEIGHT=146},
		[134]={WIDTH=144,HEIGHT=146},
		[135]={WIDTH=144,HEIGHT=146},
		[136]={WIDTH=144,HEIGHT=146},
		[137]={WIDTH=144,HEIGHT=146},
		[138]={WIDTH=144,HEIGHT=146},
		[139]={WIDTH=144,HEIGHT=146},
		[140]={WIDTH=144,HEIGHT=146},
		[141]={WIDTH=144,HEIGHT=146},
		[142]={WIDTH=144,HEIGHT=146},
		[143]={WIDTH=144,HEIGHT=146},
		[144]={WIDTH=144,HEIGHT=146},
		[145]={WIDTH=144,HEIGHT=146},
		[146]={WIDTH=144,HEIGHT=146},
		[147]={WIDTH=144,HEIGHT=146},
		[148]={WIDTH=144,HEIGHT=146},
		[149]={WIDTH=144,HEIGHT=146},
		[150]={WIDTH=144,HEIGHT=146},
		[151]={WIDTH=144,HEIGHT=146},
		[152]={WIDTH=144,HEIGHT=146},
		[153]={WIDTH=144,HEIGHT=146},
		[154]={WIDTH=144,HEIGHT=146},
		[155]={WIDTH=144,HEIGHT=146},
		[156]={WIDTH=144,HEIGHT=146},
		[157]={WIDTH=144,HEIGHT=146},
		[158]={WIDTH=144,HEIGHT=146},
		[159]={WIDTH=144,HEIGHT=146},
		[160]={WIDTH=144,HEIGHT=146},
		[161]={WIDTH=144,HEIGHT=146},
		[162]={WIDTH=144,HEIGHT=146},
		[163]={WIDTH=144,HEIGHT=146},
		[164]={WIDTH=144,HEIGHT=146},
		[165]={WIDTH=144,HEIGHT=146},
		[166]={WIDTH=144,HEIGHT=146},
		[167]={WIDTH=144,HEIGHT=146},
		[168]={WIDTH=144,HEIGHT=146},
		[169]={WIDTH=144,HEIGHT=146},
		[170]={WIDTH=144,HEIGHT=146},
		[171]={WIDTH=144,HEIGHT=146},
		[172]={WIDTH=144,HEIGHT=146},
		[173]={WIDTH=144,HEIGHT=146},
		[174]={WIDTH=144,HEIGHT=146},
		[175]={WIDTH=144,HEIGHT=146},
		[176]={WIDTH=144,HEIGHT=146},
		[177]={WIDTH=144,HEIGHT=146},
		[178]={WIDTH=144,HEIGHT=146},
		[179]={WIDTH=144,HEIGHT=146},
		[180]={WIDTH=144,HEIGHT=146},
		[181]={WIDTH=144,HEIGHT=146},
		[182]={WIDTH=144,HEIGHT=146},
		[183]={WIDTH=144,HEIGHT=146},
		[184]={WIDTH=144,HEIGHT=146},
		[185]={WIDTH=144,HEIGHT=146},
		[186]={WIDTH=144,HEIGHT=146},
		[187]={WIDTH=144,HEIGHT=146},
		[188]={WIDTH=144,HEIGHT=146},
		[189]={WIDTH=144,HEIGHT=146},
		[190]={WIDTH=144,HEIGHT=146},
		[191]={WIDTH=144,HEIGHT=146},
		[192]={WIDTH=144,HEIGHT=146},
		[193]={WIDTH=144,HEIGHT=146},
		[194]={WIDTH=144,HEIGHT=146},
		[195]={WIDTH=144,HEIGHT=146},
		[196]={WIDTH=144,HEIGHT=146},
		[197]={WIDTH=144,HEIGHT=146},
		[198]={WIDTH=144,HEIGHT=146},
		[199]={WIDTH=144,HEIGHT=146},
		[200]={WIDTH=144,HEIGHT=146},
		[201]={WIDTH=144,HEIGHT=146},
		[202]={WIDTH=144,HEIGHT=146},
		[203]={WIDTH=144,HEIGHT=146},
		[204]={WIDTH=144,HEIGHT=146},
		[205]={WIDTH=144,HEIGHT=146},
		[206]={WIDTH=144,HEIGHT=146},
		[207]={WIDTH=144,HEIGHT=146},
		[208]={WIDTH=144,HEIGHT=146},
		[209]={WIDTH=144,HEIGHT=146},
		[210]={WIDTH=144,HEIGHT=146},
		[211]={WIDTH=144,HEIGHT=146},
		[212]={WIDTH=144,HEIGHT=146},
		[213]={WIDTH=144,HEIGHT=146},
		[214]={WIDTH=144,HEIGHT=146},
		[215]={WIDTH=144,HEIGHT=146},
		[216]={WIDTH=144,HEIGHT=146},
	},
	FRAME_INDEXS={
		['e-r-heavyw-siber0144z']=1,
		['e-r-heavyw-siber0143z']=2,
		['e-r-heavyw-siber0142z']=3,
		['e-r-heavyw-siber0147z']=4,
		['e-r-heavyw-siber0146z']=5,
		['e-r-heavyw-siber0145z']=6,
		['e-r-heavyw-siber0141z']=7,
		['e-r-heavyw-siber0137z']=8,
		['e-r-heavyw-siber0136z']=9,
		['e-r-heavyw-siber0135z']=10,
		['e-r-heavyw-siber0140z']=11,
		['e-r-heavyw-siber0139z']=12,
		['e-r-heavyw-siber0138z']=13,
		['e-r-heavyw-siber0148z']=14,
		['e-r-heavyw-siber0158z']=15,
		['e-r-heavyw-siber0157z']=16,
		['e-r-heavyw-siber0156z']=17,
		['e-r-heavyw-siber0161z']=18,
		['e-r-heavyw-siber0160z']=19,
		['e-r-heavyw-siber0159z']=20,
		['e-r-heavyw-siber0155z']=21,
		['e-r-heavyw-siber0151z']=22,
		['e-r-heavyw-siber0150z']=23,
		['e-r-heavyw-siber0149z']=24,
		['e-r-heavyw-siber0154z']=25,
		['e-r-heavyw-siber0153z']=26,
		['e-r-heavyw-siber0152z']=27,
		['e-r-heavyw-siber0117z']=28,
		['e-r-heavyw-siber0116z']=29,
		['e-r-heavyw-siber0115z']=30,
		['e-r-heavyw-siber0120z']=31,
		['e-r-heavyw-siber0119z']=32,
		['e-r-heavyw-siber0118z']=33,
		['e-r-heavyw-siber0114z']=34,
		['e-r-heavyw-siber0110z']=35,
		['e-r-heavyw-siber0109z']=36,
		['e-r-heavyw-siber0108z']=37,
		['e-r-heavyw-siber0113z']=38,
		['e-r-heavyw-siber0112z']=39,
		['e-r-heavyw-siber0111z']=40,
		['e-r-heavyw-siber0121z']=41,
		['e-r-heavyw-siber0131z']=42,
		['e-r-heavyw-siber0130z']=43,
		['e-r-heavyw-siber0129z']=44,
		['e-r-heavyw-siber0134z']=45,
		['e-r-heavyw-siber0133z']=46,
		['e-r-heavyw-siber0132z']=47,
		['e-r-heavyw-siber0128z']=48,
		['e-r-heavyw-siber0124z']=49,
		['e-r-heavyw-siber0123z']=50,
		['e-r-heavyw-siber0122z']=51,
		['e-r-heavyw-siber0127z']=52,
		['e-r-heavyw-siber0126z']=53,
		['e-r-heavyw-siber0125z']=54,
		['e-r-heavyw-siber0198z']=55,
		['e-r-heavyw-siber0197z']=56,
		['e-r-heavyw-siber0196z']=57,
		['e-r-heavyw-siber0201z']=58,
		['e-r-heavyw-siber0200z']=59,
		['e-r-heavyw-siber0199z']=60,
		['e-r-heavyw-siber0195z']=61,
		['e-r-heavyw-siber0191z']=62,
		['e-r-heavyw-siber0190z']=63,
		['e-r-heavyw-siber0189z']=64,
		['e-r-heavyw-siber0194z']=65,
		['e-r-heavyw-siber0193z']=66,
		['e-r-heavyw-siber0192z']=67,
		['e-r-heavyw-siber0202z']=68,
		['e-r-heavyw-siber0212z']=69,
		['e-r-heavyw-siber0211z']=70,
		['e-r-heavyw-siber0210z']=71,
		['e-r-heavyw-siber0215z']=72,
		['e-r-heavyw-siber0214z']=73,
		['e-r-heavyw-siber0213z']=74,
		['e-r-heavyw-siber0209z']=75,
		['e-r-heavyw-siber0205z']=76,
		['e-r-heavyw-siber0204z']=77,
		['e-r-heavyw-siber0203z']=78,
		['e-r-heavyw-siber0208z']=79,
		['e-r-heavyw-siber0207z']=80,
		['e-r-heavyw-siber0206z']=81,
		['e-r-heavyw-siber0171z']=82,
		['e-r-heavyw-siber0170z']=83,
		['e-r-heavyw-siber0169z']=84,
		['e-r-heavyw-siber0174z']=85,
		['e-r-heavyw-siber0173z']=86,
		['e-r-heavyw-siber0172z']=87,
		['e-r-heavyw-siber0168z']=88,
		['e-r-heavyw-siber0164z']=89,
		['e-r-heavyw-siber0163z']=90,
		['e-r-heavyw-siber0162z']=91,
		['e-r-heavyw-siber0167z']=92,
		['e-r-heavyw-siber0166z']=93,
		['e-r-heavyw-siber0165z']=94,
		['e-r-heavyw-siber0175z']=95,
		['e-r-heavyw-siber0185z']=96,
		['e-r-heavyw-siber0184z']=97,
		['e-r-heavyw-siber0183z']=98,
		['e-r-heavyw-siber0188z']=99,
		['e-r-heavyw-siber0187z']=100,
		['e-r-heavyw-siber0186z']=101,
		['e-r-heavyw-siber0182z']=102,
		['e-r-heavyw-siber0178z']=103,
		['e-r-heavyw-siber0177z']=104,
		['e-r-heavyw-siber0176z']=105,
		['e-r-heavyw-siber0181z']=106,
		['e-r-heavyw-siber0180z']=107,
		['e-r-heavyw-siber0179z']=108,
		['e-r-heavyw-siber0036z']=109,
		['e-r-heavyw-siber0035z']=110,
		['e-r-heavyw-siber0034z']=111,
		['e-r-heavyw-siber0039z']=112,
		['e-r-heavyw-siber0038z']=113,
		['e-r-heavyw-siber0037z']=114,
		['e-r-heavyw-siber0033z']=115,
		['e-r-heavyw-siber0029z']=116,
		['e-r-heavyw-siber0028z']=117,
		['e-r-heavyw-siber0027z']=118,
		['e-r-heavyw-siber0032z']=119,
		['e-r-heavyw-siber0031z']=120,
		['e-r-heavyw-siber0030z']=121,
		['e-r-heavyw-siber0040z']=122,
		['e-r-heavyw-siber0050z']=123,
		['e-r-heavyw-siber0049z']=124,
		['e-r-heavyw-siber0048z']=125,
		['e-r-heavyw-siber0053z']=126,
		['e-r-heavyw-siber0052z']=127,
		['e-r-heavyw-siber0051z']=128,
		['e-r-heavyw-siber0047z']=129,
		['e-r-heavyw-siber0043z']=130,
		['e-r-heavyw-siber0042z']=131,
		['e-r-heavyw-siber0041z']=132,
		['e-r-heavyw-siber0046z']=133,
		['e-r-heavyw-siber0045z']=134,
		['e-r-heavyw-siber0044z']=135,
		['e-r-heavyw-siber0009z']=136,
		['e-r-heavyw-siber0008z']=137,
		['e-r-heavyw-siber0007z']=138,
		['e-r-heavyw-siber0012z']=139,
		['e-r-heavyw-siber0011z']=140,
		['e-r-heavyw-siber0010z']=141,
		['e-r-heavyw-siber0006z']=142,
		['e-r-heavyw-siber0002z']=143,
		['e-r-heavyw-siber0001z']=144,
		['e-r-heavyw-siber0000z']=145,
		['e-r-heavyw-siber0005z']=146,
		['e-r-heavyw-siber0004z']=147,
		['e-r-heavyw-siber0003z']=148,
		['e-r-heavyw-siber0013z']=149,
		['e-r-heavyw-siber0023z']=150,
		['e-r-heavyw-siber0022z']=151,
		['e-r-heavyw-siber0021z']=152,
		['e-r-heavyw-siber0026z']=153,
		['e-r-heavyw-siber0025z']=154,
		['e-r-heavyw-siber0024z']=155,
		['e-r-heavyw-siber0020z']=156,
		['e-r-heavyw-siber0016z']=157,
		['e-r-heavyw-siber0015z']=158,
		['e-r-heavyw-siber0014z']=159,
		['e-r-heavyw-siber0019z']=160,
		['e-r-heavyw-siber0018z']=161,
		['e-r-heavyw-siber0017z']=162,
		['e-r-heavyw-siber0090z']=163,
		['e-r-heavyw-siber0089z']=164,
		['e-r-heavyw-siber0088z']=165,
		['e-r-heavyw-siber0093z']=166,
		['e-r-heavyw-siber0092z']=167,
		['e-r-heavyw-siber0091z']=168,
		['e-r-heavyw-siber0087z']=169,
		['e-r-heavyw-siber0083z']=170,
		['e-r-heavyw-siber0082z']=171,
		['e-r-heavyw-siber0081z']=172,
		['e-r-heavyw-siber0086z']=173,
		['e-r-heavyw-siber0085z']=174,
		['e-r-heavyw-siber0084z']=175,
		['e-r-heavyw-siber0094z']=176,
		['e-r-heavyw-siber0104z']=177,
		['e-r-heavyw-siber0103z']=178,
		['e-r-heavyw-siber0102z']=179,
		['e-r-heavyw-siber0107z']=180,
		['e-r-heavyw-siber0106z']=181,
		['e-r-heavyw-siber0105z']=182,
		['e-r-heavyw-siber0101z']=183,
		['e-r-heavyw-siber0097z']=184,
		['e-r-heavyw-siber0096z']=185,
		['e-r-heavyw-siber0095z']=186,
		['e-r-heavyw-siber0100z']=187,
		['e-r-heavyw-siber0099z']=188,
		['e-r-heavyw-siber0098z']=189,
		['e-r-heavyw-siber0063z']=190,
		['e-r-heavyw-siber0062z']=191,
		['e-r-heavyw-siber0061z']=192,
		['e-r-heavyw-siber0066z']=193,
		['e-r-heavyw-siber0065z']=194,
		['e-r-heavyw-siber0064z']=195,
		['e-r-heavyw-siber0060z']=196,
		['e-r-heavyw-siber0056z']=197,
		['e-r-heavyw-siber0055z']=198,
		['e-r-heavyw-siber0054z']=199,
		['e-r-heavyw-siber0059z']=200,
		['e-r-heavyw-siber0058z']=201,
		['e-r-heavyw-siber0057z']=202,
		['e-r-heavyw-siber0067z']=203,
		['e-r-heavyw-siber0077z']=204,
		['e-r-heavyw-siber0076z']=205,
		['e-r-heavyw-siber0075z']=206,
		['e-r-heavyw-siber0080z']=207,
		['e-r-heavyw-siber0079z']=208,
		['e-r-heavyw-siber0078z']=209,
		['e-r-heavyw-siber0074z']=210,
		['e-r-heavyw-siber0070z']=211,
		['e-r-heavyw-siber0069z']=212,
		['e-r-heavyw-siber0068z']=213,
		['e-r-heavyw-siber0073z']=214,
		['e-r-heavyw-siber0072z']=215,
		['e-r-heavyw-siber0071z']=216,
	},
};
