--GENERATED ON: 05/12/2024 06:59:38


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['e-r-mediumt-siberz'] = {
	TEX_WIDTH=1992,
	TEX_HEIGHT=1020,
	FRAME_COUNT=216,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0441767068273092,0),
		[3]=v2fM(0,0883534136546185,0),
		[4]=v2fM(0,132530120481928,0),
		[5]=v2fM(0,176706827309237,0),
		[6]=v2fM(0,220883534136546,0),
		[7]=v2fM(0,265060240963855,0),
		[8]=v2fM(0,309236947791165,0),
		[9]=v2fM(0,353413654618474,0),
		[10]=v2fM(0,397590361445783,0),
		[11]=v2fM(0,441767068273092,0),
		[12]=v2fM(0,0,1),
		[13]=v2fM(0,0441767068273092,0,1),
		[14]=v2fM(0,0883534136546185,0,1),
		[15]=v2fM(0,132530120481928,0,1),
		[16]=v2fM(0,176706827309237,0,1),
		[17]=v2fM(0,220883534136546,0,1),
		[18]=v2fM(0,265060240963855,0,1),
		[19]=v2fM(0,309236947791165,0,1),
		[20]=v2fM(0,353413654618474,0,1),
		[21]=v2fM(0,397590361445783,0,1),
		[22]=v2fM(0,441767068273092,0,1),
		[23]=v2fM(0,0,2),
		[24]=v2fM(0,0441767068273092,0,2),
		[25]=v2fM(0,0883534136546185,0,2),
		[26]=v2fM(0,132530120481928,0,2),
		[27]=v2fM(0,176706827309237,0,2),
		[28]=v2fM(0,220883534136546,0,2),
		[29]=v2fM(0,265060240963855,0,2),
		[30]=v2fM(0,309236947791165,0,2),
		[31]=v2fM(0,353413654618474,0,2),
		[32]=v2fM(0,397590361445783,0,2),
		[33]=v2fM(0,441767068273092,0,2),
		[34]=v2fM(0,0,3),
		[35]=v2fM(0,0441767068273092,0,3),
		[36]=v2fM(0,0883534136546185,0,3),
		[37]=v2fM(0,132530120481928,0,3),
		[38]=v2fM(0,176706827309237,0,3),
		[39]=v2fM(0,220883534136546,0,3),
		[40]=v2fM(0,265060240963855,0,3),
		[41]=v2fM(0,309236947791165,0,3),
		[42]=v2fM(0,353413654618474,0,3),
		[43]=v2fM(0,397590361445783,0,3),
		[44]=v2fM(0,441767068273092,0,3),
		[45]=v2fM(0,0,4),
		[46]=v2fM(0,0441767068273092,0,4),
		[47]=v2fM(0,0883534136546185,0,4),
		[48]=v2fM(0,132530120481928,0,4),
		[49]=v2fM(0,176706827309237,0,4),
		[50]=v2fM(0,220883534136546,0,4),
		[51]=v2fM(0,265060240963855,0,4),
		[52]=v2fM(0,309236947791165,0,4),
		[53]=v2fM(0,353413654618474,0,4),
		[54]=v2fM(0,397590361445783,0,4),
		[55]=v2fM(0,441767068273092,0,4),
		[56]=v2fM(0,0,5),
		[57]=v2fM(0,0441767068273092,0,5),
		[58]=v2fM(0,0883534136546185,0,5),
		[59]=v2fM(0,132530120481928,0,5),
		[60]=v2fM(0,176706827309237,0,5),
		[61]=v2fM(0,220883534136546,0,5),
		[62]=v2fM(0,265060240963855,0,5),
		[63]=v2fM(0,309236947791165,0,5),
		[64]=v2fM(0,353413654618474,0,5),
		[65]=v2fM(0,397590361445783,0,5),
		[66]=v2fM(0,441767068273092,0,5),
		[67]=v2fM(0,0,6),
		[68]=v2fM(0,0441767068273092,0,6),
		[69]=v2fM(0,0883534136546185,0,6),
		[70]=v2fM(0,132530120481928,0,6),
		[71]=v2fM(0,176706827309237,0,6),
		[72]=v2fM(0,220883534136546,0,6),
		[73]=v2fM(0,265060240963855,0,6),
		[74]=v2fM(0,309236947791165,0,6),
		[75]=v2fM(0,353413654618474,0,6),
		[76]=v2fM(0,397590361445783,0,6),
		[77]=v2fM(0,441767068273092,0,6),
		[78]=v2fM(0,0,7),
		[79]=v2fM(0,0441767068273092,0,7),
		[80]=v2fM(0,0883534136546185,0,7),
		[81]=v2fM(0,132530120481928,0,7),
		[82]=v2fM(0,176706827309237,0,7),
		[83]=v2fM(0,220883534136546,0,7),
		[84]=v2fM(0,265060240963855,0,7),
		[85]=v2fM(0,309236947791165,0,7),
		[86]=v2fM(0,353413654618474,0,7),
		[87]=v2fM(0,397590361445783,0,7),
		[88]=v2fM(0,441767068273092,0,7),
		[89]=v2fM(0,0,8),
		[90]=v2fM(0,0441767068273092,0,8),
		[91]=v2fM(0,0883534136546185,0,8),
		[92]=v2fM(0,132530120481928,0,8),
		[93]=v2fM(0,176706827309237,0,8),
		[94]=v2fM(0,220883534136546,0,8),
		[95]=v2fM(0,265060240963855,0,8),
		[96]=v2fM(0,309236947791165,0,8),
		[97]=v2fM(0,353413654618474,0,8),
		[98]=v2fM(0,397590361445783,0,8),
		[99]=v2fM(0,441767068273092,0,8),
		[100]=v2fM(0,0,9),
		[101]=v2fM(0,0441767068273092,0,9),
		[102]=v2fM(0,0883534136546185,0,9),
		[103]=v2fM(0,132530120481928,0,9),
		[104]=v2fM(0,176706827309237,0,9),
		[105]=v2fM(0,220883534136546,0,9),
		[106]=v2fM(0,265060240963855,0,9),
		[107]=v2fM(0,309236947791165,0,9),
		[108]=v2fM(0,353413654618474,0,9),
		[109]=v2fM(0,397590361445783,0,9),
		[110]=v2fM(0,441767068273092,0,9),
		[111]=v2fM(0,514056224899598,0),
		[112]=v2fM(0,558232931726908,0),
		[113]=v2fM(0,602409638554217,0),
		[114]=v2fM(0,646586345381526,0),
		[115]=v2fM(0,690763052208835,0),
		[116]=v2fM(0,734939759036145,0),
		[117]=v2fM(0,779116465863454,0),
		[118]=v2fM(0,823293172690763,0),
		[119]=v2fM(0,867469879518072,0),
		[120]=v2fM(0,911646586345382,0),
		[121]=v2fM(0,955823293172691,0),
		[122]=v2fM(0,514056224899598,0,1),
		[123]=v2fM(0,558232931726908,0,1),
		[124]=v2fM(0,602409638554217,0,1),
		[125]=v2fM(0,646586345381526,0,1),
		[126]=v2fM(0,690763052208835,0,1),
		[127]=v2fM(0,734939759036145,0,1),
		[128]=v2fM(0,779116465863454,0,1),
		[129]=v2fM(0,823293172690763,0,1),
		[130]=v2fM(0,867469879518072,0,1),
		[131]=v2fM(0,911646586345382,0,1),
		[132]=v2fM(0,955823293172691,0,1),
		[133]=v2fM(0,514056224899598,0,2),
		[134]=v2fM(0,558232931726908,0,2),
		[135]=v2fM(0,602409638554217,0,2),
		[136]=v2fM(0,646586345381526,0,2),
		[137]=v2fM(0,690763052208835,0,2),
		[138]=v2fM(0,734939759036145,0,2),
		[139]=v2fM(0,779116465863454,0,2),
		[140]=v2fM(0,823293172690763,0,2),
		[141]=v2fM(0,867469879518072,0,2),
		[142]=v2fM(0,911646586345382,0,2),
		[143]=v2fM(0,955823293172691,0,2),
		[144]=v2fM(0,514056224899598,0,3),
		[145]=v2fM(0,558232931726908,0,3),
		[146]=v2fM(0,602409638554217,0,3),
		[147]=v2fM(0,646586345381526,0,3),
		[148]=v2fM(0,690763052208835,0,3),
		[149]=v2fM(0,734939759036145,0,3),
		[150]=v2fM(0,779116465863454,0,3),
		[151]=v2fM(0,823293172690763,0,3),
		[152]=v2fM(0,867469879518072,0,3),
		[153]=v2fM(0,911646586345382,0,3),
		[154]=v2fM(0,955823293172691,0,3),
		[155]=v2fM(0,514056224899598,0,4),
		[156]=v2fM(0,558232931726908,0,4),
		[157]=v2fM(0,602409638554217,0,4),
		[158]=v2fM(0,646586345381526,0,4),
		[159]=v2fM(0,690763052208835,0,4),
		[160]=v2fM(0,734939759036145,0,4),
		[161]=v2fM(0,779116465863454,0,4),
		[162]=v2fM(0,823293172690763,0,4),
		[163]=v2fM(0,867469879518072,0,4),
		[164]=v2fM(0,911646586345382,0,4),
		[165]=v2fM(0,955823293172691,0,4),
		[166]=v2fM(0,514056224899598,0,5),
		[167]=v2fM(0,558232931726908,0,5),
		[168]=v2fM(0,602409638554217,0,5),
		[169]=v2fM(0,646586345381526,0,5),
		[170]=v2fM(0,690763052208835,0,5),
		[171]=v2fM(0,734939759036145,0,5),
		[172]=v2fM(0,779116465863454,0,5),
		[173]=v2fM(0,823293172690763,0,5),
		[174]=v2fM(0,867469879518072,0,5),
		[175]=v2fM(0,911646586345382,0,5),
		[176]=v2fM(0,955823293172691,0,5),
		[177]=v2fM(0,514056224899598,0,6),
		[178]=v2fM(0,558232931726908,0,6),
		[179]=v2fM(0,602409638554217,0,6),
		[180]=v2fM(0,646586345381526,0,6),
		[181]=v2fM(0,690763052208835,0,6),
		[182]=v2fM(0,734939759036145,0,6),
		[183]=v2fM(0,779116465863454,0,6),
		[184]=v2fM(0,823293172690763,0,6),
		[185]=v2fM(0,867469879518072,0,6),
		[186]=v2fM(0,911646586345382,0,6),
		[187]=v2fM(0,955823293172691,0,6),
		[188]=v2fM(0,514056224899598,0,7),
		[189]=v2fM(0,558232931726908,0,7),
		[190]=v2fM(0,602409638554217,0,7),
		[191]=v2fM(0,646586345381526,0,7),
		[192]=v2fM(0,690763052208835,0,7),
		[193]=v2fM(0,734939759036145,0,7),
		[194]=v2fM(0,779116465863454,0,7),
		[195]=v2fM(0,823293172690763,0,7),
		[196]=v2fM(0,867469879518072,0,7),
		[197]=v2fM(0,911646586345382,0,7),
		[198]=v2fM(0,955823293172691,0,7),
		[199]=v2fM(0,514056224899598,0,8),
		[200]=v2fM(0,558232931726908,0,8),
		[201]=v2fM(0,602409638554217,0,8),
		[202]=v2fM(0,646586345381526,0,8),
		[203]=v2fM(0,690763052208835,0,8),
		[204]=v2fM(0,734939759036145,0,8),
		[205]=v2fM(0,779116465863454,0,8),
		[206]=v2fM(0,823293172690763,0,8),
		[207]=v2fM(0,867469879518072,0,8),
		[208]=v2fM(0,911646586345382,0,8),
		[209]=v2fM(0,955823293172691,0,8),
		[210]=v2fM(0,514056224899598,0,9),
		[211]=v2fM(0,558232931726908,0,9),
		[212]=v2fM(0,602409638554217,0,9),
		[213]=v2fM(0,646586345381526,0,9),
		[214]=v2fM(0,690763052208835,0,9),
		[215]=v2fM(0,734939759036145,0,9),
		[216]=v2fM(0,779116465863454,0,9),
	},
	FRAME_SIZES={
		[1]={WIDTH=88,HEIGHT=102},
		[2]={WIDTH=88,HEIGHT=102},
		[3]={WIDTH=88,HEIGHT=102},
		[4]={WIDTH=88,HEIGHT=102},
		[5]={WIDTH=88,HEIGHT=102},
		[6]={WIDTH=88,HEIGHT=102},
		[7]={WIDTH=88,HEIGHT=102},
		[8]={WIDTH=88,HEIGHT=102},
		[9]={WIDTH=88,HEIGHT=102},
		[10]={WIDTH=88,HEIGHT=102},
		[11]={WIDTH=88,HEIGHT=102},
		[12]={WIDTH=88,HEIGHT=102},
		[13]={WIDTH=88,HEIGHT=102},
		[14]={WIDTH=88,HEIGHT=102},
		[15]={WIDTH=88,HEIGHT=102},
		[16]={WIDTH=88,HEIGHT=102},
		[17]={WIDTH=88,HEIGHT=102},
		[18]={WIDTH=88,HEIGHT=102},
		[19]={WIDTH=88,HEIGHT=102},
		[20]={WIDTH=88,HEIGHT=102},
		[21]={WIDTH=88,HEIGHT=102},
		[22]={WIDTH=88,HEIGHT=102},
		[23]={WIDTH=88,HEIGHT=102},
		[24]={WIDTH=88,HEIGHT=102},
		[25]={WIDTH=88,HEIGHT=102},
		[26]={WIDTH=88,HEIGHT=102},
		[27]={WIDTH=88,HEIGHT=102},
		[28]={WIDTH=88,HEIGHT=102},
		[29]={WIDTH=88,HEIGHT=102},
		[30]={WIDTH=88,HEIGHT=102},
		[31]={WIDTH=88,HEIGHT=102},
		[32]={WIDTH=88,HEIGHT=102},
		[33]={WIDTH=88,HEIGHT=102},
		[34]={WIDTH=88,HEIGHT=102},
		[35]={WIDTH=88,HEIGHT=102},
		[36]={WIDTH=88,HEIGHT=102},
		[37]={WIDTH=88,HEIGHT=102},
		[38]={WIDTH=88,HEIGHT=102},
		[39]={WIDTH=88,HEIGHT=102},
		[40]={WIDTH=88,HEIGHT=102},
		[41]={WIDTH=88,HEIGHT=102},
		[42]={WIDTH=88,HEIGHT=102},
		[43]={WIDTH=88,HEIGHT=102},
		[44]={WIDTH=88,HEIGHT=102},
		[45]={WIDTH=88,HEIGHT=102},
		[46]={WIDTH=88,HEIGHT=102},
		[47]={WIDTH=88,HEIGHT=102},
		[48]={WIDTH=88,HEIGHT=102},
		[49]={WIDTH=88,HEIGHT=102},
		[50]={WIDTH=88,HEIGHT=102},
		[51]={WIDTH=88,HEIGHT=102},
		[52]={WIDTH=88,HEIGHT=102},
		[53]={WIDTH=88,HEIGHT=102},
		[54]={WIDTH=88,HEIGHT=102},
		[55]={WIDTH=88,HEIGHT=102},
		[56]={WIDTH=88,HEIGHT=102},
		[57]={WIDTH=88,HEIGHT=102},
		[58]={WIDTH=88,HEIGHT=102},
		[59]={WIDTH=88,HEIGHT=102},
		[60]={WIDTH=88,HEIGHT=102},
		[61]={WIDTH=88,HEIGHT=102},
		[62]={WIDTH=88,HEIGHT=102},
		[63]={WIDTH=88,HEIGHT=102},
		[64]={WIDTH=88,HEIGHT=102},
		[65]={WIDTH=88,HEIGHT=102},
		[66]={WIDTH=88,HEIGHT=102},
		[67]={WIDTH=88,HEIGHT=102},
		[68]={WIDTH=88,HEIGHT=102},
		[69]={WIDTH=88,HEIGHT=102},
		[70]={WIDTH=88,HEIGHT=102},
		[71]={WIDTH=88,HEIGHT=102},
		[72]={WIDTH=88,HEIGHT=102},
		[73]={WIDTH=88,HEIGHT=102},
		[74]={WIDTH=88,HEIGHT=102},
		[75]={WIDTH=88,HEIGHT=102},
		[76]={WIDTH=88,HEIGHT=102},
		[77]={WIDTH=88,HEIGHT=102},
		[78]={WIDTH=88,HEIGHT=102},
		[79]={WIDTH=88,HEIGHT=102},
		[80]={WIDTH=88,HEIGHT=102},
		[81]={WIDTH=88,HEIGHT=102},
		[82]={WIDTH=88,HEIGHT=102},
		[83]={WIDTH=88,HEIGHT=102},
		[84]={WIDTH=88,HEIGHT=102},
		[85]={WIDTH=88,HEIGHT=102},
		[86]={WIDTH=88,HEIGHT=102},
		[87]={WIDTH=88,HEIGHT=102},
		[88]={WIDTH=88,HEIGHT=102},
		[89]={WIDTH=88,HEIGHT=102},
		[90]={WIDTH=88,HEIGHT=102},
		[91]={WIDTH=88,HEIGHT=102},
		[92]={WIDTH=88,HEIGHT=102},
		[93]={WIDTH=88,HEIGHT=102},
		[94]={WIDTH=88,HEIGHT=102},
		[95]={WIDTH=88,HEIGHT=102},
		[96]={WIDTH=88,HEIGHT=102},
		[97]={WIDTH=88,HEIGHT=102},
		[98]={WIDTH=88,HEIGHT=102},
		[99]={WIDTH=88,HEIGHT=102},
		[100]={WIDTH=88,HEIGHT=102},
		[101]={WIDTH=88,HEIGHT=102},
		[102]={WIDTH=88,HEIGHT=102},
		[103]={WIDTH=88,HEIGHT=102},
		[104]={WIDTH=88,HEIGHT=102},
		[105]={WIDTH=88,HEIGHT=102},
		[106]={WIDTH=88,HEIGHT=102},
		[107]={WIDTH=88,HEIGHT=102},
		[108]={WIDTH=88,HEIGHT=102},
		[109]={WIDTH=88,HEIGHT=102},
		[110]={WIDTH=88,HEIGHT=102},
		[111]={WIDTH=88,HEIGHT=102},
		[112]={WIDTH=88,HEIGHT=102},
		[113]={WIDTH=88,HEIGHT=102},
		[114]={WIDTH=88,HEIGHT=102},
		[115]={WIDTH=88,HEIGHT=102},
		[116]={WIDTH=88,HEIGHT=102},
		[117]={WIDTH=88,HEIGHT=102},
		[118]={WIDTH=88,HEIGHT=102},
		[119]={WIDTH=88,HEIGHT=102},
		[120]={WIDTH=88,HEIGHT=102},
		[121]={WIDTH=88,HEIGHT=102},
		[122]={WIDTH=88,HEIGHT=102},
		[123]={WIDTH=88,HEIGHT=102},
		[124]={WIDTH=88,HEIGHT=102},
		[125]={WIDTH=88,HEIGHT=102},
		[126]={WIDTH=88,HEIGHT=102},
		[127]={WIDTH=88,HEIGHT=102},
		[128]={WIDTH=88,HEIGHT=102},
		[129]={WIDTH=88,HEIGHT=102},
		[130]={WIDTH=88,HEIGHT=102},
		[131]={WIDTH=88,HEIGHT=102},
		[132]={WIDTH=88,HEIGHT=102},
		[133]={WIDTH=88,HEIGHT=102},
		[134]={WIDTH=88,HEIGHT=102},
		[135]={WIDTH=88,HEIGHT=102},
		[136]={WIDTH=88,HEIGHT=102},
		[137]={WIDTH=88,HEIGHT=102},
		[138]={WIDTH=88,HEIGHT=102},
		[139]={WIDTH=88,HEIGHT=102},
		[140]={WIDTH=88,HEIGHT=102},
		[141]={WIDTH=88,HEIGHT=102},
		[142]={WIDTH=88,HEIGHT=102},
		[143]={WIDTH=88,HEIGHT=102},
		[144]={WIDTH=88,HEIGHT=102},
		[145]={WIDTH=88,HEIGHT=102},
		[146]={WIDTH=88,HEIGHT=102},
		[147]={WIDTH=88,HEIGHT=102},
		[148]={WIDTH=88,HEIGHT=102},
		[149]={WIDTH=88,HEIGHT=102},
		[150]={WIDTH=88,HEIGHT=102},
		[151]={WIDTH=88,HEIGHT=102},
		[152]={WIDTH=88,HEIGHT=102},
		[153]={WIDTH=88,HEIGHT=102},
		[154]={WIDTH=88,HEIGHT=102},
		[155]={WIDTH=88,HEIGHT=102},
		[156]={WIDTH=88,HEIGHT=102},
		[157]={WIDTH=88,HEIGHT=102},
		[158]={WIDTH=88,HEIGHT=102},
		[159]={WIDTH=88,HEIGHT=102},
		[160]={WIDTH=88,HEIGHT=102},
		[161]={WIDTH=88,HEIGHT=102},
		[162]={WIDTH=88,HEIGHT=102},
		[163]={WIDTH=88,HEIGHT=102},
		[164]={WIDTH=88,HEIGHT=102},
		[165]={WIDTH=88,HEIGHT=102},
		[166]={WIDTH=88,HEIGHT=102},
		[167]={WIDTH=88,HEIGHT=102},
		[168]={WIDTH=88,HEIGHT=102},
		[169]={WIDTH=88,HEIGHT=102},
		[170]={WIDTH=88,HEIGHT=102},
		[171]={WIDTH=88,HEIGHT=102},
		[172]={WIDTH=88,HEIGHT=102},
		[173]={WIDTH=88,HEIGHT=102},
		[174]={WIDTH=88,HEIGHT=102},
		[175]={WIDTH=88,HEIGHT=102},
		[176]={WIDTH=88,HEIGHT=102},
		[177]={WIDTH=88,HEIGHT=102},
		[178]={WIDTH=88,HEIGHT=102},
		[179]={WIDTH=88,HEIGHT=102},
		[180]={WIDTH=88,HEIGHT=102},
		[181]={WIDTH=88,HEIGHT=102},
		[182]={WIDTH=88,HEIGHT=102},
		[183]={WIDTH=88,HEIGHT=102},
		[184]={WIDTH=88,HEIGHT=102},
		[185]={WIDTH=88,HEIGHT=102},
		[186]={WIDTH=88,HEIGHT=102},
		[187]={WIDTH=88,HEIGHT=102},
		[188]={WIDTH=88,HEIGHT=102},
		[189]={WIDTH=88,HEIGHT=102},
		[190]={WIDTH=88,HEIGHT=102},
		[191]={WIDTH=88,HEIGHT=102},
		[192]={WIDTH=88,HEIGHT=102},
		[193]={WIDTH=88,HEIGHT=102},
		[194]={WIDTH=88,HEIGHT=102},
		[195]={WIDTH=88,HEIGHT=102},
		[196]={WIDTH=88,HEIGHT=102},
		[197]={WIDTH=88,HEIGHT=102},
		[198]={WIDTH=88,HEIGHT=102},
		[199]={WIDTH=88,HEIGHT=102},
		[200]={WIDTH=88,HEIGHT=102},
		[201]={WIDTH=88,HEIGHT=102},
		[202]={WIDTH=88,HEIGHT=102},
		[203]={WIDTH=88,HEIGHT=102},
		[204]={WIDTH=88,HEIGHT=102},
		[205]={WIDTH=88,HEIGHT=102},
		[206]={WIDTH=88,HEIGHT=102},
		[207]={WIDTH=88,HEIGHT=102},
		[208]={WIDTH=88,HEIGHT=102},
		[209]={WIDTH=88,HEIGHT=102},
		[210]={WIDTH=88,HEIGHT=102},
		[211]={WIDTH=88,HEIGHT=102},
		[212]={WIDTH=88,HEIGHT=102},
		[213]={WIDTH=88,HEIGHT=102},
		[214]={WIDTH=88,HEIGHT=102},
		[215]={WIDTH=88,HEIGHT=102},
		[216]={WIDTH=88,HEIGHT=102},
	},
	FRAME_INDEXS={
		['e-r-mediumt-siber0144z']=1,
		['e-r-mediumt-siber0143z']=2,
		['e-r-mediumt-siber0142z']=3,
		['e-r-mediumt-siber0147z']=4,
		['e-r-mediumt-siber0146z']=5,
		['e-r-mediumt-siber0145z']=6,
		['e-r-mediumt-siber0141z']=7,
		['e-r-mediumt-siber0137z']=8,
		['e-r-mediumt-siber0136z']=9,
		['e-r-mediumt-siber0135z']=10,
		['e-r-mediumt-siber0140z']=11,
		['e-r-mediumt-siber0139z']=12,
		['e-r-mediumt-siber0138z']=13,
		['e-r-mediumt-siber0148z']=14,
		['e-r-mediumt-siber0158z']=15,
		['e-r-mediumt-siber0157z']=16,
		['e-r-mediumt-siber0156z']=17,
		['e-r-mediumt-siber0161z']=18,
		['e-r-mediumt-siber0160z']=19,
		['e-r-mediumt-siber0159z']=20,
		['e-r-mediumt-siber0155z']=21,
		['e-r-mediumt-siber0151z']=22,
		['e-r-mediumt-siber0150z']=23,
		['e-r-mediumt-siber0149z']=24,
		['e-r-mediumt-siber0154z']=25,
		['e-r-mediumt-siber0153z']=26,
		['e-r-mediumt-siber0152z']=27,
		['e-r-mediumt-siber0117z']=28,
		['e-r-mediumt-siber0116z']=29,
		['e-r-mediumt-siber0115z']=30,
		['e-r-mediumt-siber0120z']=31,
		['e-r-mediumt-siber0119z']=32,
		['e-r-mediumt-siber0118z']=33,
		['e-r-mediumt-siber0114z']=34,
		['e-r-mediumt-siber0110z']=35,
		['e-r-mediumt-siber0109z']=36,
		['e-r-mediumt-siber0108z']=37,
		['e-r-mediumt-siber0113z']=38,
		['e-r-mediumt-siber0112z']=39,
		['e-r-mediumt-siber0111z']=40,
		['e-r-mediumt-siber0121z']=41,
		['e-r-mediumt-siber0131z']=42,
		['e-r-mediumt-siber0130z']=43,
		['e-r-mediumt-siber0129z']=44,
		['e-r-mediumt-siber0134z']=45,
		['e-r-mediumt-siber0133z']=46,
		['e-r-mediumt-siber0132z']=47,
		['e-r-mediumt-siber0128z']=48,
		['e-r-mediumt-siber0124z']=49,
		['e-r-mediumt-siber0123z']=50,
		['e-r-mediumt-siber0122z']=51,
		['e-r-mediumt-siber0127z']=52,
		['e-r-mediumt-siber0126z']=53,
		['e-r-mediumt-siber0125z']=54,
		['e-r-mediumt-siber0198z']=55,
		['e-r-mediumt-siber0197z']=56,
		['e-r-mediumt-siber0196z']=57,
		['e-r-mediumt-siber0201z']=58,
		['e-r-mediumt-siber0200z']=59,
		['e-r-mediumt-siber0199z']=60,
		['e-r-mediumt-siber0195z']=61,
		['e-r-mediumt-siber0191z']=62,
		['e-r-mediumt-siber0190z']=63,
		['e-r-mediumt-siber0189z']=64,
		['e-r-mediumt-siber0194z']=65,
		['e-r-mediumt-siber0193z']=66,
		['e-r-mediumt-siber0192z']=67,
		['e-r-mediumt-siber0202z']=68,
		['e-r-mediumt-siber0212z']=69,
		['e-r-mediumt-siber0211z']=70,
		['e-r-mediumt-siber0210z']=71,
		['e-r-mediumt-siber0215z']=72,
		['e-r-mediumt-siber0214z']=73,
		['e-r-mediumt-siber0213z']=74,
		['e-r-mediumt-siber0209z']=75,
		['e-r-mediumt-siber0205z']=76,
		['e-r-mediumt-siber0204z']=77,
		['e-r-mediumt-siber0203z']=78,
		['e-r-mediumt-siber0208z']=79,
		['e-r-mediumt-siber0207z']=80,
		['e-r-mediumt-siber0206z']=81,
		['e-r-mediumt-siber0171z']=82,
		['e-r-mediumt-siber0170z']=83,
		['e-r-mediumt-siber0169z']=84,
		['e-r-mediumt-siber0174z']=85,
		['e-r-mediumt-siber0173z']=86,
		['e-r-mediumt-siber0172z']=87,
		['e-r-mediumt-siber0168z']=88,
		['e-r-mediumt-siber0164z']=89,
		['e-r-mediumt-siber0163z']=90,
		['e-r-mediumt-siber0162z']=91,
		['e-r-mediumt-siber0167z']=92,
		['e-r-mediumt-siber0166z']=93,
		['e-r-mediumt-siber0165z']=94,
		['e-r-mediumt-siber0175z']=95,
		['e-r-mediumt-siber0185z']=96,
		['e-r-mediumt-siber0184z']=97,
		['e-r-mediumt-siber0183z']=98,
		['e-r-mediumt-siber0188z']=99,
		['e-r-mediumt-siber0187z']=100,
		['e-r-mediumt-siber0186z']=101,
		['e-r-mediumt-siber0182z']=102,
		['e-r-mediumt-siber0178z']=103,
		['e-r-mediumt-siber0177z']=104,
		['e-r-mediumt-siber0176z']=105,
		['e-r-mediumt-siber0181z']=106,
		['e-r-mediumt-siber0180z']=107,
		['e-r-mediumt-siber0179z']=108,
		['e-r-mediumt-siber0036z']=109,
		['e-r-mediumt-siber0035z']=110,
		['e-r-mediumt-siber0034z']=111,
		['e-r-mediumt-siber0039z']=112,
		['e-r-mediumt-siber0038z']=113,
		['e-r-mediumt-siber0037z']=114,
		['e-r-mediumt-siber0033z']=115,
		['e-r-mediumt-siber0029z']=116,
		['e-r-mediumt-siber0028z']=117,
		['e-r-mediumt-siber0027z']=118,
		['e-r-mediumt-siber0032z']=119,
		['e-r-mediumt-siber0031z']=120,
		['e-r-mediumt-siber0030z']=121,
		['e-r-mediumt-siber0040z']=122,
		['e-r-mediumt-siber0050z']=123,
		['e-r-mediumt-siber0049z']=124,
		['e-r-mediumt-siber0048z']=125,
		['e-r-mediumt-siber0053z']=126,
		['e-r-mediumt-siber0052z']=127,
		['e-r-mediumt-siber0051z']=128,
		['e-r-mediumt-siber0047z']=129,
		['e-r-mediumt-siber0043z']=130,
		['e-r-mediumt-siber0042z']=131,
		['e-r-mediumt-siber0041z']=132,
		['e-r-mediumt-siber0046z']=133,
		['e-r-mediumt-siber0045z']=134,
		['e-r-mediumt-siber0044z']=135,
		['e-r-mediumt-siber0009z']=136,
		['e-r-mediumt-siber0008z']=137,
		['e-r-mediumt-siber0007z']=138,
		['e-r-mediumt-siber0012z']=139,
		['e-r-mediumt-siber0011z']=140,
		['e-r-mediumt-siber0010z']=141,
		['e-r-mediumt-siber0006z']=142,
		['e-r-mediumt-siber0002z']=143,
		['e-r-mediumt-siber0001z']=144,
		['e-r-mediumt-siber0000z']=145,
		['e-r-mediumt-siber0005z']=146,
		['e-r-mediumt-siber0004z']=147,
		['e-r-mediumt-siber0003z']=148,
		['e-r-mediumt-siber0013z']=149,
		['e-r-mediumt-siber0023z']=150,
		['e-r-mediumt-siber0022z']=151,
		['e-r-mediumt-siber0021z']=152,
		['e-r-mediumt-siber0026z']=153,
		['e-r-mediumt-siber0025z']=154,
		['e-r-mediumt-siber0024z']=155,
		['e-r-mediumt-siber0020z']=156,
		['e-r-mediumt-siber0016z']=157,
		['e-r-mediumt-siber0015z']=158,
		['e-r-mediumt-siber0014z']=159,
		['e-r-mediumt-siber0019z']=160,
		['e-r-mediumt-siber0018z']=161,
		['e-r-mediumt-siber0017z']=162,
		['e-r-mediumt-siber0090z']=163,
		['e-r-mediumt-siber0089z']=164,
		['e-r-mediumt-siber0088z']=165,
		['e-r-mediumt-siber0093z']=166,
		['e-r-mediumt-siber0092z']=167,
		['e-r-mediumt-siber0091z']=168,
		['e-r-mediumt-siber0087z']=169,
		['e-r-mediumt-siber0083z']=170,
		['e-r-mediumt-siber0082z']=171,
		['e-r-mediumt-siber0081z']=172,
		['e-r-mediumt-siber0086z']=173,
		['e-r-mediumt-siber0085z']=174,
		['e-r-mediumt-siber0084z']=175,
		['e-r-mediumt-siber0094z']=176,
		['e-r-mediumt-siber0104z']=177,
		['e-r-mediumt-siber0103z']=178,
		['e-r-mediumt-siber0102z']=179,
		['e-r-mediumt-siber0107z']=180,
		['e-r-mediumt-siber0106z']=181,
		['e-r-mediumt-siber0105z']=182,
		['e-r-mediumt-siber0101z']=183,
		['e-r-mediumt-siber0097z']=184,
		['e-r-mediumt-siber0096z']=185,
		['e-r-mediumt-siber0095z']=186,
		['e-r-mediumt-siber0100z']=187,
		['e-r-mediumt-siber0099z']=188,
		['e-r-mediumt-siber0098z']=189,
		['e-r-mediumt-siber0063z']=190,
		['e-r-mediumt-siber0062z']=191,
		['e-r-mediumt-siber0061z']=192,
		['e-r-mediumt-siber0066z']=193,
		['e-r-mediumt-siber0065z']=194,
		['e-r-mediumt-siber0064z']=195,
		['e-r-mediumt-siber0060z']=196,
		['e-r-mediumt-siber0056z']=197,
		['e-r-mediumt-siber0055z']=198,
		['e-r-mediumt-siber0054z']=199,
		['e-r-mediumt-siber0059z']=200,
		['e-r-mediumt-siber0058z']=201,
		['e-r-mediumt-siber0057z']=202,
		['e-r-mediumt-siber0067z']=203,
		['e-r-mediumt-siber0077z']=204,
		['e-r-mediumt-siber0076z']=205,
		['e-r-mediumt-siber0075z']=206,
		['e-r-mediumt-siber0080z']=207,
		['e-r-mediumt-siber0079z']=208,
		['e-r-mediumt-siber0078z']=209,
		['e-r-mediumt-siber0074z']=210,
		['e-r-mediumt-siber0070z']=211,
		['e-r-mediumt-siber0069z']=212,
		['e-r-mediumt-siber0068z']=213,
		['e-r-mediumt-siber0073z']=214,
		['e-r-mediumt-siber0072z']=215,
		['e-r-mediumt-siber0071z']=216,
	},
};
