--GENERATED ON: 05/12/2024 06:59:45


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['e-u-heavyt-siberz'] = {
	TEX_WIDTH=3764,
	TEX_HEIGHT=1960,
	FRAME_COUNT=216,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0350690754516472,0),
		[3]=v2fM(0,0701381509032944,0),
		[4]=v2fM(0,105207226354942,0),
		[5]=v2fM(0,140276301806589,0),
		[6]=v2fM(0,175345377258236,0),
		[7]=v2fM(0,210414452709883,0),
		[8]=v2fM(0,0,0714285714285714),
		[9]=v2fM(0,0350690754516472,0,0714285714285714),
		[10]=v2fM(0,0701381509032944,0,0714285714285714),
		[11]=v2fM(0,105207226354942,0,0714285714285714),
		[12]=v2fM(0,140276301806589,0,0714285714285714),
		[13]=v2fM(0,175345377258236,0,0714285714285714),
		[14]=v2fM(0,210414452709883,0,0714285714285714),
		[15]=v2fM(0,0,142857142857143),
		[16]=v2fM(0,0350690754516472,0,142857142857143),
		[17]=v2fM(0,0701381509032944,0,142857142857143),
		[18]=v2fM(0,105207226354942,0,142857142857143),
		[19]=v2fM(0,140276301806589,0,142857142857143),
		[20]=v2fM(0,175345377258236,0,142857142857143),
		[21]=v2fM(0,210414452709883,0,142857142857143),
		[22]=v2fM(0,0,214285714285714),
		[23]=v2fM(0,0350690754516472,0,214285714285714),
		[24]=v2fM(0,0701381509032944,0,214285714285714),
		[25]=v2fM(0,105207226354942,0,214285714285714),
		[26]=v2fM(0,140276301806589,0,214285714285714),
		[27]=v2fM(0,175345377258236,0,214285714285714),
		[28]=v2fM(0,210414452709883,0,214285714285714),
		[29]=v2fM(0,0,285714285714286),
		[30]=v2fM(0,0350690754516472,0,285714285714286),
		[31]=v2fM(0,0701381509032944,0,285714285714286),
		[32]=v2fM(0,105207226354942,0,285714285714286),
		[33]=v2fM(0,140276301806589,0,285714285714286),
		[34]=v2fM(0,175345377258236,0,285714285714286),
		[35]=v2fM(0,210414452709883,0,285714285714286),
		[36]=v2fM(0,0,357142857142857),
		[37]=v2fM(0,0350690754516472,0,357142857142857),
		[38]=v2fM(0,0701381509032944,0,357142857142857),
		[39]=v2fM(0,105207226354942,0,357142857142857),
		[40]=v2fM(0,140276301806589,0,357142857142857),
		[41]=v2fM(0,175345377258236,0,357142857142857),
		[42]=v2fM(0,210414452709883,0,357142857142857),
		[43]=v2fM(0,0,428571428571429),
		[44]=v2fM(0,0350690754516472,0,428571428571429),
		[45]=v2fM(0,0701381509032944,0,428571428571429),
		[46]=v2fM(0,105207226354942,0,428571428571429),
		[47]=v2fM(0,140276301806589,0,428571428571429),
		[48]=v2fM(0,175345377258236,0,428571428571429),
		[49]=v2fM(0,210414452709883,0,428571428571429),
		[50]=v2fM(0,272051009564293,0),
		[51]=v2fM(0,30712008501594,0),
		[52]=v2fM(0,342189160467588,0),
		[53]=v2fM(0,377258235919235,0),
		[54]=v2fM(0,412327311370882,0),
		[55]=v2fM(0,447396386822529,0),
		[56]=v2fM(0,482465462274176,0),
		[57]=v2fM(0,272051009564293,0,0714285714285714),
		[58]=v2fM(0,30712008501594,0,0714285714285714),
		[59]=v2fM(0,342189160467588,0,0714285714285714),
		[60]=v2fM(0,377258235919235,0,0714285714285714),
		[61]=v2fM(0,412327311370882,0,0714285714285714),
		[62]=v2fM(0,447396386822529,0,0714285714285714),
		[63]=v2fM(0,482465462274176,0,0714285714285714),
		[64]=v2fM(0,272051009564293,0,142857142857143),
		[65]=v2fM(0,30712008501594,0,142857142857143),
		[66]=v2fM(0,342189160467588,0,142857142857143),
		[67]=v2fM(0,377258235919235,0,142857142857143),
		[68]=v2fM(0,412327311370882,0,142857142857143),
		[69]=v2fM(0,447396386822529,0,142857142857143),
		[70]=v2fM(0,482465462274176,0,142857142857143),
		[71]=v2fM(0,272051009564293,0,214285714285714),
		[72]=v2fM(0,30712008501594,0,214285714285714),
		[73]=v2fM(0,342189160467588,0,214285714285714),
		[74]=v2fM(0,377258235919235,0,214285714285714),
		[75]=v2fM(0,412327311370882,0,214285714285714),
		[76]=v2fM(0,447396386822529,0,214285714285714),
		[77]=v2fM(0,482465462274176,0,214285714285714),
		[78]=v2fM(0,272051009564293,0,285714285714286),
		[79]=v2fM(0,30712008501594,0,285714285714286),
		[80]=v2fM(0,342189160467588,0,285714285714286),
		[81]=v2fM(0,377258235919235,0,285714285714286),
		[82]=v2fM(0,412327311370882,0,285714285714286),
		[83]=v2fM(0,447396386822529,0,285714285714286),
		[84]=v2fM(0,482465462274176,0,285714285714286),
		[85]=v2fM(0,272051009564293,0,357142857142857),
		[86]=v2fM(0,30712008501594,0,357142857142857),
		[87]=v2fM(0,342189160467588,0,357142857142857),
		[88]=v2fM(0,377258235919235,0,357142857142857),
		[89]=v2fM(0,412327311370882,0,357142857142857),
		[90]=v2fM(0,447396386822529,0,357142857142857),
		[91]=v2fM(0,482465462274176,0,357142857142857),
		[92]=v2fM(0,272051009564293,0,428571428571429),
		[93]=v2fM(0,30712008501594,0,428571428571429),
		[94]=v2fM(0,342189160467588,0,428571428571429),
		[95]=v2fM(0,377258235919235,0,428571428571429),
		[96]=v2fM(0,412327311370882,0,428571428571429),
		[97]=v2fM(0,447396386822529,0,428571428571429),
		[98]=v2fM(0,482465462274176,0,428571428571429),
		[99]=v2fM(0,0,5),
		[100]=v2fM(0,0350690754516472,0,5),
		[101]=v2fM(0,0701381509032944,0,5),
		[102]=v2fM(0,105207226354942,0,5),
		[103]=v2fM(0,140276301806589,0,5),
		[104]=v2fM(0,175345377258236,0,5),
		[105]=v2fM(0,210414452709883,0,5),
		[106]=v2fM(0,24548352816153,0,5),
		[107]=v2fM(0,280552603613177,0,5),
		[108]=v2fM(0,315621679064825,0,5),
		[109]=v2fM(0,350690754516472,0,5),
		[110]=v2fM(0,385759829968119,0,5),
		[111]=v2fM(0,420828905419766,0,5),
		[112]=v2fM(0,455897980871413,0,5),
		[113]=v2fM(0,490967056323061,0,5),
		[114]=v2fM(0,0,571428571428571),
		[115]=v2fM(0,0350690754516472,0,571428571428571),
		[116]=v2fM(0,0701381509032944,0,571428571428571),
		[117]=v2fM(0,105207226354942,0,571428571428571),
		[118]=v2fM(0,140276301806589,0,571428571428571),
		[119]=v2fM(0,175345377258236,0,571428571428571),
		[120]=v2fM(0,210414452709883,0,571428571428571),
		[121]=v2fM(0,24548352816153,0,571428571428571),
		[122]=v2fM(0,280552603613177,0,571428571428571),
		[123]=v2fM(0,315621679064825,0,571428571428571),
		[124]=v2fM(0,350690754516472,0,571428571428571),
		[125]=v2fM(0,385759829968119,0,571428571428571),
		[126]=v2fM(0,420828905419766,0,571428571428571),
		[127]=v2fM(0,455897980871413,0,571428571428571),
		[128]=v2fM(0,490967056323061,0,571428571428571),
		[129]=v2fM(0,0,642857142857143),
		[130]=v2fM(0,0350690754516472,0,642857142857143),
		[131]=v2fM(0,0701381509032944,0,642857142857143),
		[132]=v2fM(0,105207226354942,0,642857142857143),
		[133]=v2fM(0,140276301806589,0,642857142857143),
		[134]=v2fM(0,175345377258236,0,642857142857143),
		[135]=v2fM(0,210414452709883,0,642857142857143),
		[136]=v2fM(0,24548352816153,0,642857142857143),
		[137]=v2fM(0,280552603613177,0,642857142857143),
		[138]=v2fM(0,315621679064825,0,642857142857143),
		[139]=v2fM(0,350690754516472,0,642857142857143),
		[140]=v2fM(0,385759829968119,0,642857142857143),
		[141]=v2fM(0,420828905419766,0,642857142857143),
		[142]=v2fM(0,455897980871413,0,642857142857143),
		[143]=v2fM(0,490967056323061,0,642857142857143),
		[144]=v2fM(0,0,714285714285714),
		[145]=v2fM(0,0350690754516472,0,714285714285714),
		[146]=v2fM(0,0701381509032944,0,714285714285714),
		[147]=v2fM(0,105207226354942,0,714285714285714),
		[148]=v2fM(0,140276301806589,0,714285714285714),
		[149]=v2fM(0,175345377258236,0,714285714285714),
		[150]=v2fM(0,210414452709883,0,714285714285714),
		[151]=v2fM(0,24548352816153,0,714285714285714),
		[152]=v2fM(0,280552603613177,0,714285714285714),
		[153]=v2fM(0,315621679064825,0,714285714285714),
		[154]=v2fM(0,350690754516472,0,714285714285714),
		[155]=v2fM(0,385759829968119,0,714285714285714),
		[156]=v2fM(0,420828905419766,0,714285714285714),
		[157]=v2fM(0,455897980871413,0,714285714285714),
		[158]=v2fM(0,490967056323061,0,714285714285714),
		[159]=v2fM(0,0,785714285714286),
		[160]=v2fM(0,0350690754516472,0,785714285714286),
		[161]=v2fM(0,0701381509032944,0,785714285714286),
		[162]=v2fM(0,105207226354942,0,785714285714286),
		[163]=v2fM(0,140276301806589,0,785714285714286),
		[164]=v2fM(0,175345377258236,0,785714285714286),
		[165]=v2fM(0,210414452709883,0,785714285714286),
		[166]=v2fM(0,24548352816153,0,785714285714286),
		[167]=v2fM(0,280552603613177,0,785714285714286),
		[168]=v2fM(0,315621679064825,0,785714285714286),
		[169]=v2fM(0,350690754516472,0,785714285714286),
		[170]=v2fM(0,385759829968119,0,785714285714286),
		[171]=v2fM(0,420828905419766,0,785714285714286),
		[172]=v2fM(0,455897980871413,0,785714285714286),
		[173]=v2fM(0,490967056323061,0,785714285714286),
		[174]=v2fM(0,0,857142857142857),
		[175]=v2fM(0,0350690754516472,0,857142857142857),
		[176]=v2fM(0,0701381509032944,0,857142857142857),
		[177]=v2fM(0,105207226354942,0,857142857142857),
		[178]=v2fM(0,140276301806589,0,857142857142857),
		[179]=v2fM(0,175345377258236,0,857142857142857),
		[180]=v2fM(0,210414452709883,0,857142857142857),
		[181]=v2fM(0,24548352816153,0,857142857142857),
		[182]=v2fM(0,280552603613177,0,857142857142857),
		[183]=v2fM(0,315621679064825,0,857142857142857),
		[184]=v2fM(0,350690754516472,0,857142857142857),
		[185]=v2fM(0,385759829968119,0,857142857142857),
		[186]=v2fM(0,420828905419766,0,857142857142857),
		[187]=v2fM(0,455897980871413,0,857142857142857),
		[188]=v2fM(0,490967056323061,0,857142857142857),
		[189]=v2fM(0,0,928571428571429),
		[190]=v2fM(0,0350690754516472,0,928571428571429),
		[191]=v2fM(0,0701381509032944,0,928571428571429),
		[192]=v2fM(0,105207226354942,0,928571428571429),
		[193]=v2fM(0,140276301806589,0,928571428571429),
		[194]=v2fM(0,175345377258236,0,928571428571429),
		[195]=v2fM(0,210414452709883,0,928571428571429),
		[196]=v2fM(0,24548352816153,0,928571428571429),
		[197]=v2fM(0,280552603613177,0,928571428571429),
		[198]=v2fM(0,315621679064825,0,928571428571429),
		[199]=v2fM(0,350690754516472,0,928571428571429),
		[200]=v2fM(0,385759829968119,0,928571428571429),
		[201]=v2fM(0,420828905419766,0,928571428571429),
		[202]=v2fM(0,455897980871413,0,928571428571429),
		[203]=v2fM(0,490967056323061,0,928571428571429),
		[204]=v2fM(0,544102019128587,0),
		[205]=v2fM(0,579171094580234,0),
		[206]=v2fM(0,614240170031881,0),
		[207]=v2fM(0,649309245483528,0),
		[208]=v2fM(0,684378320935175,0),
		[209]=v2fM(0,719447396386823,0),
		[210]=v2fM(0,75451647183847,0),
		[211]=v2fM(0,789585547290117,0),
		[212]=v2fM(0,824654622741764,0),
		[213]=v2fM(0,859723698193411,0),
		[214]=v2fM(0,894792773645058,0),
		[215]=v2fM(0,929861849096706,0),
		[216]=v2fM(0,964930924548353,0),
	},
	FRAME_SIZES={
		[1]={WIDTH=132,HEIGHT=140},
		[2]={WIDTH=132,HEIGHT=140},
		[3]={WIDTH=132,HEIGHT=140},
		[4]={WIDTH=132,HEIGHT=140},
		[5]={WIDTH=132,HEIGHT=140},
		[6]={WIDTH=132,HEIGHT=140},
		[7]={WIDTH=132,HEIGHT=140},
		[8]={WIDTH=132,HEIGHT=140},
		[9]={WIDTH=132,HEIGHT=140},
		[10]={WIDTH=132,HEIGHT=140},
		[11]={WIDTH=132,HEIGHT=140},
		[12]={WIDTH=132,HEIGHT=140},
		[13]={WIDTH=132,HEIGHT=140},
		[14]={WIDTH=132,HEIGHT=140},
		[15]={WIDTH=132,HEIGHT=140},
		[16]={WIDTH=132,HEIGHT=140},
		[17]={WIDTH=132,HEIGHT=140},
		[18]={WIDTH=132,HEIGHT=140},
		[19]={WIDTH=132,HEIGHT=140},
		[20]={WIDTH=132,HEIGHT=140},
		[21]={WIDTH=132,HEIGHT=140},
		[22]={WIDTH=132,HEIGHT=140},
		[23]={WIDTH=132,HEIGHT=140},
		[24]={WIDTH=132,HEIGHT=140},
		[25]={WIDTH=132,HEIGHT=140},
		[26]={WIDTH=132,HEIGHT=140},
		[27]={WIDTH=132,HEIGHT=140},
		[28]={WIDTH=132,HEIGHT=140},
		[29]={WIDTH=132,HEIGHT=140},
		[30]={WIDTH=132,HEIGHT=140},
		[31]={WIDTH=132,HEIGHT=140},
		[32]={WIDTH=132,HEIGHT=140},
		[33]={WIDTH=132,HEIGHT=140},
		[34]={WIDTH=132,HEIGHT=140},
		[35]={WIDTH=132,HEIGHT=140},
		[36]={WIDTH=132,HEIGHT=140},
		[37]={WIDTH=132,HEIGHT=140},
		[38]={WIDTH=132,HEIGHT=140},
		[39]={WIDTH=132,HEIGHT=140},
		[40]={WIDTH=132,HEIGHT=140},
		[41]={WIDTH=132,HEIGHT=140},
		[42]={WIDTH=132,HEIGHT=140},
		[43]={WIDTH=132,HEIGHT=140},
		[44]={WIDTH=132,HEIGHT=140},
		[45]={WIDTH=132,HEIGHT=140},
		[46]={WIDTH=132,HEIGHT=140},
		[47]={WIDTH=132,HEIGHT=140},
		[48]={WIDTH=132,HEIGHT=140},
		[49]={WIDTH=132,HEIGHT=140},
		[50]={WIDTH=132,HEIGHT=140},
		[51]={WIDTH=132,HEIGHT=140},
		[52]={WIDTH=132,HEIGHT=140},
		[53]={WIDTH=132,HEIGHT=140},
		[54]={WIDTH=132,HEIGHT=140},
		[55]={WIDTH=132,HEIGHT=140},
		[56]={WIDTH=132,HEIGHT=140},
		[57]={WIDTH=132,HEIGHT=140},
		[58]={WIDTH=132,HEIGHT=140},
		[59]={WIDTH=132,HEIGHT=140},
		[60]={WIDTH=132,HEIGHT=140},
		[61]={WIDTH=132,HEIGHT=140},
		[62]={WIDTH=132,HEIGHT=140},
		[63]={WIDTH=132,HEIGHT=140},
		[64]={WIDTH=132,HEIGHT=140},
		[65]={WIDTH=132,HEIGHT=140},
		[66]={WIDTH=132,HEIGHT=140},
		[67]={WIDTH=132,HEIGHT=140},
		[68]={WIDTH=132,HEIGHT=140},
		[69]={WIDTH=132,HEIGHT=140},
		[70]={WIDTH=132,HEIGHT=140},
		[71]={WIDTH=132,HEIGHT=140},
		[72]={WIDTH=132,HEIGHT=140},
		[73]={WIDTH=132,HEIGHT=140},
		[74]={WIDTH=132,HEIGHT=140},
		[75]={WIDTH=132,HEIGHT=140},
		[76]={WIDTH=132,HEIGHT=140},
		[77]={WIDTH=132,HEIGHT=140},
		[78]={WIDTH=132,HEIGHT=140},
		[79]={WIDTH=132,HEIGHT=140},
		[80]={WIDTH=132,HEIGHT=140},
		[81]={WIDTH=132,HEIGHT=140},
		[82]={WIDTH=132,HEIGHT=140},
		[83]={WIDTH=132,HEIGHT=140},
		[84]={WIDTH=132,HEIGHT=140},
		[85]={WIDTH=132,HEIGHT=140},
		[86]={WIDTH=132,HEIGHT=140},
		[87]={WIDTH=132,HEIGHT=140},
		[88]={WIDTH=132,HEIGHT=140},
		[89]={WIDTH=132,HEIGHT=140},
		[90]={WIDTH=132,HEIGHT=140},
		[91]={WIDTH=132,HEIGHT=140},
		[92]={WIDTH=132,HEIGHT=140},
		[93]={WIDTH=132,HEIGHT=140},
		[94]={WIDTH=132,HEIGHT=140},
		[95]={WIDTH=132,HEIGHT=140},
		[96]={WIDTH=132,HEIGHT=140},
		[97]={WIDTH=132,HEIGHT=140},
		[98]={WIDTH=132,HEIGHT=140},
		[99]={WIDTH=132,HEIGHT=140},
		[100]={WIDTH=132,HEIGHT=140},
		[101]={WIDTH=132,HEIGHT=140},
		[102]={WIDTH=132,HEIGHT=140},
		[103]={WIDTH=132,HEIGHT=140},
		[104]={WIDTH=132,HEIGHT=140},
		[105]={WIDTH=132,HEIGHT=140},
		[106]={WIDTH=132,HEIGHT=140},
		[107]={WIDTH=132,HEIGHT=140},
		[108]={WIDTH=132,HEIGHT=140},
		[109]={WIDTH=132,HEIGHT=140},
		[110]={WIDTH=132,HEIGHT=140},
		[111]={WIDTH=132,HEIGHT=140},
		[112]={WIDTH=132,HEIGHT=140},
		[113]={WIDTH=132,HEIGHT=140},
		[114]={WIDTH=132,HEIGHT=140},
		[115]={WIDTH=132,HEIGHT=140},
		[116]={WIDTH=132,HEIGHT=140},
		[117]={WIDTH=132,HEIGHT=140},
		[118]={WIDTH=132,HEIGHT=140},
		[119]={WIDTH=132,HEIGHT=140},
		[120]={WIDTH=132,HEIGHT=140},
		[121]={WIDTH=132,HEIGHT=140},
		[122]={WIDTH=132,HEIGHT=140},
		[123]={WIDTH=132,HEIGHT=140},
		[124]={WIDTH=132,HEIGHT=140},
		[125]={WIDTH=132,HEIGHT=140},
		[126]={WIDTH=132,HEIGHT=140},
		[127]={WIDTH=132,HEIGHT=140},
		[128]={WIDTH=132,HEIGHT=140},
		[129]={WIDTH=132,HEIGHT=140},
		[130]={WIDTH=132,HEIGHT=140},
		[131]={WIDTH=132,HEIGHT=140},
		[132]={WIDTH=132,HEIGHT=140},
		[133]={WIDTH=132,HEIGHT=140},
		[134]={WIDTH=132,HEIGHT=140},
		[135]={WIDTH=132,HEIGHT=140},
		[136]={WIDTH=132,HEIGHT=140},
		[137]={WIDTH=132,HEIGHT=140},
		[138]={WIDTH=132,HEIGHT=140},
		[139]={WIDTH=132,HEIGHT=140},
		[140]={WIDTH=132,HEIGHT=140},
		[141]={WIDTH=132,HEIGHT=140},
		[142]={WIDTH=132,HEIGHT=140},
		[143]={WIDTH=132,HEIGHT=140},
		[144]={WIDTH=132,HEIGHT=140},
		[145]={WIDTH=132,HEIGHT=140},
		[146]={WIDTH=132,HEIGHT=140},
		[147]={WIDTH=132,HEIGHT=140},
		[148]={WIDTH=132,HEIGHT=140},
		[149]={WIDTH=132,HEIGHT=140},
		[150]={WIDTH=132,HEIGHT=140},
		[151]={WIDTH=132,HEIGHT=140},
		[152]={WIDTH=132,HEIGHT=140},
		[153]={WIDTH=132,HEIGHT=140},
		[154]={WIDTH=132,HEIGHT=140},
		[155]={WIDTH=132,HEIGHT=140},
		[156]={WIDTH=132,HEIGHT=140},
		[157]={WIDTH=132,HEIGHT=140},
		[158]={WIDTH=132,HEIGHT=140},
		[159]={WIDTH=132,HEIGHT=140},
		[160]={WIDTH=132,HEIGHT=140},
		[161]={WIDTH=132,HEIGHT=140},
		[162]={WIDTH=132,HEIGHT=140},
		[163]={WIDTH=132,HEIGHT=140},
		[164]={WIDTH=132,HEIGHT=140},
		[165]={WIDTH=132,HEIGHT=140},
		[166]={WIDTH=132,HEIGHT=140},
		[167]={WIDTH=132,HEIGHT=140},
		[168]={WIDTH=132,HEIGHT=140},
		[169]={WIDTH=132,HEIGHT=140},
		[170]={WIDTH=132,HEIGHT=140},
		[171]={WIDTH=132,HEIGHT=140},
		[172]={WIDTH=132,HEIGHT=140},
		[173]={WIDTH=132,HEIGHT=140},
		[174]={WIDTH=132,HEIGHT=140},
		[175]={WIDTH=132,HEIGHT=140},
		[176]={WIDTH=132,HEIGHT=140},
		[177]={WIDTH=132,HEIGHT=140},
		[178]={WIDTH=132,HEIGHT=140},
		[179]={WIDTH=132,HEIGHT=140},
		[180]={WIDTH=132,HEIGHT=140},
		[181]={WIDTH=132,HEIGHT=140},
		[182]={WIDTH=132,HEIGHT=140},
		[183]={WIDTH=132,HEIGHT=140},
		[184]={WIDTH=132,HEIGHT=140},
		[185]={WIDTH=132,HEIGHT=140},
		[186]={WIDTH=132,HEIGHT=140},
		[187]={WIDTH=132,HEIGHT=140},
		[188]={WIDTH=132,HEIGHT=140},
		[189]={WIDTH=132,HEIGHT=140},
		[190]={WIDTH=132,HEIGHT=140},
		[191]={WIDTH=132,HEIGHT=140},
		[192]={WIDTH=132,HEIGHT=140},
		[193]={WIDTH=132,HEIGHT=140},
		[194]={WIDTH=132,HEIGHT=140},
		[195]={WIDTH=132,HEIGHT=140},
		[196]={WIDTH=132,HEIGHT=140},
		[197]={WIDTH=132,HEIGHT=140},
		[198]={WIDTH=132,HEIGHT=140},
		[199]={WIDTH=132,HEIGHT=140},
		[200]={WIDTH=132,HEIGHT=140},
		[201]={WIDTH=132,HEIGHT=140},
		[202]={WIDTH=132,HEIGHT=140},
		[203]={WIDTH=132,HEIGHT=140},
		[204]={WIDTH=132,HEIGHT=140},
		[205]={WIDTH=132,HEIGHT=140},
		[206]={WIDTH=132,HEIGHT=140},
		[207]={WIDTH=132,HEIGHT=140},
		[208]={WIDTH=132,HEIGHT=140},
		[209]={WIDTH=132,HEIGHT=140},
		[210]={WIDTH=132,HEIGHT=140},
		[211]={WIDTH=132,HEIGHT=140},
		[212]={WIDTH=132,HEIGHT=140},
		[213]={WIDTH=132,HEIGHT=140},
		[214]={WIDTH=132,HEIGHT=140},
		[215]={WIDTH=132,HEIGHT=140},
		[216]={WIDTH=132,HEIGHT=140},
	},
	FRAME_INDEXS={
		['e-u-heavyt-siber0144z']=1,
		['e-u-heavyt-siber0143z']=2,
		['e-u-heavyt-siber0142z']=3,
		['e-u-heavyt-siber0147z']=4,
		['e-u-heavyt-siber0146z']=5,
		['e-u-heavyt-siber0145z']=6,
		['e-u-heavyt-siber0141z']=7,
		['e-u-heavyt-siber0137z']=8,
		['e-u-heavyt-siber0136z']=9,
		['e-u-heavyt-siber0135z']=10,
		['e-u-heavyt-siber0140z']=11,
		['e-u-heavyt-siber0139z']=12,
		['e-u-heavyt-siber0138z']=13,
		['e-u-heavyt-siber0148z']=14,
		['e-u-heavyt-siber0158z']=15,
		['e-u-heavyt-siber0157z']=16,
		['e-u-heavyt-siber0156z']=17,
		['e-u-heavyt-siber0161z']=18,
		['e-u-heavyt-siber0160z']=19,
		['e-u-heavyt-siber0159z']=20,
		['e-u-heavyt-siber0155z']=21,
		['e-u-heavyt-siber0151z']=22,
		['e-u-heavyt-siber0150z']=23,
		['e-u-heavyt-siber0149z']=24,
		['e-u-heavyt-siber0154z']=25,
		['e-u-heavyt-siber0153z']=26,
		['e-u-heavyt-siber0152z']=27,
		['e-u-heavyt-siber0117z']=28,
		['e-u-heavyt-siber0116z']=29,
		['e-u-heavyt-siber0115z']=30,
		['e-u-heavyt-siber0120z']=31,
		['e-u-heavyt-siber0119z']=32,
		['e-u-heavyt-siber0118z']=33,
		['e-u-heavyt-siber0114z']=34,
		['e-u-heavyt-siber0110z']=35,
		['e-u-heavyt-siber0109z']=36,
		['e-u-heavyt-siber0108z']=37,
		['e-u-heavyt-siber0113z']=38,
		['e-u-heavyt-siber0112z']=39,
		['e-u-heavyt-siber0111z']=40,
		['e-u-heavyt-siber0121z']=41,
		['e-u-heavyt-siber0131z']=42,
		['e-u-heavyt-siber0130z']=43,
		['e-u-heavyt-siber0129z']=44,
		['e-u-heavyt-siber0134z']=45,
		['e-u-heavyt-siber0133z']=46,
		['e-u-heavyt-siber0132z']=47,
		['e-u-heavyt-siber0128z']=48,
		['e-u-heavyt-siber0124z']=49,
		['e-u-heavyt-siber0123z']=50,
		['e-u-heavyt-siber0122z']=51,
		['e-u-heavyt-siber0127z']=52,
		['e-u-heavyt-siber0126z']=53,
		['e-u-heavyt-siber0125z']=54,
		['e-u-heavyt-siber0198z']=55,
		['e-u-heavyt-siber0197z']=56,
		['e-u-heavyt-siber0196z']=57,
		['e-u-heavyt-siber0201z']=58,
		['e-u-heavyt-siber0200z']=59,
		['e-u-heavyt-siber0199z']=60,
		['e-u-heavyt-siber0195z']=61,
		['e-u-heavyt-siber0191z']=62,
		['e-u-heavyt-siber0190z']=63,
		['e-u-heavyt-siber0189z']=64,
		['e-u-heavyt-siber0194z']=65,
		['e-u-heavyt-siber0193z']=66,
		['e-u-heavyt-siber0192z']=67,
		['e-u-heavyt-siber0202z']=68,
		['e-u-heavyt-siber0212z']=69,
		['e-u-heavyt-siber0211z']=70,
		['e-u-heavyt-siber0210z']=71,
		['e-u-heavyt-siber0215z']=72,
		['e-u-heavyt-siber0214z']=73,
		['e-u-heavyt-siber0213z']=74,
		['e-u-heavyt-siber0209z']=75,
		['e-u-heavyt-siber0205z']=76,
		['e-u-heavyt-siber0204z']=77,
		['e-u-heavyt-siber0203z']=78,
		['e-u-heavyt-siber0208z']=79,
		['e-u-heavyt-siber0207z']=80,
		['e-u-heavyt-siber0206z']=81,
		['e-u-heavyt-siber0171z']=82,
		['e-u-heavyt-siber0170z']=83,
		['e-u-heavyt-siber0169z']=84,
		['e-u-heavyt-siber0174z']=85,
		['e-u-heavyt-siber0173z']=86,
		['e-u-heavyt-siber0172z']=87,
		['e-u-heavyt-siber0168z']=88,
		['e-u-heavyt-siber0164z']=89,
		['e-u-heavyt-siber0163z']=90,
		['e-u-heavyt-siber0162z']=91,
		['e-u-heavyt-siber0167z']=92,
		['e-u-heavyt-siber0166z']=93,
		['e-u-heavyt-siber0165z']=94,
		['e-u-heavyt-siber0175z']=95,
		['e-u-heavyt-siber0185z']=96,
		['e-u-heavyt-siber0184z']=97,
		['e-u-heavyt-siber0183z']=98,
		['e-u-heavyt-siber0188z']=99,
		['e-u-heavyt-siber0187z']=100,
		['e-u-heavyt-siber0186z']=101,
		['e-u-heavyt-siber0182z']=102,
		['e-u-heavyt-siber0178z']=103,
		['e-u-heavyt-siber0177z']=104,
		['e-u-heavyt-siber0176z']=105,
		['e-u-heavyt-siber0181z']=106,
		['e-u-heavyt-siber0180z']=107,
		['e-u-heavyt-siber0179z']=108,
		['e-u-heavyt-siber0036z']=109,
		['e-u-heavyt-siber0035z']=110,
		['e-u-heavyt-siber0034z']=111,
		['e-u-heavyt-siber0039z']=112,
		['e-u-heavyt-siber0038z']=113,
		['e-u-heavyt-siber0037z']=114,
		['e-u-heavyt-siber0033z']=115,
		['e-u-heavyt-siber0029z']=116,
		['e-u-heavyt-siber0028z']=117,
		['e-u-heavyt-siber0027z']=118,
		['e-u-heavyt-siber0032z']=119,
		['e-u-heavyt-siber0031z']=120,
		['e-u-heavyt-siber0030z']=121,
		['e-u-heavyt-siber0040z']=122,
		['e-u-heavyt-siber0050z']=123,
		['e-u-heavyt-siber0049z']=124,
		['e-u-heavyt-siber0048z']=125,
		['e-u-heavyt-siber0053z']=126,
		['e-u-heavyt-siber0052z']=127,
		['e-u-heavyt-siber0051z']=128,
		['e-u-heavyt-siber0047z']=129,
		['e-u-heavyt-siber0043z']=130,
		['e-u-heavyt-siber0042z']=131,
		['e-u-heavyt-siber0041z']=132,
		['e-u-heavyt-siber0046z']=133,
		['e-u-heavyt-siber0045z']=134,
		['e-u-heavyt-siber0044z']=135,
		['e-u-heavyt-siber0009z']=136,
		['e-u-heavyt-siber0008z']=137,
		['e-u-heavyt-siber0007z']=138,
		['e-u-heavyt-siber0012z']=139,
		['e-u-heavyt-siber0011z']=140,
		['e-u-heavyt-siber0010z']=141,
		['e-u-heavyt-siber0006z']=142,
		['e-u-heavyt-siber0002z']=143,
		['e-u-heavyt-siber0001z']=144,
		['e-u-heavyt-siber0000z']=145,
		['e-u-heavyt-siber0005z']=146,
		['e-u-heavyt-siber0004z']=147,
		['e-u-heavyt-siber0003z']=148,
		['e-u-heavyt-siber0013z']=149,
		['e-u-heavyt-siber0023z']=150,
		['e-u-heavyt-siber0022z']=151,
		['e-u-heavyt-siber0021z']=152,
		['e-u-heavyt-siber0026z']=153,
		['e-u-heavyt-siber0025z']=154,
		['e-u-heavyt-siber0024z']=155,
		['e-u-heavyt-siber0020z']=156,
		['e-u-heavyt-siber0016z']=157,
		['e-u-heavyt-siber0015z']=158,
		['e-u-heavyt-siber0014z']=159,
		['e-u-heavyt-siber0019z']=160,
		['e-u-heavyt-siber0018z']=161,
		['e-u-heavyt-siber0017z']=162,
		['e-u-heavyt-siber0090z']=163,
		['e-u-heavyt-siber0089z']=164,
		['e-u-heavyt-siber0088z']=165,
		['e-u-heavyt-siber0093z']=166,
		['e-u-heavyt-siber0092z']=167,
		['e-u-heavyt-siber0091z']=168,
		['e-u-heavyt-siber0087z']=169,
		['e-u-heavyt-siber0083z']=170,
		['e-u-heavyt-siber0082z']=171,
		['e-u-heavyt-siber0081z']=172,
		['e-u-heavyt-siber0086z']=173,
		['e-u-heavyt-siber0085z']=174,
		['e-u-heavyt-siber0084z']=175,
		['e-u-heavyt-siber0094z']=176,
		['e-u-heavyt-siber0104z']=177,
		['e-u-heavyt-siber0103z']=178,
		['e-u-heavyt-siber0102z']=179,
		['e-u-heavyt-siber0107z']=180,
		['e-u-heavyt-siber0106z']=181,
		['e-u-heavyt-siber0105z']=182,
		['e-u-heavyt-siber0101z']=183,
		['e-u-heavyt-siber0097z']=184,
		['e-u-heavyt-siber0096z']=185,
		['e-u-heavyt-siber0095z']=186,
		['e-u-heavyt-siber0100z']=187,
		['e-u-heavyt-siber0099z']=188,
		['e-u-heavyt-siber0098z']=189,
		['e-u-heavyt-siber0063z']=190,
		['e-u-heavyt-siber0062z']=191,
		['e-u-heavyt-siber0061z']=192,
		['e-u-heavyt-siber0066z']=193,
		['e-u-heavyt-siber0065z']=194,
		['e-u-heavyt-siber0064z']=195,
		['e-u-heavyt-siber0060z']=196,
		['e-u-heavyt-siber0056z']=197,
		['e-u-heavyt-siber0055z']=198,
		['e-u-heavyt-siber0054z']=199,
		['e-u-heavyt-siber0059z']=200,
		['e-u-heavyt-siber0058z']=201,
		['e-u-heavyt-siber0057z']=202,
		['e-u-heavyt-siber0067z']=203,
		['e-u-heavyt-siber0077z']=204,
		['e-u-heavyt-siber0076z']=205,
		['e-u-heavyt-siber0075z']=206,
		['e-u-heavyt-siber0080z']=207,
		['e-u-heavyt-siber0079z']=208,
		['e-u-heavyt-siber0078z']=209,
		['e-u-heavyt-siber0074z']=210,
		['e-u-heavyt-siber0070z']=211,
		['e-u-heavyt-siber0069z']=212,
		['e-u-heavyt-siber0068z']=213,
		['e-u-heavyt-siber0073z']=214,
		['e-u-heavyt-siber0072z']=215,
		['e-u-heavyt-siber0071z']=216,
	},
};
