--GENERATED ON: 05/12/2024 06:59:46


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['e-u-lw-solarz'] = {
	TEX_WIDTH=1012,
	TEX_HEIGHT=728,
	FRAME_COUNT=72,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0909090909090909,0),
		[3]=v2fM(0,181818181818182,0),
		[4]=v2fM(0,272727272727273,0),
		[5]=v2fM(0,363636363636364,0),
		[6]=v2fM(0,454545454545455,0),
		[7]=v2fM(0,545454545454545,0),
		[8]=v2fM(0,636363636363636,0),
		[9]=v2fM(0,727272727272727,0),
		[10]=v2fM(0,818181818181818,0),
		[11]=v2fM(0,909090909090909,0),
		[12]=v2fM(0,0,142857142857143),
		[13]=v2fM(0,0909090909090909,0,142857142857143),
		[14]=v2fM(0,181818181818182,0,142857142857143),
		[15]=v2fM(0,272727272727273,0,142857142857143),
		[16]=v2fM(0,363636363636364,0,142857142857143),
		[17]=v2fM(0,454545454545455,0,142857142857143),
		[18]=v2fM(0,545454545454545,0,142857142857143),
		[19]=v2fM(0,636363636363636,0,142857142857143),
		[20]=v2fM(0,727272727272727,0,142857142857143),
		[21]=v2fM(0,818181818181818,0,142857142857143),
		[22]=v2fM(0,909090909090909,0,142857142857143),
		[23]=v2fM(0,0,285714285714286),
		[24]=v2fM(0,0909090909090909,0,285714285714286),
		[25]=v2fM(0,181818181818182,0,285714285714286),
		[26]=v2fM(0,272727272727273,0,285714285714286),
		[27]=v2fM(0,363636363636364,0,285714285714286),
		[28]=v2fM(0,454545454545455,0,285714285714286),
		[29]=v2fM(0,545454545454545,0,285714285714286),
		[30]=v2fM(0,636363636363636,0,285714285714286),
		[31]=v2fM(0,727272727272727,0,285714285714286),
		[32]=v2fM(0,818181818181818,0,285714285714286),
		[33]=v2fM(0,909090909090909,0,285714285714286),
		[34]=v2fM(0,0,428571428571429),
		[35]=v2fM(0,0909090909090909,0,428571428571429),
		[36]=v2fM(0,181818181818182,0,428571428571429),
		[37]=v2fM(0,272727272727273,0,428571428571429),
		[38]=v2fM(0,363636363636364,0,428571428571429),
		[39]=v2fM(0,454545454545455,0,428571428571429),
		[40]=v2fM(0,545454545454545,0,428571428571429),
		[41]=v2fM(0,636363636363636,0,428571428571429),
		[42]=v2fM(0,727272727272727,0,428571428571429),
		[43]=v2fM(0,818181818181818,0,428571428571429),
		[44]=v2fM(0,909090909090909,0,428571428571429),
		[45]=v2fM(0,0,571428571428571),
		[46]=v2fM(0,0909090909090909,0,571428571428571),
		[47]=v2fM(0,181818181818182,0,571428571428571),
		[48]=v2fM(0,272727272727273,0,571428571428571),
		[49]=v2fM(0,363636363636364,0,571428571428571),
		[50]=v2fM(0,454545454545455,0,571428571428571),
		[51]=v2fM(0,545454545454545,0,571428571428571),
		[52]=v2fM(0,636363636363636,0,571428571428571),
		[53]=v2fM(0,727272727272727,0,571428571428571),
		[54]=v2fM(0,818181818181818,0,571428571428571),
		[55]=v2fM(0,909090909090909,0,571428571428571),
		[56]=v2fM(0,0,714285714285714),
		[57]=v2fM(0,0909090909090909,0,714285714285714),
		[58]=v2fM(0,181818181818182,0,714285714285714),
		[59]=v2fM(0,272727272727273,0,714285714285714),
		[60]=v2fM(0,363636363636364,0,714285714285714),
		[61]=v2fM(0,454545454545455,0,714285714285714),
		[62]=v2fM(0,545454545454545,0,714285714285714),
		[63]=v2fM(0,636363636363636,0,714285714285714),
		[64]=v2fM(0,727272727272727,0,714285714285714),
		[65]=v2fM(0,818181818181818,0,714285714285714),
		[66]=v2fM(0,909090909090909,0,714285714285714),
		[67]=v2fM(0,0,857142857142857),
		[68]=v2fM(0,0909090909090909,0,857142857142857),
		[69]=v2fM(0,181818181818182,0,857142857142857),
		[70]=v2fM(0,272727272727273,0,857142857142857),
		[71]=v2fM(0,363636363636364,0,857142857142857),
		[72]=v2fM(0,454545454545455,0,857142857142857),
	},
	FRAME_SIZES={
		[1]={WIDTH=92,HEIGHT=104},
		[2]={WIDTH=92,HEIGHT=104},
		[3]={WIDTH=92,HEIGHT=104},
		[4]={WIDTH=92,HEIGHT=104},
		[5]={WIDTH=92,HEIGHT=104},
		[6]={WIDTH=92,HEIGHT=104},
		[7]={WIDTH=92,HEIGHT=104},
		[8]={WIDTH=92,HEIGHT=104},
		[9]={WIDTH=92,HEIGHT=104},
		[10]={WIDTH=92,HEIGHT=104},
		[11]={WIDTH=92,HEIGHT=104},
		[12]={WIDTH=92,HEIGHT=104},
		[13]={WIDTH=92,HEIGHT=104},
		[14]={WIDTH=92,HEIGHT=104},
		[15]={WIDTH=92,HEIGHT=104},
		[16]={WIDTH=92,HEIGHT=104},
		[17]={WIDTH=92,HEIGHT=104},
		[18]={WIDTH=92,HEIGHT=104},
		[19]={WIDTH=92,HEIGHT=104},
		[20]={WIDTH=92,HEIGHT=104},
		[21]={WIDTH=92,HEIGHT=104},
		[22]={WIDTH=92,HEIGHT=104},
		[23]={WIDTH=92,HEIGHT=104},
		[24]={WIDTH=92,HEIGHT=104},
		[25]={WIDTH=92,HEIGHT=104},
		[26]={WIDTH=92,HEIGHT=104},
		[27]={WIDTH=92,HEIGHT=104},
		[28]={WIDTH=92,HEIGHT=104},
		[29]={WIDTH=92,HEIGHT=104},
		[30]={WIDTH=92,HEIGHT=104},
		[31]={WIDTH=92,HEIGHT=104},
		[32]={WIDTH=92,HEIGHT=104},
		[33]={WIDTH=92,HEIGHT=104},
		[34]={WIDTH=92,HEIGHT=104},
		[35]={WIDTH=92,HEIGHT=104},
		[36]={WIDTH=92,HEIGHT=104},
		[37]={WIDTH=92,HEIGHT=104},
		[38]={WIDTH=92,HEIGHT=104},
		[39]={WIDTH=92,HEIGHT=104},
		[40]={WIDTH=92,HEIGHT=104},
		[41]={WIDTH=92,HEIGHT=104},
		[42]={WIDTH=92,HEIGHT=104},
		[43]={WIDTH=92,HEIGHT=104},
		[44]={WIDTH=92,HEIGHT=104},
		[45]={WIDTH=92,HEIGHT=104},
		[46]={WIDTH=92,HEIGHT=104},
		[47]={WIDTH=92,HEIGHT=104},
		[48]={WIDTH=92,HEIGHT=104},
		[49]={WIDTH=92,HEIGHT=104},
		[50]={WIDTH=92,HEIGHT=104},
		[51]={WIDTH=92,HEIGHT=104},
		[52]={WIDTH=92,HEIGHT=104},
		[53]={WIDTH=92,HEIGHT=104},
		[54]={WIDTH=92,HEIGHT=104},
		[55]={WIDTH=92,HEIGHT=104},
		[56]={WIDTH=92,HEIGHT=104},
		[57]={WIDTH=92,HEIGHT=104},
		[58]={WIDTH=92,HEIGHT=104},
		[59]={WIDTH=92,HEIGHT=104},
		[60]={WIDTH=92,HEIGHT=104},
		[61]={WIDTH=92,HEIGHT=104},
		[62]={WIDTH=92,HEIGHT=104},
		[63]={WIDTH=92,HEIGHT=104},
		[64]={WIDTH=92,HEIGHT=104},
		[65]={WIDTH=92,HEIGHT=104},
		[66]={WIDTH=92,HEIGHT=104},
		[67]={WIDTH=92,HEIGHT=104},
		[68]={WIDTH=92,HEIGHT=104},
		[69]={WIDTH=92,HEIGHT=104},
		[70]={WIDTH=92,HEIGHT=104},
		[71]={WIDTH=92,HEIGHT=104},
		[72]={WIDTH=92,HEIGHT=104},
	},
	FRAME_INDEXS={
		['e-u-lw-solar0047z']=1,
		['e-u-lw-solar0048z']=2,
		['e-u-lw-solar0045z']=3,
		['e-u-lw-solar0046z']=4,
		['e-u-lw-solar0049z']=5,
		['e-u-lw-solar0052z']=6,
		['e-u-lw-solar0053z']=7,
		['e-u-lw-solar0050z']=8,
		['e-u-lw-solar0051z']=9,
		['e-u-lw-solar0038z']=10,
		['e-u-lw-solar0039z']=11,
		['e-u-lw-solar0036z']=12,
		['e-u-lw-solar0037z']=13,
		['e-u-lw-solar0040z']=14,
		['e-u-lw-solar0043z']=15,
		['e-u-lw-solar0044z']=16,
		['e-u-lw-solar0041z']=17,
		['e-u-lw-solar0042z']=18,
		['e-u-lw-solar0065z']=19,
		['e-u-lw-solar0066z']=20,
		['e-u-lw-solar0063z']=21,
		['e-u-lw-solar0064z']=22,
		['e-u-lw-solar0067z']=23,
		['e-u-lw-solar0070z']=24,
		['e-u-lw-solar0071z']=25,
		['e-u-lw-solar0068z']=26,
		['e-u-lw-solar0069z']=27,
		['e-u-lw-solar0056z']=28,
		['e-u-lw-solar0057z']=29,
		['e-u-lw-solar0054z']=30,
		['e-u-lw-solar0055z']=31,
		['e-u-lw-solar0058z']=32,
		['e-u-lw-solar0061z']=33,
		['e-u-lw-solar0062z']=34,
		['e-u-lw-solar0059z']=35,
		['e-u-lw-solar0060z']=36,
		['e-u-lw-solar0011z']=37,
		['e-u-lw-solar0012z']=38,
		['e-u-lw-solar0009z']=39,
		['e-u-lw-solar0010z']=40,
		['e-u-lw-solar0013z']=41,
		['e-u-lw-solar0016z']=42,
		['e-u-lw-solar0017z']=43,
		['e-u-lw-solar0014z']=44,
		['e-u-lw-solar0015z']=45,
		['e-u-lw-solar0002z']=46,
		['e-u-lw-solar0003z']=47,
		['e-u-lw-solar0000z']=48,
		['e-u-lw-solar0001z']=49,
		['e-u-lw-solar0004z']=50,
		['e-u-lw-solar0007z']=51,
		['e-u-lw-solar0008z']=52,
		['e-u-lw-solar0005z']=53,
		['e-u-lw-solar0006z']=54,
		['e-u-lw-solar0029z']=55,
		['e-u-lw-solar0030z']=56,
		['e-u-lw-solar0027z']=57,
		['e-u-lw-solar0028z']=58,
		['e-u-lw-solar0031z']=59,
		['e-u-lw-solar0034z']=60,
		['e-u-lw-solar0035z']=61,
		['e-u-lw-solar0032z']=62,
		['e-u-lw-solar0033z']=63,
		['e-u-lw-solar0020z']=64,
		['e-u-lw-solar0021z']=65,
		['e-u-lw-solar0018z']=66,
		['e-u-lw-solar0019z']=67,
		['e-u-lw-solar0022z']=68,
		['e-u-lw-solar0025z']=69,
		['e-u-lw-solar0026z']=70,
		['e-u-lw-solar0023z']=71,
		['e-u-lw-solar0024z']=72,
	},
};
