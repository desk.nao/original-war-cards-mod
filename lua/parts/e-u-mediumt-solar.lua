--GENERATED ON: 05/12/2024 06:59:49


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['e-u-mediumt-solar'] = {
	TEX_WIDTH=2036,
	TEX_HEIGHT=954,
	FRAME_COUNT=144,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0451866404715128,0),
		[3]=v2fM(0,0903732809430255,0),
		[4]=v2fM(0,135559921414538,0),
		[5]=v2fM(0,180746561886051,0),
		[6]=v2fM(0,225933202357564,0),
		[7]=v2fM(0,271119842829077,0),
		[8]=v2fM(0,316306483300589,0),
		[9]=v2fM(0,361493123772102,0),
		[10]=v2fM(0,406679764243615,0),
		[11]=v2fM(0,451866404715128,0),
		[12]=v2fM(0,0,111111111111111),
		[13]=v2fM(0,0451866404715128,0,111111111111111),
		[14]=v2fM(0,0903732809430255,0,111111111111111),
		[15]=v2fM(0,135559921414538,0,111111111111111),
		[16]=v2fM(0,180746561886051,0,111111111111111),
		[17]=v2fM(0,225933202357564,0,111111111111111),
		[18]=v2fM(0,271119842829077,0,111111111111111),
		[19]=v2fM(0,316306483300589,0,111111111111111),
		[20]=v2fM(0,361493123772102,0,111111111111111),
		[21]=v2fM(0,406679764243615,0,111111111111111),
		[22]=v2fM(0,451866404715128,0,111111111111111),
		[23]=v2fM(0,0,222222222222222),
		[24]=v2fM(0,0451866404715128,0,222222222222222),
		[25]=v2fM(0,0903732809430255,0,222222222222222),
		[26]=v2fM(0,135559921414538,0,222222222222222),
		[27]=v2fM(0,180746561886051,0,222222222222222),
		[28]=v2fM(0,225933202357564,0,222222222222222),
		[29]=v2fM(0,271119842829077,0,222222222222222),
		[30]=v2fM(0,316306483300589,0,222222222222222),
		[31]=v2fM(0,361493123772102,0,222222222222222),
		[32]=v2fM(0,406679764243615,0,222222222222222),
		[33]=v2fM(0,451866404715128,0,222222222222222),
		[34]=v2fM(0,0,333333333333333),
		[35]=v2fM(0,0451866404715128,0,333333333333333),
		[36]=v2fM(0,0903732809430255,0,333333333333333),
		[37]=v2fM(0,135559921414538,0,333333333333333),
		[38]=v2fM(0,180746561886051,0,333333333333333),
		[39]=v2fM(0,225933202357564,0,333333333333333),
		[40]=v2fM(0,271119842829077,0,333333333333333),
		[41]=v2fM(0,316306483300589,0,333333333333333),
		[42]=v2fM(0,361493123772102,0,333333333333333),
		[43]=v2fM(0,406679764243615,0,333333333333333),
		[44]=v2fM(0,451866404715128,0,333333333333333),
		[45]=v2fM(0,0,444444444444444),
		[46]=v2fM(0,0451866404715128,0,444444444444444),
		[47]=v2fM(0,0903732809430255,0,444444444444444),
		[48]=v2fM(0,135559921414538,0,444444444444444),
		[49]=v2fM(0,180746561886051,0,444444444444444),
		[50]=v2fM(0,225933202357564,0,444444444444444),
		[51]=v2fM(0,271119842829077,0,444444444444444),
		[52]=v2fM(0,316306483300589,0,444444444444444),
		[53]=v2fM(0,361493123772102,0,444444444444444),
		[54]=v2fM(0,406679764243615,0,444444444444444),
		[55]=v2fM(0,451866404715128,0,444444444444444),
		[56]=v2fM(0,0,555555555555556),
		[57]=v2fM(0,0451866404715128,0,555555555555556),
		[58]=v2fM(0,0903732809430255,0,555555555555556),
		[59]=v2fM(0,135559921414538,0,555555555555556),
		[60]=v2fM(0,180746561886051,0,555555555555556),
		[61]=v2fM(0,225933202357564,0,555555555555556),
		[62]=v2fM(0,271119842829077,0,555555555555556),
		[63]=v2fM(0,316306483300589,0,555555555555556),
		[64]=v2fM(0,361493123772102,0,555555555555556),
		[65]=v2fM(0,406679764243615,0,555555555555556),
		[66]=v2fM(0,451866404715128,0,555555555555556),
		[67]=v2fM(0,0,666666666666667),
		[68]=v2fM(0,0451866404715128,0,666666666666667),
		[69]=v2fM(0,0903732809430255,0,666666666666667),
		[70]=v2fM(0,135559921414538,0,666666666666667),
		[71]=v2fM(0,180746561886051,0,666666666666667),
		[72]=v2fM(0,225933202357564,0,666666666666667),
		[73]=v2fM(0,271119842829077,0,666666666666667),
		[74]=v2fM(0,316306483300589,0,666666666666667),
		[75]=v2fM(0,361493123772102,0,666666666666667),
		[76]=v2fM(0,406679764243615,0,666666666666667),
		[77]=v2fM(0,451866404715128,0,666666666666667),
		[78]=v2fM(0,0,777777777777778),
		[79]=v2fM(0,0451866404715128,0,777777777777778),
		[80]=v2fM(0,0903732809430255,0,777777777777778),
		[81]=v2fM(0,135559921414538,0,777777777777778),
		[82]=v2fM(0,180746561886051,0,777777777777778),
		[83]=v2fM(0,225933202357564,0,777777777777778),
		[84]=v2fM(0,271119842829077,0,777777777777778),
		[85]=v2fM(0,316306483300589,0,777777777777778),
		[86]=v2fM(0,361493123772102,0,777777777777778),
		[87]=v2fM(0,406679764243615,0,777777777777778),
		[88]=v2fM(0,451866404715128,0,777777777777778),
		[89]=v2fM(0,0,888888888888889),
		[90]=v2fM(0,0451866404715128,0,888888888888889),
		[91]=v2fM(0,0903732809430255,0,888888888888889),
		[92]=v2fM(0,135559921414538,0,888888888888889),
		[93]=v2fM(0,180746561886051,0,888888888888889),
		[94]=v2fM(0,225933202357564,0,888888888888889),
		[95]=v2fM(0,271119842829077,0,888888888888889),
		[96]=v2fM(0,316306483300589,0,888888888888889),
		[97]=v2fM(0,361493123772102,0,888888888888889),
		[98]=v2fM(0,406679764243615,0,888888888888889),
		[99]=v2fM(0,451866404715128,0,888888888888889),
		[100]=v2fM(0,50294695481336,0),
		[101]=v2fM(0,548133595284872,0),
		[102]=v2fM(0,593320235756385,0),
		[103]=v2fM(0,638506876227898,0),
		[104]=v2fM(0,683693516699411,0),
		[105]=v2fM(0,728880157170923,0),
		[106]=v2fM(0,774066797642436,0),
		[107]=v2fM(0,819253438113949,0),
		[108]=v2fM(0,864440078585462,0),
		[109]=v2fM(0,909626719056974,0),
		[110]=v2fM(0,954813359528487,0),
		[111]=v2fM(0,50294695481336,0,111111111111111),
		[112]=v2fM(0,548133595284872,0,111111111111111),
		[113]=v2fM(0,593320235756385,0,111111111111111),
		[114]=v2fM(0,638506876227898,0,111111111111111),
		[115]=v2fM(0,683693516699411,0,111111111111111),
		[116]=v2fM(0,728880157170923,0,111111111111111),
		[117]=v2fM(0,774066797642436,0,111111111111111),
		[118]=v2fM(0,819253438113949,0,111111111111111),
		[119]=v2fM(0,864440078585462,0,111111111111111),
		[120]=v2fM(0,909626719056974,0,111111111111111),
		[121]=v2fM(0,954813359528487,0,111111111111111),
		[122]=v2fM(0,50294695481336,0,222222222222222),
		[123]=v2fM(0,548133595284872,0,222222222222222),
		[124]=v2fM(0,593320235756385,0,222222222222222),
		[125]=v2fM(0,638506876227898,0,222222222222222),
		[126]=v2fM(0,683693516699411,0,222222222222222),
		[127]=v2fM(0,728880157170923,0,222222222222222),
		[128]=v2fM(0,774066797642436,0,222222222222222),
		[129]=v2fM(0,819253438113949,0,222222222222222),
		[130]=v2fM(0,864440078585462,0,222222222222222),
		[131]=v2fM(0,909626719056974,0,222222222222222),
		[132]=v2fM(0,954813359528487,0,222222222222222),
		[133]=v2fM(0,50294695481336,0,333333333333333),
		[134]=v2fM(0,548133595284872,0,333333333333333),
		[135]=v2fM(0,593320235756385,0,333333333333333),
		[136]=v2fM(0,638506876227898,0,333333333333333),
		[137]=v2fM(0,683693516699411,0,333333333333333),
		[138]=v2fM(0,728880157170923,0,333333333333333),
		[139]=v2fM(0,774066797642436,0,333333333333333),
		[140]=v2fM(0,819253438113949,0,333333333333333),
		[141]=v2fM(0,864440078585462,0,333333333333333),
		[142]=v2fM(0,909626719056974,0,333333333333333),
		[143]=v2fM(0,954813359528487,0,333333333333333),
		[144]=v2fM(0,50294695481336,0,444444444444444),
	},
	FRAME_SIZES={
		[1]={WIDTH=92,HEIGHT=106},
		[2]={WIDTH=92,HEIGHT=106},
		[3]={WIDTH=92,HEIGHT=106},
		[4]={WIDTH=92,HEIGHT=106},
		[5]={WIDTH=92,HEIGHT=106},
		[6]={WIDTH=92,HEIGHT=106},
		[7]={WIDTH=92,HEIGHT=106},
		[8]={WIDTH=92,HEIGHT=106},
		[9]={WIDTH=92,HEIGHT=106},
		[10]={WIDTH=92,HEIGHT=106},
		[11]={WIDTH=92,HEIGHT=106},
		[12]={WIDTH=92,HEIGHT=106},
		[13]={WIDTH=92,HEIGHT=106},
		[14]={WIDTH=92,HEIGHT=106},
		[15]={WIDTH=92,HEIGHT=106},
		[16]={WIDTH=92,HEIGHT=106},
		[17]={WIDTH=92,HEIGHT=106},
		[18]={WIDTH=92,HEIGHT=106},
		[19]={WIDTH=92,HEIGHT=106},
		[20]={WIDTH=92,HEIGHT=106},
		[21]={WIDTH=92,HEIGHT=106},
		[22]={WIDTH=92,HEIGHT=106},
		[23]={WIDTH=92,HEIGHT=106},
		[24]={WIDTH=92,HEIGHT=106},
		[25]={WIDTH=92,HEIGHT=106},
		[26]={WIDTH=92,HEIGHT=106},
		[27]={WIDTH=92,HEIGHT=106},
		[28]={WIDTH=92,HEIGHT=106},
		[29]={WIDTH=92,HEIGHT=106},
		[30]={WIDTH=92,HEIGHT=106},
		[31]={WIDTH=92,HEIGHT=106},
		[32]={WIDTH=92,HEIGHT=106},
		[33]={WIDTH=92,HEIGHT=106},
		[34]={WIDTH=92,HEIGHT=106},
		[35]={WIDTH=92,HEIGHT=106},
		[36]={WIDTH=92,HEIGHT=106},
		[37]={WIDTH=92,HEIGHT=106},
		[38]={WIDTH=92,HEIGHT=106},
		[39]={WIDTH=92,HEIGHT=106},
		[40]={WIDTH=92,HEIGHT=106},
		[41]={WIDTH=92,HEIGHT=106},
		[42]={WIDTH=92,HEIGHT=106},
		[43]={WIDTH=92,HEIGHT=106},
		[44]={WIDTH=92,HEIGHT=106},
		[45]={WIDTH=92,HEIGHT=106},
		[46]={WIDTH=92,HEIGHT=106},
		[47]={WIDTH=92,HEIGHT=106},
		[48]={WIDTH=92,HEIGHT=106},
		[49]={WIDTH=92,HEIGHT=106},
		[50]={WIDTH=92,HEIGHT=106},
		[51]={WIDTH=92,HEIGHT=106},
		[52]={WIDTH=92,HEIGHT=106},
		[53]={WIDTH=92,HEIGHT=106},
		[54]={WIDTH=92,HEIGHT=106},
		[55]={WIDTH=92,HEIGHT=106},
		[56]={WIDTH=92,HEIGHT=106},
		[57]={WIDTH=92,HEIGHT=106},
		[58]={WIDTH=92,HEIGHT=106},
		[59]={WIDTH=92,HEIGHT=106},
		[60]={WIDTH=92,HEIGHT=106},
		[61]={WIDTH=92,HEIGHT=106},
		[62]={WIDTH=92,HEIGHT=106},
		[63]={WIDTH=92,HEIGHT=106},
		[64]={WIDTH=92,HEIGHT=106},
		[65]={WIDTH=92,HEIGHT=106},
		[66]={WIDTH=92,HEIGHT=106},
		[67]={WIDTH=92,HEIGHT=106},
		[68]={WIDTH=92,HEIGHT=106},
		[69]={WIDTH=92,HEIGHT=106},
		[70]={WIDTH=92,HEIGHT=106},
		[71]={WIDTH=92,HEIGHT=106},
		[72]={WIDTH=92,HEIGHT=106},
		[73]={WIDTH=92,HEIGHT=106},
		[74]={WIDTH=92,HEIGHT=106},
		[75]={WIDTH=92,HEIGHT=106},
		[76]={WIDTH=92,HEIGHT=106},
		[77]={WIDTH=92,HEIGHT=106},
		[78]={WIDTH=92,HEIGHT=106},
		[79]={WIDTH=92,HEIGHT=106},
		[80]={WIDTH=92,HEIGHT=106},
		[81]={WIDTH=92,HEIGHT=106},
		[82]={WIDTH=92,HEIGHT=106},
		[83]={WIDTH=92,HEIGHT=106},
		[84]={WIDTH=92,HEIGHT=106},
		[85]={WIDTH=92,HEIGHT=106},
		[86]={WIDTH=92,HEIGHT=106},
		[87]={WIDTH=92,HEIGHT=106},
		[88]={WIDTH=92,HEIGHT=106},
		[89]={WIDTH=92,HEIGHT=106},
		[90]={WIDTH=92,HEIGHT=106},
		[91]={WIDTH=92,HEIGHT=106},
		[92]={WIDTH=92,HEIGHT=106},
		[93]={WIDTH=92,HEIGHT=106},
		[94]={WIDTH=92,HEIGHT=106},
		[95]={WIDTH=92,HEIGHT=106},
		[96]={WIDTH=92,HEIGHT=106},
		[97]={WIDTH=92,HEIGHT=106},
		[98]={WIDTH=92,HEIGHT=106},
		[99]={WIDTH=92,HEIGHT=106},
		[100]={WIDTH=92,HEIGHT=106},
		[101]={WIDTH=92,HEIGHT=106},
		[102]={WIDTH=92,HEIGHT=106},
		[103]={WIDTH=92,HEIGHT=106},
		[104]={WIDTH=92,HEIGHT=106},
		[105]={WIDTH=92,HEIGHT=106},
		[106]={WIDTH=92,HEIGHT=106},
		[107]={WIDTH=92,HEIGHT=106},
		[108]={WIDTH=92,HEIGHT=106},
		[109]={WIDTH=92,HEIGHT=106},
		[110]={WIDTH=92,HEIGHT=106},
		[111]={WIDTH=92,HEIGHT=106},
		[112]={WIDTH=92,HEIGHT=106},
		[113]={WIDTH=92,HEIGHT=106},
		[114]={WIDTH=92,HEIGHT=106},
		[115]={WIDTH=92,HEIGHT=106},
		[116]={WIDTH=92,HEIGHT=106},
		[117]={WIDTH=92,HEIGHT=106},
		[118]={WIDTH=92,HEIGHT=106},
		[119]={WIDTH=92,HEIGHT=106},
		[120]={WIDTH=92,HEIGHT=106},
		[121]={WIDTH=92,HEIGHT=106},
		[122]={WIDTH=92,HEIGHT=106},
		[123]={WIDTH=92,HEIGHT=106},
		[124]={WIDTH=92,HEIGHT=106},
		[125]={WIDTH=92,HEIGHT=106},
		[126]={WIDTH=92,HEIGHT=106},
		[127]={WIDTH=92,HEIGHT=106},
		[128]={WIDTH=92,HEIGHT=106},
		[129]={WIDTH=92,HEIGHT=106},
		[130]={WIDTH=92,HEIGHT=106},
		[131]={WIDTH=92,HEIGHT=106},
		[132]={WIDTH=92,HEIGHT=106},
		[133]={WIDTH=92,HEIGHT=106},
		[134]={WIDTH=92,HEIGHT=106},
		[135]={WIDTH=92,HEIGHT=106},
		[136]={WIDTH=92,HEIGHT=106},
		[137]={WIDTH=92,HEIGHT=106},
		[138]={WIDTH=92,HEIGHT=106},
		[139]={WIDTH=92,HEIGHT=106},
		[140]={WIDTH=92,HEIGHT=106},
		[141]={WIDTH=92,HEIGHT=106},
		[142]={WIDTH=92,HEIGHT=106},
		[143]={WIDTH=92,HEIGHT=106},
		[144]={WIDTH=92,HEIGHT=106},
	},
	FRAME_INDEXS={
		['e-u-mediumt-solar0096']=1,
		['e-u-mediumt-solar0095']=2,
		['e-u-mediumt-solar0098']=3,
		['e-u-mediumt-solar0097']=4,
		['e-u-mediumt-solar0094']=5,
		['e-u-mediumt-solar0091']=6,
		['e-u-mediumt-solar0090']=7,
		['e-u-mediumt-solar0093']=8,
		['e-u-mediumt-solar0092']=9,
		['e-u-mediumt-solar0105']=10,
		['e-u-mediumt-solar0104']=11,
		['e-u-mediumt-solar0107']=12,
		['e-u-mediumt-solar0106']=13,
		['e-u-mediumt-solar0103']=14,
		['e-u-mediumt-solar0100']=15,
		['e-u-mediumt-solar0099']=16,
		['e-u-mediumt-solar0102']=17,
		['e-u-mediumt-solar0101']=18,
		['e-u-mediumt-solar0078']=19,
		['e-u-mediumt-solar0077']=20,
		['e-u-mediumt-solar0080']=21,
		['e-u-mediumt-solar0079']=22,
		['e-u-mediumt-solar0076']=23,
		['e-u-mediumt-solar0073']=24,
		['e-u-mediumt-solar0072']=25,
		['e-u-mediumt-solar0075']=26,
		['e-u-mediumt-solar0074']=27,
		['e-u-mediumt-solar0087']=28,
		['e-u-mediumt-solar0086']=29,
		['e-u-mediumt-solar0089']=30,
		['e-u-mediumt-solar0088']=31,
		['e-u-mediumt-solar0085']=32,
		['e-u-mediumt-solar0082']=33,
		['e-u-mediumt-solar0081']=34,
		['e-u-mediumt-solar0084']=35,
		['e-u-mediumt-solar0083']=36,
		['e-u-mediumt-solar0132']=37,
		['e-u-mediumt-solar0131']=38,
		['e-u-mediumt-solar0134']=39,
		['e-u-mediumt-solar0133']=40,
		['e-u-mediumt-solar0130']=41,
		['e-u-mediumt-solar0127']=42,
		['e-u-mediumt-solar0126']=43,
		['e-u-mediumt-solar0129']=44,
		['e-u-mediumt-solar0128']=45,
		['e-u-mediumt-solar0141']=46,
		['e-u-mediumt-solar0140']=47,
		['e-u-mediumt-solar0143']=48,
		['e-u-mediumt-solar0142']=49,
		['e-u-mediumt-solar0139']=50,
		['e-u-mediumt-solar0136']=51,
		['e-u-mediumt-solar0135']=52,
		['e-u-mediumt-solar0138']=53,
		['e-u-mediumt-solar0137']=54,
		['e-u-mediumt-solar0114']=55,
		['e-u-mediumt-solar0113']=56,
		['e-u-mediumt-solar0116']=57,
		['e-u-mediumt-solar0115']=58,
		['e-u-mediumt-solar0112']=59,
		['e-u-mediumt-solar0109']=60,
		['e-u-mediumt-solar0108']=61,
		['e-u-mediumt-solar0111']=62,
		['e-u-mediumt-solar0110']=63,
		['e-u-mediumt-solar0123']=64,
		['e-u-mediumt-solar0122']=65,
		['e-u-mediumt-solar0125']=66,
		['e-u-mediumt-solar0124']=67,
		['e-u-mediumt-solar0121']=68,
		['e-u-mediumt-solar0118']=69,
		['e-u-mediumt-solar0117']=70,
		['e-u-mediumt-solar0120']=71,
		['e-u-mediumt-solar0119']=72,
		['e-u-mediumt-solar0024']=73,
		['e-u-mediumt-solar0023']=74,
		['e-u-mediumt-solar0026']=75,
		['e-u-mediumt-solar0025']=76,
		['e-u-mediumt-solar0022']=77,
		['e-u-mediumt-solar0019']=78,
		['e-u-mediumt-solar0018']=79,
		['e-u-mediumt-solar0021']=80,
		['e-u-mediumt-solar0020']=81,
		['e-u-mediumt-solar0033']=82,
		['e-u-mediumt-solar0032']=83,
		['e-u-mediumt-solar0035']=84,
		['e-u-mediumt-solar0034']=85,
		['e-u-mediumt-solar0031']=86,
		['e-u-mediumt-solar0028']=87,
		['e-u-mediumt-solar0027']=88,
		['e-u-mediumt-solar0030']=89,
		['e-u-mediumt-solar0029']=90,
		['e-u-mediumt-solar0006']=91,
		['e-u-mediumt-solar0005']=92,
		['e-u-mediumt-solar0008']=93,
		['e-u-mediumt-solar0007']=94,
		['e-u-mediumt-solar0004']=95,
		['e-u-mediumt-solar0001']=96,
		['e-u-mediumt-solar0000']=97,
		['e-u-mediumt-solar0003']=98,
		['e-u-mediumt-solar0002']=99,
		['e-u-mediumt-solar0015']=100,
		['e-u-mediumt-solar0014']=101,
		['e-u-mediumt-solar0017']=102,
		['e-u-mediumt-solar0016']=103,
		['e-u-mediumt-solar0013']=104,
		['e-u-mediumt-solar0010']=105,
		['e-u-mediumt-solar0009']=106,
		['e-u-mediumt-solar0012']=107,
		['e-u-mediumt-solar0011']=108,
		['e-u-mediumt-solar0060']=109,
		['e-u-mediumt-solar0059']=110,
		['e-u-mediumt-solar0062']=111,
		['e-u-mediumt-solar0061']=112,
		['e-u-mediumt-solar0058']=113,
		['e-u-mediumt-solar0055']=114,
		['e-u-mediumt-solar0054']=115,
		['e-u-mediumt-solar0057']=116,
		['e-u-mediumt-solar0056']=117,
		['e-u-mediumt-solar0069']=118,
		['e-u-mediumt-solar0068']=119,
		['e-u-mediumt-solar0071']=120,
		['e-u-mediumt-solar0070']=121,
		['e-u-mediumt-solar0067']=122,
		['e-u-mediumt-solar0064']=123,
		['e-u-mediumt-solar0063']=124,
		['e-u-mediumt-solar0066']=125,
		['e-u-mediumt-solar0065']=126,
		['e-u-mediumt-solar0042']=127,
		['e-u-mediumt-solar0041']=128,
		['e-u-mediumt-solar0044']=129,
		['e-u-mediumt-solar0043']=130,
		['e-u-mediumt-solar0040']=131,
		['e-u-mediumt-solar0037']=132,
		['e-u-mediumt-solar0036']=133,
		['e-u-mediumt-solar0039']=134,
		['e-u-mediumt-solar0038']=135,
		['e-u-mediumt-solar0051']=136,
		['e-u-mediumt-solar0050']=137,
		['e-u-mediumt-solar0053']=138,
		['e-u-mediumt-solar0052']=139,
		['e-u-mediumt-solar0049']=140,
		['e-u-mediumt-solar0046']=141,
		['e-u-mediumt-solar0045']=142,
		['e-u-mediumt-solar0048']=143,
		['e-u-mediumt-solar0047']=144,
	},
};
