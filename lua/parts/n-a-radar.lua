--GENERATED ON: 05/12/2024 06:59:56


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['n-a-radar'] = {
	TEX_WIDTH=988,
	TEX_HEIGHT=128,
	FRAME_COUNT=36,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0384615384615385,0),
		[3]=v2fM(0,0769230769230769,0),
		[4]=v2fM(0,115384615384615,0),
		[5]=v2fM(0,153846153846154,0),
		[6]=v2fM(0,192307692307692,0),
		[7]=v2fM(0,230769230769231,0),
		[8]=v2fM(0,269230769230769,0),
		[9]=v2fM(0,307692307692308,0),
		[10]=v2fM(0,346153846153846,0),
		[11]=v2fM(0,384615384615385,0),
		[12]=v2fM(0,423076923076923,0),
		[13]=v2fM(0,461538461538462,0),
		[14]=v2fM(0,5,0),
		[15]=v2fM(0,538461538461538,0),
		[16]=v2fM(0,576923076923077,0),
		[17]=v2fM(0,615384615384615,0),
		[18]=v2fM(0,653846153846154,0),
		[19]=v2fM(0,692307692307692,0),
		[20]=v2fM(0,730769230769231,0),
		[21]=v2fM(0,769230769230769,0),
		[22]=v2fM(0,807692307692308,0),
		[23]=v2fM(0,846153846153846,0),
		[24]=v2fM(0,884615384615385,0),
		[25]=v2fM(0,923076923076923,0),
		[26]=v2fM(0,961538461538462,0),
		[27]=v2fM(0,0,5),
		[28]=v2fM(0,0384615384615385,0,5),
		[29]=v2fM(0,0769230769230769,0,5),
		[30]=v2fM(0,115384615384615,0,5),
		[31]=v2fM(0,153846153846154,0,5),
		[32]=v2fM(0,192307692307692,0,5),
		[33]=v2fM(0,230769230769231,0,5),
		[34]=v2fM(0,269230769230769,0,5),
		[35]=v2fM(0,307692307692308,0,5),
		[36]=v2fM(0,346153846153846,0,5),
	},
	FRAME_SIZES={
		[1]={WIDTH=38,HEIGHT=64},
		[2]={WIDTH=38,HEIGHT=64},
		[3]={WIDTH=38,HEIGHT=64},
		[4]={WIDTH=38,HEIGHT=64},
		[5]={WIDTH=38,HEIGHT=64},
		[6]={WIDTH=38,HEIGHT=64},
		[7]={WIDTH=38,HEIGHT=64},
		[8]={WIDTH=38,HEIGHT=64},
		[9]={WIDTH=38,HEIGHT=64},
		[10]={WIDTH=38,HEIGHT=64},
		[11]={WIDTH=38,HEIGHT=64},
		[12]={WIDTH=38,HEIGHT=64},
		[13]={WIDTH=38,HEIGHT=64},
		[14]={WIDTH=38,HEIGHT=64},
		[15]={WIDTH=38,HEIGHT=64},
		[16]={WIDTH=38,HEIGHT=64},
		[17]={WIDTH=38,HEIGHT=64},
		[18]={WIDTH=38,HEIGHT=64},
		[19]={WIDTH=38,HEIGHT=64},
		[20]={WIDTH=38,HEIGHT=64},
		[21]={WIDTH=38,HEIGHT=64},
		[22]={WIDTH=38,HEIGHT=64},
		[23]={WIDTH=38,HEIGHT=64},
		[24]={WIDTH=38,HEIGHT=64},
		[25]={WIDTH=38,HEIGHT=64},
		[26]={WIDTH=38,HEIGHT=64},
		[27]={WIDTH=38,HEIGHT=64},
		[28]={WIDTH=38,HEIGHT=64},
		[29]={WIDTH=38,HEIGHT=64},
		[30]={WIDTH=38,HEIGHT=64},
		[31]={WIDTH=38,HEIGHT=64},
		[32]={WIDTH=38,HEIGHT=64},
		[33]={WIDTH=38,HEIGHT=64},
		[34]={WIDTH=38,HEIGHT=64},
		[35]={WIDTH=38,HEIGHT=64},
		[36]={WIDTH=38,HEIGHT=64},
	},
	FRAME_INDEXS={
		['n-a-radar0024']=1,
		['n-a-radar0023']=2,
		['n-a-radar0026']=3,
		['n-a-radar0025']=4,
		['n-a-radar0022']=5,
		['n-a-radar0019']=6,
		['n-a-radar0018']=7,
		['n-a-radar0021']=8,
		['n-a-radar0020']=9,
		['n-a-radar0033']=10,
		['n-a-radar0032']=11,
		['n-a-radar0035']=12,
		['n-a-radar0034']=13,
		['n-a-radar0031']=14,
		['n-a-radar0028']=15,
		['n-a-radar0027']=16,
		['n-a-radar0030']=17,
		['n-a-radar0029']=18,
		['n-a-radar0006']=19,
		['n-a-radar0005']=20,
		['n-a-radar0008']=21,
		['n-a-radar0007']=22,
		['n-a-radar0004']=23,
		['n-a-radar0001']=24,
		['n-a-radar0000']=25,
		['n-a-radar0003']=26,
		['n-a-radar0002']=27,
		['n-a-radar0015']=28,
		['n-a-radar0014']=29,
		['n-a-radar0017']=30,
		['n-a-radar0016']=31,
		['n-a-radar0013']=32,
		['n-a-radar0010']=33,
		['n-a-radar0009']=34,
		['n-a-radar0012']=35,
		['n-a-radar0011']=36,
	},
};
