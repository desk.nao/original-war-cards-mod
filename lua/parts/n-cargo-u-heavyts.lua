--GENERATED ON: 05/12/2024 07:00:02


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['n-cargo-u-heavyts'] = {
	TEX_WIDTH=1948,
	TEX_HEIGHT=960,
	FRAME_COUNT=72,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0790554414784394,0),
		[3]=v2fM(0,158110882956879,0),
		[4]=v2fM(0,237166324435318,0),
		[5]=v2fM(0,316221765913758,0),
		[6]=v2fM(0,395277207392197,0),
		[7]=v2fM(0,0,166666666666667),
		[8]=v2fM(0,0790554414784394,0,166666666666667),
		[9]=v2fM(0,158110882956879,0,166666666666667),
		[10]=v2fM(0,237166324435318,0,166666666666667),
		[11]=v2fM(0,316221765913758,0,166666666666667),
		[12]=v2fM(0,395277207392197,0,166666666666667),
		[13]=v2fM(0,0,333333333333333),
		[14]=v2fM(0,0790554414784394,0,333333333333333),
		[15]=v2fM(0,158110882956879,0,333333333333333),
		[16]=v2fM(0,237166324435318,0,333333333333333),
		[17]=v2fM(0,316221765913758,0,333333333333333),
		[18]=v2fM(0,395277207392197,0,333333333333333),
		[19]=v2fM(0,0,5),
		[20]=v2fM(0,0790554414784394,0,5),
		[21]=v2fM(0,158110882956879,0,5),
		[22]=v2fM(0,237166324435318,0,5),
		[23]=v2fM(0,316221765913758,0,5),
		[24]=v2fM(0,395277207392197,0,5),
		[25]=v2fM(0,0,666666666666667),
		[26]=v2fM(0,0790554414784394,0,666666666666667),
		[27]=v2fM(0,158110882956879,0,666666666666667),
		[28]=v2fM(0,237166324435318,0,666666666666667),
		[29]=v2fM(0,316221765913758,0,666666666666667),
		[30]=v2fM(0,395277207392197,0,666666666666667),
		[31]=v2fM(0,0,833333333333333),
		[32]=v2fM(0,0790554414784394,0,833333333333333),
		[33]=v2fM(0,158110882956879,0,833333333333333),
		[34]=v2fM(0,237166324435318,0,833333333333333),
		[35]=v2fM(0,316221765913758,0,833333333333333),
		[36]=v2fM(0,395277207392197,0,833333333333333),
		[37]=v2fM(0,525667351129363,0),
		[38]=v2fM(0,604722792607803,0),
		[39]=v2fM(0,683778234086242,0),
		[40]=v2fM(0,762833675564682,0),
		[41]=v2fM(0,841889117043121,0),
		[42]=v2fM(0,920944558521561,0),
		[43]=v2fM(0,525667351129363,0,166666666666667),
		[44]=v2fM(0,604722792607803,0,166666666666667),
		[45]=v2fM(0,683778234086242,0,166666666666667),
		[46]=v2fM(0,762833675564682,0,166666666666667),
		[47]=v2fM(0,841889117043121,0,166666666666667),
		[48]=v2fM(0,920944558521561,0,166666666666667),
		[49]=v2fM(0,525667351129363,0,333333333333333),
		[50]=v2fM(0,604722792607803,0,333333333333333),
		[51]=v2fM(0,683778234086242,0,333333333333333),
		[52]=v2fM(0,762833675564682,0,333333333333333),
		[53]=v2fM(0,841889117043121,0,333333333333333),
		[54]=v2fM(0,920944558521561,0,333333333333333),
		[55]=v2fM(0,525667351129363,0,5),
		[56]=v2fM(0,604722792607803,0,5),
		[57]=v2fM(0,683778234086242,0,5),
		[58]=v2fM(0,762833675564682,0,5),
		[59]=v2fM(0,841889117043121,0,5),
		[60]=v2fM(0,920944558521561,0,5),
		[61]=v2fM(0,525667351129363,0,666666666666667),
		[62]=v2fM(0,604722792607803,0,666666666666667),
		[63]=v2fM(0,683778234086242,0,666666666666667),
		[64]=v2fM(0,762833675564682,0,666666666666667),
		[65]=v2fM(0,841889117043121,0,666666666666667),
		[66]=v2fM(0,920944558521561,0,666666666666667),
		[67]=v2fM(0,525667351129363,0,833333333333333),
		[68]=v2fM(0,604722792607803,0,833333333333333),
		[69]=v2fM(0,683778234086242,0,833333333333333),
		[70]=v2fM(0,762833675564682,0,833333333333333),
		[71]=v2fM(0,841889117043121,0,833333333333333),
		[72]=v2fM(0,920944558521561,0,833333333333333),
	},
	FRAME_SIZES={
		[1]={WIDTH=154,HEIGHT=160},
		[2]={WIDTH=154,HEIGHT=160},
		[3]={WIDTH=154,HEIGHT=160},
		[4]={WIDTH=154,HEIGHT=160},
		[5]={WIDTH=154,HEIGHT=160},
		[6]={WIDTH=154,HEIGHT=160},
		[7]={WIDTH=154,HEIGHT=160},
		[8]={WIDTH=154,HEIGHT=160},
		[9]={WIDTH=154,HEIGHT=160},
		[10]={WIDTH=154,HEIGHT=160},
		[11]={WIDTH=154,HEIGHT=160},
		[12]={WIDTH=154,HEIGHT=160},
		[13]={WIDTH=154,HEIGHT=160},
		[14]={WIDTH=154,HEIGHT=160},
		[15]={WIDTH=154,HEIGHT=160},
		[16]={WIDTH=154,HEIGHT=160},
		[17]={WIDTH=154,HEIGHT=160},
		[18]={WIDTH=154,HEIGHT=160},
		[19]={WIDTH=154,HEIGHT=160},
		[20]={WIDTH=154,HEIGHT=160},
		[21]={WIDTH=154,HEIGHT=160},
		[22]={WIDTH=154,HEIGHT=160},
		[23]={WIDTH=154,HEIGHT=160},
		[24]={WIDTH=154,HEIGHT=160},
		[25]={WIDTH=154,HEIGHT=160},
		[26]={WIDTH=154,HEIGHT=160},
		[27]={WIDTH=154,HEIGHT=160},
		[28]={WIDTH=154,HEIGHT=160},
		[29]={WIDTH=154,HEIGHT=160},
		[30]={WIDTH=154,HEIGHT=160},
		[31]={WIDTH=154,HEIGHT=160},
		[32]={WIDTH=154,HEIGHT=160},
		[33]={WIDTH=154,HEIGHT=160},
		[34]={WIDTH=154,HEIGHT=160},
		[35]={WIDTH=154,HEIGHT=160},
		[36]={WIDTH=154,HEIGHT=160},
		[37]={WIDTH=154,HEIGHT=160},
		[38]={WIDTH=154,HEIGHT=160},
		[39]={WIDTH=154,HEIGHT=160},
		[40]={WIDTH=154,HEIGHT=160},
		[41]={WIDTH=154,HEIGHT=160},
		[42]={WIDTH=154,HEIGHT=160},
		[43]={WIDTH=154,HEIGHT=160},
		[44]={WIDTH=154,HEIGHT=160},
		[45]={WIDTH=154,HEIGHT=160},
		[46]={WIDTH=154,HEIGHT=160},
		[47]={WIDTH=154,HEIGHT=160},
		[48]={WIDTH=154,HEIGHT=160},
		[49]={WIDTH=154,HEIGHT=160},
		[50]={WIDTH=154,HEIGHT=160},
		[51]={WIDTH=154,HEIGHT=160},
		[52]={WIDTH=154,HEIGHT=160},
		[53]={WIDTH=154,HEIGHT=160},
		[54]={WIDTH=154,HEIGHT=160},
		[55]={WIDTH=154,HEIGHT=160},
		[56]={WIDTH=154,HEIGHT=160},
		[57]={WIDTH=154,HEIGHT=160},
		[58]={WIDTH=154,HEIGHT=160},
		[59]={WIDTH=154,HEIGHT=160},
		[60]={WIDTH=154,HEIGHT=160},
		[61]={WIDTH=154,HEIGHT=160},
		[62]={WIDTH=154,HEIGHT=160},
		[63]={WIDTH=154,HEIGHT=160},
		[64]={WIDTH=154,HEIGHT=160},
		[65]={WIDTH=154,HEIGHT=160},
		[66]={WIDTH=154,HEIGHT=160},
		[67]={WIDTH=154,HEIGHT=160},
		[68]={WIDTH=154,HEIGHT=160},
		[69]={WIDTH=154,HEIGHT=160},
		[70]={WIDTH=154,HEIGHT=160},
		[71]={WIDTH=154,HEIGHT=160},
		[72]={WIDTH=154,HEIGHT=160},
	},
	FRAME_INDEXS={
		['n-cargo-u-heavyt0047s']=1,
		['n-cargo-u-heavyt0048s']=2,
		['n-cargo-u-heavyt0045s']=3,
		['n-cargo-u-heavyt0046s']=4,
		['n-cargo-u-heavyt0049s']=5,
		['n-cargo-u-heavyt0052s']=6,
		['n-cargo-u-heavyt0053s']=7,
		['n-cargo-u-heavyt0050s']=8,
		['n-cargo-u-heavyt0051s']=9,
		['n-cargo-u-heavyt0038s']=10,
		['n-cargo-u-heavyt0039s']=11,
		['n-cargo-u-heavyt0036s']=12,
		['n-cargo-u-heavyt0037s']=13,
		['n-cargo-u-heavyt0040s']=14,
		['n-cargo-u-heavyt0043s']=15,
		['n-cargo-u-heavyt0044s']=16,
		['n-cargo-u-heavyt0041s']=17,
		['n-cargo-u-heavyt0042s']=18,
		['n-cargo-u-heavyt0065s']=19,
		['n-cargo-u-heavyt0066s']=20,
		['n-cargo-u-heavyt0063s']=21,
		['n-cargo-u-heavyt0064s']=22,
		['n-cargo-u-heavyt0067s']=23,
		['n-cargo-u-heavyt0070s']=24,
		['n-cargo-u-heavyt0071s']=25,
		['n-cargo-u-heavyt0068s']=26,
		['n-cargo-u-heavyt0069s']=27,
		['n-cargo-u-heavyt0056s']=28,
		['n-cargo-u-heavyt0057s']=29,
		['n-cargo-u-heavyt0054s']=30,
		['n-cargo-u-heavyt0055s']=31,
		['n-cargo-u-heavyt0058s']=32,
		['n-cargo-u-heavyt0061s']=33,
		['n-cargo-u-heavyt0062s']=34,
		['n-cargo-u-heavyt0059s']=35,
		['n-cargo-u-heavyt0060s']=36,
		['n-cargo-u-heavyt0011s']=37,
		['n-cargo-u-heavyt0012s']=38,
		['n-cargo-u-heavyt0009s']=39,
		['n-cargo-u-heavyt0010s']=40,
		['n-cargo-u-heavyt0013s']=41,
		['n-cargo-u-heavyt0016s']=42,
		['n-cargo-u-heavyt0017s']=43,
		['n-cargo-u-heavyt0014s']=44,
		['n-cargo-u-heavyt0015s']=45,
		['n-cargo-u-heavyt0002s']=46,
		['n-cargo-u-heavyt0003s']=47,
		['n-cargo-u-heavyt0000s']=48,
		['n-cargo-u-heavyt0001s']=49,
		['n-cargo-u-heavyt0004s']=50,
		['n-cargo-u-heavyt0007s']=51,
		['n-cargo-u-heavyt0008s']=52,
		['n-cargo-u-heavyt0005s']=53,
		['n-cargo-u-heavyt0006s']=54,
		['n-cargo-u-heavyt0029s']=55,
		['n-cargo-u-heavyt0030s']=56,
		['n-cargo-u-heavyt0027s']=57,
		['n-cargo-u-heavyt0028s']=58,
		['n-cargo-u-heavyt0031s']=59,
		['n-cargo-u-heavyt0034s']=60,
		['n-cargo-u-heavyt0035s']=61,
		['n-cargo-u-heavyt0032s']=62,
		['n-cargo-u-heavyt0033s']=63,
		['n-cargo-u-heavyt0020s']=64,
		['n-cargo-u-heavyt0021s']=65,
		['n-cargo-u-heavyt0018s']=66,
		['n-cargo-u-heavyt0019s']=67,
		['n-cargo-u-heavyt0022s']=68,
		['n-cargo-u-heavyt0025s']=69,
		['n-cargo-u-heavyt0026s']=70,
		['n-cargo-u-heavyt0023s']=71,
		['n-cargo-u-heavyt0024s']=72,
	},
};
