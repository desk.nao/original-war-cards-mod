--GENERATED ON: 05/12/2024 07:00:06


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['n-doze-r-heavyw'] = {
	TEX_WIDTH=1936,
	TEX_HEIGHT=888,
	FRAME_COUNT=72,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0588842975206612,0),
		[3]=v2fM(0,117768595041322,0),
		[4]=v2fM(0,176652892561983,0),
		[5]=v2fM(0,235537190082645,0),
		[6]=v2fM(0,294421487603306,0),
		[7]=v2fM(0,353305785123967,0),
		[8]=v2fM(0,412190082644628,0),
		[9]=v2fM(0,0,166666666666667),
		[10]=v2fM(0,0588842975206612,0,166666666666667),
		[11]=v2fM(0,117768595041322,0,166666666666667),
		[12]=v2fM(0,176652892561983,0,166666666666667),
		[13]=v2fM(0,235537190082645,0,166666666666667),
		[14]=v2fM(0,294421487603306,0,166666666666667),
		[15]=v2fM(0,353305785123967,0,166666666666667),
		[16]=v2fM(0,412190082644628,0,166666666666667),
		[17]=v2fM(0,0,333333333333333),
		[18]=v2fM(0,0588842975206612,0,333333333333333),
		[19]=v2fM(0,117768595041322,0,333333333333333),
		[20]=v2fM(0,176652892561983,0,333333333333333),
		[21]=v2fM(0,235537190082645,0,333333333333333),
		[22]=v2fM(0,294421487603306,0,333333333333333),
		[23]=v2fM(0,353305785123967,0,333333333333333),
		[24]=v2fM(0,412190082644628,0,333333333333333),
		[25]=v2fM(0,0,5),
		[26]=v2fM(0,0588842975206612,0,5),
		[27]=v2fM(0,117768595041322,0,5),
		[28]=v2fM(0,176652892561983,0,5),
		[29]=v2fM(0,235537190082645,0,5),
		[30]=v2fM(0,294421487603306,0,5),
		[31]=v2fM(0,353305785123967,0,5),
		[32]=v2fM(0,412190082644628,0,5),
		[33]=v2fM(0,0,666666666666667),
		[34]=v2fM(0,0588842975206612,0,666666666666667),
		[35]=v2fM(0,117768595041322,0,666666666666667),
		[36]=v2fM(0,176652892561983,0,666666666666667),
		[37]=v2fM(0,235537190082645,0,666666666666667),
		[38]=v2fM(0,294421487603306,0,666666666666667),
		[39]=v2fM(0,353305785123967,0,666666666666667),
		[40]=v2fM(0,412190082644628,0,666666666666667),
		[41]=v2fM(0,0,833333333333333),
		[42]=v2fM(0,0588842975206612,0,833333333333333),
		[43]=v2fM(0,117768595041322,0,833333333333333),
		[44]=v2fM(0,176652892561983,0,833333333333333),
		[45]=v2fM(0,235537190082645,0,833333333333333),
		[46]=v2fM(0,294421487603306,0,833333333333333),
		[47]=v2fM(0,353305785123967,0,833333333333333),
		[48]=v2fM(0,412190082644628,0,833333333333333),
		[49]=v2fM(0,528925619834711,0),
		[50]=v2fM(0,587809917355372,0),
		[51]=v2fM(0,646694214876033,0),
		[52]=v2fM(0,705578512396694,0),
		[53]=v2fM(0,764462809917355,0),
		[54]=v2fM(0,823347107438017,0),
		[55]=v2fM(0,882231404958678,0),
		[56]=v2fM(0,941115702479339,0),
		[57]=v2fM(0,528925619834711,0,166666666666667),
		[58]=v2fM(0,587809917355372,0,166666666666667),
		[59]=v2fM(0,646694214876033,0,166666666666667),
		[60]=v2fM(0,705578512396694,0,166666666666667),
		[61]=v2fM(0,764462809917355,0,166666666666667),
		[62]=v2fM(0,823347107438017,0,166666666666667),
		[63]=v2fM(0,882231404958678,0,166666666666667),
		[64]=v2fM(0,941115702479339,0,166666666666667),
		[65]=v2fM(0,528925619834711,0,333333333333333),
		[66]=v2fM(0,587809917355372,0,333333333333333),
		[67]=v2fM(0,646694214876033,0,333333333333333),
		[68]=v2fM(0,705578512396694,0,333333333333333),
		[69]=v2fM(0,764462809917355,0,333333333333333),
		[70]=v2fM(0,823347107438017,0,333333333333333),
		[71]=v2fM(0,882231404958678,0,333333333333333),
		[72]=v2fM(0,941115702479339,0,333333333333333),
	},
	FRAME_SIZES={
		[1]={WIDTH=114,HEIGHT=148},
		[2]={WIDTH=114,HEIGHT=148},
		[3]={WIDTH=114,HEIGHT=148},
		[4]={WIDTH=114,HEIGHT=148},
		[5]={WIDTH=114,HEIGHT=148},
		[6]={WIDTH=114,HEIGHT=148},
		[7]={WIDTH=114,HEIGHT=148},
		[8]={WIDTH=114,HEIGHT=148},
		[9]={WIDTH=114,HEIGHT=148},
		[10]={WIDTH=114,HEIGHT=148},
		[11]={WIDTH=114,HEIGHT=148},
		[12]={WIDTH=114,HEIGHT=148},
		[13]={WIDTH=114,HEIGHT=148},
		[14]={WIDTH=114,HEIGHT=148},
		[15]={WIDTH=114,HEIGHT=148},
		[16]={WIDTH=114,HEIGHT=148},
		[17]={WIDTH=114,HEIGHT=148},
		[18]={WIDTH=114,HEIGHT=148},
		[19]={WIDTH=114,HEIGHT=148},
		[20]={WIDTH=114,HEIGHT=148},
		[21]={WIDTH=114,HEIGHT=148},
		[22]={WIDTH=114,HEIGHT=148},
		[23]={WIDTH=114,HEIGHT=148},
		[24]={WIDTH=114,HEIGHT=148},
		[25]={WIDTH=114,HEIGHT=148},
		[26]={WIDTH=114,HEIGHT=148},
		[27]={WIDTH=114,HEIGHT=148},
		[28]={WIDTH=114,HEIGHT=148},
		[29]={WIDTH=114,HEIGHT=148},
		[30]={WIDTH=114,HEIGHT=148},
		[31]={WIDTH=114,HEIGHT=148},
		[32]={WIDTH=114,HEIGHT=148},
		[33]={WIDTH=114,HEIGHT=148},
		[34]={WIDTH=114,HEIGHT=148},
		[35]={WIDTH=114,HEIGHT=148},
		[36]={WIDTH=114,HEIGHT=148},
		[37]={WIDTH=114,HEIGHT=148},
		[38]={WIDTH=114,HEIGHT=148},
		[39]={WIDTH=114,HEIGHT=148},
		[40]={WIDTH=114,HEIGHT=148},
		[41]={WIDTH=114,HEIGHT=148},
		[42]={WIDTH=114,HEIGHT=148},
		[43]={WIDTH=114,HEIGHT=148},
		[44]={WIDTH=114,HEIGHT=148},
		[45]={WIDTH=114,HEIGHT=148},
		[46]={WIDTH=114,HEIGHT=148},
		[47]={WIDTH=114,HEIGHT=148},
		[48]={WIDTH=114,HEIGHT=148},
		[49]={WIDTH=114,HEIGHT=148},
		[50]={WIDTH=114,HEIGHT=148},
		[51]={WIDTH=114,HEIGHT=148},
		[52]={WIDTH=114,HEIGHT=148},
		[53]={WIDTH=114,HEIGHT=148},
		[54]={WIDTH=114,HEIGHT=148},
		[55]={WIDTH=114,HEIGHT=148},
		[56]={WIDTH=114,HEIGHT=148},
		[57]={WIDTH=114,HEIGHT=148},
		[58]={WIDTH=114,HEIGHT=148},
		[59]={WIDTH=114,HEIGHT=148},
		[60]={WIDTH=114,HEIGHT=148},
		[61]={WIDTH=114,HEIGHT=148},
		[62]={WIDTH=114,HEIGHT=148},
		[63]={WIDTH=114,HEIGHT=148},
		[64]={WIDTH=114,HEIGHT=148},
		[65]={WIDTH=114,HEIGHT=148},
		[66]={WIDTH=114,HEIGHT=148},
		[67]={WIDTH=114,HEIGHT=148},
		[68]={WIDTH=114,HEIGHT=148},
		[69]={WIDTH=114,HEIGHT=148},
		[70]={WIDTH=114,HEIGHT=148},
		[71]={WIDTH=114,HEIGHT=148},
		[72]={WIDTH=114,HEIGHT=148},
	},
	FRAME_INDEXS={
		['n-doze-r-heavyw0047']=1,
		['n-doze-r-heavyw0048']=2,
		['n-doze-r-heavyw0045']=3,
		['n-doze-r-heavyw0046']=4,
		['n-doze-r-heavyw0049']=5,
		['n-doze-r-heavyw0052']=6,
		['n-doze-r-heavyw0053']=7,
		['n-doze-r-heavyw0050']=8,
		['n-doze-r-heavyw0051']=9,
		['n-doze-r-heavyw0038']=10,
		['n-doze-r-heavyw0039']=11,
		['n-doze-r-heavyw0036']=12,
		['n-doze-r-heavyw0037']=13,
		['n-doze-r-heavyw0040']=14,
		['n-doze-r-heavyw0043']=15,
		['n-doze-r-heavyw0044']=16,
		['n-doze-r-heavyw0041']=17,
		['n-doze-r-heavyw0042']=18,
		['n-doze-r-heavyw0065']=19,
		['n-doze-r-heavyw0066']=20,
		['n-doze-r-heavyw0063']=21,
		['n-doze-r-heavyw0064']=22,
		['n-doze-r-heavyw0067']=23,
		['n-doze-r-heavyw0070']=24,
		['n-doze-r-heavyw0071']=25,
		['n-doze-r-heavyw0068']=26,
		['n-doze-r-heavyw0069']=27,
		['n-doze-r-heavyw0056']=28,
		['n-doze-r-heavyw0057']=29,
		['n-doze-r-heavyw0054']=30,
		['n-doze-r-heavyw0055']=31,
		['n-doze-r-heavyw0058']=32,
		['n-doze-r-heavyw0061']=33,
		['n-doze-r-heavyw0062']=34,
		['n-doze-r-heavyw0059']=35,
		['n-doze-r-heavyw0060']=36,
		['n-doze-r-heavyw0011']=37,
		['n-doze-r-heavyw0012']=38,
		['n-doze-r-heavyw0009']=39,
		['n-doze-r-heavyw0010']=40,
		['n-doze-r-heavyw0013']=41,
		['n-doze-r-heavyw0016']=42,
		['n-doze-r-heavyw0017']=43,
		['n-doze-r-heavyw0014']=44,
		['n-doze-r-heavyw0015']=45,
		['n-doze-r-heavyw0002']=46,
		['n-doze-r-heavyw0003']=47,
		['n-doze-r-heavyw0000']=48,
		['n-doze-r-heavyw0001']=49,
		['n-doze-r-heavyw0004']=50,
		['n-doze-r-heavyw0007']=51,
		['n-doze-r-heavyw0008']=52,
		['n-doze-r-heavyw0005']=53,
		['n-doze-r-heavyw0006']=54,
		['n-doze-r-heavyw0029']=55,
		['n-doze-r-heavyw0030']=56,
		['n-doze-r-heavyw0027']=57,
		['n-doze-r-heavyw0028']=58,
		['n-doze-r-heavyw0031']=59,
		['n-doze-r-heavyw0034']=60,
		['n-doze-r-heavyw0035']=61,
		['n-doze-r-heavyw0032']=62,
		['n-doze-r-heavyw0033']=63,
		['n-doze-r-heavyw0020']=64,
		['n-doze-r-heavyw0021']=65,
		['n-doze-r-heavyw0018']=66,
		['n-doze-r-heavyw0019']=67,
		['n-doze-r-heavyw0022']=68,
		['n-doze-r-heavyw0025']=69,
		['n-doze-r-heavyw0026']=70,
		['n-doze-r-heavyw0023']=71,
		['n-doze-r-heavyw0024']=72,
	},
};
