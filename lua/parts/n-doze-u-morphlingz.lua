--GENERATED ON: 05/12/2024 07:00:08


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['n-doze-u-morphlingz'] = {
	TEX_WIDTH=960,
	TEX_HEIGHT=752,
	FRAME_COUNT=72,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,1,0),
		[3]=v2fM(0,2,0),
		[4]=v2fM(0,3,0),
		[5]=v2fM(0,4,0),
		[6]=v2fM(0,5,0),
		[7]=v2fM(0,6,0),
		[8]=v2fM(0,7,0),
		[9]=v2fM(0,8,0),
		[10]=v2fM(0,9,0),
		[11]=v2fM(0,0,125),
		[12]=v2fM(0,1,0,125),
		[13]=v2fM(0,2,0,125),
		[14]=v2fM(0,3,0,125),
		[15]=v2fM(0,4,0,125),
		[16]=v2fM(0,5,0,125),
		[17]=v2fM(0,6,0,125),
		[18]=v2fM(0,7,0,125),
		[19]=v2fM(0,8,0,125),
		[20]=v2fM(0,9,0,125),
		[21]=v2fM(0,0,25),
		[22]=v2fM(0,1,0,25),
		[23]=v2fM(0,2,0,25),
		[24]=v2fM(0,3,0,25),
		[25]=v2fM(0,4,0,25),
		[26]=v2fM(0,5,0,25),
		[27]=v2fM(0,6,0,25),
		[28]=v2fM(0,7,0,25),
		[29]=v2fM(0,8,0,25),
		[30]=v2fM(0,9,0,25),
		[31]=v2fM(0,0,375),
		[32]=v2fM(0,1,0,375),
		[33]=v2fM(0,2,0,375),
		[34]=v2fM(0,3,0,375),
		[35]=v2fM(0,4,0,375),
		[36]=v2fM(0,5,0,375),
		[37]=v2fM(0,6,0,375),
		[38]=v2fM(0,7,0,375),
		[39]=v2fM(0,8,0,375),
		[40]=v2fM(0,9,0,375),
		[41]=v2fM(0,0,5),
		[42]=v2fM(0,1,0,5),
		[43]=v2fM(0,2,0,5),
		[44]=v2fM(0,3,0,5),
		[45]=v2fM(0,4,0,5),
		[46]=v2fM(0,5,0,5),
		[47]=v2fM(0,6,0,5),
		[48]=v2fM(0,7,0,5),
		[49]=v2fM(0,8,0,5),
		[50]=v2fM(0,9,0,5),
		[51]=v2fM(0,0,625),
		[52]=v2fM(0,1,0,625),
		[53]=v2fM(0,2,0,625),
		[54]=v2fM(0,3,0,625),
		[55]=v2fM(0,4,0,625),
		[56]=v2fM(0,5,0,625),
		[57]=v2fM(0,6,0,625),
		[58]=v2fM(0,7,0,625),
		[59]=v2fM(0,8,0,625),
		[60]=v2fM(0,9,0,625),
		[61]=v2fM(0,0,75),
		[62]=v2fM(0,1,0,75),
		[63]=v2fM(0,2,0,75),
		[64]=v2fM(0,3,0,75),
		[65]=v2fM(0,4,0,75),
		[66]=v2fM(0,5,0,75),
		[67]=v2fM(0,6,0,75),
		[68]=v2fM(0,7,0,75),
		[69]=v2fM(0,8,0,75),
		[70]=v2fM(0,9,0,75),
		[71]=v2fM(0,0,875),
		[72]=v2fM(0,1,0,875),
	},
	FRAME_SIZES={
		[1]={WIDTH=96,HEIGHT=94},
		[2]={WIDTH=96,HEIGHT=94},
		[3]={WIDTH=96,HEIGHT=94},
		[4]={WIDTH=96,HEIGHT=94},
		[5]={WIDTH=96,HEIGHT=94},
		[6]={WIDTH=96,HEIGHT=94},
		[7]={WIDTH=96,HEIGHT=94},
		[8]={WIDTH=96,HEIGHT=94},
		[9]={WIDTH=96,HEIGHT=94},
		[10]={WIDTH=96,HEIGHT=94},
		[11]={WIDTH=96,HEIGHT=94},
		[12]={WIDTH=96,HEIGHT=94},
		[13]={WIDTH=96,HEIGHT=94},
		[14]={WIDTH=96,HEIGHT=94},
		[15]={WIDTH=96,HEIGHT=94},
		[16]={WIDTH=96,HEIGHT=94},
		[17]={WIDTH=96,HEIGHT=94},
		[18]={WIDTH=96,HEIGHT=94},
		[19]={WIDTH=96,HEIGHT=94},
		[20]={WIDTH=96,HEIGHT=94},
		[21]={WIDTH=96,HEIGHT=94},
		[22]={WIDTH=96,HEIGHT=94},
		[23]={WIDTH=96,HEIGHT=94},
		[24]={WIDTH=96,HEIGHT=94},
		[25]={WIDTH=96,HEIGHT=94},
		[26]={WIDTH=96,HEIGHT=94},
		[27]={WIDTH=96,HEIGHT=94},
		[28]={WIDTH=96,HEIGHT=94},
		[29]={WIDTH=96,HEIGHT=94},
		[30]={WIDTH=96,HEIGHT=94},
		[31]={WIDTH=96,HEIGHT=94},
		[32]={WIDTH=96,HEIGHT=94},
		[33]={WIDTH=96,HEIGHT=94},
		[34]={WIDTH=96,HEIGHT=94},
		[35]={WIDTH=96,HEIGHT=94},
		[36]={WIDTH=96,HEIGHT=94},
		[37]={WIDTH=96,HEIGHT=94},
		[38]={WIDTH=96,HEIGHT=94},
		[39]={WIDTH=96,HEIGHT=94},
		[40]={WIDTH=96,HEIGHT=94},
		[41]={WIDTH=96,HEIGHT=94},
		[42]={WIDTH=96,HEIGHT=94},
		[43]={WIDTH=96,HEIGHT=94},
		[44]={WIDTH=96,HEIGHT=94},
		[45]={WIDTH=96,HEIGHT=94},
		[46]={WIDTH=96,HEIGHT=94},
		[47]={WIDTH=96,HEIGHT=94},
		[48]={WIDTH=96,HEIGHT=94},
		[49]={WIDTH=96,HEIGHT=94},
		[50]={WIDTH=96,HEIGHT=94},
		[51]={WIDTH=96,HEIGHT=94},
		[52]={WIDTH=96,HEIGHT=94},
		[53]={WIDTH=96,HEIGHT=94},
		[54]={WIDTH=96,HEIGHT=94},
		[55]={WIDTH=96,HEIGHT=94},
		[56]={WIDTH=96,HEIGHT=94},
		[57]={WIDTH=96,HEIGHT=94},
		[58]={WIDTH=96,HEIGHT=94},
		[59]={WIDTH=96,HEIGHT=94},
		[60]={WIDTH=96,HEIGHT=94},
		[61]={WIDTH=96,HEIGHT=94},
		[62]={WIDTH=96,HEIGHT=94},
		[63]={WIDTH=96,HEIGHT=94},
		[64]={WIDTH=96,HEIGHT=94},
		[65]={WIDTH=96,HEIGHT=94},
		[66]={WIDTH=96,HEIGHT=94},
		[67]={WIDTH=96,HEIGHT=94},
		[68]={WIDTH=96,HEIGHT=94},
		[69]={WIDTH=96,HEIGHT=94},
		[70]={WIDTH=96,HEIGHT=94},
		[71]={WIDTH=96,HEIGHT=94},
		[72]={WIDTH=96,HEIGHT=94},
	},
	FRAME_INDEXS={
		['n-doze-u-morphling0047z']=1,
		['n-doze-u-morphling0048z']=2,
		['n-doze-u-morphling0045z']=3,
		['n-doze-u-morphling0046z']=4,
		['n-doze-u-morphling0049z']=5,
		['n-doze-u-morphling0052z']=6,
		['n-doze-u-morphling0053z']=7,
		['n-doze-u-morphling0050z']=8,
		['n-doze-u-morphling0051z']=9,
		['n-doze-u-morphling0038z']=10,
		['n-doze-u-morphling0039z']=11,
		['n-doze-u-morphling0036z']=12,
		['n-doze-u-morphling0037z']=13,
		['n-doze-u-morphling0040z']=14,
		['n-doze-u-morphling0043z']=15,
		['n-doze-u-morphling0044z']=16,
		['n-doze-u-morphling0041z']=17,
		['n-doze-u-morphling0042z']=18,
		['n-doze-u-morphling0065z']=19,
		['n-doze-u-morphling0066z']=20,
		['n-doze-u-morphling0063z']=21,
		['n-doze-u-morphling0064z']=22,
		['n-doze-u-morphling0067z']=23,
		['n-doze-u-morphling0070z']=24,
		['n-doze-u-morphling0071z']=25,
		['n-doze-u-morphling0068z']=26,
		['n-doze-u-morphling0069z']=27,
		['n-doze-u-morphling0056z']=28,
		['n-doze-u-morphling0057z']=29,
		['n-doze-u-morphling0054z']=30,
		['n-doze-u-morphling0055z']=31,
		['n-doze-u-morphling0058z']=32,
		['n-doze-u-morphling0061z']=33,
		['n-doze-u-morphling0062z']=34,
		['n-doze-u-morphling0059z']=35,
		['n-doze-u-morphling0060z']=36,
		['n-doze-u-morphling0011z']=37,
		['n-doze-u-morphling0012z']=38,
		['n-doze-u-morphling0009z']=39,
		['n-doze-u-morphling0010z']=40,
		['n-doze-u-morphling0013z']=41,
		['n-doze-u-morphling0016z']=42,
		['n-doze-u-morphling0017z']=43,
		['n-doze-u-morphling0014z']=44,
		['n-doze-u-morphling0015z']=45,
		['n-doze-u-morphling0002z']=46,
		['n-doze-u-morphling0003z']=47,
		['n-doze-u-morphling0000z']=48,
		['n-doze-u-morphling0001z']=49,
		['n-doze-u-morphling0004z']=50,
		['n-doze-u-morphling0007z']=51,
		['n-doze-u-morphling0008z']=52,
		['n-doze-u-morphling0005z']=53,
		['n-doze-u-morphling0006z']=54,
		['n-doze-u-morphling0029z']=55,
		['n-doze-u-morphling0030z']=56,
		['n-doze-u-morphling0027z']=57,
		['n-doze-u-morphling0028z']=58,
		['n-doze-u-morphling0031z']=59,
		['n-doze-u-morphling0034z']=60,
		['n-doze-u-morphling0035z']=61,
		['n-doze-u-morphling0032z']=62,
		['n-doze-u-morphling0033z']=63,
		['n-doze-u-morphling0020z']=64,
		['n-doze-u-morphling0021z']=65,
		['n-doze-u-morphling0018z']=66,
		['n-doze-u-morphling0019z']=67,
		['n-doze-u-morphling0022z']=68,
		['n-doze-u-morphling0025z']=69,
		['n-doze-u-morphling0026z']=70,
		['n-doze-u-morphling0023z']=71,
		['n-doze-u-morphling0024z']=72,
	},
};
