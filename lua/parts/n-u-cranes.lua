--GENERATED ON: 05/12/2024 07:00:09


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['n-u-cranes'] = {
	TEX_WIDTH=1992,
	TEX_HEIGHT=936,
	FRAME_COUNT=108,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0441767068273092,0),
		[3]=v2fM(0,0883534136546185,0),
		[4]=v2fM(0,132530120481928,0),
		[5]=v2fM(0,176706827309237,0),
		[6]=v2fM(0,220883534136546,0),
		[7]=v2fM(0,265060240963855,0),
		[8]=v2fM(0,309236947791165,0),
		[9]=v2fM(0,353413654618474,0),
		[10]=v2fM(0,397590361445783,0),
		[11]=v2fM(0,441767068273092,0),
		[12]=v2fM(0,0,166666666666667),
		[13]=v2fM(0,0441767068273092,0,166666666666667),
		[14]=v2fM(0,0883534136546185,0,166666666666667),
		[15]=v2fM(0,132530120481928,0,166666666666667),
		[16]=v2fM(0,176706827309237,0,166666666666667),
		[17]=v2fM(0,220883534136546,0,166666666666667),
		[18]=v2fM(0,265060240963855,0,166666666666667),
		[19]=v2fM(0,309236947791165,0,166666666666667),
		[20]=v2fM(0,353413654618474,0,166666666666667),
		[21]=v2fM(0,397590361445783,0,166666666666667),
		[22]=v2fM(0,441767068273092,0,166666666666667),
		[23]=v2fM(0,0,333333333333333),
		[24]=v2fM(0,0441767068273092,0,333333333333333),
		[25]=v2fM(0,0883534136546185,0,333333333333333),
		[26]=v2fM(0,132530120481928,0,333333333333333),
		[27]=v2fM(0,176706827309237,0,333333333333333),
		[28]=v2fM(0,220883534136546,0,333333333333333),
		[29]=v2fM(0,265060240963855,0,333333333333333),
		[30]=v2fM(0,309236947791165,0,333333333333333),
		[31]=v2fM(0,353413654618474,0,333333333333333),
		[32]=v2fM(0,397590361445783,0,333333333333333),
		[33]=v2fM(0,441767068273092,0,333333333333333),
		[34]=v2fM(0,0,5),
		[35]=v2fM(0,0441767068273092,0,5),
		[36]=v2fM(0,0883534136546185,0,5),
		[37]=v2fM(0,132530120481928,0,5),
		[38]=v2fM(0,176706827309237,0,5),
		[39]=v2fM(0,220883534136546,0,5),
		[40]=v2fM(0,265060240963855,0,5),
		[41]=v2fM(0,309236947791165,0,5),
		[42]=v2fM(0,353413654618474,0,5),
		[43]=v2fM(0,397590361445783,0,5),
		[44]=v2fM(0,441767068273092,0,5),
		[45]=v2fM(0,0,666666666666667),
		[46]=v2fM(0,0441767068273092,0,666666666666667),
		[47]=v2fM(0,0883534136546185,0,666666666666667),
		[48]=v2fM(0,132530120481928,0,666666666666667),
		[49]=v2fM(0,176706827309237,0,666666666666667),
		[50]=v2fM(0,220883534136546,0,666666666666667),
		[51]=v2fM(0,265060240963855,0,666666666666667),
		[52]=v2fM(0,309236947791165,0,666666666666667),
		[53]=v2fM(0,353413654618474,0,666666666666667),
		[54]=v2fM(0,397590361445783,0,666666666666667),
		[55]=v2fM(0,441767068273092,0,666666666666667),
		[56]=v2fM(0,0,833333333333333),
		[57]=v2fM(0,0441767068273092,0,833333333333333),
		[58]=v2fM(0,0883534136546185,0,833333333333333),
		[59]=v2fM(0,132530120481928,0,833333333333333),
		[60]=v2fM(0,176706827309237,0,833333333333333),
		[61]=v2fM(0,220883534136546,0,833333333333333),
		[62]=v2fM(0,265060240963855,0,833333333333333),
		[63]=v2fM(0,309236947791165,0,833333333333333),
		[64]=v2fM(0,353413654618474,0,833333333333333),
		[65]=v2fM(0,397590361445783,0,833333333333333),
		[66]=v2fM(0,441767068273092,0,833333333333333),
		[67]=v2fM(0,514056224899598,0),
		[68]=v2fM(0,558232931726908,0),
		[69]=v2fM(0,602409638554217,0),
		[70]=v2fM(0,646586345381526,0),
		[71]=v2fM(0,690763052208835,0),
		[72]=v2fM(0,734939759036145,0),
		[73]=v2fM(0,779116465863454,0),
		[74]=v2fM(0,823293172690763,0),
		[75]=v2fM(0,867469879518072,0),
		[76]=v2fM(0,911646586345382,0),
		[77]=v2fM(0,955823293172691,0),
		[78]=v2fM(0,514056224899598,0,166666666666667),
		[79]=v2fM(0,558232931726908,0,166666666666667),
		[80]=v2fM(0,602409638554217,0,166666666666667),
		[81]=v2fM(0,646586345381526,0,166666666666667),
		[82]=v2fM(0,690763052208835,0,166666666666667),
		[83]=v2fM(0,734939759036145,0,166666666666667),
		[84]=v2fM(0,779116465863454,0,166666666666667),
		[85]=v2fM(0,823293172690763,0,166666666666667),
		[86]=v2fM(0,867469879518072,0,166666666666667),
		[87]=v2fM(0,911646586345382,0,166666666666667),
		[88]=v2fM(0,955823293172691,0,166666666666667),
		[89]=v2fM(0,514056224899598,0,333333333333333),
		[90]=v2fM(0,558232931726908,0,333333333333333),
		[91]=v2fM(0,602409638554217,0,333333333333333),
		[92]=v2fM(0,646586345381526,0,333333333333333),
		[93]=v2fM(0,690763052208835,0,333333333333333),
		[94]=v2fM(0,734939759036145,0,333333333333333),
		[95]=v2fM(0,779116465863454,0,333333333333333),
		[96]=v2fM(0,823293172690763,0,333333333333333),
		[97]=v2fM(0,867469879518072,0,333333333333333),
		[98]=v2fM(0,911646586345382,0,333333333333333),
		[99]=v2fM(0,955823293172691,0,333333333333333),
		[100]=v2fM(0,514056224899598,0,5),
		[101]=v2fM(0,558232931726908,0,5),
		[102]=v2fM(0,602409638554217,0,5),
		[103]=v2fM(0,646586345381526,0,5),
		[104]=v2fM(0,690763052208835,0,5),
		[105]=v2fM(0,734939759036145,0,5),
		[106]=v2fM(0,779116465863454,0,5),
		[107]=v2fM(0,823293172690763,0,5),
		[108]=v2fM(0,867469879518072,0,5),
	},
	FRAME_SIZES={
		[1]={WIDTH=88,HEIGHT=156},
		[2]={WIDTH=88,HEIGHT=156},
		[3]={WIDTH=88,HEIGHT=156},
		[4]={WIDTH=88,HEIGHT=156},
		[5]={WIDTH=88,HEIGHT=156},
		[6]={WIDTH=88,HEIGHT=156},
		[7]={WIDTH=88,HEIGHT=156},
		[8]={WIDTH=88,HEIGHT=156},
		[9]={WIDTH=88,HEIGHT=156},
		[10]={WIDTH=88,HEIGHT=156},
		[11]={WIDTH=88,HEIGHT=156},
		[12]={WIDTH=88,HEIGHT=156},
		[13]={WIDTH=88,HEIGHT=156},
		[14]={WIDTH=88,HEIGHT=156},
		[15]={WIDTH=88,HEIGHT=156},
		[16]={WIDTH=88,HEIGHT=156},
		[17]={WIDTH=88,HEIGHT=156},
		[18]={WIDTH=88,HEIGHT=156},
		[19]={WIDTH=88,HEIGHT=156},
		[20]={WIDTH=88,HEIGHT=156},
		[21]={WIDTH=88,HEIGHT=156},
		[22]={WIDTH=88,HEIGHT=156},
		[23]={WIDTH=88,HEIGHT=156},
		[24]={WIDTH=88,HEIGHT=156},
		[25]={WIDTH=88,HEIGHT=156},
		[26]={WIDTH=88,HEIGHT=156},
		[27]={WIDTH=88,HEIGHT=156},
		[28]={WIDTH=88,HEIGHT=156},
		[29]={WIDTH=88,HEIGHT=156},
		[30]={WIDTH=88,HEIGHT=156},
		[31]={WIDTH=88,HEIGHT=156},
		[32]={WIDTH=88,HEIGHT=156},
		[33]={WIDTH=88,HEIGHT=156},
		[34]={WIDTH=88,HEIGHT=156},
		[35]={WIDTH=88,HEIGHT=156},
		[36]={WIDTH=88,HEIGHT=156},
		[37]={WIDTH=88,HEIGHT=156},
		[38]={WIDTH=88,HEIGHT=156},
		[39]={WIDTH=88,HEIGHT=156},
		[40]={WIDTH=88,HEIGHT=156},
		[41]={WIDTH=88,HEIGHT=156},
		[42]={WIDTH=88,HEIGHT=156},
		[43]={WIDTH=88,HEIGHT=156},
		[44]={WIDTH=88,HEIGHT=156},
		[45]={WIDTH=88,HEIGHT=156},
		[46]={WIDTH=88,HEIGHT=156},
		[47]={WIDTH=88,HEIGHT=156},
		[48]={WIDTH=88,HEIGHT=156},
		[49]={WIDTH=88,HEIGHT=156},
		[50]={WIDTH=88,HEIGHT=156},
		[51]={WIDTH=88,HEIGHT=156},
		[52]={WIDTH=88,HEIGHT=156},
		[53]={WIDTH=88,HEIGHT=156},
		[54]={WIDTH=88,HEIGHT=156},
		[55]={WIDTH=88,HEIGHT=156},
		[56]={WIDTH=88,HEIGHT=156},
		[57]={WIDTH=88,HEIGHT=156},
		[58]={WIDTH=88,HEIGHT=156},
		[59]={WIDTH=88,HEIGHT=156},
		[60]={WIDTH=88,HEIGHT=156},
		[61]={WIDTH=88,HEIGHT=156},
		[62]={WIDTH=88,HEIGHT=156},
		[63]={WIDTH=88,HEIGHT=156},
		[64]={WIDTH=88,HEIGHT=156},
		[65]={WIDTH=88,HEIGHT=156},
		[66]={WIDTH=88,HEIGHT=156},
		[67]={WIDTH=88,HEIGHT=156},
		[68]={WIDTH=88,HEIGHT=156},
		[69]={WIDTH=88,HEIGHT=156},
		[70]={WIDTH=88,HEIGHT=156},
		[71]={WIDTH=88,HEIGHT=156},
		[72]={WIDTH=88,HEIGHT=156},
		[73]={WIDTH=88,HEIGHT=156},
		[74]={WIDTH=88,HEIGHT=156},
		[75]={WIDTH=88,HEIGHT=156},
		[76]={WIDTH=88,HEIGHT=156},
		[77]={WIDTH=88,HEIGHT=156},
		[78]={WIDTH=88,HEIGHT=156},
		[79]={WIDTH=88,HEIGHT=156},
		[80]={WIDTH=88,HEIGHT=156},
		[81]={WIDTH=88,HEIGHT=156},
		[82]={WIDTH=88,HEIGHT=156},
		[83]={WIDTH=88,HEIGHT=156},
		[84]={WIDTH=88,HEIGHT=156},
		[85]={WIDTH=88,HEIGHT=156},
		[86]={WIDTH=88,HEIGHT=156},
		[87]={WIDTH=88,HEIGHT=156},
		[88]={WIDTH=88,HEIGHT=156},
		[89]={WIDTH=88,HEIGHT=156},
		[90]={WIDTH=88,HEIGHT=156},
		[91]={WIDTH=88,HEIGHT=156},
		[92]={WIDTH=88,HEIGHT=156},
		[93]={WIDTH=88,HEIGHT=156},
		[94]={WIDTH=88,HEIGHT=156},
		[95]={WIDTH=88,HEIGHT=156},
		[96]={WIDTH=88,HEIGHT=156},
		[97]={WIDTH=88,HEIGHT=156},
		[98]={WIDTH=88,HEIGHT=156},
		[99]={WIDTH=88,HEIGHT=156},
		[100]={WIDTH=88,HEIGHT=156},
		[101]={WIDTH=88,HEIGHT=156},
		[102]={WIDTH=88,HEIGHT=156},
		[103]={WIDTH=88,HEIGHT=156},
		[104]={WIDTH=88,HEIGHT=156},
		[105]={WIDTH=88,HEIGHT=156},
		[106]={WIDTH=88,HEIGHT=156},
		[107]={WIDTH=88,HEIGHT=156},
		[108]={WIDTH=88,HEIGHT=156},
	},
	FRAME_INDEXS={
		['n-u-crane0071s']=1,
		['n-u-crane0072s']=2,
		['n-u-crane0073s']=3,
		['n-u-crane0068s']=4,
		['n-u-crane0069s']=5,
		['n-u-crane0070s']=6,
		['n-u-crane0074s']=7,
		['n-u-crane0078s']=8,
		['n-u-crane0079s']=9,
		['n-u-crane0080s']=10,
		['n-u-crane0075s']=11,
		['n-u-crane0076s']=12,
		['n-u-crane0077s']=13,
		['n-u-crane0067s']=14,
		['n-u-crane0057s']=15,
		['n-u-crane0058s']=16,
		['n-u-crane0059s']=17,
		['n-u-crane0054s']=18,
		['n-u-crane0055s']=19,
		['n-u-crane0056s']=20,
		['n-u-crane0060s']=21,
		['n-u-crane0064s']=22,
		['n-u-crane0065s']=23,
		['n-u-crane0066s']=24,
		['n-u-crane0061s']=25,
		['n-u-crane0062s']=26,
		['n-u-crane0063s']=27,
		['n-u-crane0098s']=28,
		['n-u-crane0099s']=29,
		['n-u-crane0100s']=30,
		['n-u-crane0095s']=31,
		['n-u-crane0096s']=32,
		['n-u-crane0097s']=33,
		['n-u-crane0101s']=34,
		['n-u-crane0105s']=35,
		['n-u-crane0106s']=36,
		['n-u-crane0107s']=37,
		['n-u-crane0102s']=38,
		['n-u-crane0103s']=39,
		['n-u-crane0104s']=40,
		['n-u-crane0094s']=41,
		['n-u-crane0084s']=42,
		['n-u-crane0085s']=43,
		['n-u-crane0086s']=44,
		['n-u-crane0081s']=45,
		['n-u-crane0082s']=46,
		['n-u-crane0083s']=47,
		['n-u-crane0087s']=48,
		['n-u-crane0091s']=49,
		['n-u-crane0092s']=50,
		['n-u-crane0093s']=51,
		['n-u-crane0088s']=52,
		['n-u-crane0089s']=53,
		['n-u-crane0090s']=54,
		['n-u-crane0017s']=55,
		['n-u-crane0018s']=56,
		['n-u-crane0019s']=57,
		['n-u-crane0014s']=58,
		['n-u-crane0015s']=59,
		['n-u-crane0016s']=60,
		['n-u-crane0020s']=61,
		['n-u-crane0024s']=62,
		['n-u-crane0025s']=63,
		['n-u-crane0026s']=64,
		['n-u-crane0021s']=65,
		['n-u-crane0022s']=66,
		['n-u-crane0023s']=67,
		['n-u-crane0013s']=68,
		['n-u-crane0003s']=69,
		['n-u-crane0004s']=70,
		['n-u-crane0005s']=71,
		['n-u-crane0000s']=72,
		['n-u-crane0001s']=73,
		['n-u-crane0002s']=74,
		['n-u-crane0006s']=75,
		['n-u-crane0010s']=76,
		['n-u-crane0011s']=77,
		['n-u-crane0012s']=78,
		['n-u-crane0007s']=79,
		['n-u-crane0008s']=80,
		['n-u-crane0009s']=81,
		['n-u-crane0044s']=82,
		['n-u-crane0045s']=83,
		['n-u-crane0046s']=84,
		['n-u-crane0041s']=85,
		['n-u-crane0042s']=86,
		['n-u-crane0043s']=87,
		['n-u-crane0047s']=88,
		['n-u-crane0051s']=89,
		['n-u-crane0052s']=90,
		['n-u-crane0053s']=91,
		['n-u-crane0048s']=92,
		['n-u-crane0049s']=93,
		['n-u-crane0050s']=94,
		['n-u-crane0040s']=95,
		['n-u-crane0030s']=96,
		['n-u-crane0031s']=97,
		['n-u-crane0032s']=98,
		['n-u-crane0027s']=99,
		['n-u-crane0028s']=100,
		['n-u-crane0029s']=101,
		['n-u-crane0033s']=102,
		['n-u-crane0037s']=103,
		['n-u-crane0038s']=104,
		['n-u-crane0039s']=105,
		['n-u-crane0034s']=106,
		['n-u-crane0035s']=107,
		['n-u-crane0036s']=108,
	},
};
