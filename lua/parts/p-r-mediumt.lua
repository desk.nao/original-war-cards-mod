--GENERATED ON: 05/12/2024 07:00:31


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['p-r-mediumt'] = {
	TEX_WIDTH=940,
	TEX_HEIGHT=816,
	FRAME_COUNT=72,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,1,0),
		[3]=v2fM(0,2,0),
		[4]=v2fM(0,3,0),
		[5]=v2fM(0,4,0),
		[6]=v2fM(0,5,0),
		[7]=v2fM(0,6,0),
		[8]=v2fM(0,7,0),
		[9]=v2fM(0,8,0),
		[10]=v2fM(0,9,0),
		[11]=v2fM(0,0,125),
		[12]=v2fM(0,1,0,125),
		[13]=v2fM(0,2,0,125),
		[14]=v2fM(0,3,0,125),
		[15]=v2fM(0,4,0,125),
		[16]=v2fM(0,5,0,125),
		[17]=v2fM(0,6,0,125),
		[18]=v2fM(0,7,0,125),
		[19]=v2fM(0,8,0,125),
		[20]=v2fM(0,9,0,125),
		[21]=v2fM(0,0,25),
		[22]=v2fM(0,1,0,25),
		[23]=v2fM(0,2,0,25),
		[24]=v2fM(0,3,0,25),
		[25]=v2fM(0,4,0,25),
		[26]=v2fM(0,5,0,25),
		[27]=v2fM(0,6,0,25),
		[28]=v2fM(0,7,0,25),
		[29]=v2fM(0,8,0,25),
		[30]=v2fM(0,9,0,25),
		[31]=v2fM(0,0,375),
		[32]=v2fM(0,1,0,375),
		[33]=v2fM(0,2,0,375),
		[34]=v2fM(0,3,0,375),
		[35]=v2fM(0,4,0,375),
		[36]=v2fM(0,5,0,375),
		[37]=v2fM(0,6,0,375),
		[38]=v2fM(0,7,0,375),
		[39]=v2fM(0,8,0,375),
		[40]=v2fM(0,9,0,375),
		[41]=v2fM(0,0,5),
		[42]=v2fM(0,1,0,5),
		[43]=v2fM(0,2,0,5),
		[44]=v2fM(0,3,0,5),
		[45]=v2fM(0,4,0,5),
		[46]=v2fM(0,5,0,5),
		[47]=v2fM(0,6,0,5),
		[48]=v2fM(0,7,0,5),
		[49]=v2fM(0,8,0,5),
		[50]=v2fM(0,9,0,5),
		[51]=v2fM(0,0,625),
		[52]=v2fM(0,1,0,625),
		[53]=v2fM(0,2,0,625),
		[54]=v2fM(0,3,0,625),
		[55]=v2fM(0,4,0,625),
		[56]=v2fM(0,5,0,625),
		[57]=v2fM(0,6,0,625),
		[58]=v2fM(0,7,0,625),
		[59]=v2fM(0,8,0,625),
		[60]=v2fM(0,9,0,625),
		[61]=v2fM(0,0,75),
		[62]=v2fM(0,1,0,75),
		[63]=v2fM(0,2,0,75),
		[64]=v2fM(0,3,0,75),
		[65]=v2fM(0,4,0,75),
		[66]=v2fM(0,5,0,75),
		[67]=v2fM(0,6,0,75),
		[68]=v2fM(0,7,0,75),
		[69]=v2fM(0,8,0,75),
		[70]=v2fM(0,9,0,75),
		[71]=v2fM(0,0,875),
		[72]=v2fM(0,1,0,875),
	},
	FRAME_SIZES={
		[1]={WIDTH=94,HEIGHT=102},
		[2]={WIDTH=94,HEIGHT=102},
		[3]={WIDTH=94,HEIGHT=102},
		[4]={WIDTH=94,HEIGHT=102},
		[5]={WIDTH=94,HEIGHT=102},
		[6]={WIDTH=94,HEIGHT=102},
		[7]={WIDTH=94,HEIGHT=102},
		[8]={WIDTH=94,HEIGHT=102},
		[9]={WIDTH=94,HEIGHT=102},
		[10]={WIDTH=94,HEIGHT=102},
		[11]={WIDTH=94,HEIGHT=102},
		[12]={WIDTH=94,HEIGHT=102},
		[13]={WIDTH=94,HEIGHT=102},
		[14]={WIDTH=94,HEIGHT=102},
		[15]={WIDTH=94,HEIGHT=102},
		[16]={WIDTH=94,HEIGHT=102},
		[17]={WIDTH=94,HEIGHT=102},
		[18]={WIDTH=94,HEIGHT=102},
		[19]={WIDTH=94,HEIGHT=102},
		[20]={WIDTH=94,HEIGHT=102},
		[21]={WIDTH=94,HEIGHT=102},
		[22]={WIDTH=94,HEIGHT=102},
		[23]={WIDTH=94,HEIGHT=102},
		[24]={WIDTH=94,HEIGHT=102},
		[25]={WIDTH=94,HEIGHT=102},
		[26]={WIDTH=94,HEIGHT=102},
		[27]={WIDTH=94,HEIGHT=102},
		[28]={WIDTH=94,HEIGHT=102},
		[29]={WIDTH=94,HEIGHT=102},
		[30]={WIDTH=94,HEIGHT=102},
		[31]={WIDTH=94,HEIGHT=102},
		[32]={WIDTH=94,HEIGHT=102},
		[33]={WIDTH=94,HEIGHT=102},
		[34]={WIDTH=94,HEIGHT=102},
		[35]={WIDTH=94,HEIGHT=102},
		[36]={WIDTH=94,HEIGHT=102},
		[37]={WIDTH=94,HEIGHT=102},
		[38]={WIDTH=94,HEIGHT=102},
		[39]={WIDTH=94,HEIGHT=102},
		[40]={WIDTH=94,HEIGHT=102},
		[41]={WIDTH=94,HEIGHT=102},
		[42]={WIDTH=94,HEIGHT=102},
		[43]={WIDTH=94,HEIGHT=102},
		[44]={WIDTH=94,HEIGHT=102},
		[45]={WIDTH=94,HEIGHT=102},
		[46]={WIDTH=94,HEIGHT=102},
		[47]={WIDTH=94,HEIGHT=102},
		[48]={WIDTH=94,HEIGHT=102},
		[49]={WIDTH=94,HEIGHT=102},
		[50]={WIDTH=94,HEIGHT=102},
		[51]={WIDTH=94,HEIGHT=102},
		[52]={WIDTH=94,HEIGHT=102},
		[53]={WIDTH=94,HEIGHT=102},
		[54]={WIDTH=94,HEIGHT=102},
		[55]={WIDTH=94,HEIGHT=102},
		[56]={WIDTH=94,HEIGHT=102},
		[57]={WIDTH=94,HEIGHT=102},
		[58]={WIDTH=94,HEIGHT=102},
		[59]={WIDTH=94,HEIGHT=102},
		[60]={WIDTH=94,HEIGHT=102},
		[61]={WIDTH=94,HEIGHT=102},
		[62]={WIDTH=94,HEIGHT=102},
		[63]={WIDTH=94,HEIGHT=102},
		[64]={WIDTH=94,HEIGHT=102},
		[65]={WIDTH=94,HEIGHT=102},
		[66]={WIDTH=94,HEIGHT=102},
		[67]={WIDTH=94,HEIGHT=102},
		[68]={WIDTH=94,HEIGHT=102},
		[69]={WIDTH=94,HEIGHT=102},
		[70]={WIDTH=94,HEIGHT=102},
		[71]={WIDTH=94,HEIGHT=102},
		[72]={WIDTH=94,HEIGHT=102},
	},
	FRAME_INDEXS={
		['p-r-mediumt0047']=1,
		['p-r-mediumt0048']=2,
		['p-r-mediumt0045']=3,
		['p-r-mediumt0046']=4,
		['p-r-mediumt0049']=5,
		['p-r-mediumt0052']=6,
		['p-r-mediumt0053']=7,
		['p-r-mediumt0050']=8,
		['p-r-mediumt0051']=9,
		['p-r-mediumt0038']=10,
		['p-r-mediumt0039']=11,
		['p-r-mediumt0036']=12,
		['p-r-mediumt0037']=13,
		['p-r-mediumt0040']=14,
		['p-r-mediumt0043']=15,
		['p-r-mediumt0044']=16,
		['p-r-mediumt0041']=17,
		['p-r-mediumt0042']=18,
		['p-r-mediumt0065']=19,
		['p-r-mediumt0066']=20,
		['p-r-mediumt0063']=21,
		['p-r-mediumt0064']=22,
		['p-r-mediumt0067']=23,
		['p-r-mediumt0070']=24,
		['p-r-mediumt0071']=25,
		['p-r-mediumt0068']=26,
		['p-r-mediumt0069']=27,
		['p-r-mediumt0056']=28,
		['p-r-mediumt0057']=29,
		['p-r-mediumt0054']=30,
		['p-r-mediumt0055']=31,
		['p-r-mediumt0058']=32,
		['p-r-mediumt0061']=33,
		['p-r-mediumt0062']=34,
		['p-r-mediumt0059']=35,
		['p-r-mediumt0060']=36,
		['p-r-mediumt0011']=37,
		['p-r-mediumt0012']=38,
		['p-r-mediumt0009']=39,
		['p-r-mediumt0010']=40,
		['p-r-mediumt0013']=41,
		['p-r-mediumt0016']=42,
		['p-r-mediumt0017']=43,
		['p-r-mediumt0014']=44,
		['p-r-mediumt0015']=45,
		['p-r-mediumt0002']=46,
		['p-r-mediumt0003']=47,
		['p-r-mediumt0000']=48,
		['p-r-mediumt0001']=49,
		['p-r-mediumt0004']=50,
		['p-r-mediumt0007']=51,
		['p-r-mediumt0008']=52,
		['p-r-mediumt0005']=53,
		['p-r-mediumt0006']=54,
		['p-r-mediumt0029']=55,
		['p-r-mediumt0030']=56,
		['p-r-mediumt0027']=57,
		['p-r-mediumt0028']=58,
		['p-r-mediumt0031']=59,
		['p-r-mediumt0034']=60,
		['p-r-mediumt0035']=61,
		['p-r-mediumt0032']=62,
		['p-r-mediumt0033']=63,
		['p-r-mediumt0020']=64,
		['p-r-mediumt0021']=65,
		['p-r-mediumt0018']=66,
		['p-r-mediumt0019']=67,
		['p-r-mediumt0022']=68,
		['p-r-mediumt0025']=69,
		['p-r-mediumt0026']=70,
		['p-r-mediumt0023']=71,
		['p-r-mediumt0024']=72,
	},
};
