--GENERATED ON: 05/12/2024 07:00:36


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['p-u-mediumws'] = {
	TEX_WIDTH=940,
	TEX_HEIGHT=800,
	FRAME_COUNT=72,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,1,0),
		[3]=v2fM(0,2,0),
		[4]=v2fM(0,3,0),
		[5]=v2fM(0,4,0),
		[6]=v2fM(0,5,0),
		[7]=v2fM(0,6,0),
		[8]=v2fM(0,7,0),
		[9]=v2fM(0,8,0),
		[10]=v2fM(0,9,0),
		[11]=v2fM(0,0,125),
		[12]=v2fM(0,1,0,125),
		[13]=v2fM(0,2,0,125),
		[14]=v2fM(0,3,0,125),
		[15]=v2fM(0,4,0,125),
		[16]=v2fM(0,5,0,125),
		[17]=v2fM(0,6,0,125),
		[18]=v2fM(0,7,0,125),
		[19]=v2fM(0,8,0,125),
		[20]=v2fM(0,9,0,125),
		[21]=v2fM(0,0,25),
		[22]=v2fM(0,1,0,25),
		[23]=v2fM(0,2,0,25),
		[24]=v2fM(0,3,0,25),
		[25]=v2fM(0,4,0,25),
		[26]=v2fM(0,5,0,25),
		[27]=v2fM(0,6,0,25),
		[28]=v2fM(0,7,0,25),
		[29]=v2fM(0,8,0,25),
		[30]=v2fM(0,9,0,25),
		[31]=v2fM(0,0,375),
		[32]=v2fM(0,1,0,375),
		[33]=v2fM(0,2,0,375),
		[34]=v2fM(0,3,0,375),
		[35]=v2fM(0,4,0,375),
		[36]=v2fM(0,5,0,375),
		[37]=v2fM(0,6,0,375),
		[38]=v2fM(0,7,0,375),
		[39]=v2fM(0,8,0,375),
		[40]=v2fM(0,9,0,375),
		[41]=v2fM(0,0,5),
		[42]=v2fM(0,1,0,5),
		[43]=v2fM(0,2,0,5),
		[44]=v2fM(0,3,0,5),
		[45]=v2fM(0,4,0,5),
		[46]=v2fM(0,5,0,5),
		[47]=v2fM(0,6,0,5),
		[48]=v2fM(0,7,0,5),
		[49]=v2fM(0,8,0,5),
		[50]=v2fM(0,9,0,5),
		[51]=v2fM(0,0,625),
		[52]=v2fM(0,1,0,625),
		[53]=v2fM(0,2,0,625),
		[54]=v2fM(0,3,0,625),
		[55]=v2fM(0,4,0,625),
		[56]=v2fM(0,5,0,625),
		[57]=v2fM(0,6,0,625),
		[58]=v2fM(0,7,0,625),
		[59]=v2fM(0,8,0,625),
		[60]=v2fM(0,9,0,625),
		[61]=v2fM(0,0,75),
		[62]=v2fM(0,1,0,75),
		[63]=v2fM(0,2,0,75),
		[64]=v2fM(0,3,0,75),
		[65]=v2fM(0,4,0,75),
		[66]=v2fM(0,5,0,75),
		[67]=v2fM(0,6,0,75),
		[68]=v2fM(0,7,0,75),
		[69]=v2fM(0,8,0,75),
		[70]=v2fM(0,9,0,75),
		[71]=v2fM(0,0,875),
		[72]=v2fM(0,1,0,875),
	},
	FRAME_SIZES={
		[1]={WIDTH=94,HEIGHT=100},
		[2]={WIDTH=94,HEIGHT=100},
		[3]={WIDTH=94,HEIGHT=100},
		[4]={WIDTH=94,HEIGHT=100},
		[5]={WIDTH=94,HEIGHT=100},
		[6]={WIDTH=94,HEIGHT=100},
		[7]={WIDTH=94,HEIGHT=100},
		[8]={WIDTH=94,HEIGHT=100},
		[9]={WIDTH=94,HEIGHT=100},
		[10]={WIDTH=94,HEIGHT=100},
		[11]={WIDTH=94,HEIGHT=100},
		[12]={WIDTH=94,HEIGHT=100},
		[13]={WIDTH=94,HEIGHT=100},
		[14]={WIDTH=94,HEIGHT=100},
		[15]={WIDTH=94,HEIGHT=100},
		[16]={WIDTH=94,HEIGHT=100},
		[17]={WIDTH=94,HEIGHT=100},
		[18]={WIDTH=94,HEIGHT=100},
		[19]={WIDTH=94,HEIGHT=100},
		[20]={WIDTH=94,HEIGHT=100},
		[21]={WIDTH=94,HEIGHT=100},
		[22]={WIDTH=94,HEIGHT=100},
		[23]={WIDTH=94,HEIGHT=100},
		[24]={WIDTH=94,HEIGHT=100},
		[25]={WIDTH=94,HEIGHT=100},
		[26]={WIDTH=94,HEIGHT=100},
		[27]={WIDTH=94,HEIGHT=100},
		[28]={WIDTH=94,HEIGHT=100},
		[29]={WIDTH=94,HEIGHT=100},
		[30]={WIDTH=94,HEIGHT=100},
		[31]={WIDTH=94,HEIGHT=100},
		[32]={WIDTH=94,HEIGHT=100},
		[33]={WIDTH=94,HEIGHT=100},
		[34]={WIDTH=94,HEIGHT=100},
		[35]={WIDTH=94,HEIGHT=100},
		[36]={WIDTH=94,HEIGHT=100},
		[37]={WIDTH=94,HEIGHT=100},
		[38]={WIDTH=94,HEIGHT=100},
		[39]={WIDTH=94,HEIGHT=100},
		[40]={WIDTH=94,HEIGHT=100},
		[41]={WIDTH=94,HEIGHT=100},
		[42]={WIDTH=94,HEIGHT=100},
		[43]={WIDTH=94,HEIGHT=100},
		[44]={WIDTH=94,HEIGHT=100},
		[45]={WIDTH=94,HEIGHT=100},
		[46]={WIDTH=94,HEIGHT=100},
		[47]={WIDTH=94,HEIGHT=100},
		[48]={WIDTH=94,HEIGHT=100},
		[49]={WIDTH=94,HEIGHT=100},
		[50]={WIDTH=94,HEIGHT=100},
		[51]={WIDTH=94,HEIGHT=100},
		[52]={WIDTH=94,HEIGHT=100},
		[53]={WIDTH=94,HEIGHT=100},
		[54]={WIDTH=94,HEIGHT=100},
		[55]={WIDTH=94,HEIGHT=100},
		[56]={WIDTH=94,HEIGHT=100},
		[57]={WIDTH=94,HEIGHT=100},
		[58]={WIDTH=94,HEIGHT=100},
		[59]={WIDTH=94,HEIGHT=100},
		[60]={WIDTH=94,HEIGHT=100},
		[61]={WIDTH=94,HEIGHT=100},
		[62]={WIDTH=94,HEIGHT=100},
		[63]={WIDTH=94,HEIGHT=100},
		[64]={WIDTH=94,HEIGHT=100},
		[65]={WIDTH=94,HEIGHT=100},
		[66]={WIDTH=94,HEIGHT=100},
		[67]={WIDTH=94,HEIGHT=100},
		[68]={WIDTH=94,HEIGHT=100},
		[69]={WIDTH=94,HEIGHT=100},
		[70]={WIDTH=94,HEIGHT=100},
		[71]={WIDTH=94,HEIGHT=100},
		[72]={WIDTH=94,HEIGHT=100},
	},
	FRAME_INDEXS={
		['p-u-mediumw0047s']=1,
		['p-u-mediumw0048s']=2,
		['p-u-mediumw0045s']=3,
		['p-u-mediumw0046s']=4,
		['p-u-mediumw0049s']=5,
		['p-u-mediumw0052s']=6,
		['p-u-mediumw0053s']=7,
		['p-u-mediumw0050s']=8,
		['p-u-mediumw0051s']=9,
		['p-u-mediumw0038s']=10,
		['p-u-mediumw0039s']=11,
		['p-u-mediumw0036s']=12,
		['p-u-mediumw0037s']=13,
		['p-u-mediumw0040s']=14,
		['p-u-mediumw0043s']=15,
		['p-u-mediumw0044s']=16,
		['p-u-mediumw0041s']=17,
		['p-u-mediumw0042s']=18,
		['p-u-mediumw0065s']=19,
		['p-u-mediumw0066s']=20,
		['p-u-mediumw0063s']=21,
		['p-u-mediumw0064s']=22,
		['p-u-mediumw0067s']=23,
		['p-u-mediumw0070s']=24,
		['p-u-mediumw0071s']=25,
		['p-u-mediumw0068s']=26,
		['p-u-mediumw0069s']=27,
		['p-u-mediumw0056s']=28,
		['p-u-mediumw0057s']=29,
		['p-u-mediumw0054s']=30,
		['p-u-mediumw0055s']=31,
		['p-u-mediumw0058s']=32,
		['p-u-mediumw0061s']=33,
		['p-u-mediumw0062s']=34,
		['p-u-mediumw0059s']=35,
		['p-u-mediumw0060s']=36,
		['p-u-mediumw0011s']=37,
		['p-u-mediumw0012s']=38,
		['p-u-mediumw0009s']=39,
		['p-u-mediumw0010s']=40,
		['p-u-mediumw0013s']=41,
		['p-u-mediumw0016s']=42,
		['p-u-mediumw0017s']=43,
		['p-u-mediumw0014s']=44,
		['p-u-mediumw0015s']=45,
		['p-u-mediumw0002s']=46,
		['p-u-mediumw0003s']=47,
		['p-u-mediumw0000s']=48,
		['p-u-mediumw0001s']=49,
		['p-u-mediumw0004s']=50,
		['p-u-mediumw0007s']=51,
		['p-u-mediumw0008s']=52,
		['p-u-mediumw0005s']=53,
		['p-u-mediumw0006s']=54,
		['p-u-mediumw0029s']=55,
		['p-u-mediumw0030s']=56,
		['p-u-mediumw0027s']=57,
		['p-u-mediumw0028s']=58,
		['p-u-mediumw0031s']=59,
		['p-u-mediumw0034s']=60,
		['p-u-mediumw0035s']=61,
		['p-u-mediumw0032s']=62,
		['p-u-mediumw0033s']=63,
		['p-u-mediumw0020s']=64,
		['p-u-mediumw0021s']=65,
		['p-u-mediumw0018s']=66,
		['p-u-mediumw0019s']=67,
		['p-u-mediumw0022s']=68,
		['p-u-mediumw0025s']=69,
		['p-u-mediumw0026s']=70,
		['p-u-mediumw0023s']=71,
		['p-u-mediumw0024s']=72,
	},
};
