--GENERATED ON: 05/12/2024 07:00:37


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['p-u-morphlings'] = {
	TEX_WIDTH=2018,
	TEX_HEIGHT=966,
	FRAME_COUNT=72,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0703666997026759,0),
		[3]=v2fM(0,140733399405352,0),
		[4]=v2fM(0,211100099108028,0),
		[5]=v2fM(0,281466798810704,0),
		[6]=v2fM(0,35183349851338,0),
		[7]=v2fM(0,422200198216056,0),
		[8]=v2fM(0,0,142857142857143),
		[9]=v2fM(0,0703666997026759,0,142857142857143),
		[10]=v2fM(0,140733399405352,0,142857142857143),
		[11]=v2fM(0,211100099108028,0,142857142857143),
		[12]=v2fM(0,281466798810704,0,142857142857143),
		[13]=v2fM(0,35183349851338,0,142857142857143),
		[14]=v2fM(0,422200198216056,0,142857142857143),
		[15]=v2fM(0,0,285714285714286),
		[16]=v2fM(0,0703666997026759,0,285714285714286),
		[17]=v2fM(0,140733399405352,0,285714285714286),
		[18]=v2fM(0,211100099108028,0,285714285714286),
		[19]=v2fM(0,281466798810704,0,285714285714286),
		[20]=v2fM(0,35183349851338,0,285714285714286),
		[21]=v2fM(0,422200198216056,0,285714285714286),
		[22]=v2fM(0,0,428571428571429),
		[23]=v2fM(0,0703666997026759,0,428571428571429),
		[24]=v2fM(0,140733399405352,0,428571428571429),
		[25]=v2fM(0,211100099108028,0,428571428571429),
		[26]=v2fM(0,281466798810704,0,428571428571429),
		[27]=v2fM(0,35183349851338,0,428571428571429),
		[28]=v2fM(0,422200198216056,0,428571428571429),
		[29]=v2fM(0,0,571428571428571),
		[30]=v2fM(0,0703666997026759,0,571428571428571),
		[31]=v2fM(0,140733399405352,0,571428571428571),
		[32]=v2fM(0,211100099108028,0,571428571428571),
		[33]=v2fM(0,281466798810704,0,571428571428571),
		[34]=v2fM(0,35183349851338,0,571428571428571),
		[35]=v2fM(0,422200198216056,0,571428571428571),
		[36]=v2fM(0,0,714285714285714),
		[37]=v2fM(0,0703666997026759,0,714285714285714),
		[38]=v2fM(0,140733399405352,0,714285714285714),
		[39]=v2fM(0,211100099108028,0,714285714285714),
		[40]=v2fM(0,281466798810704,0,714285714285714),
		[41]=v2fM(0,35183349851338,0,714285714285714),
		[42]=v2fM(0,422200198216056,0,714285714285714),
		[43]=v2fM(0,0,857142857142857),
		[44]=v2fM(0,0703666997026759,0,857142857142857),
		[45]=v2fM(0,140733399405352,0,857142857142857),
		[46]=v2fM(0,211100099108028,0,857142857142857),
		[47]=v2fM(0,281466798810704,0,857142857142857),
		[48]=v2fM(0,35183349851338,0,857142857142857),
		[49]=v2fM(0,422200198216056,0,857142857142857),
		[50]=v2fM(0,507433102081269,0),
		[51]=v2fM(0,577799801783945,0),
		[52]=v2fM(0,64816650148662,0),
		[53]=v2fM(0,718533201189296,0),
		[54]=v2fM(0,788899900891972,0),
		[55]=v2fM(0,859266600594648,0),
		[56]=v2fM(0,929633300297324,0),
		[57]=v2fM(0,507433102081269,0,142857142857143),
		[58]=v2fM(0,577799801783945,0,142857142857143),
		[59]=v2fM(0,64816650148662,0,142857142857143),
		[60]=v2fM(0,718533201189296,0,142857142857143),
		[61]=v2fM(0,788899900891972,0,142857142857143),
		[62]=v2fM(0,859266600594648,0,142857142857143),
		[63]=v2fM(0,929633300297324,0,142857142857143),
		[64]=v2fM(0,507433102081269,0,285714285714286),
		[65]=v2fM(0,577799801783945,0,285714285714286),
		[66]=v2fM(0,64816650148662,0,285714285714286),
		[67]=v2fM(0,718533201189296,0,285714285714286),
		[68]=v2fM(0,788899900891972,0,285714285714286),
		[69]=v2fM(0,859266600594648,0,285714285714286),
		[70]=v2fM(0,929633300297324,0,285714285714286),
		[71]=v2fM(0,507433102081269,0,428571428571429),
		[72]=v2fM(0,577799801783945,0,428571428571429),
	},
	FRAME_SIZES={
		[1]={WIDTH=142,HEIGHT=138},
		[2]={WIDTH=142,HEIGHT=138},
		[3]={WIDTH=142,HEIGHT=138},
		[4]={WIDTH=142,HEIGHT=138},
		[5]={WIDTH=142,HEIGHT=138},
		[6]={WIDTH=142,HEIGHT=138},
		[7]={WIDTH=142,HEIGHT=138},
		[8]={WIDTH=142,HEIGHT=138},
		[9]={WIDTH=142,HEIGHT=138},
		[10]={WIDTH=142,HEIGHT=138},
		[11]={WIDTH=142,HEIGHT=138},
		[12]={WIDTH=142,HEIGHT=138},
		[13]={WIDTH=142,HEIGHT=138},
		[14]={WIDTH=142,HEIGHT=138},
		[15]={WIDTH=142,HEIGHT=138},
		[16]={WIDTH=142,HEIGHT=138},
		[17]={WIDTH=142,HEIGHT=138},
		[18]={WIDTH=142,HEIGHT=138},
		[19]={WIDTH=142,HEIGHT=138},
		[20]={WIDTH=142,HEIGHT=138},
		[21]={WIDTH=142,HEIGHT=138},
		[22]={WIDTH=142,HEIGHT=138},
		[23]={WIDTH=142,HEIGHT=138},
		[24]={WIDTH=142,HEIGHT=138},
		[25]={WIDTH=142,HEIGHT=138},
		[26]={WIDTH=142,HEIGHT=138},
		[27]={WIDTH=142,HEIGHT=138},
		[28]={WIDTH=142,HEIGHT=138},
		[29]={WIDTH=142,HEIGHT=138},
		[30]={WIDTH=142,HEIGHT=138},
		[31]={WIDTH=142,HEIGHT=138},
		[32]={WIDTH=142,HEIGHT=138},
		[33]={WIDTH=142,HEIGHT=138},
		[34]={WIDTH=142,HEIGHT=138},
		[35]={WIDTH=142,HEIGHT=138},
		[36]={WIDTH=142,HEIGHT=138},
		[37]={WIDTH=142,HEIGHT=138},
		[38]={WIDTH=142,HEIGHT=138},
		[39]={WIDTH=142,HEIGHT=138},
		[40]={WIDTH=142,HEIGHT=138},
		[41]={WIDTH=142,HEIGHT=138},
		[42]={WIDTH=142,HEIGHT=138},
		[43]={WIDTH=142,HEIGHT=138},
		[44]={WIDTH=142,HEIGHT=138},
		[45]={WIDTH=142,HEIGHT=138},
		[46]={WIDTH=142,HEIGHT=138},
		[47]={WIDTH=142,HEIGHT=138},
		[48]={WIDTH=142,HEIGHT=138},
		[49]={WIDTH=142,HEIGHT=138},
		[50]={WIDTH=142,HEIGHT=138},
		[51]={WIDTH=142,HEIGHT=138},
		[52]={WIDTH=142,HEIGHT=138},
		[53]={WIDTH=142,HEIGHT=138},
		[54]={WIDTH=142,HEIGHT=138},
		[55]={WIDTH=142,HEIGHT=138},
		[56]={WIDTH=142,HEIGHT=138},
		[57]={WIDTH=142,HEIGHT=138},
		[58]={WIDTH=142,HEIGHT=138},
		[59]={WIDTH=142,HEIGHT=138},
		[60]={WIDTH=142,HEIGHT=138},
		[61]={WIDTH=142,HEIGHT=138},
		[62]={WIDTH=142,HEIGHT=138},
		[63]={WIDTH=142,HEIGHT=138},
		[64]={WIDTH=142,HEIGHT=138},
		[65]={WIDTH=142,HEIGHT=138},
		[66]={WIDTH=142,HEIGHT=138},
		[67]={WIDTH=142,HEIGHT=138},
		[68]={WIDTH=142,HEIGHT=138},
		[69]={WIDTH=142,HEIGHT=138},
		[70]={WIDTH=142,HEIGHT=138},
		[71]={WIDTH=142,HEIGHT=138},
		[72]={WIDTH=142,HEIGHT=138},
	},
	FRAME_INDEXS={
		['p-u-morphling0047s']=1,
		['p-u-morphling0048s']=2,
		['p-u-morphling0045s']=3,
		['p-u-morphling0046s']=4,
		['p-u-morphling0049s']=5,
		['p-u-morphling0052s']=6,
		['p-u-morphling0053s']=7,
		['p-u-morphling0050s']=8,
		['p-u-morphling0051s']=9,
		['p-u-morphling0038s']=10,
		['p-u-morphling0039s']=11,
		['p-u-morphling0036s']=12,
		['p-u-morphling0037s']=13,
		['p-u-morphling0040s']=14,
		['p-u-morphling0043s']=15,
		['p-u-morphling0044s']=16,
		['p-u-morphling0041s']=17,
		['p-u-morphling0042s']=18,
		['p-u-morphling0065s']=19,
		['p-u-morphling0066s']=20,
		['p-u-morphling0063s']=21,
		['p-u-morphling0064s']=22,
		['p-u-morphling0067s']=23,
		['p-u-morphling0070s']=24,
		['p-u-morphling0071s']=25,
		['p-u-morphling0068s']=26,
		['p-u-morphling0069s']=27,
		['p-u-morphling0056s']=28,
		['p-u-morphling0057s']=29,
		['p-u-morphling0054s']=30,
		['p-u-morphling0055s']=31,
		['p-u-morphling0058s']=32,
		['p-u-morphling0061s']=33,
		['p-u-morphling0062s']=34,
		['p-u-morphling0059s']=35,
		['p-u-morphling0060s']=36,
		['p-u-morphling0011s']=37,
		['p-u-morphling0012s']=38,
		['p-u-morphling0009s']=39,
		['p-u-morphling0010s']=40,
		['p-u-morphling0013s']=41,
		['p-u-morphling0016s']=42,
		['p-u-morphling0017s']=43,
		['p-u-morphling0014s']=44,
		['p-u-morphling0015s']=45,
		['p-u-morphling0002s']=46,
		['p-u-morphling0003s']=47,
		['p-u-morphling0000s']=48,
		['p-u-morphling0001s']=49,
		['p-u-morphling0004s']=50,
		['p-u-morphling0007s']=51,
		['p-u-morphling0008s']=52,
		['p-u-morphling0005s']=53,
		['p-u-morphling0006s']=54,
		['p-u-morphling0029s']=55,
		['p-u-morphling0030s']=56,
		['p-u-morphling0027s']=57,
		['p-u-morphling0028s']=58,
		['p-u-morphling0031s']=59,
		['p-u-morphling0034s']=60,
		['p-u-morphling0035s']=61,
		['p-u-morphling0032s']=62,
		['p-u-morphling0033s']=63,
		['p-u-morphling0020s']=64,
		['p-u-morphling0021s']=65,
		['p-u-morphling0018s']=66,
		['p-u-morphling0019s']=67,
		['p-u-morphling0022s']=68,
		['p-u-morphling0025s']=69,
		['p-u-morphling0026s']=70,
		['p-u-morphling0023s']=71,
		['p-u-morphling0024s']=72,
	},
};
