--GENERATED ON: 05/12/2024 07:00:43


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['w-a-mmb'] = {
	TEX_WIDTH=1000,
	TEX_HEIGHT=600,
	FRAME_COUNT=288,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,04,0),
		[3]=v2fM(0,08,0),
		[4]=v2fM(0,12,0),
		[5]=v2fM(0,16,0),
		[6]=v2fM(0,2,0),
		[7]=v2fM(0,24,0),
		[8]=v2fM(0,28,0),
		[9]=v2fM(0,32,0),
		[10]=v2fM(0,36,0),
		[11]=v2fM(0,4,0),
		[12]=v2fM(0,44,0),
		[13]=v2fM(0,48,0),
		[14]=v2fM(0,52,0),
		[15]=v2fM(0,56,0),
		[16]=v2fM(0,6,0),
		[17]=v2fM(0,64,0),
		[18]=v2fM(0,68,0),
		[19]=v2fM(0,72,0),
		[20]=v2fM(0,76,0),
		[21]=v2fM(0,8,0),
		[22]=v2fM(0,84,0),
		[23]=v2fM(0,88,0),
		[24]=v2fM(0,92,0),
		[25]=v2fM(0,96,0),
		[26]=v2fM(0,0,0833333333333333),
		[27]=v2fM(0,04,0,0833333333333333),
		[28]=v2fM(0,08,0,0833333333333333),
		[29]=v2fM(0,12,0,0833333333333333),
		[30]=v2fM(0,16,0,0833333333333333),
		[31]=v2fM(0,2,0,0833333333333333),
		[32]=v2fM(0,24,0,0833333333333333),
		[33]=v2fM(0,28,0,0833333333333333),
		[34]=v2fM(0,32,0,0833333333333333),
		[35]=v2fM(0,36,0,0833333333333333),
		[36]=v2fM(0,4,0,0833333333333333),
		[37]=v2fM(0,44,0,0833333333333333),
		[38]=v2fM(0,48,0,0833333333333333),
		[39]=v2fM(0,52,0,0833333333333333),
		[40]=v2fM(0,56,0,0833333333333333),
		[41]=v2fM(0,6,0,0833333333333333),
		[42]=v2fM(0,64,0,0833333333333333),
		[43]=v2fM(0,68,0,0833333333333333),
		[44]=v2fM(0,72,0,0833333333333333),
		[45]=v2fM(0,76,0,0833333333333333),
		[46]=v2fM(0,8,0,0833333333333333),
		[47]=v2fM(0,84,0,0833333333333333),
		[48]=v2fM(0,88,0,0833333333333333),
		[49]=v2fM(0,92,0,0833333333333333),
		[50]=v2fM(0,96,0,0833333333333333),
		[51]=v2fM(0,0,166666666666667),
		[52]=v2fM(0,04,0,166666666666667),
		[53]=v2fM(0,08,0,166666666666667),
		[54]=v2fM(0,12,0,166666666666667),
		[55]=v2fM(0,16,0,166666666666667),
		[56]=v2fM(0,2,0,166666666666667),
		[57]=v2fM(0,24,0,166666666666667),
		[58]=v2fM(0,28,0,166666666666667),
		[59]=v2fM(0,32,0,166666666666667),
		[60]=v2fM(0,36,0,166666666666667),
		[61]=v2fM(0,4,0,166666666666667),
		[62]=v2fM(0,44,0,166666666666667),
		[63]=v2fM(0,48,0,166666666666667),
		[64]=v2fM(0,52,0,166666666666667),
		[65]=v2fM(0,56,0,166666666666667),
		[66]=v2fM(0,6,0,166666666666667),
		[67]=v2fM(0,64,0,166666666666667),
		[68]=v2fM(0,68,0,166666666666667),
		[69]=v2fM(0,72,0,166666666666667),
		[70]=v2fM(0,76,0,166666666666667),
		[71]=v2fM(0,8,0,166666666666667),
		[72]=v2fM(0,84,0,166666666666667),
		[73]=v2fM(0,88,0,166666666666667),
		[74]=v2fM(0,92,0,166666666666667),
		[75]=v2fM(0,96,0,166666666666667),
		[76]=v2fM(0,0,25),
		[77]=v2fM(0,04,0,25),
		[78]=v2fM(0,08,0,25),
		[79]=v2fM(0,12,0,25),
		[80]=v2fM(0,16,0,25),
		[81]=v2fM(0,2,0,25),
		[82]=v2fM(0,24,0,25),
		[83]=v2fM(0,28,0,25),
		[84]=v2fM(0,32,0,25),
		[85]=v2fM(0,36,0,25),
		[86]=v2fM(0,4,0,25),
		[87]=v2fM(0,44,0,25),
		[88]=v2fM(0,48,0,25),
		[89]=v2fM(0,52,0,25),
		[90]=v2fM(0,56,0,25),
		[91]=v2fM(0,6,0,25),
		[92]=v2fM(0,64,0,25),
		[93]=v2fM(0,68,0,25),
		[94]=v2fM(0,72,0,25),
		[95]=v2fM(0,76,0,25),
		[96]=v2fM(0,8,0,25),
		[97]=v2fM(0,84,0,25),
		[98]=v2fM(0,88,0,25),
		[99]=v2fM(0,92,0,25),
		[100]=v2fM(0,96,0,25),
		[101]=v2fM(0,0,333333333333333),
		[102]=v2fM(0,04,0,333333333333333),
		[103]=v2fM(0,08,0,333333333333333),
		[104]=v2fM(0,12,0,333333333333333),
		[105]=v2fM(0,16,0,333333333333333),
		[106]=v2fM(0,2,0,333333333333333),
		[107]=v2fM(0,24,0,333333333333333),
		[108]=v2fM(0,28,0,333333333333333),
		[109]=v2fM(0,32,0,333333333333333),
		[110]=v2fM(0,36,0,333333333333333),
		[111]=v2fM(0,4,0,333333333333333),
		[112]=v2fM(0,44,0,333333333333333),
		[113]=v2fM(0,48,0,333333333333333),
		[114]=v2fM(0,52,0,333333333333333),
		[115]=v2fM(0,56,0,333333333333333),
		[116]=v2fM(0,6,0,333333333333333),
		[117]=v2fM(0,64,0,333333333333333),
		[118]=v2fM(0,68,0,333333333333333),
		[119]=v2fM(0,72,0,333333333333333),
		[120]=v2fM(0,76,0,333333333333333),
		[121]=v2fM(0,8,0,333333333333333),
		[122]=v2fM(0,84,0,333333333333333),
		[123]=v2fM(0,88,0,333333333333333),
		[124]=v2fM(0,92,0,333333333333333),
		[125]=v2fM(0,96,0,333333333333333),
		[126]=v2fM(0,0,416666666666667),
		[127]=v2fM(0,04,0,416666666666667),
		[128]=v2fM(0,08,0,416666666666667),
		[129]=v2fM(0,12,0,416666666666667),
		[130]=v2fM(0,16,0,416666666666667),
		[131]=v2fM(0,2,0,416666666666667),
		[132]=v2fM(0,24,0,416666666666667),
		[133]=v2fM(0,28,0,416666666666667),
		[134]=v2fM(0,32,0,416666666666667),
		[135]=v2fM(0,36,0,416666666666667),
		[136]=v2fM(0,4,0,416666666666667),
		[137]=v2fM(0,44,0,416666666666667),
		[138]=v2fM(0,48,0,416666666666667),
		[139]=v2fM(0,52,0,416666666666667),
		[140]=v2fM(0,56,0,416666666666667),
		[141]=v2fM(0,6,0,416666666666667),
		[142]=v2fM(0,64,0,416666666666667),
		[143]=v2fM(0,68,0,416666666666667),
		[144]=v2fM(0,72,0,416666666666667),
		[145]=v2fM(0,76,0,416666666666667),
		[146]=v2fM(0,8,0,416666666666667),
		[147]=v2fM(0,84,0,416666666666667),
		[148]=v2fM(0,88,0,416666666666667),
		[149]=v2fM(0,92,0,416666666666667),
		[150]=v2fM(0,96,0,416666666666667),
		[151]=v2fM(0,0,5),
		[152]=v2fM(0,04,0,5),
		[153]=v2fM(0,08,0,5),
		[154]=v2fM(0,12,0,5),
		[155]=v2fM(0,16,0,5),
		[156]=v2fM(0,2,0,5),
		[157]=v2fM(0,24,0,5),
		[158]=v2fM(0,28,0,5),
		[159]=v2fM(0,32,0,5),
		[160]=v2fM(0,36,0,5),
		[161]=v2fM(0,4,0,5),
		[162]=v2fM(0,44,0,5),
		[163]=v2fM(0,48,0,5),
		[164]=v2fM(0,52,0,5),
		[165]=v2fM(0,56,0,5),
		[166]=v2fM(0,6,0,5),
		[167]=v2fM(0,64,0,5),
		[168]=v2fM(0,68,0,5),
		[169]=v2fM(0,72,0,5),
		[170]=v2fM(0,76,0,5),
		[171]=v2fM(0,8,0,5),
		[172]=v2fM(0,84,0,5),
		[173]=v2fM(0,88,0,5),
		[174]=v2fM(0,92,0,5),
		[175]=v2fM(0,96,0,5),
		[176]=v2fM(0,0,583333333333333),
		[177]=v2fM(0,04,0,583333333333333),
		[178]=v2fM(0,08,0,583333333333333),
		[179]=v2fM(0,12,0,583333333333333),
		[180]=v2fM(0,16,0,583333333333333),
		[181]=v2fM(0,2,0,583333333333333),
		[182]=v2fM(0,24,0,583333333333333),
		[183]=v2fM(0,28,0,583333333333333),
		[184]=v2fM(0,32,0,583333333333333),
		[185]=v2fM(0,36,0,583333333333333),
		[186]=v2fM(0,4,0,583333333333333),
		[187]=v2fM(0,44,0,583333333333333),
		[188]=v2fM(0,48,0,583333333333333),
		[189]=v2fM(0,52,0,583333333333333),
		[190]=v2fM(0,56,0,583333333333333),
		[191]=v2fM(0,6,0,583333333333333),
		[192]=v2fM(0,64,0,583333333333333),
		[193]=v2fM(0,68,0,583333333333333),
		[194]=v2fM(0,72,0,583333333333333),
		[195]=v2fM(0,76,0,583333333333333),
		[196]=v2fM(0,8,0,583333333333333),
		[197]=v2fM(0,84,0,583333333333333),
		[198]=v2fM(0,88,0,583333333333333),
		[199]=v2fM(0,92,0,583333333333333),
		[200]=v2fM(0,96,0,583333333333333),
		[201]=v2fM(0,0,666666666666667),
		[202]=v2fM(0,04,0,666666666666667),
		[203]=v2fM(0,08,0,666666666666667),
		[204]=v2fM(0,12,0,666666666666667),
		[205]=v2fM(0,16,0,666666666666667),
		[206]=v2fM(0,2,0,666666666666667),
		[207]=v2fM(0,24,0,666666666666667),
		[208]=v2fM(0,28,0,666666666666667),
		[209]=v2fM(0,32,0,666666666666667),
		[210]=v2fM(0,36,0,666666666666667),
		[211]=v2fM(0,4,0,666666666666667),
		[212]=v2fM(0,44,0,666666666666667),
		[213]=v2fM(0,48,0,666666666666667),
		[214]=v2fM(0,52,0,666666666666667),
		[215]=v2fM(0,56,0,666666666666667),
		[216]=v2fM(0,6,0,666666666666667),
		[217]=v2fM(0,64,0,666666666666667),
		[218]=v2fM(0,68,0,666666666666667),
		[219]=v2fM(0,72,0,666666666666667),
		[220]=v2fM(0,76,0,666666666666667),
		[221]=v2fM(0,8,0,666666666666667),
		[222]=v2fM(0,84,0,666666666666667),
		[223]=v2fM(0,88,0,666666666666667),
		[224]=v2fM(0,92,0,666666666666667),
		[225]=v2fM(0,96,0,666666666666667),
		[226]=v2fM(0,0,75),
		[227]=v2fM(0,04,0,75),
		[228]=v2fM(0,08,0,75),
		[229]=v2fM(0,12,0,75),
		[230]=v2fM(0,16,0,75),
		[231]=v2fM(0,2,0,75),
		[232]=v2fM(0,24,0,75),
		[233]=v2fM(0,28,0,75),
		[234]=v2fM(0,32,0,75),
		[235]=v2fM(0,36,0,75),
		[236]=v2fM(0,4,0,75),
		[237]=v2fM(0,44,0,75),
		[238]=v2fM(0,48,0,75),
		[239]=v2fM(0,52,0,75),
		[240]=v2fM(0,56,0,75),
		[241]=v2fM(0,6,0,75),
		[242]=v2fM(0,64,0,75),
		[243]=v2fM(0,68,0,75),
		[244]=v2fM(0,72,0,75),
		[245]=v2fM(0,76,0,75),
		[246]=v2fM(0,8,0,75),
		[247]=v2fM(0,84,0,75),
		[248]=v2fM(0,88,0,75),
		[249]=v2fM(0,92,0,75),
		[250]=v2fM(0,96,0,75),
		[251]=v2fM(0,0,833333333333333),
		[252]=v2fM(0,04,0,833333333333333),
		[253]=v2fM(0,08,0,833333333333333),
		[254]=v2fM(0,12,0,833333333333333),
		[255]=v2fM(0,16,0,833333333333333),
		[256]=v2fM(0,2,0,833333333333333),
		[257]=v2fM(0,24,0,833333333333333),
		[258]=v2fM(0,28,0,833333333333333),
		[259]=v2fM(0,32,0,833333333333333),
		[260]=v2fM(0,36,0,833333333333333),
		[261]=v2fM(0,4,0,833333333333333),
		[262]=v2fM(0,44,0,833333333333333),
		[263]=v2fM(0,48,0,833333333333333),
		[264]=v2fM(0,52,0,833333333333333),
		[265]=v2fM(0,56,0,833333333333333),
		[266]=v2fM(0,6,0,833333333333333),
		[267]=v2fM(0,64,0,833333333333333),
		[268]=v2fM(0,68,0,833333333333333),
		[269]=v2fM(0,72,0,833333333333333),
		[270]=v2fM(0,76,0,833333333333333),
		[271]=v2fM(0,8,0,833333333333333),
		[272]=v2fM(0,84,0,833333333333333),
		[273]=v2fM(0,88,0,833333333333333),
		[274]=v2fM(0,92,0,833333333333333),
		[275]=v2fM(0,96,0,833333333333333),
		[276]=v2fM(0,0,916666666666667),
		[277]=v2fM(0,04,0,916666666666667),
		[278]=v2fM(0,08,0,916666666666667),
		[279]=v2fM(0,12,0,916666666666667),
		[280]=v2fM(0,16,0,916666666666667),
		[281]=v2fM(0,2,0,916666666666667),
		[282]=v2fM(0,24,0,916666666666667),
		[283]=v2fM(0,28,0,916666666666667),
		[284]=v2fM(0,32,0,916666666666667),
		[285]=v2fM(0,36,0,916666666666667),
		[286]=v2fM(0,4,0,916666666666667),
		[287]=v2fM(0,44,0,916666666666667),
		[288]=v2fM(0,48,0,916666666666667),
	},
	FRAME_SIZES={
		[1]={WIDTH=40,HEIGHT=50},
		[2]={WIDTH=40,HEIGHT=50},
		[3]={WIDTH=40,HEIGHT=50},
		[4]={WIDTH=40,HEIGHT=50},
		[5]={WIDTH=40,HEIGHT=50},
		[6]={WIDTH=40,HEIGHT=50},
		[7]={WIDTH=40,HEIGHT=50},
		[8]={WIDTH=40,HEIGHT=50},
		[9]={WIDTH=40,HEIGHT=50},
		[10]={WIDTH=40,HEIGHT=50},
		[11]={WIDTH=40,HEIGHT=50},
		[12]={WIDTH=40,HEIGHT=50},
		[13]={WIDTH=40,HEIGHT=50},
		[14]={WIDTH=40,HEIGHT=50},
		[15]={WIDTH=40,HEIGHT=50},
		[16]={WIDTH=40,HEIGHT=50},
		[17]={WIDTH=40,HEIGHT=50},
		[18]={WIDTH=40,HEIGHT=50},
		[19]={WIDTH=40,HEIGHT=50},
		[20]={WIDTH=40,HEIGHT=50},
		[21]={WIDTH=40,HEIGHT=50},
		[22]={WIDTH=40,HEIGHT=50},
		[23]={WIDTH=40,HEIGHT=50},
		[24]={WIDTH=40,HEIGHT=50},
		[25]={WIDTH=40,HEIGHT=50},
		[26]={WIDTH=40,HEIGHT=50},
		[27]={WIDTH=40,HEIGHT=50},
		[28]={WIDTH=40,HEIGHT=50},
		[29]={WIDTH=40,HEIGHT=50},
		[30]={WIDTH=40,HEIGHT=50},
		[31]={WIDTH=40,HEIGHT=50},
		[32]={WIDTH=40,HEIGHT=50},
		[33]={WIDTH=40,HEIGHT=50},
		[34]={WIDTH=40,HEIGHT=50},
		[35]={WIDTH=40,HEIGHT=50},
		[36]={WIDTH=40,HEIGHT=50},
		[37]={WIDTH=40,HEIGHT=50},
		[38]={WIDTH=40,HEIGHT=50},
		[39]={WIDTH=40,HEIGHT=50},
		[40]={WIDTH=40,HEIGHT=50},
		[41]={WIDTH=40,HEIGHT=50},
		[42]={WIDTH=40,HEIGHT=50},
		[43]={WIDTH=40,HEIGHT=50},
		[44]={WIDTH=40,HEIGHT=50},
		[45]={WIDTH=40,HEIGHT=50},
		[46]={WIDTH=40,HEIGHT=50},
		[47]={WIDTH=40,HEIGHT=50},
		[48]={WIDTH=40,HEIGHT=50},
		[49]={WIDTH=40,HEIGHT=50},
		[50]={WIDTH=40,HEIGHT=50},
		[51]={WIDTH=40,HEIGHT=50},
		[52]={WIDTH=40,HEIGHT=50},
		[53]={WIDTH=40,HEIGHT=50},
		[54]={WIDTH=40,HEIGHT=50},
		[55]={WIDTH=40,HEIGHT=50},
		[56]={WIDTH=40,HEIGHT=50},
		[57]={WIDTH=40,HEIGHT=50},
		[58]={WIDTH=40,HEIGHT=50},
		[59]={WIDTH=40,HEIGHT=50},
		[60]={WIDTH=40,HEIGHT=50},
		[61]={WIDTH=40,HEIGHT=50},
		[62]={WIDTH=40,HEIGHT=50},
		[63]={WIDTH=40,HEIGHT=50},
		[64]={WIDTH=40,HEIGHT=50},
		[65]={WIDTH=40,HEIGHT=50},
		[66]={WIDTH=40,HEIGHT=50},
		[67]={WIDTH=40,HEIGHT=50},
		[68]={WIDTH=40,HEIGHT=50},
		[69]={WIDTH=40,HEIGHT=50},
		[70]={WIDTH=40,HEIGHT=50},
		[71]={WIDTH=40,HEIGHT=50},
		[72]={WIDTH=40,HEIGHT=50},
		[73]={WIDTH=40,HEIGHT=50},
		[74]={WIDTH=40,HEIGHT=50},
		[75]={WIDTH=40,HEIGHT=50},
		[76]={WIDTH=40,HEIGHT=50},
		[77]={WIDTH=40,HEIGHT=50},
		[78]={WIDTH=40,HEIGHT=50},
		[79]={WIDTH=40,HEIGHT=50},
		[80]={WIDTH=40,HEIGHT=50},
		[81]={WIDTH=40,HEIGHT=50},
		[82]={WIDTH=40,HEIGHT=50},
		[83]={WIDTH=40,HEIGHT=50},
		[84]={WIDTH=40,HEIGHT=50},
		[85]={WIDTH=40,HEIGHT=50},
		[86]={WIDTH=40,HEIGHT=50},
		[87]={WIDTH=40,HEIGHT=50},
		[88]={WIDTH=40,HEIGHT=50},
		[89]={WIDTH=40,HEIGHT=50},
		[90]={WIDTH=40,HEIGHT=50},
		[91]={WIDTH=40,HEIGHT=50},
		[92]={WIDTH=40,HEIGHT=50},
		[93]={WIDTH=40,HEIGHT=50},
		[94]={WIDTH=40,HEIGHT=50},
		[95]={WIDTH=40,HEIGHT=50},
		[96]={WIDTH=40,HEIGHT=50},
		[97]={WIDTH=40,HEIGHT=50},
		[98]={WIDTH=40,HEIGHT=50},
		[99]={WIDTH=40,HEIGHT=50},
		[100]={WIDTH=40,HEIGHT=50},
		[101]={WIDTH=40,HEIGHT=50},
		[102]={WIDTH=40,HEIGHT=50},
		[103]={WIDTH=40,HEIGHT=50},
		[104]={WIDTH=40,HEIGHT=50},
		[105]={WIDTH=40,HEIGHT=50},
		[106]={WIDTH=40,HEIGHT=50},
		[107]={WIDTH=40,HEIGHT=50},
		[108]={WIDTH=40,HEIGHT=50},
		[109]={WIDTH=40,HEIGHT=50},
		[110]={WIDTH=40,HEIGHT=50},
		[111]={WIDTH=40,HEIGHT=50},
		[112]={WIDTH=40,HEIGHT=50},
		[113]={WIDTH=40,HEIGHT=50},
		[114]={WIDTH=40,HEIGHT=50},
		[115]={WIDTH=40,HEIGHT=50},
		[116]={WIDTH=40,HEIGHT=50},
		[117]={WIDTH=40,HEIGHT=50},
		[118]={WIDTH=40,HEIGHT=50},
		[119]={WIDTH=40,HEIGHT=50},
		[120]={WIDTH=40,HEIGHT=50},
		[121]={WIDTH=40,HEIGHT=50},
		[122]={WIDTH=40,HEIGHT=50},
		[123]={WIDTH=40,HEIGHT=50},
		[124]={WIDTH=40,HEIGHT=50},
		[125]={WIDTH=40,HEIGHT=50},
		[126]={WIDTH=40,HEIGHT=50},
		[127]={WIDTH=40,HEIGHT=50},
		[128]={WIDTH=40,HEIGHT=50},
		[129]={WIDTH=40,HEIGHT=50},
		[130]={WIDTH=40,HEIGHT=50},
		[131]={WIDTH=40,HEIGHT=50},
		[132]={WIDTH=40,HEIGHT=50},
		[133]={WIDTH=40,HEIGHT=50},
		[134]={WIDTH=40,HEIGHT=50},
		[135]={WIDTH=40,HEIGHT=50},
		[136]={WIDTH=40,HEIGHT=50},
		[137]={WIDTH=40,HEIGHT=50},
		[138]={WIDTH=40,HEIGHT=50},
		[139]={WIDTH=40,HEIGHT=50},
		[140]={WIDTH=40,HEIGHT=50},
		[141]={WIDTH=40,HEIGHT=50},
		[142]={WIDTH=40,HEIGHT=50},
		[143]={WIDTH=40,HEIGHT=50},
		[144]={WIDTH=40,HEIGHT=50},
		[145]={WIDTH=40,HEIGHT=50},
		[146]={WIDTH=40,HEIGHT=50},
		[147]={WIDTH=40,HEIGHT=50},
		[148]={WIDTH=40,HEIGHT=50},
		[149]={WIDTH=40,HEIGHT=50},
		[150]={WIDTH=40,HEIGHT=50},
		[151]={WIDTH=40,HEIGHT=50},
		[152]={WIDTH=40,HEIGHT=50},
		[153]={WIDTH=40,HEIGHT=50},
		[154]={WIDTH=40,HEIGHT=50},
		[155]={WIDTH=40,HEIGHT=50},
		[156]={WIDTH=40,HEIGHT=50},
		[157]={WIDTH=40,HEIGHT=50},
		[158]={WIDTH=40,HEIGHT=50},
		[159]={WIDTH=40,HEIGHT=50},
		[160]={WIDTH=40,HEIGHT=50},
		[161]={WIDTH=40,HEIGHT=50},
		[162]={WIDTH=40,HEIGHT=50},
		[163]={WIDTH=40,HEIGHT=50},
		[164]={WIDTH=40,HEIGHT=50},
		[165]={WIDTH=40,HEIGHT=50},
		[166]={WIDTH=40,HEIGHT=50},
		[167]={WIDTH=40,HEIGHT=50},
		[168]={WIDTH=40,HEIGHT=50},
		[169]={WIDTH=40,HEIGHT=50},
		[170]={WIDTH=40,HEIGHT=50},
		[171]={WIDTH=40,HEIGHT=50},
		[172]={WIDTH=40,HEIGHT=50},
		[173]={WIDTH=40,HEIGHT=50},
		[174]={WIDTH=40,HEIGHT=50},
		[175]={WIDTH=40,HEIGHT=50},
		[176]={WIDTH=40,HEIGHT=50},
		[177]={WIDTH=40,HEIGHT=50},
		[178]={WIDTH=40,HEIGHT=50},
		[179]={WIDTH=40,HEIGHT=50},
		[180]={WIDTH=40,HEIGHT=50},
		[181]={WIDTH=40,HEIGHT=50},
		[182]={WIDTH=40,HEIGHT=50},
		[183]={WIDTH=40,HEIGHT=50},
		[184]={WIDTH=40,HEIGHT=50},
		[185]={WIDTH=40,HEIGHT=50},
		[186]={WIDTH=40,HEIGHT=50},
		[187]={WIDTH=40,HEIGHT=50},
		[188]={WIDTH=40,HEIGHT=50},
		[189]={WIDTH=40,HEIGHT=50},
		[190]={WIDTH=40,HEIGHT=50},
		[191]={WIDTH=40,HEIGHT=50},
		[192]={WIDTH=40,HEIGHT=50},
		[193]={WIDTH=40,HEIGHT=50},
		[194]={WIDTH=40,HEIGHT=50},
		[195]={WIDTH=40,HEIGHT=50},
		[196]={WIDTH=40,HEIGHT=50},
		[197]={WIDTH=40,HEIGHT=50},
		[198]={WIDTH=40,HEIGHT=50},
		[199]={WIDTH=40,HEIGHT=50},
		[200]={WIDTH=40,HEIGHT=50},
		[201]={WIDTH=40,HEIGHT=50},
		[202]={WIDTH=40,HEIGHT=50},
		[203]={WIDTH=40,HEIGHT=50},
		[204]={WIDTH=40,HEIGHT=50},
		[205]={WIDTH=40,HEIGHT=50},
		[206]={WIDTH=40,HEIGHT=50},
		[207]={WIDTH=40,HEIGHT=50},
		[208]={WIDTH=40,HEIGHT=50},
		[209]={WIDTH=40,HEIGHT=50},
		[210]={WIDTH=40,HEIGHT=50},
		[211]={WIDTH=40,HEIGHT=50},
		[212]={WIDTH=40,HEIGHT=50},
		[213]={WIDTH=40,HEIGHT=50},
		[214]={WIDTH=40,HEIGHT=50},
		[215]={WIDTH=40,HEIGHT=50},
		[216]={WIDTH=40,HEIGHT=50},
		[217]={WIDTH=40,HEIGHT=50},
		[218]={WIDTH=40,HEIGHT=50},
		[219]={WIDTH=40,HEIGHT=50},
		[220]={WIDTH=40,HEIGHT=50},
		[221]={WIDTH=40,HEIGHT=50},
		[222]={WIDTH=40,HEIGHT=50},
		[223]={WIDTH=40,HEIGHT=50},
		[224]={WIDTH=40,HEIGHT=50},
		[225]={WIDTH=40,HEIGHT=50},
		[226]={WIDTH=40,HEIGHT=50},
		[227]={WIDTH=40,HEIGHT=50},
		[228]={WIDTH=40,HEIGHT=50},
		[229]={WIDTH=40,HEIGHT=50},
		[230]={WIDTH=40,HEIGHT=50},
		[231]={WIDTH=40,HEIGHT=50},
		[232]={WIDTH=40,HEIGHT=50},
		[233]={WIDTH=40,HEIGHT=50},
		[234]={WIDTH=40,HEIGHT=50},
		[235]={WIDTH=40,HEIGHT=50},
		[236]={WIDTH=40,HEIGHT=50},
		[237]={WIDTH=40,HEIGHT=50},
		[238]={WIDTH=40,HEIGHT=50},
		[239]={WIDTH=40,HEIGHT=50},
		[240]={WIDTH=40,HEIGHT=50},
		[241]={WIDTH=40,HEIGHT=50},
		[242]={WIDTH=40,HEIGHT=50},
		[243]={WIDTH=40,HEIGHT=50},
		[244]={WIDTH=40,HEIGHT=50},
		[245]={WIDTH=40,HEIGHT=50},
		[246]={WIDTH=40,HEIGHT=50},
		[247]={WIDTH=40,HEIGHT=50},
		[248]={WIDTH=40,HEIGHT=50},
		[249]={WIDTH=40,HEIGHT=50},
		[250]={WIDTH=40,HEIGHT=50},
		[251]={WIDTH=40,HEIGHT=50},
		[252]={WIDTH=40,HEIGHT=50},
		[253]={WIDTH=40,HEIGHT=50},
		[254]={WIDTH=40,HEIGHT=50},
		[255]={WIDTH=40,HEIGHT=50},
		[256]={WIDTH=40,HEIGHT=50},
		[257]={WIDTH=40,HEIGHT=50},
		[258]={WIDTH=40,HEIGHT=50},
		[259]={WIDTH=40,HEIGHT=50},
		[260]={WIDTH=40,HEIGHT=50},
		[261]={WIDTH=40,HEIGHT=50},
		[262]={WIDTH=40,HEIGHT=50},
		[263]={WIDTH=40,HEIGHT=50},
		[264]={WIDTH=40,HEIGHT=50},
		[265]={WIDTH=40,HEIGHT=50},
		[266]={WIDTH=40,HEIGHT=50},
		[267]={WIDTH=40,HEIGHT=50},
		[268]={WIDTH=40,HEIGHT=50},
		[269]={WIDTH=40,HEIGHT=50},
		[270]={WIDTH=40,HEIGHT=50},
		[271]={WIDTH=40,HEIGHT=50},
		[272]={WIDTH=40,HEIGHT=50},
		[273]={WIDTH=40,HEIGHT=50},
		[274]={WIDTH=40,HEIGHT=50},
		[275]={WIDTH=40,HEIGHT=50},
		[276]={WIDTH=40,HEIGHT=50},
		[277]={WIDTH=40,HEIGHT=50},
		[278]={WIDTH=40,HEIGHT=50},
		[279]={WIDTH=40,HEIGHT=50},
		[280]={WIDTH=40,HEIGHT=50},
		[281]={WIDTH=40,HEIGHT=50},
		[282]={WIDTH=40,HEIGHT=50},
		[283]={WIDTH=40,HEIGHT=50},
		[284]={WIDTH=40,HEIGHT=50},
		[285]={WIDTH=40,HEIGHT=50},
		[286]={WIDTH=40,HEIGHT=50},
		[287]={WIDTH=40,HEIGHT=50},
		[288]={WIDTH=40,HEIGHT=50},
	},
	FRAME_INDEXS={
		['w-a-mmb0191']=1,
		['w-a-mmb0192']=2,
		['w-a-mmb0189']=3,
		['w-a-mmb0190']=4,
		['w-a-mmb0193']=5,
		['w-a-mmb0196']=6,
		['w-a-mmb0197']=7,
		['w-a-mmb0194']=8,
		['w-a-mmb0195']=9,
		['w-a-mmb0182']=10,
		['w-a-mmb0183']=11,
		['w-a-mmb0180']=12,
		['w-a-mmb0181']=13,
		['w-a-mmb0184']=14,
		['w-a-mmb0187']=15,
		['w-a-mmb0188']=16,
		['w-a-mmb0185']=17,
		['w-a-mmb0186']=18,
		['w-a-mmb0209']=19,
		['w-a-mmb0210']=20,
		['w-a-mmb0207']=21,
		['w-a-mmb0208']=22,
		['w-a-mmb0211']=23,
		['w-a-mmb0214']=24,
		['w-a-mmb0215']=25,
		['w-a-mmb0212']=26,
		['w-a-mmb0213']=27,
		['w-a-mmb0200']=28,
		['w-a-mmb0201']=29,
		['w-a-mmb0198']=30,
		['w-a-mmb0199']=31,
		['w-a-mmb0202']=32,
		['w-a-mmb0205']=33,
		['w-a-mmb0206']=34,
		['w-a-mmb0203']=35,
		['w-a-mmb0204']=36,
		['w-a-mmb0155']=37,
		['w-a-mmb0156']=38,
		['w-a-mmb0153']=39,
		['w-a-mmb0154']=40,
		['w-a-mmb0157']=41,
		['w-a-mmb0160']=42,
		['w-a-mmb0161']=43,
		['w-a-mmb0158']=44,
		['w-a-mmb0159']=45,
		['w-a-mmb0146']=46,
		['w-a-mmb0147']=47,
		['w-a-mmb0144']=48,
		['w-a-mmb0145']=49,
		['w-a-mmb0148']=50,
		['w-a-mmb0151']=51,
		['w-a-mmb0152']=52,
		['w-a-mmb0149']=53,
		['w-a-mmb0150']=54,
		['w-a-mmb0173']=55,
		['w-a-mmb0174']=56,
		['w-a-mmb0171']=57,
		['w-a-mmb0172']=58,
		['w-a-mmb0175']=59,
		['w-a-mmb0178']=60,
		['w-a-mmb0179']=61,
		['w-a-mmb0176']=62,
		['w-a-mmb0177']=63,
		['w-a-mmb0164']=64,
		['w-a-mmb0165']=65,
		['w-a-mmb0162']=66,
		['w-a-mmb0163']=67,
		['w-a-mmb0166']=68,
		['w-a-mmb0169']=69,
		['w-a-mmb0170']=70,
		['w-a-mmb0167']=71,
		['w-a-mmb0168']=72,
		['w-a-mmb0263']=73,
		['w-a-mmb0264']=74,
		['w-a-mmb0261']=75,
		['w-a-mmb0262']=76,
		['w-a-mmb0265']=77,
		['w-a-mmb0268']=78,
		['w-a-mmb0269']=79,
		['w-a-mmb0266']=80,
		['w-a-mmb0267']=81,
		['w-a-mmb0254']=82,
		['w-a-mmb0255']=83,
		['w-a-mmb0252']=84,
		['w-a-mmb0253']=85,
		['w-a-mmb0256']=86,
		['w-a-mmb0259']=87,
		['w-a-mmb0260']=88,
		['w-a-mmb0257']=89,
		['w-a-mmb0258']=90,
		['w-a-mmb0281']=91,
		['w-a-mmb0282']=92,
		['w-a-mmb0279']=93,
		['w-a-mmb0280']=94,
		['w-a-mmb0283']=95,
		['w-a-mmb0286']=96,
		['w-a-mmb0287']=97,
		['w-a-mmb0284']=98,
		['w-a-mmb0285']=99,
		['w-a-mmb0272']=100,
		['w-a-mmb0273']=101,
		['w-a-mmb0270']=102,
		['w-a-mmb0271']=103,
		['w-a-mmb0274']=104,
		['w-a-mmb0277']=105,
		['w-a-mmb0278']=106,
		['w-a-mmb0275']=107,
		['w-a-mmb0276']=108,
		['w-a-mmb0227']=109,
		['w-a-mmb0228']=110,
		['w-a-mmb0225']=111,
		['w-a-mmb0226']=112,
		['w-a-mmb0229']=113,
		['w-a-mmb0232']=114,
		['w-a-mmb0233']=115,
		['w-a-mmb0230']=116,
		['w-a-mmb0231']=117,
		['w-a-mmb0218']=118,
		['w-a-mmb0219']=119,
		['w-a-mmb0216']=120,
		['w-a-mmb0217']=121,
		['w-a-mmb0220']=122,
		['w-a-mmb0223']=123,
		['w-a-mmb0224']=124,
		['w-a-mmb0221']=125,
		['w-a-mmb0222']=126,
		['w-a-mmb0245']=127,
		['w-a-mmb0246']=128,
		['w-a-mmb0243']=129,
		['w-a-mmb0244']=130,
		['w-a-mmb0247']=131,
		['w-a-mmb0250']=132,
		['w-a-mmb0251']=133,
		['w-a-mmb0248']=134,
		['w-a-mmb0249']=135,
		['w-a-mmb0236']=136,
		['w-a-mmb0237']=137,
		['w-a-mmb0234']=138,
		['w-a-mmb0235']=139,
		['w-a-mmb0238']=140,
		['w-a-mmb0241']=141,
		['w-a-mmb0242']=142,
		['w-a-mmb0239']=143,
		['w-a-mmb0240']=144,
		['w-a-mmb0047']=145,
		['w-a-mmb0048']=146,
		['w-a-mmb0045']=147,
		['w-a-mmb0046']=148,
		['w-a-mmb0049']=149,
		['w-a-mmb0052']=150,
		['w-a-mmb0053']=151,
		['w-a-mmb0050']=152,
		['w-a-mmb0051']=153,
		['w-a-mmb0038']=154,
		['w-a-mmb0039']=155,
		['w-a-mmb0036']=156,
		['w-a-mmb0037']=157,
		['w-a-mmb0040']=158,
		['w-a-mmb0043']=159,
		['w-a-mmb0044']=160,
		['w-a-mmb0041']=161,
		['w-a-mmb0042']=162,
		['w-a-mmb0065']=163,
		['w-a-mmb0066']=164,
		['w-a-mmb0063']=165,
		['w-a-mmb0064']=166,
		['w-a-mmb0067']=167,
		['w-a-mmb0070']=168,
		['w-a-mmb0071']=169,
		['w-a-mmb0068']=170,
		['w-a-mmb0069']=171,
		['w-a-mmb0056']=172,
		['w-a-mmb0057']=173,
		['w-a-mmb0054']=174,
		['w-a-mmb0055']=175,
		['w-a-mmb0058']=176,
		['w-a-mmb0061']=177,
		['w-a-mmb0062']=178,
		['w-a-mmb0059']=179,
		['w-a-mmb0060']=180,
		['w-a-mmb0011']=181,
		['w-a-mmb0012']=182,
		['w-a-mmb0009']=183,
		['w-a-mmb0010']=184,
		['w-a-mmb0013']=185,
		['w-a-mmb0016']=186,
		['w-a-mmb0017']=187,
		['w-a-mmb0014']=188,
		['w-a-mmb0015']=189,
		['w-a-mmb0002']=190,
		['w-a-mmb0003']=191,
		['w-a-mmb0000']=192,
		['w-a-mmb0001']=193,
		['w-a-mmb0004']=194,
		['w-a-mmb0007']=195,
		['w-a-mmb0008']=196,
		['w-a-mmb0005']=197,
		['w-a-mmb0006']=198,
		['w-a-mmb0029']=199,
		['w-a-mmb0030']=200,
		['w-a-mmb0027']=201,
		['w-a-mmb0028']=202,
		['w-a-mmb0031']=203,
		['w-a-mmb0034']=204,
		['w-a-mmb0035']=205,
		['w-a-mmb0032']=206,
		['w-a-mmb0033']=207,
		['w-a-mmb0020']=208,
		['w-a-mmb0021']=209,
		['w-a-mmb0018']=210,
		['w-a-mmb0019']=211,
		['w-a-mmb0022']=212,
		['w-a-mmb0025']=213,
		['w-a-mmb0026']=214,
		['w-a-mmb0023']=215,
		['w-a-mmb0024']=216,
		['w-a-mmb0119']=217,
		['w-a-mmb0120']=218,
		['w-a-mmb0117']=219,
		['w-a-mmb0118']=220,
		['w-a-mmb0121']=221,
		['w-a-mmb0124']=222,
		['w-a-mmb0125']=223,
		['w-a-mmb0122']=224,
		['w-a-mmb0123']=225,
		['w-a-mmb0110']=226,
		['w-a-mmb0111']=227,
		['w-a-mmb0108']=228,
		['w-a-mmb0109']=229,
		['w-a-mmb0112']=230,
		['w-a-mmb0115']=231,
		['w-a-mmb0116']=232,
		['w-a-mmb0113']=233,
		['w-a-mmb0114']=234,
		['w-a-mmb0137']=235,
		['w-a-mmb0138']=236,
		['w-a-mmb0135']=237,
		['w-a-mmb0136']=238,
		['w-a-mmb0139']=239,
		['w-a-mmb0142']=240,
		['w-a-mmb0143']=241,
		['w-a-mmb0140']=242,
		['w-a-mmb0141']=243,
		['w-a-mmb0128']=244,
		['w-a-mmb0129']=245,
		['w-a-mmb0126']=246,
		['w-a-mmb0127']=247,
		['w-a-mmb0130']=248,
		['w-a-mmb0133']=249,
		['w-a-mmb0134']=250,
		['w-a-mmb0131']=251,
		['w-a-mmb0132']=252,
		['w-a-mmb0083']=253,
		['w-a-mmb0084']=254,
		['w-a-mmb0081']=255,
		['w-a-mmb0082']=256,
		['w-a-mmb0085']=257,
		['w-a-mmb0088']=258,
		['w-a-mmb0089']=259,
		['w-a-mmb0086']=260,
		['w-a-mmb0087']=261,
		['w-a-mmb0074']=262,
		['w-a-mmb0075']=263,
		['w-a-mmb0072']=264,
		['w-a-mmb0073']=265,
		['w-a-mmb0076']=266,
		['w-a-mmb0079']=267,
		['w-a-mmb0080']=268,
		['w-a-mmb0077']=269,
		['w-a-mmb0078']=270,
		['w-a-mmb0101']=271,
		['w-a-mmb0102']=272,
		['w-a-mmb0099']=273,
		['w-a-mmb0100']=274,
		['w-a-mmb0103']=275,
		['w-a-mmb0106']=276,
		['w-a-mmb0107']=277,
		['w-a-mmb0104']=278,
		['w-a-mmb0105']=279,
		['w-a-mmb0092']=280,
		['w-a-mmb0093']=281,
		['w-a-mmb0090']=282,
		['w-a-mmb0091']=283,
		['w-a-mmb0094']=284,
		['w-a-mmb0097']=285,
		['w-a-mmb0098']=286,
		['w-a-mmb0095']=287,
		['w-a-mmb0096']=288,
	},
};
