--GENERATED ON: 05/12/2024 07:00:43


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['w-a-mmbs'] = {
	TEX_WIDTH=1000,
	TEX_HEIGHT=600,
	FRAME_COUNT=288,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,04,0),
		[3]=v2fM(0,08,0),
		[4]=v2fM(0,12,0),
		[5]=v2fM(0,16,0),
		[6]=v2fM(0,2,0),
		[7]=v2fM(0,24,0),
		[8]=v2fM(0,28,0),
		[9]=v2fM(0,32,0),
		[10]=v2fM(0,36,0),
		[11]=v2fM(0,4,0),
		[12]=v2fM(0,44,0),
		[13]=v2fM(0,48,0),
		[14]=v2fM(0,52,0),
		[15]=v2fM(0,56,0),
		[16]=v2fM(0,6,0),
		[17]=v2fM(0,64,0),
		[18]=v2fM(0,68,0),
		[19]=v2fM(0,72,0),
		[20]=v2fM(0,76,0),
		[21]=v2fM(0,8,0),
		[22]=v2fM(0,84,0),
		[23]=v2fM(0,88,0),
		[24]=v2fM(0,92,0),
		[25]=v2fM(0,96,0),
		[26]=v2fM(0,0,0833333333333333),
		[27]=v2fM(0,04,0,0833333333333333),
		[28]=v2fM(0,08,0,0833333333333333),
		[29]=v2fM(0,12,0,0833333333333333),
		[30]=v2fM(0,16,0,0833333333333333),
		[31]=v2fM(0,2,0,0833333333333333),
		[32]=v2fM(0,24,0,0833333333333333),
		[33]=v2fM(0,28,0,0833333333333333),
		[34]=v2fM(0,32,0,0833333333333333),
		[35]=v2fM(0,36,0,0833333333333333),
		[36]=v2fM(0,4,0,0833333333333333),
		[37]=v2fM(0,44,0,0833333333333333),
		[38]=v2fM(0,48,0,0833333333333333),
		[39]=v2fM(0,52,0,0833333333333333),
		[40]=v2fM(0,56,0,0833333333333333),
		[41]=v2fM(0,6,0,0833333333333333),
		[42]=v2fM(0,64,0,0833333333333333),
		[43]=v2fM(0,68,0,0833333333333333),
		[44]=v2fM(0,72,0,0833333333333333),
		[45]=v2fM(0,76,0,0833333333333333),
		[46]=v2fM(0,8,0,0833333333333333),
		[47]=v2fM(0,84,0,0833333333333333),
		[48]=v2fM(0,88,0,0833333333333333),
		[49]=v2fM(0,92,0,0833333333333333),
		[50]=v2fM(0,96,0,0833333333333333),
		[51]=v2fM(0,0,166666666666667),
		[52]=v2fM(0,04,0,166666666666667),
		[53]=v2fM(0,08,0,166666666666667),
		[54]=v2fM(0,12,0,166666666666667),
		[55]=v2fM(0,16,0,166666666666667),
		[56]=v2fM(0,2,0,166666666666667),
		[57]=v2fM(0,24,0,166666666666667),
		[58]=v2fM(0,28,0,166666666666667),
		[59]=v2fM(0,32,0,166666666666667),
		[60]=v2fM(0,36,0,166666666666667),
		[61]=v2fM(0,4,0,166666666666667),
		[62]=v2fM(0,44,0,166666666666667),
		[63]=v2fM(0,48,0,166666666666667),
		[64]=v2fM(0,52,0,166666666666667),
		[65]=v2fM(0,56,0,166666666666667),
		[66]=v2fM(0,6,0,166666666666667),
		[67]=v2fM(0,64,0,166666666666667),
		[68]=v2fM(0,68,0,166666666666667),
		[69]=v2fM(0,72,0,166666666666667),
		[70]=v2fM(0,76,0,166666666666667),
		[71]=v2fM(0,8,0,166666666666667),
		[72]=v2fM(0,84,0,166666666666667),
		[73]=v2fM(0,88,0,166666666666667),
		[74]=v2fM(0,92,0,166666666666667),
		[75]=v2fM(0,96,0,166666666666667),
		[76]=v2fM(0,0,25),
		[77]=v2fM(0,04,0,25),
		[78]=v2fM(0,08,0,25),
		[79]=v2fM(0,12,0,25),
		[80]=v2fM(0,16,0,25),
		[81]=v2fM(0,2,0,25),
		[82]=v2fM(0,24,0,25),
		[83]=v2fM(0,28,0,25),
		[84]=v2fM(0,32,0,25),
		[85]=v2fM(0,36,0,25),
		[86]=v2fM(0,4,0,25),
		[87]=v2fM(0,44,0,25),
		[88]=v2fM(0,48,0,25),
		[89]=v2fM(0,52,0,25),
		[90]=v2fM(0,56,0,25),
		[91]=v2fM(0,6,0,25),
		[92]=v2fM(0,64,0,25),
		[93]=v2fM(0,68,0,25),
		[94]=v2fM(0,72,0,25),
		[95]=v2fM(0,76,0,25),
		[96]=v2fM(0,8,0,25),
		[97]=v2fM(0,84,0,25),
		[98]=v2fM(0,88,0,25),
		[99]=v2fM(0,92,0,25),
		[100]=v2fM(0,96,0,25),
		[101]=v2fM(0,0,333333333333333),
		[102]=v2fM(0,04,0,333333333333333),
		[103]=v2fM(0,08,0,333333333333333),
		[104]=v2fM(0,12,0,333333333333333),
		[105]=v2fM(0,16,0,333333333333333),
		[106]=v2fM(0,2,0,333333333333333),
		[107]=v2fM(0,24,0,333333333333333),
		[108]=v2fM(0,28,0,333333333333333),
		[109]=v2fM(0,32,0,333333333333333),
		[110]=v2fM(0,36,0,333333333333333),
		[111]=v2fM(0,4,0,333333333333333),
		[112]=v2fM(0,44,0,333333333333333),
		[113]=v2fM(0,48,0,333333333333333),
		[114]=v2fM(0,52,0,333333333333333),
		[115]=v2fM(0,56,0,333333333333333),
		[116]=v2fM(0,6,0,333333333333333),
		[117]=v2fM(0,64,0,333333333333333),
		[118]=v2fM(0,68,0,333333333333333),
		[119]=v2fM(0,72,0,333333333333333),
		[120]=v2fM(0,76,0,333333333333333),
		[121]=v2fM(0,8,0,333333333333333),
		[122]=v2fM(0,84,0,333333333333333),
		[123]=v2fM(0,88,0,333333333333333),
		[124]=v2fM(0,92,0,333333333333333),
		[125]=v2fM(0,96,0,333333333333333),
		[126]=v2fM(0,0,416666666666667),
		[127]=v2fM(0,04,0,416666666666667),
		[128]=v2fM(0,08,0,416666666666667),
		[129]=v2fM(0,12,0,416666666666667),
		[130]=v2fM(0,16,0,416666666666667),
		[131]=v2fM(0,2,0,416666666666667),
		[132]=v2fM(0,24,0,416666666666667),
		[133]=v2fM(0,28,0,416666666666667),
		[134]=v2fM(0,32,0,416666666666667),
		[135]=v2fM(0,36,0,416666666666667),
		[136]=v2fM(0,4,0,416666666666667),
		[137]=v2fM(0,44,0,416666666666667),
		[138]=v2fM(0,48,0,416666666666667),
		[139]=v2fM(0,52,0,416666666666667),
		[140]=v2fM(0,56,0,416666666666667),
		[141]=v2fM(0,6,0,416666666666667),
		[142]=v2fM(0,64,0,416666666666667),
		[143]=v2fM(0,68,0,416666666666667),
		[144]=v2fM(0,72,0,416666666666667),
		[145]=v2fM(0,76,0,416666666666667),
		[146]=v2fM(0,8,0,416666666666667),
		[147]=v2fM(0,84,0,416666666666667),
		[148]=v2fM(0,88,0,416666666666667),
		[149]=v2fM(0,92,0,416666666666667),
		[150]=v2fM(0,96,0,416666666666667),
		[151]=v2fM(0,0,5),
		[152]=v2fM(0,04,0,5),
		[153]=v2fM(0,08,0,5),
		[154]=v2fM(0,12,0,5),
		[155]=v2fM(0,16,0,5),
		[156]=v2fM(0,2,0,5),
		[157]=v2fM(0,24,0,5),
		[158]=v2fM(0,28,0,5),
		[159]=v2fM(0,32,0,5),
		[160]=v2fM(0,36,0,5),
		[161]=v2fM(0,4,0,5),
		[162]=v2fM(0,44,0,5),
		[163]=v2fM(0,48,0,5),
		[164]=v2fM(0,52,0,5),
		[165]=v2fM(0,56,0,5),
		[166]=v2fM(0,6,0,5),
		[167]=v2fM(0,64,0,5),
		[168]=v2fM(0,68,0,5),
		[169]=v2fM(0,72,0,5),
		[170]=v2fM(0,76,0,5),
		[171]=v2fM(0,8,0,5),
		[172]=v2fM(0,84,0,5),
		[173]=v2fM(0,88,0,5),
		[174]=v2fM(0,92,0,5),
		[175]=v2fM(0,96,0,5),
		[176]=v2fM(0,0,583333333333333),
		[177]=v2fM(0,04,0,583333333333333),
		[178]=v2fM(0,08,0,583333333333333),
		[179]=v2fM(0,12,0,583333333333333),
		[180]=v2fM(0,16,0,583333333333333),
		[181]=v2fM(0,2,0,583333333333333),
		[182]=v2fM(0,24,0,583333333333333),
		[183]=v2fM(0,28,0,583333333333333),
		[184]=v2fM(0,32,0,583333333333333),
		[185]=v2fM(0,36,0,583333333333333),
		[186]=v2fM(0,4,0,583333333333333),
		[187]=v2fM(0,44,0,583333333333333),
		[188]=v2fM(0,48,0,583333333333333),
		[189]=v2fM(0,52,0,583333333333333),
		[190]=v2fM(0,56,0,583333333333333),
		[191]=v2fM(0,6,0,583333333333333),
		[192]=v2fM(0,64,0,583333333333333),
		[193]=v2fM(0,68,0,583333333333333),
		[194]=v2fM(0,72,0,583333333333333),
		[195]=v2fM(0,76,0,583333333333333),
		[196]=v2fM(0,8,0,583333333333333),
		[197]=v2fM(0,84,0,583333333333333),
		[198]=v2fM(0,88,0,583333333333333),
		[199]=v2fM(0,92,0,583333333333333),
		[200]=v2fM(0,96,0,583333333333333),
		[201]=v2fM(0,0,666666666666667),
		[202]=v2fM(0,04,0,666666666666667),
		[203]=v2fM(0,08,0,666666666666667),
		[204]=v2fM(0,12,0,666666666666667),
		[205]=v2fM(0,16,0,666666666666667),
		[206]=v2fM(0,2,0,666666666666667),
		[207]=v2fM(0,24,0,666666666666667),
		[208]=v2fM(0,28,0,666666666666667),
		[209]=v2fM(0,32,0,666666666666667),
		[210]=v2fM(0,36,0,666666666666667),
		[211]=v2fM(0,4,0,666666666666667),
		[212]=v2fM(0,44,0,666666666666667),
		[213]=v2fM(0,48,0,666666666666667),
		[214]=v2fM(0,52,0,666666666666667),
		[215]=v2fM(0,56,0,666666666666667),
		[216]=v2fM(0,6,0,666666666666667),
		[217]=v2fM(0,64,0,666666666666667),
		[218]=v2fM(0,68,0,666666666666667),
		[219]=v2fM(0,72,0,666666666666667),
		[220]=v2fM(0,76,0,666666666666667),
		[221]=v2fM(0,8,0,666666666666667),
		[222]=v2fM(0,84,0,666666666666667),
		[223]=v2fM(0,88,0,666666666666667),
		[224]=v2fM(0,92,0,666666666666667),
		[225]=v2fM(0,96,0,666666666666667),
		[226]=v2fM(0,0,75),
		[227]=v2fM(0,04,0,75),
		[228]=v2fM(0,08,0,75),
		[229]=v2fM(0,12,0,75),
		[230]=v2fM(0,16,0,75),
		[231]=v2fM(0,2,0,75),
		[232]=v2fM(0,24,0,75),
		[233]=v2fM(0,28,0,75),
		[234]=v2fM(0,32,0,75),
		[235]=v2fM(0,36,0,75),
		[236]=v2fM(0,4,0,75),
		[237]=v2fM(0,44,0,75),
		[238]=v2fM(0,48,0,75),
		[239]=v2fM(0,52,0,75),
		[240]=v2fM(0,56,0,75),
		[241]=v2fM(0,6,0,75),
		[242]=v2fM(0,64,0,75),
		[243]=v2fM(0,68,0,75),
		[244]=v2fM(0,72,0,75),
		[245]=v2fM(0,76,0,75),
		[246]=v2fM(0,8,0,75),
		[247]=v2fM(0,84,0,75),
		[248]=v2fM(0,88,0,75),
		[249]=v2fM(0,92,0,75),
		[250]=v2fM(0,96,0,75),
		[251]=v2fM(0,0,833333333333333),
		[252]=v2fM(0,04,0,833333333333333),
		[253]=v2fM(0,08,0,833333333333333),
		[254]=v2fM(0,12,0,833333333333333),
		[255]=v2fM(0,16,0,833333333333333),
		[256]=v2fM(0,2,0,833333333333333),
		[257]=v2fM(0,24,0,833333333333333),
		[258]=v2fM(0,28,0,833333333333333),
		[259]=v2fM(0,32,0,833333333333333),
		[260]=v2fM(0,36,0,833333333333333),
		[261]=v2fM(0,4,0,833333333333333),
		[262]=v2fM(0,44,0,833333333333333),
		[263]=v2fM(0,48,0,833333333333333),
		[264]=v2fM(0,52,0,833333333333333),
		[265]=v2fM(0,56,0,833333333333333),
		[266]=v2fM(0,6,0,833333333333333),
		[267]=v2fM(0,64,0,833333333333333),
		[268]=v2fM(0,68,0,833333333333333),
		[269]=v2fM(0,72,0,833333333333333),
		[270]=v2fM(0,76,0,833333333333333),
		[271]=v2fM(0,8,0,833333333333333),
		[272]=v2fM(0,84,0,833333333333333),
		[273]=v2fM(0,88,0,833333333333333),
		[274]=v2fM(0,92,0,833333333333333),
		[275]=v2fM(0,96,0,833333333333333),
		[276]=v2fM(0,0,916666666666667),
		[277]=v2fM(0,04,0,916666666666667),
		[278]=v2fM(0,08,0,916666666666667),
		[279]=v2fM(0,12,0,916666666666667),
		[280]=v2fM(0,16,0,916666666666667),
		[281]=v2fM(0,2,0,916666666666667),
		[282]=v2fM(0,24,0,916666666666667),
		[283]=v2fM(0,28,0,916666666666667),
		[284]=v2fM(0,32,0,916666666666667),
		[285]=v2fM(0,36,0,916666666666667),
		[286]=v2fM(0,4,0,916666666666667),
		[287]=v2fM(0,44,0,916666666666667),
		[288]=v2fM(0,48,0,916666666666667),
	},
	FRAME_SIZES={
		[1]={WIDTH=40,HEIGHT=50},
		[2]={WIDTH=40,HEIGHT=50},
		[3]={WIDTH=40,HEIGHT=50},
		[4]={WIDTH=40,HEIGHT=50},
		[5]={WIDTH=40,HEIGHT=50},
		[6]={WIDTH=40,HEIGHT=50},
		[7]={WIDTH=40,HEIGHT=50},
		[8]={WIDTH=40,HEIGHT=50},
		[9]={WIDTH=40,HEIGHT=50},
		[10]={WIDTH=40,HEIGHT=50},
		[11]={WIDTH=40,HEIGHT=50},
		[12]={WIDTH=40,HEIGHT=50},
		[13]={WIDTH=40,HEIGHT=50},
		[14]={WIDTH=40,HEIGHT=50},
		[15]={WIDTH=40,HEIGHT=50},
		[16]={WIDTH=40,HEIGHT=50},
		[17]={WIDTH=40,HEIGHT=50},
		[18]={WIDTH=40,HEIGHT=50},
		[19]={WIDTH=40,HEIGHT=50},
		[20]={WIDTH=40,HEIGHT=50},
		[21]={WIDTH=40,HEIGHT=50},
		[22]={WIDTH=40,HEIGHT=50},
		[23]={WIDTH=40,HEIGHT=50},
		[24]={WIDTH=40,HEIGHT=50},
		[25]={WIDTH=40,HEIGHT=50},
		[26]={WIDTH=40,HEIGHT=50},
		[27]={WIDTH=40,HEIGHT=50},
		[28]={WIDTH=40,HEIGHT=50},
		[29]={WIDTH=40,HEIGHT=50},
		[30]={WIDTH=40,HEIGHT=50},
		[31]={WIDTH=40,HEIGHT=50},
		[32]={WIDTH=40,HEIGHT=50},
		[33]={WIDTH=40,HEIGHT=50},
		[34]={WIDTH=40,HEIGHT=50},
		[35]={WIDTH=40,HEIGHT=50},
		[36]={WIDTH=40,HEIGHT=50},
		[37]={WIDTH=40,HEIGHT=50},
		[38]={WIDTH=40,HEIGHT=50},
		[39]={WIDTH=40,HEIGHT=50},
		[40]={WIDTH=40,HEIGHT=50},
		[41]={WIDTH=40,HEIGHT=50},
		[42]={WIDTH=40,HEIGHT=50},
		[43]={WIDTH=40,HEIGHT=50},
		[44]={WIDTH=40,HEIGHT=50},
		[45]={WIDTH=40,HEIGHT=50},
		[46]={WIDTH=40,HEIGHT=50},
		[47]={WIDTH=40,HEIGHT=50},
		[48]={WIDTH=40,HEIGHT=50},
		[49]={WIDTH=40,HEIGHT=50},
		[50]={WIDTH=40,HEIGHT=50},
		[51]={WIDTH=40,HEIGHT=50},
		[52]={WIDTH=40,HEIGHT=50},
		[53]={WIDTH=40,HEIGHT=50},
		[54]={WIDTH=40,HEIGHT=50},
		[55]={WIDTH=40,HEIGHT=50},
		[56]={WIDTH=40,HEIGHT=50},
		[57]={WIDTH=40,HEIGHT=50},
		[58]={WIDTH=40,HEIGHT=50},
		[59]={WIDTH=40,HEIGHT=50},
		[60]={WIDTH=40,HEIGHT=50},
		[61]={WIDTH=40,HEIGHT=50},
		[62]={WIDTH=40,HEIGHT=50},
		[63]={WIDTH=40,HEIGHT=50},
		[64]={WIDTH=40,HEIGHT=50},
		[65]={WIDTH=40,HEIGHT=50},
		[66]={WIDTH=40,HEIGHT=50},
		[67]={WIDTH=40,HEIGHT=50},
		[68]={WIDTH=40,HEIGHT=50},
		[69]={WIDTH=40,HEIGHT=50},
		[70]={WIDTH=40,HEIGHT=50},
		[71]={WIDTH=40,HEIGHT=50},
		[72]={WIDTH=40,HEIGHT=50},
		[73]={WIDTH=40,HEIGHT=50},
		[74]={WIDTH=40,HEIGHT=50},
		[75]={WIDTH=40,HEIGHT=50},
		[76]={WIDTH=40,HEIGHT=50},
		[77]={WIDTH=40,HEIGHT=50},
		[78]={WIDTH=40,HEIGHT=50},
		[79]={WIDTH=40,HEIGHT=50},
		[80]={WIDTH=40,HEIGHT=50},
		[81]={WIDTH=40,HEIGHT=50},
		[82]={WIDTH=40,HEIGHT=50},
		[83]={WIDTH=40,HEIGHT=50},
		[84]={WIDTH=40,HEIGHT=50},
		[85]={WIDTH=40,HEIGHT=50},
		[86]={WIDTH=40,HEIGHT=50},
		[87]={WIDTH=40,HEIGHT=50},
		[88]={WIDTH=40,HEIGHT=50},
		[89]={WIDTH=40,HEIGHT=50},
		[90]={WIDTH=40,HEIGHT=50},
		[91]={WIDTH=40,HEIGHT=50},
		[92]={WIDTH=40,HEIGHT=50},
		[93]={WIDTH=40,HEIGHT=50},
		[94]={WIDTH=40,HEIGHT=50},
		[95]={WIDTH=40,HEIGHT=50},
		[96]={WIDTH=40,HEIGHT=50},
		[97]={WIDTH=40,HEIGHT=50},
		[98]={WIDTH=40,HEIGHT=50},
		[99]={WIDTH=40,HEIGHT=50},
		[100]={WIDTH=40,HEIGHT=50},
		[101]={WIDTH=40,HEIGHT=50},
		[102]={WIDTH=40,HEIGHT=50},
		[103]={WIDTH=40,HEIGHT=50},
		[104]={WIDTH=40,HEIGHT=50},
		[105]={WIDTH=40,HEIGHT=50},
		[106]={WIDTH=40,HEIGHT=50},
		[107]={WIDTH=40,HEIGHT=50},
		[108]={WIDTH=40,HEIGHT=50},
		[109]={WIDTH=40,HEIGHT=50},
		[110]={WIDTH=40,HEIGHT=50},
		[111]={WIDTH=40,HEIGHT=50},
		[112]={WIDTH=40,HEIGHT=50},
		[113]={WIDTH=40,HEIGHT=50},
		[114]={WIDTH=40,HEIGHT=50},
		[115]={WIDTH=40,HEIGHT=50},
		[116]={WIDTH=40,HEIGHT=50},
		[117]={WIDTH=40,HEIGHT=50},
		[118]={WIDTH=40,HEIGHT=50},
		[119]={WIDTH=40,HEIGHT=50},
		[120]={WIDTH=40,HEIGHT=50},
		[121]={WIDTH=40,HEIGHT=50},
		[122]={WIDTH=40,HEIGHT=50},
		[123]={WIDTH=40,HEIGHT=50},
		[124]={WIDTH=40,HEIGHT=50},
		[125]={WIDTH=40,HEIGHT=50},
		[126]={WIDTH=40,HEIGHT=50},
		[127]={WIDTH=40,HEIGHT=50},
		[128]={WIDTH=40,HEIGHT=50},
		[129]={WIDTH=40,HEIGHT=50},
		[130]={WIDTH=40,HEIGHT=50},
		[131]={WIDTH=40,HEIGHT=50},
		[132]={WIDTH=40,HEIGHT=50},
		[133]={WIDTH=40,HEIGHT=50},
		[134]={WIDTH=40,HEIGHT=50},
		[135]={WIDTH=40,HEIGHT=50},
		[136]={WIDTH=40,HEIGHT=50},
		[137]={WIDTH=40,HEIGHT=50},
		[138]={WIDTH=40,HEIGHT=50},
		[139]={WIDTH=40,HEIGHT=50},
		[140]={WIDTH=40,HEIGHT=50},
		[141]={WIDTH=40,HEIGHT=50},
		[142]={WIDTH=40,HEIGHT=50},
		[143]={WIDTH=40,HEIGHT=50},
		[144]={WIDTH=40,HEIGHT=50},
		[145]={WIDTH=40,HEIGHT=50},
		[146]={WIDTH=40,HEIGHT=50},
		[147]={WIDTH=40,HEIGHT=50},
		[148]={WIDTH=40,HEIGHT=50},
		[149]={WIDTH=40,HEIGHT=50},
		[150]={WIDTH=40,HEIGHT=50},
		[151]={WIDTH=40,HEIGHT=50},
		[152]={WIDTH=40,HEIGHT=50},
		[153]={WIDTH=40,HEIGHT=50},
		[154]={WIDTH=40,HEIGHT=50},
		[155]={WIDTH=40,HEIGHT=50},
		[156]={WIDTH=40,HEIGHT=50},
		[157]={WIDTH=40,HEIGHT=50},
		[158]={WIDTH=40,HEIGHT=50},
		[159]={WIDTH=40,HEIGHT=50},
		[160]={WIDTH=40,HEIGHT=50},
		[161]={WIDTH=40,HEIGHT=50},
		[162]={WIDTH=40,HEIGHT=50},
		[163]={WIDTH=40,HEIGHT=50},
		[164]={WIDTH=40,HEIGHT=50},
		[165]={WIDTH=40,HEIGHT=50},
		[166]={WIDTH=40,HEIGHT=50},
		[167]={WIDTH=40,HEIGHT=50},
		[168]={WIDTH=40,HEIGHT=50},
		[169]={WIDTH=40,HEIGHT=50},
		[170]={WIDTH=40,HEIGHT=50},
		[171]={WIDTH=40,HEIGHT=50},
		[172]={WIDTH=40,HEIGHT=50},
		[173]={WIDTH=40,HEIGHT=50},
		[174]={WIDTH=40,HEIGHT=50},
		[175]={WIDTH=40,HEIGHT=50},
		[176]={WIDTH=40,HEIGHT=50},
		[177]={WIDTH=40,HEIGHT=50},
		[178]={WIDTH=40,HEIGHT=50},
		[179]={WIDTH=40,HEIGHT=50},
		[180]={WIDTH=40,HEIGHT=50},
		[181]={WIDTH=40,HEIGHT=50},
		[182]={WIDTH=40,HEIGHT=50},
		[183]={WIDTH=40,HEIGHT=50},
		[184]={WIDTH=40,HEIGHT=50},
		[185]={WIDTH=40,HEIGHT=50},
		[186]={WIDTH=40,HEIGHT=50},
		[187]={WIDTH=40,HEIGHT=50},
		[188]={WIDTH=40,HEIGHT=50},
		[189]={WIDTH=40,HEIGHT=50},
		[190]={WIDTH=40,HEIGHT=50},
		[191]={WIDTH=40,HEIGHT=50},
		[192]={WIDTH=40,HEIGHT=50},
		[193]={WIDTH=40,HEIGHT=50},
		[194]={WIDTH=40,HEIGHT=50},
		[195]={WIDTH=40,HEIGHT=50},
		[196]={WIDTH=40,HEIGHT=50},
		[197]={WIDTH=40,HEIGHT=50},
		[198]={WIDTH=40,HEIGHT=50},
		[199]={WIDTH=40,HEIGHT=50},
		[200]={WIDTH=40,HEIGHT=50},
		[201]={WIDTH=40,HEIGHT=50},
		[202]={WIDTH=40,HEIGHT=50},
		[203]={WIDTH=40,HEIGHT=50},
		[204]={WIDTH=40,HEIGHT=50},
		[205]={WIDTH=40,HEIGHT=50},
		[206]={WIDTH=40,HEIGHT=50},
		[207]={WIDTH=40,HEIGHT=50},
		[208]={WIDTH=40,HEIGHT=50},
		[209]={WIDTH=40,HEIGHT=50},
		[210]={WIDTH=40,HEIGHT=50},
		[211]={WIDTH=40,HEIGHT=50},
		[212]={WIDTH=40,HEIGHT=50},
		[213]={WIDTH=40,HEIGHT=50},
		[214]={WIDTH=40,HEIGHT=50},
		[215]={WIDTH=40,HEIGHT=50},
		[216]={WIDTH=40,HEIGHT=50},
		[217]={WIDTH=40,HEIGHT=50},
		[218]={WIDTH=40,HEIGHT=50},
		[219]={WIDTH=40,HEIGHT=50},
		[220]={WIDTH=40,HEIGHT=50},
		[221]={WIDTH=40,HEIGHT=50},
		[222]={WIDTH=40,HEIGHT=50},
		[223]={WIDTH=40,HEIGHT=50},
		[224]={WIDTH=40,HEIGHT=50},
		[225]={WIDTH=40,HEIGHT=50},
		[226]={WIDTH=40,HEIGHT=50},
		[227]={WIDTH=40,HEIGHT=50},
		[228]={WIDTH=40,HEIGHT=50},
		[229]={WIDTH=40,HEIGHT=50},
		[230]={WIDTH=40,HEIGHT=50},
		[231]={WIDTH=40,HEIGHT=50},
		[232]={WIDTH=40,HEIGHT=50},
		[233]={WIDTH=40,HEIGHT=50},
		[234]={WIDTH=40,HEIGHT=50},
		[235]={WIDTH=40,HEIGHT=50},
		[236]={WIDTH=40,HEIGHT=50},
		[237]={WIDTH=40,HEIGHT=50},
		[238]={WIDTH=40,HEIGHT=50},
		[239]={WIDTH=40,HEIGHT=50},
		[240]={WIDTH=40,HEIGHT=50},
		[241]={WIDTH=40,HEIGHT=50},
		[242]={WIDTH=40,HEIGHT=50},
		[243]={WIDTH=40,HEIGHT=50},
		[244]={WIDTH=40,HEIGHT=50},
		[245]={WIDTH=40,HEIGHT=50},
		[246]={WIDTH=40,HEIGHT=50},
		[247]={WIDTH=40,HEIGHT=50},
		[248]={WIDTH=40,HEIGHT=50},
		[249]={WIDTH=40,HEIGHT=50},
		[250]={WIDTH=40,HEIGHT=50},
		[251]={WIDTH=40,HEIGHT=50},
		[252]={WIDTH=40,HEIGHT=50},
		[253]={WIDTH=40,HEIGHT=50},
		[254]={WIDTH=40,HEIGHT=50},
		[255]={WIDTH=40,HEIGHT=50},
		[256]={WIDTH=40,HEIGHT=50},
		[257]={WIDTH=40,HEIGHT=50},
		[258]={WIDTH=40,HEIGHT=50},
		[259]={WIDTH=40,HEIGHT=50},
		[260]={WIDTH=40,HEIGHT=50},
		[261]={WIDTH=40,HEIGHT=50},
		[262]={WIDTH=40,HEIGHT=50},
		[263]={WIDTH=40,HEIGHT=50},
		[264]={WIDTH=40,HEIGHT=50},
		[265]={WIDTH=40,HEIGHT=50},
		[266]={WIDTH=40,HEIGHT=50},
		[267]={WIDTH=40,HEIGHT=50},
		[268]={WIDTH=40,HEIGHT=50},
		[269]={WIDTH=40,HEIGHT=50},
		[270]={WIDTH=40,HEIGHT=50},
		[271]={WIDTH=40,HEIGHT=50},
		[272]={WIDTH=40,HEIGHT=50},
		[273]={WIDTH=40,HEIGHT=50},
		[274]={WIDTH=40,HEIGHT=50},
		[275]={WIDTH=40,HEIGHT=50},
		[276]={WIDTH=40,HEIGHT=50},
		[277]={WIDTH=40,HEIGHT=50},
		[278]={WIDTH=40,HEIGHT=50},
		[279]={WIDTH=40,HEIGHT=50},
		[280]={WIDTH=40,HEIGHT=50},
		[281]={WIDTH=40,HEIGHT=50},
		[282]={WIDTH=40,HEIGHT=50},
		[283]={WIDTH=40,HEIGHT=50},
		[284]={WIDTH=40,HEIGHT=50},
		[285]={WIDTH=40,HEIGHT=50},
		[286]={WIDTH=40,HEIGHT=50},
		[287]={WIDTH=40,HEIGHT=50},
		[288]={WIDTH=40,HEIGHT=50},
	},
	FRAME_INDEXS={
		['w-a-mmb0191s']=1,
		['w-a-mmb0192s']=2,
		['w-a-mmb0189s']=3,
		['w-a-mmb0190s']=4,
		['w-a-mmb0193s']=5,
		['w-a-mmb0196s']=6,
		['w-a-mmb0197s']=7,
		['w-a-mmb0194s']=8,
		['w-a-mmb0195s']=9,
		['w-a-mmb0182s']=10,
		['w-a-mmb0183s']=11,
		['w-a-mmb0180s']=12,
		['w-a-mmb0181s']=13,
		['w-a-mmb0184s']=14,
		['w-a-mmb0187s']=15,
		['w-a-mmb0188s']=16,
		['w-a-mmb0185s']=17,
		['w-a-mmb0186s']=18,
		['w-a-mmb0209s']=19,
		['w-a-mmb0210s']=20,
		['w-a-mmb0207s']=21,
		['w-a-mmb0208s']=22,
		['w-a-mmb0211s']=23,
		['w-a-mmb0214s']=24,
		['w-a-mmb0215s']=25,
		['w-a-mmb0212s']=26,
		['w-a-mmb0213s']=27,
		['w-a-mmb0200s']=28,
		['w-a-mmb0201s']=29,
		['w-a-mmb0198s']=30,
		['w-a-mmb0199s']=31,
		['w-a-mmb0202s']=32,
		['w-a-mmb0205s']=33,
		['w-a-mmb0206s']=34,
		['w-a-mmb0203s']=35,
		['w-a-mmb0204s']=36,
		['w-a-mmb0155s']=37,
		['w-a-mmb0156s']=38,
		['w-a-mmb0153s']=39,
		['w-a-mmb0154s']=40,
		['w-a-mmb0157s']=41,
		['w-a-mmb0160s']=42,
		['w-a-mmb0161s']=43,
		['w-a-mmb0158s']=44,
		['w-a-mmb0159s']=45,
		['w-a-mmb0146s']=46,
		['w-a-mmb0147s']=47,
		['w-a-mmb0144s']=48,
		['w-a-mmb0145s']=49,
		['w-a-mmb0148s']=50,
		['w-a-mmb0151s']=51,
		['w-a-mmb0152s']=52,
		['w-a-mmb0149s']=53,
		['w-a-mmb0150s']=54,
		['w-a-mmb0173s']=55,
		['w-a-mmb0174s']=56,
		['w-a-mmb0171s']=57,
		['w-a-mmb0172s']=58,
		['w-a-mmb0175s']=59,
		['w-a-mmb0178s']=60,
		['w-a-mmb0179s']=61,
		['w-a-mmb0176s']=62,
		['w-a-mmb0177s']=63,
		['w-a-mmb0164s']=64,
		['w-a-mmb0165s']=65,
		['w-a-mmb0162s']=66,
		['w-a-mmb0163s']=67,
		['w-a-mmb0166s']=68,
		['w-a-mmb0169s']=69,
		['w-a-mmb0170s']=70,
		['w-a-mmb0167s']=71,
		['w-a-mmb0168s']=72,
		['w-a-mmb0263s']=73,
		['w-a-mmb0264s']=74,
		['w-a-mmb0261s']=75,
		['w-a-mmb0262s']=76,
		['w-a-mmb0265s']=77,
		['w-a-mmb0268s']=78,
		['w-a-mmb0269s']=79,
		['w-a-mmb0266s']=80,
		['w-a-mmb0267s']=81,
		['w-a-mmb0254s']=82,
		['w-a-mmb0255s']=83,
		['w-a-mmb0252s']=84,
		['w-a-mmb0253s']=85,
		['w-a-mmb0256s']=86,
		['w-a-mmb0259s']=87,
		['w-a-mmb0260s']=88,
		['w-a-mmb0257s']=89,
		['w-a-mmb0258s']=90,
		['w-a-mmb0281s']=91,
		['w-a-mmb0282s']=92,
		['w-a-mmb0279s']=93,
		['w-a-mmb0280s']=94,
		['w-a-mmb0283s']=95,
		['w-a-mmb0286s']=96,
		['w-a-mmb0287s']=97,
		['w-a-mmb0284s']=98,
		['w-a-mmb0285s']=99,
		['w-a-mmb0272s']=100,
		['w-a-mmb0273s']=101,
		['w-a-mmb0270s']=102,
		['w-a-mmb0271s']=103,
		['w-a-mmb0274s']=104,
		['w-a-mmb0277s']=105,
		['w-a-mmb0278s']=106,
		['w-a-mmb0275s']=107,
		['w-a-mmb0276s']=108,
		['w-a-mmb0227s']=109,
		['w-a-mmb0228s']=110,
		['w-a-mmb0225s']=111,
		['w-a-mmb0226s']=112,
		['w-a-mmb0229s']=113,
		['w-a-mmb0232s']=114,
		['w-a-mmb0233s']=115,
		['w-a-mmb0230s']=116,
		['w-a-mmb0231s']=117,
		['w-a-mmb0218s']=118,
		['w-a-mmb0219s']=119,
		['w-a-mmb0216s']=120,
		['w-a-mmb0217s']=121,
		['w-a-mmb0220s']=122,
		['w-a-mmb0223s']=123,
		['w-a-mmb0224s']=124,
		['w-a-mmb0221s']=125,
		['w-a-mmb0222s']=126,
		['w-a-mmb0245s']=127,
		['w-a-mmb0246s']=128,
		['w-a-mmb0243s']=129,
		['w-a-mmb0244s']=130,
		['w-a-mmb0247s']=131,
		['w-a-mmb0250s']=132,
		['w-a-mmb0251s']=133,
		['w-a-mmb0248s']=134,
		['w-a-mmb0249s']=135,
		['w-a-mmb0236s']=136,
		['w-a-mmb0237s']=137,
		['w-a-mmb0234s']=138,
		['w-a-mmb0235s']=139,
		['w-a-mmb0238s']=140,
		['w-a-mmb0241s']=141,
		['w-a-mmb0242s']=142,
		['w-a-mmb0239s']=143,
		['w-a-mmb0240s']=144,
		['w-a-mmb0047s']=145,
		['w-a-mmb0048s']=146,
		['w-a-mmb0045s']=147,
		['w-a-mmb0046s']=148,
		['w-a-mmb0049s']=149,
		['w-a-mmb0052s']=150,
		['w-a-mmb0053s']=151,
		['w-a-mmb0050s']=152,
		['w-a-mmb0051s']=153,
		['w-a-mmb0038s']=154,
		['w-a-mmb0039s']=155,
		['w-a-mmb0036s']=156,
		['w-a-mmb0037s']=157,
		['w-a-mmb0040s']=158,
		['w-a-mmb0043s']=159,
		['w-a-mmb0044s']=160,
		['w-a-mmb0041s']=161,
		['w-a-mmb0042s']=162,
		['w-a-mmb0065s']=163,
		['w-a-mmb0066s']=164,
		['w-a-mmb0063s']=165,
		['w-a-mmb0064s']=166,
		['w-a-mmb0067s']=167,
		['w-a-mmb0070s']=168,
		['w-a-mmb0071s']=169,
		['w-a-mmb0068s']=170,
		['w-a-mmb0069s']=171,
		['w-a-mmb0056s']=172,
		['w-a-mmb0057s']=173,
		['w-a-mmb0054s']=174,
		['w-a-mmb0055s']=175,
		['w-a-mmb0058s']=176,
		['w-a-mmb0061s']=177,
		['w-a-mmb0062s']=178,
		['w-a-mmb0059s']=179,
		['w-a-mmb0060s']=180,
		['w-a-mmb0011s']=181,
		['w-a-mmb0012s']=182,
		['w-a-mmb0009s']=183,
		['w-a-mmb0010s']=184,
		['w-a-mmb0013s']=185,
		['w-a-mmb0016s']=186,
		['w-a-mmb0017s']=187,
		['w-a-mmb0014s']=188,
		['w-a-mmb0015s']=189,
		['w-a-mmb0002s']=190,
		['w-a-mmb0003s']=191,
		['w-a-mmb0000s']=192,
		['w-a-mmb0001s']=193,
		['w-a-mmb0004s']=194,
		['w-a-mmb0007s']=195,
		['w-a-mmb0008s']=196,
		['w-a-mmb0005s']=197,
		['w-a-mmb0006s']=198,
		['w-a-mmb0029s']=199,
		['w-a-mmb0030s']=200,
		['w-a-mmb0027s']=201,
		['w-a-mmb0028s']=202,
		['w-a-mmb0031s']=203,
		['w-a-mmb0034s']=204,
		['w-a-mmb0035s']=205,
		['w-a-mmb0032s']=206,
		['w-a-mmb0033s']=207,
		['w-a-mmb0020s']=208,
		['w-a-mmb0021s']=209,
		['w-a-mmb0018s']=210,
		['w-a-mmb0019s']=211,
		['w-a-mmb0022s']=212,
		['w-a-mmb0025s']=213,
		['w-a-mmb0026s']=214,
		['w-a-mmb0023s']=215,
		['w-a-mmb0024s']=216,
		['w-a-mmb0119s']=217,
		['w-a-mmb0120s']=218,
		['w-a-mmb0117s']=219,
		['w-a-mmb0118s']=220,
		['w-a-mmb0121s']=221,
		['w-a-mmb0124s']=222,
		['w-a-mmb0125s']=223,
		['w-a-mmb0122s']=224,
		['w-a-mmb0123s']=225,
		['w-a-mmb0110s']=226,
		['w-a-mmb0111s']=227,
		['w-a-mmb0108s']=228,
		['w-a-mmb0109s']=229,
		['w-a-mmb0112s']=230,
		['w-a-mmb0115s']=231,
		['w-a-mmb0116s']=232,
		['w-a-mmb0113s']=233,
		['w-a-mmb0114s']=234,
		['w-a-mmb0137s']=235,
		['w-a-mmb0138s']=236,
		['w-a-mmb0135s']=237,
		['w-a-mmb0136s']=238,
		['w-a-mmb0139s']=239,
		['w-a-mmb0142s']=240,
		['w-a-mmb0143s']=241,
		['w-a-mmb0140s']=242,
		['w-a-mmb0141s']=243,
		['w-a-mmb0128s']=244,
		['w-a-mmb0129s']=245,
		['w-a-mmb0126s']=246,
		['w-a-mmb0127s']=247,
		['w-a-mmb0130s']=248,
		['w-a-mmb0133s']=249,
		['w-a-mmb0134s']=250,
		['w-a-mmb0131s']=251,
		['w-a-mmb0132s']=252,
		['w-a-mmb0083s']=253,
		['w-a-mmb0084s']=254,
		['w-a-mmb0081s']=255,
		['w-a-mmb0082s']=256,
		['w-a-mmb0085s']=257,
		['w-a-mmb0088s']=258,
		['w-a-mmb0089s']=259,
		['w-a-mmb0086s']=260,
		['w-a-mmb0087s']=261,
		['w-a-mmb0074s']=262,
		['w-a-mmb0075s']=263,
		['w-a-mmb0072s']=264,
		['w-a-mmb0073s']=265,
		['w-a-mmb0076s']=266,
		['w-a-mmb0079s']=267,
		['w-a-mmb0080s']=268,
		['w-a-mmb0077s']=269,
		['w-a-mmb0078s']=270,
		['w-a-mmb0101s']=271,
		['w-a-mmb0102s']=272,
		['w-a-mmb0099s']=273,
		['w-a-mmb0100s']=274,
		['w-a-mmb0103s']=275,
		['w-a-mmb0106s']=276,
		['w-a-mmb0107s']=277,
		['w-a-mmb0104s']=278,
		['w-a-mmb0105s']=279,
		['w-a-mmb0092s']=280,
		['w-a-mmb0093s']=281,
		['w-a-mmb0090s']=282,
		['w-a-mmb0091s']=283,
		['w-a-mmb0094s']=284,
		['w-a-mmb0097s']=285,
		['w-a-mmb0098s']=286,
		['w-a-mmb0095s']=287,
		['w-a-mmb0096s']=288,
	},
};
