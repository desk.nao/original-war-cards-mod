--GENERATED ON: 05/12/2024 07:00:44


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['w-a-mmbz'] = {
	TEX_WIDTH=1000,
	TEX_HEIGHT=600,
	FRAME_COUNT=288,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,04,0),
		[3]=v2fM(0,08,0),
		[4]=v2fM(0,12,0),
		[5]=v2fM(0,16,0),
		[6]=v2fM(0,2,0),
		[7]=v2fM(0,24,0),
		[8]=v2fM(0,28,0),
		[9]=v2fM(0,32,0),
		[10]=v2fM(0,36,0),
		[11]=v2fM(0,4,0),
		[12]=v2fM(0,44,0),
		[13]=v2fM(0,48,0),
		[14]=v2fM(0,52,0),
		[15]=v2fM(0,56,0),
		[16]=v2fM(0,6,0),
		[17]=v2fM(0,64,0),
		[18]=v2fM(0,68,0),
		[19]=v2fM(0,72,0),
		[20]=v2fM(0,76,0),
		[21]=v2fM(0,8,0),
		[22]=v2fM(0,84,0),
		[23]=v2fM(0,88,0),
		[24]=v2fM(0,92,0),
		[25]=v2fM(0,96,0),
		[26]=v2fM(0,0,0833333333333333),
		[27]=v2fM(0,04,0,0833333333333333),
		[28]=v2fM(0,08,0,0833333333333333),
		[29]=v2fM(0,12,0,0833333333333333),
		[30]=v2fM(0,16,0,0833333333333333),
		[31]=v2fM(0,2,0,0833333333333333),
		[32]=v2fM(0,24,0,0833333333333333),
		[33]=v2fM(0,28,0,0833333333333333),
		[34]=v2fM(0,32,0,0833333333333333),
		[35]=v2fM(0,36,0,0833333333333333),
		[36]=v2fM(0,4,0,0833333333333333),
		[37]=v2fM(0,44,0,0833333333333333),
		[38]=v2fM(0,48,0,0833333333333333),
		[39]=v2fM(0,52,0,0833333333333333),
		[40]=v2fM(0,56,0,0833333333333333),
		[41]=v2fM(0,6,0,0833333333333333),
		[42]=v2fM(0,64,0,0833333333333333),
		[43]=v2fM(0,68,0,0833333333333333),
		[44]=v2fM(0,72,0,0833333333333333),
		[45]=v2fM(0,76,0,0833333333333333),
		[46]=v2fM(0,8,0,0833333333333333),
		[47]=v2fM(0,84,0,0833333333333333),
		[48]=v2fM(0,88,0,0833333333333333),
		[49]=v2fM(0,92,0,0833333333333333),
		[50]=v2fM(0,96,0,0833333333333333),
		[51]=v2fM(0,0,166666666666667),
		[52]=v2fM(0,04,0,166666666666667),
		[53]=v2fM(0,08,0,166666666666667),
		[54]=v2fM(0,12,0,166666666666667),
		[55]=v2fM(0,16,0,166666666666667),
		[56]=v2fM(0,2,0,166666666666667),
		[57]=v2fM(0,24,0,166666666666667),
		[58]=v2fM(0,28,0,166666666666667),
		[59]=v2fM(0,32,0,166666666666667),
		[60]=v2fM(0,36,0,166666666666667),
		[61]=v2fM(0,4,0,166666666666667),
		[62]=v2fM(0,44,0,166666666666667),
		[63]=v2fM(0,48,0,166666666666667),
		[64]=v2fM(0,52,0,166666666666667),
		[65]=v2fM(0,56,0,166666666666667),
		[66]=v2fM(0,6,0,166666666666667),
		[67]=v2fM(0,64,0,166666666666667),
		[68]=v2fM(0,68,0,166666666666667),
		[69]=v2fM(0,72,0,166666666666667),
		[70]=v2fM(0,76,0,166666666666667),
		[71]=v2fM(0,8,0,166666666666667),
		[72]=v2fM(0,84,0,166666666666667),
		[73]=v2fM(0,88,0,166666666666667),
		[74]=v2fM(0,92,0,166666666666667),
		[75]=v2fM(0,96,0,166666666666667),
		[76]=v2fM(0,0,25),
		[77]=v2fM(0,04,0,25),
		[78]=v2fM(0,08,0,25),
		[79]=v2fM(0,12,0,25),
		[80]=v2fM(0,16,0,25),
		[81]=v2fM(0,2,0,25),
		[82]=v2fM(0,24,0,25),
		[83]=v2fM(0,28,0,25),
		[84]=v2fM(0,32,0,25),
		[85]=v2fM(0,36,0,25),
		[86]=v2fM(0,4,0,25),
		[87]=v2fM(0,44,0,25),
		[88]=v2fM(0,48,0,25),
		[89]=v2fM(0,52,0,25),
		[90]=v2fM(0,56,0,25),
		[91]=v2fM(0,6,0,25),
		[92]=v2fM(0,64,0,25),
		[93]=v2fM(0,68,0,25),
		[94]=v2fM(0,72,0,25),
		[95]=v2fM(0,76,0,25),
		[96]=v2fM(0,8,0,25),
		[97]=v2fM(0,84,0,25),
		[98]=v2fM(0,88,0,25),
		[99]=v2fM(0,92,0,25),
		[100]=v2fM(0,96,0,25),
		[101]=v2fM(0,0,333333333333333),
		[102]=v2fM(0,04,0,333333333333333),
		[103]=v2fM(0,08,0,333333333333333),
		[104]=v2fM(0,12,0,333333333333333),
		[105]=v2fM(0,16,0,333333333333333),
		[106]=v2fM(0,2,0,333333333333333),
		[107]=v2fM(0,24,0,333333333333333),
		[108]=v2fM(0,28,0,333333333333333),
		[109]=v2fM(0,32,0,333333333333333),
		[110]=v2fM(0,36,0,333333333333333),
		[111]=v2fM(0,4,0,333333333333333),
		[112]=v2fM(0,44,0,333333333333333),
		[113]=v2fM(0,48,0,333333333333333),
		[114]=v2fM(0,52,0,333333333333333),
		[115]=v2fM(0,56,0,333333333333333),
		[116]=v2fM(0,6,0,333333333333333),
		[117]=v2fM(0,64,0,333333333333333),
		[118]=v2fM(0,68,0,333333333333333),
		[119]=v2fM(0,72,0,333333333333333),
		[120]=v2fM(0,76,0,333333333333333),
		[121]=v2fM(0,8,0,333333333333333),
		[122]=v2fM(0,84,0,333333333333333),
		[123]=v2fM(0,88,0,333333333333333),
		[124]=v2fM(0,92,0,333333333333333),
		[125]=v2fM(0,96,0,333333333333333),
		[126]=v2fM(0,0,416666666666667),
		[127]=v2fM(0,04,0,416666666666667),
		[128]=v2fM(0,08,0,416666666666667),
		[129]=v2fM(0,12,0,416666666666667),
		[130]=v2fM(0,16,0,416666666666667),
		[131]=v2fM(0,2,0,416666666666667),
		[132]=v2fM(0,24,0,416666666666667),
		[133]=v2fM(0,28,0,416666666666667),
		[134]=v2fM(0,32,0,416666666666667),
		[135]=v2fM(0,36,0,416666666666667),
		[136]=v2fM(0,4,0,416666666666667),
		[137]=v2fM(0,44,0,416666666666667),
		[138]=v2fM(0,48,0,416666666666667),
		[139]=v2fM(0,52,0,416666666666667),
		[140]=v2fM(0,56,0,416666666666667),
		[141]=v2fM(0,6,0,416666666666667),
		[142]=v2fM(0,64,0,416666666666667),
		[143]=v2fM(0,68,0,416666666666667),
		[144]=v2fM(0,72,0,416666666666667),
		[145]=v2fM(0,76,0,416666666666667),
		[146]=v2fM(0,8,0,416666666666667),
		[147]=v2fM(0,84,0,416666666666667),
		[148]=v2fM(0,88,0,416666666666667),
		[149]=v2fM(0,92,0,416666666666667),
		[150]=v2fM(0,96,0,416666666666667),
		[151]=v2fM(0,0,5),
		[152]=v2fM(0,04,0,5),
		[153]=v2fM(0,08,0,5),
		[154]=v2fM(0,12,0,5),
		[155]=v2fM(0,16,0,5),
		[156]=v2fM(0,2,0,5),
		[157]=v2fM(0,24,0,5),
		[158]=v2fM(0,28,0,5),
		[159]=v2fM(0,32,0,5),
		[160]=v2fM(0,36,0,5),
		[161]=v2fM(0,4,0,5),
		[162]=v2fM(0,44,0,5),
		[163]=v2fM(0,48,0,5),
		[164]=v2fM(0,52,0,5),
		[165]=v2fM(0,56,0,5),
		[166]=v2fM(0,6,0,5),
		[167]=v2fM(0,64,0,5),
		[168]=v2fM(0,68,0,5),
		[169]=v2fM(0,72,0,5),
		[170]=v2fM(0,76,0,5),
		[171]=v2fM(0,8,0,5),
		[172]=v2fM(0,84,0,5),
		[173]=v2fM(0,88,0,5),
		[174]=v2fM(0,92,0,5),
		[175]=v2fM(0,96,0,5),
		[176]=v2fM(0,0,583333333333333),
		[177]=v2fM(0,04,0,583333333333333),
		[178]=v2fM(0,08,0,583333333333333),
		[179]=v2fM(0,12,0,583333333333333),
		[180]=v2fM(0,16,0,583333333333333),
		[181]=v2fM(0,2,0,583333333333333),
		[182]=v2fM(0,24,0,583333333333333),
		[183]=v2fM(0,28,0,583333333333333),
		[184]=v2fM(0,32,0,583333333333333),
		[185]=v2fM(0,36,0,583333333333333),
		[186]=v2fM(0,4,0,583333333333333),
		[187]=v2fM(0,44,0,583333333333333),
		[188]=v2fM(0,48,0,583333333333333),
		[189]=v2fM(0,52,0,583333333333333),
		[190]=v2fM(0,56,0,583333333333333),
		[191]=v2fM(0,6,0,583333333333333),
		[192]=v2fM(0,64,0,583333333333333),
		[193]=v2fM(0,68,0,583333333333333),
		[194]=v2fM(0,72,0,583333333333333),
		[195]=v2fM(0,76,0,583333333333333),
		[196]=v2fM(0,8,0,583333333333333),
		[197]=v2fM(0,84,0,583333333333333),
		[198]=v2fM(0,88,0,583333333333333),
		[199]=v2fM(0,92,0,583333333333333),
		[200]=v2fM(0,96,0,583333333333333),
		[201]=v2fM(0,0,666666666666667),
		[202]=v2fM(0,04,0,666666666666667),
		[203]=v2fM(0,08,0,666666666666667),
		[204]=v2fM(0,12,0,666666666666667),
		[205]=v2fM(0,16,0,666666666666667),
		[206]=v2fM(0,2,0,666666666666667),
		[207]=v2fM(0,24,0,666666666666667),
		[208]=v2fM(0,28,0,666666666666667),
		[209]=v2fM(0,32,0,666666666666667),
		[210]=v2fM(0,36,0,666666666666667),
		[211]=v2fM(0,4,0,666666666666667),
		[212]=v2fM(0,44,0,666666666666667),
		[213]=v2fM(0,48,0,666666666666667),
		[214]=v2fM(0,52,0,666666666666667),
		[215]=v2fM(0,56,0,666666666666667),
		[216]=v2fM(0,6,0,666666666666667),
		[217]=v2fM(0,64,0,666666666666667),
		[218]=v2fM(0,68,0,666666666666667),
		[219]=v2fM(0,72,0,666666666666667),
		[220]=v2fM(0,76,0,666666666666667),
		[221]=v2fM(0,8,0,666666666666667),
		[222]=v2fM(0,84,0,666666666666667),
		[223]=v2fM(0,88,0,666666666666667),
		[224]=v2fM(0,92,0,666666666666667),
		[225]=v2fM(0,96,0,666666666666667),
		[226]=v2fM(0,0,75),
		[227]=v2fM(0,04,0,75),
		[228]=v2fM(0,08,0,75),
		[229]=v2fM(0,12,0,75),
		[230]=v2fM(0,16,0,75),
		[231]=v2fM(0,2,0,75),
		[232]=v2fM(0,24,0,75),
		[233]=v2fM(0,28,0,75),
		[234]=v2fM(0,32,0,75),
		[235]=v2fM(0,36,0,75),
		[236]=v2fM(0,4,0,75),
		[237]=v2fM(0,44,0,75),
		[238]=v2fM(0,48,0,75),
		[239]=v2fM(0,52,0,75),
		[240]=v2fM(0,56,0,75),
		[241]=v2fM(0,6,0,75),
		[242]=v2fM(0,64,0,75),
		[243]=v2fM(0,68,0,75),
		[244]=v2fM(0,72,0,75),
		[245]=v2fM(0,76,0,75),
		[246]=v2fM(0,8,0,75),
		[247]=v2fM(0,84,0,75),
		[248]=v2fM(0,88,0,75),
		[249]=v2fM(0,92,0,75),
		[250]=v2fM(0,96,0,75),
		[251]=v2fM(0,0,833333333333333),
		[252]=v2fM(0,04,0,833333333333333),
		[253]=v2fM(0,08,0,833333333333333),
		[254]=v2fM(0,12,0,833333333333333),
		[255]=v2fM(0,16,0,833333333333333),
		[256]=v2fM(0,2,0,833333333333333),
		[257]=v2fM(0,24,0,833333333333333),
		[258]=v2fM(0,28,0,833333333333333),
		[259]=v2fM(0,32,0,833333333333333),
		[260]=v2fM(0,36,0,833333333333333),
		[261]=v2fM(0,4,0,833333333333333),
		[262]=v2fM(0,44,0,833333333333333),
		[263]=v2fM(0,48,0,833333333333333),
		[264]=v2fM(0,52,0,833333333333333),
		[265]=v2fM(0,56,0,833333333333333),
		[266]=v2fM(0,6,0,833333333333333),
		[267]=v2fM(0,64,0,833333333333333),
		[268]=v2fM(0,68,0,833333333333333),
		[269]=v2fM(0,72,0,833333333333333),
		[270]=v2fM(0,76,0,833333333333333),
		[271]=v2fM(0,8,0,833333333333333),
		[272]=v2fM(0,84,0,833333333333333),
		[273]=v2fM(0,88,0,833333333333333),
		[274]=v2fM(0,92,0,833333333333333),
		[275]=v2fM(0,96,0,833333333333333),
		[276]=v2fM(0,0,916666666666667),
		[277]=v2fM(0,04,0,916666666666667),
		[278]=v2fM(0,08,0,916666666666667),
		[279]=v2fM(0,12,0,916666666666667),
		[280]=v2fM(0,16,0,916666666666667),
		[281]=v2fM(0,2,0,916666666666667),
		[282]=v2fM(0,24,0,916666666666667),
		[283]=v2fM(0,28,0,916666666666667),
		[284]=v2fM(0,32,0,916666666666667),
		[285]=v2fM(0,36,0,916666666666667),
		[286]=v2fM(0,4,0,916666666666667),
		[287]=v2fM(0,44,0,916666666666667),
		[288]=v2fM(0,48,0,916666666666667),
	},
	FRAME_SIZES={
		[1]={WIDTH=40,HEIGHT=50},
		[2]={WIDTH=40,HEIGHT=50},
		[3]={WIDTH=40,HEIGHT=50},
		[4]={WIDTH=40,HEIGHT=50},
		[5]={WIDTH=40,HEIGHT=50},
		[6]={WIDTH=40,HEIGHT=50},
		[7]={WIDTH=40,HEIGHT=50},
		[8]={WIDTH=40,HEIGHT=50},
		[9]={WIDTH=40,HEIGHT=50},
		[10]={WIDTH=40,HEIGHT=50},
		[11]={WIDTH=40,HEIGHT=50},
		[12]={WIDTH=40,HEIGHT=50},
		[13]={WIDTH=40,HEIGHT=50},
		[14]={WIDTH=40,HEIGHT=50},
		[15]={WIDTH=40,HEIGHT=50},
		[16]={WIDTH=40,HEIGHT=50},
		[17]={WIDTH=40,HEIGHT=50},
		[18]={WIDTH=40,HEIGHT=50},
		[19]={WIDTH=40,HEIGHT=50},
		[20]={WIDTH=40,HEIGHT=50},
		[21]={WIDTH=40,HEIGHT=50},
		[22]={WIDTH=40,HEIGHT=50},
		[23]={WIDTH=40,HEIGHT=50},
		[24]={WIDTH=40,HEIGHT=50},
		[25]={WIDTH=40,HEIGHT=50},
		[26]={WIDTH=40,HEIGHT=50},
		[27]={WIDTH=40,HEIGHT=50},
		[28]={WIDTH=40,HEIGHT=50},
		[29]={WIDTH=40,HEIGHT=50},
		[30]={WIDTH=40,HEIGHT=50},
		[31]={WIDTH=40,HEIGHT=50},
		[32]={WIDTH=40,HEIGHT=50},
		[33]={WIDTH=40,HEIGHT=50},
		[34]={WIDTH=40,HEIGHT=50},
		[35]={WIDTH=40,HEIGHT=50},
		[36]={WIDTH=40,HEIGHT=50},
		[37]={WIDTH=40,HEIGHT=50},
		[38]={WIDTH=40,HEIGHT=50},
		[39]={WIDTH=40,HEIGHT=50},
		[40]={WIDTH=40,HEIGHT=50},
		[41]={WIDTH=40,HEIGHT=50},
		[42]={WIDTH=40,HEIGHT=50},
		[43]={WIDTH=40,HEIGHT=50},
		[44]={WIDTH=40,HEIGHT=50},
		[45]={WIDTH=40,HEIGHT=50},
		[46]={WIDTH=40,HEIGHT=50},
		[47]={WIDTH=40,HEIGHT=50},
		[48]={WIDTH=40,HEIGHT=50},
		[49]={WIDTH=40,HEIGHT=50},
		[50]={WIDTH=40,HEIGHT=50},
		[51]={WIDTH=40,HEIGHT=50},
		[52]={WIDTH=40,HEIGHT=50},
		[53]={WIDTH=40,HEIGHT=50},
		[54]={WIDTH=40,HEIGHT=50},
		[55]={WIDTH=40,HEIGHT=50},
		[56]={WIDTH=40,HEIGHT=50},
		[57]={WIDTH=40,HEIGHT=50},
		[58]={WIDTH=40,HEIGHT=50},
		[59]={WIDTH=40,HEIGHT=50},
		[60]={WIDTH=40,HEIGHT=50},
		[61]={WIDTH=40,HEIGHT=50},
		[62]={WIDTH=40,HEIGHT=50},
		[63]={WIDTH=40,HEIGHT=50},
		[64]={WIDTH=40,HEIGHT=50},
		[65]={WIDTH=40,HEIGHT=50},
		[66]={WIDTH=40,HEIGHT=50},
		[67]={WIDTH=40,HEIGHT=50},
		[68]={WIDTH=40,HEIGHT=50},
		[69]={WIDTH=40,HEIGHT=50},
		[70]={WIDTH=40,HEIGHT=50},
		[71]={WIDTH=40,HEIGHT=50},
		[72]={WIDTH=40,HEIGHT=50},
		[73]={WIDTH=40,HEIGHT=50},
		[74]={WIDTH=40,HEIGHT=50},
		[75]={WIDTH=40,HEIGHT=50},
		[76]={WIDTH=40,HEIGHT=50},
		[77]={WIDTH=40,HEIGHT=50},
		[78]={WIDTH=40,HEIGHT=50},
		[79]={WIDTH=40,HEIGHT=50},
		[80]={WIDTH=40,HEIGHT=50},
		[81]={WIDTH=40,HEIGHT=50},
		[82]={WIDTH=40,HEIGHT=50},
		[83]={WIDTH=40,HEIGHT=50},
		[84]={WIDTH=40,HEIGHT=50},
		[85]={WIDTH=40,HEIGHT=50},
		[86]={WIDTH=40,HEIGHT=50},
		[87]={WIDTH=40,HEIGHT=50},
		[88]={WIDTH=40,HEIGHT=50},
		[89]={WIDTH=40,HEIGHT=50},
		[90]={WIDTH=40,HEIGHT=50},
		[91]={WIDTH=40,HEIGHT=50},
		[92]={WIDTH=40,HEIGHT=50},
		[93]={WIDTH=40,HEIGHT=50},
		[94]={WIDTH=40,HEIGHT=50},
		[95]={WIDTH=40,HEIGHT=50},
		[96]={WIDTH=40,HEIGHT=50},
		[97]={WIDTH=40,HEIGHT=50},
		[98]={WIDTH=40,HEIGHT=50},
		[99]={WIDTH=40,HEIGHT=50},
		[100]={WIDTH=40,HEIGHT=50},
		[101]={WIDTH=40,HEIGHT=50},
		[102]={WIDTH=40,HEIGHT=50},
		[103]={WIDTH=40,HEIGHT=50},
		[104]={WIDTH=40,HEIGHT=50},
		[105]={WIDTH=40,HEIGHT=50},
		[106]={WIDTH=40,HEIGHT=50},
		[107]={WIDTH=40,HEIGHT=50},
		[108]={WIDTH=40,HEIGHT=50},
		[109]={WIDTH=40,HEIGHT=50},
		[110]={WIDTH=40,HEIGHT=50},
		[111]={WIDTH=40,HEIGHT=50},
		[112]={WIDTH=40,HEIGHT=50},
		[113]={WIDTH=40,HEIGHT=50},
		[114]={WIDTH=40,HEIGHT=50},
		[115]={WIDTH=40,HEIGHT=50},
		[116]={WIDTH=40,HEIGHT=50},
		[117]={WIDTH=40,HEIGHT=50},
		[118]={WIDTH=40,HEIGHT=50},
		[119]={WIDTH=40,HEIGHT=50},
		[120]={WIDTH=40,HEIGHT=50},
		[121]={WIDTH=40,HEIGHT=50},
		[122]={WIDTH=40,HEIGHT=50},
		[123]={WIDTH=40,HEIGHT=50},
		[124]={WIDTH=40,HEIGHT=50},
		[125]={WIDTH=40,HEIGHT=50},
		[126]={WIDTH=40,HEIGHT=50},
		[127]={WIDTH=40,HEIGHT=50},
		[128]={WIDTH=40,HEIGHT=50},
		[129]={WIDTH=40,HEIGHT=50},
		[130]={WIDTH=40,HEIGHT=50},
		[131]={WIDTH=40,HEIGHT=50},
		[132]={WIDTH=40,HEIGHT=50},
		[133]={WIDTH=40,HEIGHT=50},
		[134]={WIDTH=40,HEIGHT=50},
		[135]={WIDTH=40,HEIGHT=50},
		[136]={WIDTH=40,HEIGHT=50},
		[137]={WIDTH=40,HEIGHT=50},
		[138]={WIDTH=40,HEIGHT=50},
		[139]={WIDTH=40,HEIGHT=50},
		[140]={WIDTH=40,HEIGHT=50},
		[141]={WIDTH=40,HEIGHT=50},
		[142]={WIDTH=40,HEIGHT=50},
		[143]={WIDTH=40,HEIGHT=50},
		[144]={WIDTH=40,HEIGHT=50},
		[145]={WIDTH=40,HEIGHT=50},
		[146]={WIDTH=40,HEIGHT=50},
		[147]={WIDTH=40,HEIGHT=50},
		[148]={WIDTH=40,HEIGHT=50},
		[149]={WIDTH=40,HEIGHT=50},
		[150]={WIDTH=40,HEIGHT=50},
		[151]={WIDTH=40,HEIGHT=50},
		[152]={WIDTH=40,HEIGHT=50},
		[153]={WIDTH=40,HEIGHT=50},
		[154]={WIDTH=40,HEIGHT=50},
		[155]={WIDTH=40,HEIGHT=50},
		[156]={WIDTH=40,HEIGHT=50},
		[157]={WIDTH=40,HEIGHT=50},
		[158]={WIDTH=40,HEIGHT=50},
		[159]={WIDTH=40,HEIGHT=50},
		[160]={WIDTH=40,HEIGHT=50},
		[161]={WIDTH=40,HEIGHT=50},
		[162]={WIDTH=40,HEIGHT=50},
		[163]={WIDTH=40,HEIGHT=50},
		[164]={WIDTH=40,HEIGHT=50},
		[165]={WIDTH=40,HEIGHT=50},
		[166]={WIDTH=40,HEIGHT=50},
		[167]={WIDTH=40,HEIGHT=50},
		[168]={WIDTH=40,HEIGHT=50},
		[169]={WIDTH=40,HEIGHT=50},
		[170]={WIDTH=40,HEIGHT=50},
		[171]={WIDTH=40,HEIGHT=50},
		[172]={WIDTH=40,HEIGHT=50},
		[173]={WIDTH=40,HEIGHT=50},
		[174]={WIDTH=40,HEIGHT=50},
		[175]={WIDTH=40,HEIGHT=50},
		[176]={WIDTH=40,HEIGHT=50},
		[177]={WIDTH=40,HEIGHT=50},
		[178]={WIDTH=40,HEIGHT=50},
		[179]={WIDTH=40,HEIGHT=50},
		[180]={WIDTH=40,HEIGHT=50},
		[181]={WIDTH=40,HEIGHT=50},
		[182]={WIDTH=40,HEIGHT=50},
		[183]={WIDTH=40,HEIGHT=50},
		[184]={WIDTH=40,HEIGHT=50},
		[185]={WIDTH=40,HEIGHT=50},
		[186]={WIDTH=40,HEIGHT=50},
		[187]={WIDTH=40,HEIGHT=50},
		[188]={WIDTH=40,HEIGHT=50},
		[189]={WIDTH=40,HEIGHT=50},
		[190]={WIDTH=40,HEIGHT=50},
		[191]={WIDTH=40,HEIGHT=50},
		[192]={WIDTH=40,HEIGHT=50},
		[193]={WIDTH=40,HEIGHT=50},
		[194]={WIDTH=40,HEIGHT=50},
		[195]={WIDTH=40,HEIGHT=50},
		[196]={WIDTH=40,HEIGHT=50},
		[197]={WIDTH=40,HEIGHT=50},
		[198]={WIDTH=40,HEIGHT=50},
		[199]={WIDTH=40,HEIGHT=50},
		[200]={WIDTH=40,HEIGHT=50},
		[201]={WIDTH=40,HEIGHT=50},
		[202]={WIDTH=40,HEIGHT=50},
		[203]={WIDTH=40,HEIGHT=50},
		[204]={WIDTH=40,HEIGHT=50},
		[205]={WIDTH=40,HEIGHT=50},
		[206]={WIDTH=40,HEIGHT=50},
		[207]={WIDTH=40,HEIGHT=50},
		[208]={WIDTH=40,HEIGHT=50},
		[209]={WIDTH=40,HEIGHT=50},
		[210]={WIDTH=40,HEIGHT=50},
		[211]={WIDTH=40,HEIGHT=50},
		[212]={WIDTH=40,HEIGHT=50},
		[213]={WIDTH=40,HEIGHT=50},
		[214]={WIDTH=40,HEIGHT=50},
		[215]={WIDTH=40,HEIGHT=50},
		[216]={WIDTH=40,HEIGHT=50},
		[217]={WIDTH=40,HEIGHT=50},
		[218]={WIDTH=40,HEIGHT=50},
		[219]={WIDTH=40,HEIGHT=50},
		[220]={WIDTH=40,HEIGHT=50},
		[221]={WIDTH=40,HEIGHT=50},
		[222]={WIDTH=40,HEIGHT=50},
		[223]={WIDTH=40,HEIGHT=50},
		[224]={WIDTH=40,HEIGHT=50},
		[225]={WIDTH=40,HEIGHT=50},
		[226]={WIDTH=40,HEIGHT=50},
		[227]={WIDTH=40,HEIGHT=50},
		[228]={WIDTH=40,HEIGHT=50},
		[229]={WIDTH=40,HEIGHT=50},
		[230]={WIDTH=40,HEIGHT=50},
		[231]={WIDTH=40,HEIGHT=50},
		[232]={WIDTH=40,HEIGHT=50},
		[233]={WIDTH=40,HEIGHT=50},
		[234]={WIDTH=40,HEIGHT=50},
		[235]={WIDTH=40,HEIGHT=50},
		[236]={WIDTH=40,HEIGHT=50},
		[237]={WIDTH=40,HEIGHT=50},
		[238]={WIDTH=40,HEIGHT=50},
		[239]={WIDTH=40,HEIGHT=50},
		[240]={WIDTH=40,HEIGHT=50},
		[241]={WIDTH=40,HEIGHT=50},
		[242]={WIDTH=40,HEIGHT=50},
		[243]={WIDTH=40,HEIGHT=50},
		[244]={WIDTH=40,HEIGHT=50},
		[245]={WIDTH=40,HEIGHT=50},
		[246]={WIDTH=40,HEIGHT=50},
		[247]={WIDTH=40,HEIGHT=50},
		[248]={WIDTH=40,HEIGHT=50},
		[249]={WIDTH=40,HEIGHT=50},
		[250]={WIDTH=40,HEIGHT=50},
		[251]={WIDTH=40,HEIGHT=50},
		[252]={WIDTH=40,HEIGHT=50},
		[253]={WIDTH=40,HEIGHT=50},
		[254]={WIDTH=40,HEIGHT=50},
		[255]={WIDTH=40,HEIGHT=50},
		[256]={WIDTH=40,HEIGHT=50},
		[257]={WIDTH=40,HEIGHT=50},
		[258]={WIDTH=40,HEIGHT=50},
		[259]={WIDTH=40,HEIGHT=50},
		[260]={WIDTH=40,HEIGHT=50},
		[261]={WIDTH=40,HEIGHT=50},
		[262]={WIDTH=40,HEIGHT=50},
		[263]={WIDTH=40,HEIGHT=50},
		[264]={WIDTH=40,HEIGHT=50},
		[265]={WIDTH=40,HEIGHT=50},
		[266]={WIDTH=40,HEIGHT=50},
		[267]={WIDTH=40,HEIGHT=50},
		[268]={WIDTH=40,HEIGHT=50},
		[269]={WIDTH=40,HEIGHT=50},
		[270]={WIDTH=40,HEIGHT=50},
		[271]={WIDTH=40,HEIGHT=50},
		[272]={WIDTH=40,HEIGHT=50},
		[273]={WIDTH=40,HEIGHT=50},
		[274]={WIDTH=40,HEIGHT=50},
		[275]={WIDTH=40,HEIGHT=50},
		[276]={WIDTH=40,HEIGHT=50},
		[277]={WIDTH=40,HEIGHT=50},
		[278]={WIDTH=40,HEIGHT=50},
		[279]={WIDTH=40,HEIGHT=50},
		[280]={WIDTH=40,HEIGHT=50},
		[281]={WIDTH=40,HEIGHT=50},
		[282]={WIDTH=40,HEIGHT=50},
		[283]={WIDTH=40,HEIGHT=50},
		[284]={WIDTH=40,HEIGHT=50},
		[285]={WIDTH=40,HEIGHT=50},
		[286]={WIDTH=40,HEIGHT=50},
		[287]={WIDTH=40,HEIGHT=50},
		[288]={WIDTH=40,HEIGHT=50},
	},
	FRAME_INDEXS={
		['w-a-mmb0191z']=1,
		['w-a-mmb0192z']=2,
		['w-a-mmb0189z']=3,
		['w-a-mmb0190z']=4,
		['w-a-mmb0193z']=5,
		['w-a-mmb0196z']=6,
		['w-a-mmb0197z']=7,
		['w-a-mmb0194z']=8,
		['w-a-mmb0195z']=9,
		['w-a-mmb0182z']=10,
		['w-a-mmb0183z']=11,
		['w-a-mmb0180z']=12,
		['w-a-mmb0181z']=13,
		['w-a-mmb0184z']=14,
		['w-a-mmb0187z']=15,
		['w-a-mmb0188z']=16,
		['w-a-mmb0185z']=17,
		['w-a-mmb0186z']=18,
		['w-a-mmb0209z']=19,
		['w-a-mmb0210z']=20,
		['w-a-mmb0207z']=21,
		['w-a-mmb0208z']=22,
		['w-a-mmb0211z']=23,
		['w-a-mmb0214z']=24,
		['w-a-mmb0215z']=25,
		['w-a-mmb0212z']=26,
		['w-a-mmb0213z']=27,
		['w-a-mmb0200z']=28,
		['w-a-mmb0201z']=29,
		['w-a-mmb0198z']=30,
		['w-a-mmb0199z']=31,
		['w-a-mmb0202z']=32,
		['w-a-mmb0205z']=33,
		['w-a-mmb0206z']=34,
		['w-a-mmb0203z']=35,
		['w-a-mmb0204z']=36,
		['w-a-mmb0155z']=37,
		['w-a-mmb0156z']=38,
		['w-a-mmb0153z']=39,
		['w-a-mmb0154z']=40,
		['w-a-mmb0157z']=41,
		['w-a-mmb0160z']=42,
		['w-a-mmb0161z']=43,
		['w-a-mmb0158z']=44,
		['w-a-mmb0159z']=45,
		['w-a-mmb0146z']=46,
		['w-a-mmb0147z']=47,
		['w-a-mmb0144z']=48,
		['w-a-mmb0145z']=49,
		['w-a-mmb0148z']=50,
		['w-a-mmb0151z']=51,
		['w-a-mmb0152z']=52,
		['w-a-mmb0149z']=53,
		['w-a-mmb0150z']=54,
		['w-a-mmb0173z']=55,
		['w-a-mmb0174z']=56,
		['w-a-mmb0171z']=57,
		['w-a-mmb0172z']=58,
		['w-a-mmb0175z']=59,
		['w-a-mmb0178z']=60,
		['w-a-mmb0179z']=61,
		['w-a-mmb0176z']=62,
		['w-a-mmb0177z']=63,
		['w-a-mmb0164z']=64,
		['w-a-mmb0165z']=65,
		['w-a-mmb0162z']=66,
		['w-a-mmb0163z']=67,
		['w-a-mmb0166z']=68,
		['w-a-mmb0169z']=69,
		['w-a-mmb0170z']=70,
		['w-a-mmb0167z']=71,
		['w-a-mmb0168z']=72,
		['w-a-mmb0263z']=73,
		['w-a-mmb0264z']=74,
		['w-a-mmb0261z']=75,
		['w-a-mmb0262z']=76,
		['w-a-mmb0265z']=77,
		['w-a-mmb0268z']=78,
		['w-a-mmb0269z']=79,
		['w-a-mmb0266z']=80,
		['w-a-mmb0267z']=81,
		['w-a-mmb0254z']=82,
		['w-a-mmb0255z']=83,
		['w-a-mmb0252z']=84,
		['w-a-mmb0253z']=85,
		['w-a-mmb0256z']=86,
		['w-a-mmb0259z']=87,
		['w-a-mmb0260z']=88,
		['w-a-mmb0257z']=89,
		['w-a-mmb0258z']=90,
		['w-a-mmb0281z']=91,
		['w-a-mmb0282z']=92,
		['w-a-mmb0279z']=93,
		['w-a-mmb0280z']=94,
		['w-a-mmb0283z']=95,
		['w-a-mmb0286z']=96,
		['w-a-mmb0287z']=97,
		['w-a-mmb0284z']=98,
		['w-a-mmb0285z']=99,
		['w-a-mmb0272z']=100,
		['w-a-mmb0273z']=101,
		['w-a-mmb0270z']=102,
		['w-a-mmb0271z']=103,
		['w-a-mmb0274z']=104,
		['w-a-mmb0277z']=105,
		['w-a-mmb0278z']=106,
		['w-a-mmb0275z']=107,
		['w-a-mmb0276z']=108,
		['w-a-mmb0227z']=109,
		['w-a-mmb0228z']=110,
		['w-a-mmb0225z']=111,
		['w-a-mmb0226z']=112,
		['w-a-mmb0229z']=113,
		['w-a-mmb0232z']=114,
		['w-a-mmb0233z']=115,
		['w-a-mmb0230z']=116,
		['w-a-mmb0231z']=117,
		['w-a-mmb0218z']=118,
		['w-a-mmb0219z']=119,
		['w-a-mmb0216z']=120,
		['w-a-mmb0217z']=121,
		['w-a-mmb0220z']=122,
		['w-a-mmb0223z']=123,
		['w-a-mmb0224z']=124,
		['w-a-mmb0221z']=125,
		['w-a-mmb0222z']=126,
		['w-a-mmb0245z']=127,
		['w-a-mmb0246z']=128,
		['w-a-mmb0243z']=129,
		['w-a-mmb0244z']=130,
		['w-a-mmb0247z']=131,
		['w-a-mmb0250z']=132,
		['w-a-mmb0251z']=133,
		['w-a-mmb0248z']=134,
		['w-a-mmb0249z']=135,
		['w-a-mmb0236z']=136,
		['w-a-mmb0237z']=137,
		['w-a-mmb0234z']=138,
		['w-a-mmb0235z']=139,
		['w-a-mmb0238z']=140,
		['w-a-mmb0241z']=141,
		['w-a-mmb0242z']=142,
		['w-a-mmb0239z']=143,
		['w-a-mmb0240z']=144,
		['w-a-mmb0047z']=145,
		['w-a-mmb0048z']=146,
		['w-a-mmb0045z']=147,
		['w-a-mmb0046z']=148,
		['w-a-mmb0049z']=149,
		['w-a-mmb0052z']=150,
		['w-a-mmb0053z']=151,
		['w-a-mmb0050z']=152,
		['w-a-mmb0051z']=153,
		['w-a-mmb0038z']=154,
		['w-a-mmb0039z']=155,
		['w-a-mmb0036z']=156,
		['w-a-mmb0037z']=157,
		['w-a-mmb0040z']=158,
		['w-a-mmb0043z']=159,
		['w-a-mmb0044z']=160,
		['w-a-mmb0041z']=161,
		['w-a-mmb0042z']=162,
		['w-a-mmb0065z']=163,
		['w-a-mmb0066z']=164,
		['w-a-mmb0063z']=165,
		['w-a-mmb0064z']=166,
		['w-a-mmb0067z']=167,
		['w-a-mmb0070z']=168,
		['w-a-mmb0071z']=169,
		['w-a-mmb0068z']=170,
		['w-a-mmb0069z']=171,
		['w-a-mmb0056z']=172,
		['w-a-mmb0057z']=173,
		['w-a-mmb0054z']=174,
		['w-a-mmb0055z']=175,
		['w-a-mmb0058z']=176,
		['w-a-mmb0061z']=177,
		['w-a-mmb0062z']=178,
		['w-a-mmb0059z']=179,
		['w-a-mmb0060z']=180,
		['w-a-mmb0011z']=181,
		['w-a-mmb0012z']=182,
		['w-a-mmb0009z']=183,
		['w-a-mmb0010z']=184,
		['w-a-mmb0013z']=185,
		['w-a-mmb0016z']=186,
		['w-a-mmb0017z']=187,
		['w-a-mmb0014z']=188,
		['w-a-mmb0015z']=189,
		['w-a-mmb0002z']=190,
		['w-a-mmb0003z']=191,
		['w-a-mmb0000z']=192,
		['w-a-mmb0001z']=193,
		['w-a-mmb0004z']=194,
		['w-a-mmb0007z']=195,
		['w-a-mmb0008z']=196,
		['w-a-mmb0005z']=197,
		['w-a-mmb0006z']=198,
		['w-a-mmb0029z']=199,
		['w-a-mmb0030z']=200,
		['w-a-mmb0027z']=201,
		['w-a-mmb0028z']=202,
		['w-a-mmb0031z']=203,
		['w-a-mmb0034z']=204,
		['w-a-mmb0035z']=205,
		['w-a-mmb0032z']=206,
		['w-a-mmb0033z']=207,
		['w-a-mmb0020z']=208,
		['w-a-mmb0021z']=209,
		['w-a-mmb0018z']=210,
		['w-a-mmb0019z']=211,
		['w-a-mmb0022z']=212,
		['w-a-mmb0025z']=213,
		['w-a-mmb0026z']=214,
		['w-a-mmb0023z']=215,
		['w-a-mmb0024z']=216,
		['w-a-mmb0119z']=217,
		['w-a-mmb0120z']=218,
		['w-a-mmb0117z']=219,
		['w-a-mmb0118z']=220,
		['w-a-mmb0121z']=221,
		['w-a-mmb0124z']=222,
		['w-a-mmb0125z']=223,
		['w-a-mmb0122z']=224,
		['w-a-mmb0123z']=225,
		['w-a-mmb0110z']=226,
		['w-a-mmb0111z']=227,
		['w-a-mmb0108z']=228,
		['w-a-mmb0109z']=229,
		['w-a-mmb0112z']=230,
		['w-a-mmb0115z']=231,
		['w-a-mmb0116z']=232,
		['w-a-mmb0113z']=233,
		['w-a-mmb0114z']=234,
		['w-a-mmb0137z']=235,
		['w-a-mmb0138z']=236,
		['w-a-mmb0135z']=237,
		['w-a-mmb0136z']=238,
		['w-a-mmb0139z']=239,
		['w-a-mmb0142z']=240,
		['w-a-mmb0143z']=241,
		['w-a-mmb0140z']=242,
		['w-a-mmb0141z']=243,
		['w-a-mmb0128z']=244,
		['w-a-mmb0129z']=245,
		['w-a-mmb0126z']=246,
		['w-a-mmb0127z']=247,
		['w-a-mmb0130z']=248,
		['w-a-mmb0133z']=249,
		['w-a-mmb0134z']=250,
		['w-a-mmb0131z']=251,
		['w-a-mmb0132z']=252,
		['w-a-mmb0083z']=253,
		['w-a-mmb0084z']=254,
		['w-a-mmb0081z']=255,
		['w-a-mmb0082z']=256,
		['w-a-mmb0085z']=257,
		['w-a-mmb0088z']=258,
		['w-a-mmb0089z']=259,
		['w-a-mmb0086z']=260,
		['w-a-mmb0087z']=261,
		['w-a-mmb0074z']=262,
		['w-a-mmb0075z']=263,
		['w-a-mmb0072z']=264,
		['w-a-mmb0073z']=265,
		['w-a-mmb0076z']=266,
		['w-a-mmb0079z']=267,
		['w-a-mmb0080z']=268,
		['w-a-mmb0077z']=269,
		['w-a-mmb0078z']=270,
		['w-a-mmb0101z']=271,
		['w-a-mmb0102z']=272,
		['w-a-mmb0099z']=273,
		['w-a-mmb0100z']=274,
		['w-a-mmb0103z']=275,
		['w-a-mmb0106z']=276,
		['w-a-mmb0107z']=277,
		['w-a-mmb0104z']=278,
		['w-a-mmb0105z']=279,
		['w-a-mmb0092z']=280,
		['w-a-mmb0093z']=281,
		['w-a-mmb0090z']=282,
		['w-a-mmb0091z']=283,
		['w-a-mmb0094z']=284,
		['w-a-mmb0097z']=285,
		['w-a-mmb0098z']=286,
		['w-a-mmb0095z']=287,
		['w-a-mmb0096z']=288,
	},
};
