--GENERATED ON: 05/12/2024 07:00:48


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['w-r-behemoth-gun'] = {
	TEX_WIDTH=986,
	TEX_HEIGHT=234,
	FRAME_COUNT=36,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0588235294117647,0),
		[3]=v2fM(0,117647058823529,0),
		[4]=v2fM(0,176470588235294,0),
		[5]=v2fM(0,235294117647059,0),
		[6]=v2fM(0,294117647058824,0),
		[7]=v2fM(0,352941176470588,0),
		[8]=v2fM(0,411764705882353,0),
		[9]=v2fM(0,470588235294118,0),
		[10]=v2fM(0,529411764705882,0),
		[11]=v2fM(0,588235294117647,0),
		[12]=v2fM(0,647058823529412,0),
		[13]=v2fM(0,705882352941176,0),
		[14]=v2fM(0,764705882352941,0),
		[15]=v2fM(0,823529411764706,0),
		[16]=v2fM(0,882352941176471,0),
		[17]=v2fM(0,941176470588235,0),
		[18]=v2fM(0,0,333333333333333),
		[19]=v2fM(0,0588235294117647,0,333333333333333),
		[20]=v2fM(0,117647058823529,0,333333333333333),
		[21]=v2fM(0,176470588235294,0,333333333333333),
		[22]=v2fM(0,235294117647059,0,333333333333333),
		[23]=v2fM(0,294117647058824,0,333333333333333),
		[24]=v2fM(0,352941176470588,0,333333333333333),
		[25]=v2fM(0,411764705882353,0,333333333333333),
		[26]=v2fM(0,470588235294118,0,333333333333333),
		[27]=v2fM(0,529411764705882,0,333333333333333),
		[28]=v2fM(0,588235294117647,0,333333333333333),
		[29]=v2fM(0,647058823529412,0,333333333333333),
		[30]=v2fM(0,705882352941176,0,333333333333333),
		[31]=v2fM(0,764705882352941,0,333333333333333),
		[32]=v2fM(0,823529411764706,0,333333333333333),
		[33]=v2fM(0,882352941176471,0,333333333333333),
		[34]=v2fM(0,941176470588235,0,333333333333333),
		[35]=v2fM(0,0,666666666666667),
		[36]=v2fM(0,0588235294117647,0,666666666666667),
	},
	FRAME_SIZES={
		[1]={WIDTH=58,HEIGHT=78},
		[2]={WIDTH=58,HEIGHT=78},
		[3]={WIDTH=58,HEIGHT=78},
		[4]={WIDTH=58,HEIGHT=78},
		[5]={WIDTH=58,HEIGHT=78},
		[6]={WIDTH=58,HEIGHT=78},
		[7]={WIDTH=58,HEIGHT=78},
		[8]={WIDTH=58,HEIGHT=78},
		[9]={WIDTH=58,HEIGHT=78},
		[10]={WIDTH=58,HEIGHT=78},
		[11]={WIDTH=58,HEIGHT=78},
		[12]={WIDTH=58,HEIGHT=78},
		[13]={WIDTH=58,HEIGHT=78},
		[14]={WIDTH=58,HEIGHT=78},
		[15]={WIDTH=58,HEIGHT=78},
		[16]={WIDTH=58,HEIGHT=78},
		[17]={WIDTH=58,HEIGHT=78},
		[18]={WIDTH=58,HEIGHT=78},
		[19]={WIDTH=58,HEIGHT=78},
		[20]={WIDTH=58,HEIGHT=78},
		[21]={WIDTH=58,HEIGHT=78},
		[22]={WIDTH=58,HEIGHT=78},
		[23]={WIDTH=58,HEIGHT=78},
		[24]={WIDTH=58,HEIGHT=78},
		[25]={WIDTH=58,HEIGHT=78},
		[26]={WIDTH=58,HEIGHT=78},
		[27]={WIDTH=58,HEIGHT=78},
		[28]={WIDTH=58,HEIGHT=78},
		[29]={WIDTH=58,HEIGHT=78},
		[30]={WIDTH=58,HEIGHT=78},
		[31]={WIDTH=58,HEIGHT=78},
		[32]={WIDTH=58,HEIGHT=78},
		[33]={WIDTH=58,HEIGHT=78},
		[34]={WIDTH=58,HEIGHT=78},
		[35]={WIDTH=58,HEIGHT=78},
		[36]={WIDTH=58,HEIGHT=78},
	},
	FRAME_INDEXS={
		['w-r-behemoth-gun0024']=1,
		['w-r-behemoth-gun0023']=2,
		['w-r-behemoth-gun0026']=3,
		['w-r-behemoth-gun0025']=4,
		['w-r-behemoth-gun0022']=5,
		['w-r-behemoth-gun0019']=6,
		['w-r-behemoth-gun0018']=7,
		['w-r-behemoth-gun0021']=8,
		['w-r-behemoth-gun0020']=9,
		['w-r-behemoth-gun0033']=10,
		['w-r-behemoth-gun0032']=11,
		['w-r-behemoth-gun0035']=12,
		['w-r-behemoth-gun0034']=13,
		['w-r-behemoth-gun0031']=14,
		['w-r-behemoth-gun0028']=15,
		['w-r-behemoth-gun0027']=16,
		['w-r-behemoth-gun0030']=17,
		['w-r-behemoth-gun0029']=18,
		['w-r-behemoth-gun0006']=19,
		['w-r-behemoth-gun0005']=20,
		['w-r-behemoth-gun0008']=21,
		['w-r-behemoth-gun0007']=22,
		['w-r-behemoth-gun0004']=23,
		['w-r-behemoth-gun0001']=24,
		['w-r-behemoth-gun0000']=25,
		['w-r-behemoth-gun0003']=26,
		['w-r-behemoth-gun0002']=27,
		['w-r-behemoth-gun0015']=28,
		['w-r-behemoth-gun0014']=29,
		['w-r-behemoth-gun0017']=30,
		['w-r-behemoth-gun0016']=31,
		['w-r-behemoth-gun0013']=32,
		['w-r-behemoth-gun0010']=33,
		['w-r-behemoth-gun0009']=34,
		['w-r-behemoth-gun0012']=35,
		['w-r-behemoth-gun0011']=36,
	},
};
