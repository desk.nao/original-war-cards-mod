--GENERATED ON: 05/12/2024 07:00:57


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['w-r-rlaunch'] = {
	TEX_WIDTH=2010,
	TEX_HEIGHT=1014,
	FRAME_COUNT=288,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0288557213930348,0),
		[3]=v2fM(0,0577114427860697,0),
		[4]=v2fM(0,0865671641791045,0),
		[5]=v2fM(0,115422885572139,0),
		[6]=v2fM(0,144278606965174,0),
		[7]=v2fM(0,173134328358209,0),
		[8]=v2fM(0,201990049751244,0),
		[9]=v2fM(0,230845771144279,0),
		[10]=v2fM(0,259701492537313,0),
		[11]=v2fM(0,288557213930348,0),
		[12]=v2fM(0,317412935323383,0),
		[13]=v2fM(0,346268656716418,0),
		[14]=v2fM(0,375124378109453,0),
		[15]=v2fM(0,403980099502488,0),
		[16]=v2fM(0,432835820895522,0),
		[17]=v2fM(0,461691542288557,0),
		[18]=v2fM(0,0,0769230769230769),
		[19]=v2fM(0,0288557213930348,0,0769230769230769),
		[20]=v2fM(0,0577114427860697,0,0769230769230769),
		[21]=v2fM(0,0865671641791045,0,0769230769230769),
		[22]=v2fM(0,115422885572139,0,0769230769230769),
		[23]=v2fM(0,144278606965174,0,0769230769230769),
		[24]=v2fM(0,173134328358209,0,0769230769230769),
		[25]=v2fM(0,201990049751244,0,0769230769230769),
		[26]=v2fM(0,230845771144279,0,0769230769230769),
		[27]=v2fM(0,259701492537313,0,0769230769230769),
		[28]=v2fM(0,288557213930348,0,0769230769230769),
		[29]=v2fM(0,317412935323383,0,0769230769230769),
		[30]=v2fM(0,346268656716418,0,0769230769230769),
		[31]=v2fM(0,375124378109453,0,0769230769230769),
		[32]=v2fM(0,403980099502488,0,0769230769230769),
		[33]=v2fM(0,432835820895522,0,0769230769230769),
		[34]=v2fM(0,461691542288557,0,0769230769230769),
		[35]=v2fM(0,0,153846153846154),
		[36]=v2fM(0,0288557213930348,0,153846153846154),
		[37]=v2fM(0,0577114427860697,0,153846153846154),
		[38]=v2fM(0,0865671641791045,0,153846153846154),
		[39]=v2fM(0,115422885572139,0,153846153846154),
		[40]=v2fM(0,144278606965174,0,153846153846154),
		[41]=v2fM(0,173134328358209,0,153846153846154),
		[42]=v2fM(0,201990049751244,0,153846153846154),
		[43]=v2fM(0,230845771144279,0,153846153846154),
		[44]=v2fM(0,259701492537313,0,153846153846154),
		[45]=v2fM(0,288557213930348,0,153846153846154),
		[46]=v2fM(0,317412935323383,0,153846153846154),
		[47]=v2fM(0,346268656716418,0,153846153846154),
		[48]=v2fM(0,375124378109453,0,153846153846154),
		[49]=v2fM(0,403980099502488,0,153846153846154),
		[50]=v2fM(0,432835820895522,0,153846153846154),
		[51]=v2fM(0,461691542288557,0,153846153846154),
		[52]=v2fM(0,0,230769230769231),
		[53]=v2fM(0,0288557213930348,0,230769230769231),
		[54]=v2fM(0,0577114427860697,0,230769230769231),
		[55]=v2fM(0,0865671641791045,0,230769230769231),
		[56]=v2fM(0,115422885572139,0,230769230769231),
		[57]=v2fM(0,144278606965174,0,230769230769231),
		[58]=v2fM(0,173134328358209,0,230769230769231),
		[59]=v2fM(0,201990049751244,0,230769230769231),
		[60]=v2fM(0,230845771144279,0,230769230769231),
		[61]=v2fM(0,259701492537313,0,230769230769231),
		[62]=v2fM(0,288557213930348,0,230769230769231),
		[63]=v2fM(0,317412935323383,0,230769230769231),
		[64]=v2fM(0,346268656716418,0,230769230769231),
		[65]=v2fM(0,375124378109453,0,230769230769231),
		[66]=v2fM(0,403980099502488,0,230769230769231),
		[67]=v2fM(0,432835820895522,0,230769230769231),
		[68]=v2fM(0,461691542288557,0,230769230769231),
		[69]=v2fM(0,0,307692307692308),
		[70]=v2fM(0,0288557213930348,0,307692307692308),
		[71]=v2fM(0,0577114427860697,0,307692307692308),
		[72]=v2fM(0,0865671641791045,0,307692307692308),
		[73]=v2fM(0,115422885572139,0,307692307692308),
		[74]=v2fM(0,144278606965174,0,307692307692308),
		[75]=v2fM(0,173134328358209,0,307692307692308),
		[76]=v2fM(0,201990049751244,0,307692307692308),
		[77]=v2fM(0,230845771144279,0,307692307692308),
		[78]=v2fM(0,259701492537313,0,307692307692308),
		[79]=v2fM(0,288557213930348,0,307692307692308),
		[80]=v2fM(0,317412935323383,0,307692307692308),
		[81]=v2fM(0,346268656716418,0,307692307692308),
		[82]=v2fM(0,375124378109453,0,307692307692308),
		[83]=v2fM(0,403980099502488,0,307692307692308),
		[84]=v2fM(0,432835820895522,0,307692307692308),
		[85]=v2fM(0,461691542288557,0,307692307692308),
		[86]=v2fM(0,0,384615384615385),
		[87]=v2fM(0,0288557213930348,0,384615384615385),
		[88]=v2fM(0,0577114427860697,0,384615384615385),
		[89]=v2fM(0,0865671641791045,0,384615384615385),
		[90]=v2fM(0,115422885572139,0,384615384615385),
		[91]=v2fM(0,144278606965174,0,384615384615385),
		[92]=v2fM(0,173134328358209,0,384615384615385),
		[93]=v2fM(0,201990049751244,0,384615384615385),
		[94]=v2fM(0,230845771144279,0,384615384615385),
		[95]=v2fM(0,259701492537313,0,384615384615385),
		[96]=v2fM(0,288557213930348,0,384615384615385),
		[97]=v2fM(0,317412935323383,0,384615384615385),
		[98]=v2fM(0,346268656716418,0,384615384615385),
		[99]=v2fM(0,375124378109453,0,384615384615385),
		[100]=v2fM(0,403980099502488,0,384615384615385),
		[101]=v2fM(0,432835820895522,0,384615384615385),
		[102]=v2fM(0,461691542288557,0,384615384615385),
		[103]=v2fM(0,0,461538461538462),
		[104]=v2fM(0,0288557213930348,0,461538461538462),
		[105]=v2fM(0,0577114427860697,0,461538461538462),
		[106]=v2fM(0,0865671641791045,0,461538461538462),
		[107]=v2fM(0,115422885572139,0,461538461538462),
		[108]=v2fM(0,144278606965174,0,461538461538462),
		[109]=v2fM(0,173134328358209,0,461538461538462),
		[110]=v2fM(0,201990049751244,0,461538461538462),
		[111]=v2fM(0,230845771144279,0,461538461538462),
		[112]=v2fM(0,259701492537313,0,461538461538462),
		[113]=v2fM(0,288557213930348,0,461538461538462),
		[114]=v2fM(0,317412935323383,0,461538461538462),
		[115]=v2fM(0,346268656716418,0,461538461538462),
		[116]=v2fM(0,375124378109453,0,461538461538462),
		[117]=v2fM(0,403980099502488,0,461538461538462),
		[118]=v2fM(0,432835820895522,0,461538461538462),
		[119]=v2fM(0,461691542288557,0,461538461538462),
		[120]=v2fM(0,0,538461538461538),
		[121]=v2fM(0,0288557213930348,0,538461538461538),
		[122]=v2fM(0,0577114427860697,0,538461538461538),
		[123]=v2fM(0,0865671641791045,0,538461538461538),
		[124]=v2fM(0,115422885572139,0,538461538461538),
		[125]=v2fM(0,144278606965174,0,538461538461538),
		[126]=v2fM(0,173134328358209,0,538461538461538),
		[127]=v2fM(0,201990049751244,0,538461538461538),
		[128]=v2fM(0,230845771144279,0,538461538461538),
		[129]=v2fM(0,259701492537313,0,538461538461538),
		[130]=v2fM(0,288557213930348,0,538461538461538),
		[131]=v2fM(0,317412935323383,0,538461538461538),
		[132]=v2fM(0,346268656716418,0,538461538461538),
		[133]=v2fM(0,375124378109453,0,538461538461538),
		[134]=v2fM(0,403980099502488,0,538461538461538),
		[135]=v2fM(0,432835820895522,0,538461538461538),
		[136]=v2fM(0,461691542288557,0,538461538461538),
		[137]=v2fM(0,0,615384615384615),
		[138]=v2fM(0,0288557213930348,0,615384615384615),
		[139]=v2fM(0,0577114427860697,0,615384615384615),
		[140]=v2fM(0,0865671641791045,0,615384615384615),
		[141]=v2fM(0,115422885572139,0,615384615384615),
		[142]=v2fM(0,144278606965174,0,615384615384615),
		[143]=v2fM(0,173134328358209,0,615384615384615),
		[144]=v2fM(0,201990049751244,0,615384615384615),
		[145]=v2fM(0,230845771144279,0,615384615384615),
		[146]=v2fM(0,259701492537313,0,615384615384615),
		[147]=v2fM(0,288557213930348,0,615384615384615),
		[148]=v2fM(0,317412935323383,0,615384615384615),
		[149]=v2fM(0,346268656716418,0,615384615384615),
		[150]=v2fM(0,375124378109453,0,615384615384615),
		[151]=v2fM(0,403980099502488,0,615384615384615),
		[152]=v2fM(0,432835820895522,0,615384615384615),
		[153]=v2fM(0,461691542288557,0,615384615384615),
		[154]=v2fM(0,0,692307692307692),
		[155]=v2fM(0,0288557213930348,0,692307692307692),
		[156]=v2fM(0,0577114427860697,0,692307692307692),
		[157]=v2fM(0,0865671641791045,0,692307692307692),
		[158]=v2fM(0,115422885572139,0,692307692307692),
		[159]=v2fM(0,144278606965174,0,692307692307692),
		[160]=v2fM(0,173134328358209,0,692307692307692),
		[161]=v2fM(0,201990049751244,0,692307692307692),
		[162]=v2fM(0,230845771144279,0,692307692307692),
		[163]=v2fM(0,259701492537313,0,692307692307692),
		[164]=v2fM(0,288557213930348,0,692307692307692),
		[165]=v2fM(0,317412935323383,0,692307692307692),
		[166]=v2fM(0,346268656716418,0,692307692307692),
		[167]=v2fM(0,375124378109453,0,692307692307692),
		[168]=v2fM(0,403980099502488,0,692307692307692),
		[169]=v2fM(0,432835820895522,0,692307692307692),
		[170]=v2fM(0,461691542288557,0,692307692307692),
		[171]=v2fM(0,0,769230769230769),
		[172]=v2fM(0,0288557213930348,0,769230769230769),
		[173]=v2fM(0,0577114427860697,0,769230769230769),
		[174]=v2fM(0,0865671641791045,0,769230769230769),
		[175]=v2fM(0,115422885572139,0,769230769230769),
		[176]=v2fM(0,144278606965174,0,769230769230769),
		[177]=v2fM(0,173134328358209,0,769230769230769),
		[178]=v2fM(0,201990049751244,0,769230769230769),
		[179]=v2fM(0,230845771144279,0,769230769230769),
		[180]=v2fM(0,259701492537313,0,769230769230769),
		[181]=v2fM(0,288557213930348,0,769230769230769),
		[182]=v2fM(0,317412935323383,0,769230769230769),
		[183]=v2fM(0,346268656716418,0,769230769230769),
		[184]=v2fM(0,375124378109453,0,769230769230769),
		[185]=v2fM(0,403980099502488,0,769230769230769),
		[186]=v2fM(0,432835820895522,0,769230769230769),
		[187]=v2fM(0,461691542288557,0,769230769230769),
		[188]=v2fM(0,0,846153846153846),
		[189]=v2fM(0,0288557213930348,0,846153846153846),
		[190]=v2fM(0,0577114427860697,0,846153846153846),
		[191]=v2fM(0,0865671641791045,0,846153846153846),
		[192]=v2fM(0,115422885572139,0,846153846153846),
		[193]=v2fM(0,144278606965174,0,846153846153846),
		[194]=v2fM(0,173134328358209,0,846153846153846),
		[195]=v2fM(0,201990049751244,0,846153846153846),
		[196]=v2fM(0,230845771144279,0,846153846153846),
		[197]=v2fM(0,259701492537313,0,846153846153846),
		[198]=v2fM(0,288557213930348,0,846153846153846),
		[199]=v2fM(0,317412935323383,0,846153846153846),
		[200]=v2fM(0,346268656716418,0,846153846153846),
		[201]=v2fM(0,375124378109453,0,846153846153846),
		[202]=v2fM(0,403980099502488,0,846153846153846),
		[203]=v2fM(0,432835820895522,0,846153846153846),
		[204]=v2fM(0,461691542288557,0,846153846153846),
		[205]=v2fM(0,0,923076923076923),
		[206]=v2fM(0,0288557213930348,0,923076923076923),
		[207]=v2fM(0,0577114427860697,0,923076923076923),
		[208]=v2fM(0,0865671641791045,0,923076923076923),
		[209]=v2fM(0,115422885572139,0,923076923076923),
		[210]=v2fM(0,144278606965174,0,923076923076923),
		[211]=v2fM(0,173134328358209,0,923076923076923),
		[212]=v2fM(0,201990049751244,0,923076923076923),
		[213]=v2fM(0,230845771144279,0,923076923076923),
		[214]=v2fM(0,259701492537313,0,923076923076923),
		[215]=v2fM(0,288557213930348,0,923076923076923),
		[216]=v2fM(0,317412935323383,0,923076923076923),
		[217]=v2fM(0,346268656716418,0,923076923076923),
		[218]=v2fM(0,375124378109453,0,923076923076923),
		[219]=v2fM(0,403980099502488,0,923076923076923),
		[220]=v2fM(0,432835820895522,0,923076923076923),
		[221]=v2fM(0,461691542288557,0,923076923076923),
		[222]=v2fM(0,509452736318408,0),
		[223]=v2fM(0,538308457711443,0),
		[224]=v2fM(0,567164179104478,0),
		[225]=v2fM(0,596019900497512,0),
		[226]=v2fM(0,624875621890547,0),
		[227]=v2fM(0,653731343283582,0),
		[228]=v2fM(0,682587064676617,0),
		[229]=v2fM(0,711442786069652,0),
		[230]=v2fM(0,740298507462687,0),
		[231]=v2fM(0,769154228855721,0),
		[232]=v2fM(0,798009950248756,0),
		[233]=v2fM(0,826865671641791,0),
		[234]=v2fM(0,855721393034826,0),
		[235]=v2fM(0,884577114427861,0),
		[236]=v2fM(0,913432835820896,0),
		[237]=v2fM(0,94228855721393,0),
		[238]=v2fM(0,971144278606965,0),
		[239]=v2fM(0,509452736318408,0,0769230769230769),
		[240]=v2fM(0,538308457711443,0,0769230769230769),
		[241]=v2fM(0,567164179104478,0,0769230769230769),
		[242]=v2fM(0,596019900497512,0,0769230769230769),
		[243]=v2fM(0,624875621890547,0,0769230769230769),
		[244]=v2fM(0,653731343283582,0,0769230769230769),
		[245]=v2fM(0,682587064676617,0,0769230769230769),
		[246]=v2fM(0,711442786069652,0,0769230769230769),
		[247]=v2fM(0,740298507462687,0,0769230769230769),
		[248]=v2fM(0,769154228855721,0,0769230769230769),
		[249]=v2fM(0,798009950248756,0,0769230769230769),
		[250]=v2fM(0,826865671641791,0,0769230769230769),
		[251]=v2fM(0,855721393034826,0,0769230769230769),
		[252]=v2fM(0,884577114427861,0,0769230769230769),
		[253]=v2fM(0,913432835820896,0,0769230769230769),
		[254]=v2fM(0,94228855721393,0,0769230769230769),
		[255]=v2fM(0,971144278606965,0,0769230769230769),
		[256]=v2fM(0,509452736318408,0,153846153846154),
		[257]=v2fM(0,538308457711443,0,153846153846154),
		[258]=v2fM(0,567164179104478,0,153846153846154),
		[259]=v2fM(0,596019900497512,0,153846153846154),
		[260]=v2fM(0,624875621890547,0,153846153846154),
		[261]=v2fM(0,653731343283582,0,153846153846154),
		[262]=v2fM(0,682587064676617,0,153846153846154),
		[263]=v2fM(0,711442786069652,0,153846153846154),
		[264]=v2fM(0,740298507462687,0,153846153846154),
		[265]=v2fM(0,769154228855721,0,153846153846154),
		[266]=v2fM(0,798009950248756,0,153846153846154),
		[267]=v2fM(0,826865671641791,0,153846153846154),
		[268]=v2fM(0,855721393034826,0,153846153846154),
		[269]=v2fM(0,884577114427861,0,153846153846154),
		[270]=v2fM(0,913432835820896,0,153846153846154),
		[271]=v2fM(0,94228855721393,0,153846153846154),
		[272]=v2fM(0,971144278606965,0,153846153846154),
		[273]=v2fM(0,509452736318408,0,230769230769231),
		[274]=v2fM(0,538308457711443,0,230769230769231),
		[275]=v2fM(0,567164179104478,0,230769230769231),
		[276]=v2fM(0,596019900497512,0,230769230769231),
		[277]=v2fM(0,624875621890547,0,230769230769231),
		[278]=v2fM(0,653731343283582,0,230769230769231),
		[279]=v2fM(0,682587064676617,0,230769230769231),
		[280]=v2fM(0,711442786069652,0,230769230769231),
		[281]=v2fM(0,740298507462687,0,230769230769231),
		[282]=v2fM(0,769154228855721,0,230769230769231),
		[283]=v2fM(0,798009950248756,0,230769230769231),
		[284]=v2fM(0,826865671641791,0,230769230769231),
		[285]=v2fM(0,855721393034826,0,230769230769231),
		[286]=v2fM(0,884577114427861,0,230769230769231),
		[287]=v2fM(0,913432835820896,0,230769230769231),
		[288]=v2fM(0,94228855721393,0,230769230769231),
	},
	FRAME_SIZES={
		[1]={WIDTH=58,HEIGHT=78},
		[2]={WIDTH=58,HEIGHT=78},
		[3]={WIDTH=58,HEIGHT=78},
		[4]={WIDTH=58,HEIGHT=78},
		[5]={WIDTH=58,HEIGHT=78},
		[6]={WIDTH=58,HEIGHT=78},
		[7]={WIDTH=58,HEIGHT=78},
		[8]={WIDTH=58,HEIGHT=78},
		[9]={WIDTH=58,HEIGHT=78},
		[10]={WIDTH=58,HEIGHT=78},
		[11]={WIDTH=58,HEIGHT=78},
		[12]={WIDTH=58,HEIGHT=78},
		[13]={WIDTH=58,HEIGHT=78},
		[14]={WIDTH=58,HEIGHT=78},
		[15]={WIDTH=58,HEIGHT=78},
		[16]={WIDTH=58,HEIGHT=78},
		[17]={WIDTH=58,HEIGHT=78},
		[18]={WIDTH=58,HEIGHT=78},
		[19]={WIDTH=58,HEIGHT=78},
		[20]={WIDTH=58,HEIGHT=78},
		[21]={WIDTH=58,HEIGHT=78},
		[22]={WIDTH=58,HEIGHT=78},
		[23]={WIDTH=58,HEIGHT=78},
		[24]={WIDTH=58,HEIGHT=78},
		[25]={WIDTH=58,HEIGHT=78},
		[26]={WIDTH=58,HEIGHT=78},
		[27]={WIDTH=58,HEIGHT=78},
		[28]={WIDTH=58,HEIGHT=78},
		[29]={WIDTH=58,HEIGHT=78},
		[30]={WIDTH=58,HEIGHT=78},
		[31]={WIDTH=58,HEIGHT=78},
		[32]={WIDTH=58,HEIGHT=78},
		[33]={WIDTH=58,HEIGHT=78},
		[34]={WIDTH=58,HEIGHT=78},
		[35]={WIDTH=58,HEIGHT=78},
		[36]={WIDTH=58,HEIGHT=78},
		[37]={WIDTH=58,HEIGHT=78},
		[38]={WIDTH=58,HEIGHT=78},
		[39]={WIDTH=58,HEIGHT=78},
		[40]={WIDTH=58,HEIGHT=78},
		[41]={WIDTH=58,HEIGHT=78},
		[42]={WIDTH=58,HEIGHT=78},
		[43]={WIDTH=58,HEIGHT=78},
		[44]={WIDTH=58,HEIGHT=78},
		[45]={WIDTH=58,HEIGHT=78},
		[46]={WIDTH=58,HEIGHT=78},
		[47]={WIDTH=58,HEIGHT=78},
		[48]={WIDTH=58,HEIGHT=78},
		[49]={WIDTH=58,HEIGHT=78},
		[50]={WIDTH=58,HEIGHT=78},
		[51]={WIDTH=58,HEIGHT=78},
		[52]={WIDTH=58,HEIGHT=78},
		[53]={WIDTH=58,HEIGHT=78},
		[54]={WIDTH=58,HEIGHT=78},
		[55]={WIDTH=58,HEIGHT=78},
		[56]={WIDTH=58,HEIGHT=78},
		[57]={WIDTH=58,HEIGHT=78},
		[58]={WIDTH=58,HEIGHT=78},
		[59]={WIDTH=58,HEIGHT=78},
		[60]={WIDTH=58,HEIGHT=78},
		[61]={WIDTH=58,HEIGHT=78},
		[62]={WIDTH=58,HEIGHT=78},
		[63]={WIDTH=58,HEIGHT=78},
		[64]={WIDTH=58,HEIGHT=78},
		[65]={WIDTH=58,HEIGHT=78},
		[66]={WIDTH=58,HEIGHT=78},
		[67]={WIDTH=58,HEIGHT=78},
		[68]={WIDTH=58,HEIGHT=78},
		[69]={WIDTH=58,HEIGHT=78},
		[70]={WIDTH=58,HEIGHT=78},
		[71]={WIDTH=58,HEIGHT=78},
		[72]={WIDTH=58,HEIGHT=78},
		[73]={WIDTH=58,HEIGHT=78},
		[74]={WIDTH=58,HEIGHT=78},
		[75]={WIDTH=58,HEIGHT=78},
		[76]={WIDTH=58,HEIGHT=78},
		[77]={WIDTH=58,HEIGHT=78},
		[78]={WIDTH=58,HEIGHT=78},
		[79]={WIDTH=58,HEIGHT=78},
		[80]={WIDTH=58,HEIGHT=78},
		[81]={WIDTH=58,HEIGHT=78},
		[82]={WIDTH=58,HEIGHT=78},
		[83]={WIDTH=58,HEIGHT=78},
		[84]={WIDTH=58,HEIGHT=78},
		[85]={WIDTH=58,HEIGHT=78},
		[86]={WIDTH=58,HEIGHT=78},
		[87]={WIDTH=58,HEIGHT=78},
		[88]={WIDTH=58,HEIGHT=78},
		[89]={WIDTH=58,HEIGHT=78},
		[90]={WIDTH=58,HEIGHT=78},
		[91]={WIDTH=58,HEIGHT=78},
		[92]={WIDTH=58,HEIGHT=78},
		[93]={WIDTH=58,HEIGHT=78},
		[94]={WIDTH=58,HEIGHT=78},
		[95]={WIDTH=58,HEIGHT=78},
		[96]={WIDTH=58,HEIGHT=78},
		[97]={WIDTH=58,HEIGHT=78},
		[98]={WIDTH=58,HEIGHT=78},
		[99]={WIDTH=58,HEIGHT=78},
		[100]={WIDTH=58,HEIGHT=78},
		[101]={WIDTH=58,HEIGHT=78},
		[102]={WIDTH=58,HEIGHT=78},
		[103]={WIDTH=58,HEIGHT=78},
		[104]={WIDTH=58,HEIGHT=78},
		[105]={WIDTH=58,HEIGHT=78},
		[106]={WIDTH=58,HEIGHT=78},
		[107]={WIDTH=58,HEIGHT=78},
		[108]={WIDTH=58,HEIGHT=78},
		[109]={WIDTH=58,HEIGHT=78},
		[110]={WIDTH=58,HEIGHT=78},
		[111]={WIDTH=58,HEIGHT=78},
		[112]={WIDTH=58,HEIGHT=78},
		[113]={WIDTH=58,HEIGHT=78},
		[114]={WIDTH=58,HEIGHT=78},
		[115]={WIDTH=58,HEIGHT=78},
		[116]={WIDTH=58,HEIGHT=78},
		[117]={WIDTH=58,HEIGHT=78},
		[118]={WIDTH=58,HEIGHT=78},
		[119]={WIDTH=58,HEIGHT=78},
		[120]={WIDTH=58,HEIGHT=78},
		[121]={WIDTH=58,HEIGHT=78},
		[122]={WIDTH=58,HEIGHT=78},
		[123]={WIDTH=58,HEIGHT=78},
		[124]={WIDTH=58,HEIGHT=78},
		[125]={WIDTH=58,HEIGHT=78},
		[126]={WIDTH=58,HEIGHT=78},
		[127]={WIDTH=58,HEIGHT=78},
		[128]={WIDTH=58,HEIGHT=78},
		[129]={WIDTH=58,HEIGHT=78},
		[130]={WIDTH=58,HEIGHT=78},
		[131]={WIDTH=58,HEIGHT=78},
		[132]={WIDTH=58,HEIGHT=78},
		[133]={WIDTH=58,HEIGHT=78},
		[134]={WIDTH=58,HEIGHT=78},
		[135]={WIDTH=58,HEIGHT=78},
		[136]={WIDTH=58,HEIGHT=78},
		[137]={WIDTH=58,HEIGHT=78},
		[138]={WIDTH=58,HEIGHT=78},
		[139]={WIDTH=58,HEIGHT=78},
		[140]={WIDTH=58,HEIGHT=78},
		[141]={WIDTH=58,HEIGHT=78},
		[142]={WIDTH=58,HEIGHT=78},
		[143]={WIDTH=58,HEIGHT=78},
		[144]={WIDTH=58,HEIGHT=78},
		[145]={WIDTH=58,HEIGHT=78},
		[146]={WIDTH=58,HEIGHT=78},
		[147]={WIDTH=58,HEIGHT=78},
		[148]={WIDTH=58,HEIGHT=78},
		[149]={WIDTH=58,HEIGHT=78},
		[150]={WIDTH=58,HEIGHT=78},
		[151]={WIDTH=58,HEIGHT=78},
		[152]={WIDTH=58,HEIGHT=78},
		[153]={WIDTH=58,HEIGHT=78},
		[154]={WIDTH=58,HEIGHT=78},
		[155]={WIDTH=58,HEIGHT=78},
		[156]={WIDTH=58,HEIGHT=78},
		[157]={WIDTH=58,HEIGHT=78},
		[158]={WIDTH=58,HEIGHT=78},
		[159]={WIDTH=58,HEIGHT=78},
		[160]={WIDTH=58,HEIGHT=78},
		[161]={WIDTH=58,HEIGHT=78},
		[162]={WIDTH=58,HEIGHT=78},
		[163]={WIDTH=58,HEIGHT=78},
		[164]={WIDTH=58,HEIGHT=78},
		[165]={WIDTH=58,HEIGHT=78},
		[166]={WIDTH=58,HEIGHT=78},
		[167]={WIDTH=58,HEIGHT=78},
		[168]={WIDTH=58,HEIGHT=78},
		[169]={WIDTH=58,HEIGHT=78},
		[170]={WIDTH=58,HEIGHT=78},
		[171]={WIDTH=58,HEIGHT=78},
		[172]={WIDTH=58,HEIGHT=78},
		[173]={WIDTH=58,HEIGHT=78},
		[174]={WIDTH=58,HEIGHT=78},
		[175]={WIDTH=58,HEIGHT=78},
		[176]={WIDTH=58,HEIGHT=78},
		[177]={WIDTH=58,HEIGHT=78},
		[178]={WIDTH=58,HEIGHT=78},
		[179]={WIDTH=58,HEIGHT=78},
		[180]={WIDTH=58,HEIGHT=78},
		[181]={WIDTH=58,HEIGHT=78},
		[182]={WIDTH=58,HEIGHT=78},
		[183]={WIDTH=58,HEIGHT=78},
		[184]={WIDTH=58,HEIGHT=78},
		[185]={WIDTH=58,HEIGHT=78},
		[186]={WIDTH=58,HEIGHT=78},
		[187]={WIDTH=58,HEIGHT=78},
		[188]={WIDTH=58,HEIGHT=78},
		[189]={WIDTH=58,HEIGHT=78},
		[190]={WIDTH=58,HEIGHT=78},
		[191]={WIDTH=58,HEIGHT=78},
		[192]={WIDTH=58,HEIGHT=78},
		[193]={WIDTH=58,HEIGHT=78},
		[194]={WIDTH=58,HEIGHT=78},
		[195]={WIDTH=58,HEIGHT=78},
		[196]={WIDTH=58,HEIGHT=78},
		[197]={WIDTH=58,HEIGHT=78},
		[198]={WIDTH=58,HEIGHT=78},
		[199]={WIDTH=58,HEIGHT=78},
		[200]={WIDTH=58,HEIGHT=78},
		[201]={WIDTH=58,HEIGHT=78},
		[202]={WIDTH=58,HEIGHT=78},
		[203]={WIDTH=58,HEIGHT=78},
		[204]={WIDTH=58,HEIGHT=78},
		[205]={WIDTH=58,HEIGHT=78},
		[206]={WIDTH=58,HEIGHT=78},
		[207]={WIDTH=58,HEIGHT=78},
		[208]={WIDTH=58,HEIGHT=78},
		[209]={WIDTH=58,HEIGHT=78},
		[210]={WIDTH=58,HEIGHT=78},
		[211]={WIDTH=58,HEIGHT=78},
		[212]={WIDTH=58,HEIGHT=78},
		[213]={WIDTH=58,HEIGHT=78},
		[214]={WIDTH=58,HEIGHT=78},
		[215]={WIDTH=58,HEIGHT=78},
		[216]={WIDTH=58,HEIGHT=78},
		[217]={WIDTH=58,HEIGHT=78},
		[218]={WIDTH=58,HEIGHT=78},
		[219]={WIDTH=58,HEIGHT=78},
		[220]={WIDTH=58,HEIGHT=78},
		[221]={WIDTH=58,HEIGHT=78},
		[222]={WIDTH=58,HEIGHT=78},
		[223]={WIDTH=58,HEIGHT=78},
		[224]={WIDTH=58,HEIGHT=78},
		[225]={WIDTH=58,HEIGHT=78},
		[226]={WIDTH=58,HEIGHT=78},
		[227]={WIDTH=58,HEIGHT=78},
		[228]={WIDTH=58,HEIGHT=78},
		[229]={WIDTH=58,HEIGHT=78},
		[230]={WIDTH=58,HEIGHT=78},
		[231]={WIDTH=58,HEIGHT=78},
		[232]={WIDTH=58,HEIGHT=78},
		[233]={WIDTH=58,HEIGHT=78},
		[234]={WIDTH=58,HEIGHT=78},
		[235]={WIDTH=58,HEIGHT=78},
		[236]={WIDTH=58,HEIGHT=78},
		[237]={WIDTH=58,HEIGHT=78},
		[238]={WIDTH=58,HEIGHT=78},
		[239]={WIDTH=58,HEIGHT=78},
		[240]={WIDTH=58,HEIGHT=78},
		[241]={WIDTH=58,HEIGHT=78},
		[242]={WIDTH=58,HEIGHT=78},
		[243]={WIDTH=58,HEIGHT=78},
		[244]={WIDTH=58,HEIGHT=78},
		[245]={WIDTH=58,HEIGHT=78},
		[246]={WIDTH=58,HEIGHT=78},
		[247]={WIDTH=58,HEIGHT=78},
		[248]={WIDTH=58,HEIGHT=78},
		[249]={WIDTH=58,HEIGHT=78},
		[250]={WIDTH=58,HEIGHT=78},
		[251]={WIDTH=58,HEIGHT=78},
		[252]={WIDTH=58,HEIGHT=78},
		[253]={WIDTH=58,HEIGHT=78},
		[254]={WIDTH=58,HEIGHT=78},
		[255]={WIDTH=58,HEIGHT=78},
		[256]={WIDTH=58,HEIGHT=78},
		[257]={WIDTH=58,HEIGHT=78},
		[258]={WIDTH=58,HEIGHT=78},
		[259]={WIDTH=58,HEIGHT=78},
		[260]={WIDTH=58,HEIGHT=78},
		[261]={WIDTH=58,HEIGHT=78},
		[262]={WIDTH=58,HEIGHT=78},
		[263]={WIDTH=58,HEIGHT=78},
		[264]={WIDTH=58,HEIGHT=78},
		[265]={WIDTH=58,HEIGHT=78},
		[266]={WIDTH=58,HEIGHT=78},
		[267]={WIDTH=58,HEIGHT=78},
		[268]={WIDTH=58,HEIGHT=78},
		[269]={WIDTH=58,HEIGHT=78},
		[270]={WIDTH=58,HEIGHT=78},
		[271]={WIDTH=58,HEIGHT=78},
		[272]={WIDTH=58,HEIGHT=78},
		[273]={WIDTH=58,HEIGHT=78},
		[274]={WIDTH=58,HEIGHT=78},
		[275]={WIDTH=58,HEIGHT=78},
		[276]={WIDTH=58,HEIGHT=78},
		[277]={WIDTH=58,HEIGHT=78},
		[278]={WIDTH=58,HEIGHT=78},
		[279]={WIDTH=58,HEIGHT=78},
		[280]={WIDTH=58,HEIGHT=78},
		[281]={WIDTH=58,HEIGHT=78},
		[282]={WIDTH=58,HEIGHT=78},
		[283]={WIDTH=58,HEIGHT=78},
		[284]={WIDTH=58,HEIGHT=78},
		[285]={WIDTH=58,HEIGHT=78},
		[286]={WIDTH=58,HEIGHT=78},
		[287]={WIDTH=58,HEIGHT=78},
		[288]={WIDTH=58,HEIGHT=78},
	},
	FRAME_INDEXS={
		['w-r-rlaunch0191']=1,
		['w-r-rlaunch0192']=2,
		['w-r-rlaunch0189']=3,
		['w-r-rlaunch0190']=4,
		['w-r-rlaunch0193']=5,
		['w-r-rlaunch0196']=6,
		['w-r-rlaunch0197']=7,
		['w-r-rlaunch0194']=8,
		['w-r-rlaunch0195']=9,
		['w-r-rlaunch0182']=10,
		['w-r-rlaunch0183']=11,
		['w-r-rlaunch0180']=12,
		['w-r-rlaunch0181']=13,
		['w-r-rlaunch0184']=14,
		['w-r-rlaunch0187']=15,
		['w-r-rlaunch0188']=16,
		['w-r-rlaunch0185']=17,
		['w-r-rlaunch0186']=18,
		['w-r-rlaunch0209']=19,
		['w-r-rlaunch0210']=20,
		['w-r-rlaunch0207']=21,
		['w-r-rlaunch0208']=22,
		['w-r-rlaunch0211']=23,
		['w-r-rlaunch0214']=24,
		['w-r-rlaunch0215']=25,
		['w-r-rlaunch0212']=26,
		['w-r-rlaunch0213']=27,
		['w-r-rlaunch0200']=28,
		['w-r-rlaunch0201']=29,
		['w-r-rlaunch0198']=30,
		['w-r-rlaunch0199']=31,
		['w-r-rlaunch0202']=32,
		['w-r-rlaunch0205']=33,
		['w-r-rlaunch0206']=34,
		['w-r-rlaunch0203']=35,
		['w-r-rlaunch0204']=36,
		['w-r-rlaunch0155']=37,
		['w-r-rlaunch0156']=38,
		['w-r-rlaunch0153']=39,
		['w-r-rlaunch0154']=40,
		['w-r-rlaunch0157']=41,
		['w-r-rlaunch0160']=42,
		['w-r-rlaunch0161']=43,
		['w-r-rlaunch0158']=44,
		['w-r-rlaunch0159']=45,
		['w-r-rlaunch0146']=46,
		['w-r-rlaunch0147']=47,
		['w-r-rlaunch0144']=48,
		['w-r-rlaunch0145']=49,
		['w-r-rlaunch0148']=50,
		['w-r-rlaunch0151']=51,
		['w-r-rlaunch0152']=52,
		['w-r-rlaunch0149']=53,
		['w-r-rlaunch0150']=54,
		['w-r-rlaunch0173']=55,
		['w-r-rlaunch0174']=56,
		['w-r-rlaunch0171']=57,
		['w-r-rlaunch0172']=58,
		['w-r-rlaunch0175']=59,
		['w-r-rlaunch0178']=60,
		['w-r-rlaunch0179']=61,
		['w-r-rlaunch0176']=62,
		['w-r-rlaunch0177']=63,
		['w-r-rlaunch0164']=64,
		['w-r-rlaunch0165']=65,
		['w-r-rlaunch0162']=66,
		['w-r-rlaunch0163']=67,
		['w-r-rlaunch0166']=68,
		['w-r-rlaunch0169']=69,
		['w-r-rlaunch0170']=70,
		['w-r-rlaunch0167']=71,
		['w-r-rlaunch0168']=72,
		['w-r-rlaunch0263']=73,
		['w-r-rlaunch0264']=74,
		['w-r-rlaunch0261']=75,
		['w-r-rlaunch0262']=76,
		['w-r-rlaunch0265']=77,
		['w-r-rlaunch0268']=78,
		['w-r-rlaunch0269']=79,
		['w-r-rlaunch0266']=80,
		['w-r-rlaunch0267']=81,
		['w-r-rlaunch0254']=82,
		['w-r-rlaunch0255']=83,
		['w-r-rlaunch0252']=84,
		['w-r-rlaunch0253']=85,
		['w-r-rlaunch0256']=86,
		['w-r-rlaunch0259']=87,
		['w-r-rlaunch0260']=88,
		['w-r-rlaunch0257']=89,
		['w-r-rlaunch0258']=90,
		['w-r-rlaunch0281']=91,
		['w-r-rlaunch0282']=92,
		['w-r-rlaunch0279']=93,
		['w-r-rlaunch0280']=94,
		['w-r-rlaunch0283']=95,
		['w-r-rlaunch0286']=96,
		['w-r-rlaunch0287']=97,
		['w-r-rlaunch0284']=98,
		['w-r-rlaunch0285']=99,
		['w-r-rlaunch0272']=100,
		['w-r-rlaunch0273']=101,
		['w-r-rlaunch0270']=102,
		['w-r-rlaunch0271']=103,
		['w-r-rlaunch0274']=104,
		['w-r-rlaunch0277']=105,
		['w-r-rlaunch0278']=106,
		['w-r-rlaunch0275']=107,
		['w-r-rlaunch0276']=108,
		['w-r-rlaunch0227']=109,
		['w-r-rlaunch0228']=110,
		['w-r-rlaunch0225']=111,
		['w-r-rlaunch0226']=112,
		['w-r-rlaunch0229']=113,
		['w-r-rlaunch0232']=114,
		['w-r-rlaunch0233']=115,
		['w-r-rlaunch0230']=116,
		['w-r-rlaunch0231']=117,
		['w-r-rlaunch0218']=118,
		['w-r-rlaunch0219']=119,
		['w-r-rlaunch0216']=120,
		['w-r-rlaunch0217']=121,
		['w-r-rlaunch0220']=122,
		['w-r-rlaunch0223']=123,
		['w-r-rlaunch0224']=124,
		['w-r-rlaunch0221']=125,
		['w-r-rlaunch0222']=126,
		['w-r-rlaunch0245']=127,
		['w-r-rlaunch0246']=128,
		['w-r-rlaunch0243']=129,
		['w-r-rlaunch0244']=130,
		['w-r-rlaunch0247']=131,
		['w-r-rlaunch0250']=132,
		['w-r-rlaunch0251']=133,
		['w-r-rlaunch0248']=134,
		['w-r-rlaunch0249']=135,
		['w-r-rlaunch0236']=136,
		['w-r-rlaunch0237']=137,
		['w-r-rlaunch0234']=138,
		['w-r-rlaunch0235']=139,
		['w-r-rlaunch0238']=140,
		['w-r-rlaunch0241']=141,
		['w-r-rlaunch0242']=142,
		['w-r-rlaunch0239']=143,
		['w-r-rlaunch0240']=144,
		['w-r-rlaunch0047']=145,
		['w-r-rlaunch0048']=146,
		['w-r-rlaunch0045']=147,
		['w-r-rlaunch0046']=148,
		['w-r-rlaunch0049']=149,
		['w-r-rlaunch0052']=150,
		['w-r-rlaunch0053']=151,
		['w-r-rlaunch0050']=152,
		['w-r-rlaunch0051']=153,
		['w-r-rlaunch0038']=154,
		['w-r-rlaunch0039']=155,
		['w-r-rlaunch0036']=156,
		['w-r-rlaunch0037']=157,
		['w-r-rlaunch0040']=158,
		['w-r-rlaunch0043']=159,
		['w-r-rlaunch0044']=160,
		['w-r-rlaunch0041']=161,
		['w-r-rlaunch0042']=162,
		['w-r-rlaunch0065']=163,
		['w-r-rlaunch0066']=164,
		['w-r-rlaunch0063']=165,
		['w-r-rlaunch0064']=166,
		['w-r-rlaunch0067']=167,
		['w-r-rlaunch0070']=168,
		['w-r-rlaunch0071']=169,
		['w-r-rlaunch0068']=170,
		['w-r-rlaunch0069']=171,
		['w-r-rlaunch0056']=172,
		['w-r-rlaunch0057']=173,
		['w-r-rlaunch0054']=174,
		['w-r-rlaunch0055']=175,
		['w-r-rlaunch0058']=176,
		['w-r-rlaunch0061']=177,
		['w-r-rlaunch0062']=178,
		['w-r-rlaunch0059']=179,
		['w-r-rlaunch0060']=180,
		['w-r-rlaunch0011']=181,
		['w-r-rlaunch0012']=182,
		['w-r-rlaunch0009']=183,
		['w-r-rlaunch0010']=184,
		['w-r-rlaunch0013']=185,
		['w-r-rlaunch0016']=186,
		['w-r-rlaunch0017']=187,
		['w-r-rlaunch0014']=188,
		['w-r-rlaunch0015']=189,
		['w-r-rlaunch0002']=190,
		['w-r-rlaunch0003']=191,
		['w-r-rlaunch0000']=192,
		['w-r-rlaunch0001']=193,
		['w-r-rlaunch0004']=194,
		['w-r-rlaunch0007']=195,
		['w-r-rlaunch0008']=196,
		['w-r-rlaunch0005']=197,
		['w-r-rlaunch0006']=198,
		['w-r-rlaunch0029']=199,
		['w-r-rlaunch0030']=200,
		['w-r-rlaunch0027']=201,
		['w-r-rlaunch0028']=202,
		['w-r-rlaunch0031']=203,
		['w-r-rlaunch0034']=204,
		['w-r-rlaunch0035']=205,
		['w-r-rlaunch0032']=206,
		['w-r-rlaunch0033']=207,
		['w-r-rlaunch0020']=208,
		['w-r-rlaunch0021']=209,
		['w-r-rlaunch0018']=210,
		['w-r-rlaunch0019']=211,
		['w-r-rlaunch0022']=212,
		['w-r-rlaunch0025']=213,
		['w-r-rlaunch0026']=214,
		['w-r-rlaunch0023']=215,
		['w-r-rlaunch0024']=216,
		['w-r-rlaunch0119']=217,
		['w-r-rlaunch0120']=218,
		['w-r-rlaunch0117']=219,
		['w-r-rlaunch0118']=220,
		['w-r-rlaunch0121']=221,
		['w-r-rlaunch0124']=222,
		['w-r-rlaunch0125']=223,
		['w-r-rlaunch0122']=224,
		['w-r-rlaunch0123']=225,
		['w-r-rlaunch0110']=226,
		['w-r-rlaunch0111']=227,
		['w-r-rlaunch0108']=228,
		['w-r-rlaunch0109']=229,
		['w-r-rlaunch0112']=230,
		['w-r-rlaunch0115']=231,
		['w-r-rlaunch0116']=232,
		['w-r-rlaunch0113']=233,
		['w-r-rlaunch0114']=234,
		['w-r-rlaunch0137']=235,
		['w-r-rlaunch0138']=236,
		['w-r-rlaunch0135']=237,
		['w-r-rlaunch0136']=238,
		['w-r-rlaunch0139']=239,
		['w-r-rlaunch0142']=240,
		['w-r-rlaunch0143']=241,
		['w-r-rlaunch0140']=242,
		['w-r-rlaunch0141']=243,
		['w-r-rlaunch0128']=244,
		['w-r-rlaunch0129']=245,
		['w-r-rlaunch0126']=246,
		['w-r-rlaunch0127']=247,
		['w-r-rlaunch0130']=248,
		['w-r-rlaunch0133']=249,
		['w-r-rlaunch0134']=250,
		['w-r-rlaunch0131']=251,
		['w-r-rlaunch0132']=252,
		['w-r-rlaunch0083']=253,
		['w-r-rlaunch0084']=254,
		['w-r-rlaunch0081']=255,
		['w-r-rlaunch0082']=256,
		['w-r-rlaunch0085']=257,
		['w-r-rlaunch0088']=258,
		['w-r-rlaunch0089']=259,
		['w-r-rlaunch0086']=260,
		['w-r-rlaunch0087']=261,
		['w-r-rlaunch0074']=262,
		['w-r-rlaunch0075']=263,
		['w-r-rlaunch0072']=264,
		['w-r-rlaunch0073']=265,
		['w-r-rlaunch0076']=266,
		['w-r-rlaunch0079']=267,
		['w-r-rlaunch0080']=268,
		['w-r-rlaunch0077']=269,
		['w-r-rlaunch0078']=270,
		['w-r-rlaunch0101']=271,
		['w-r-rlaunch0102']=272,
		['w-r-rlaunch0099']=273,
		['w-r-rlaunch0100']=274,
		['w-r-rlaunch0103']=275,
		['w-r-rlaunch0106']=276,
		['w-r-rlaunch0107']=277,
		['w-r-rlaunch0104']=278,
		['w-r-rlaunch0105']=279,
		['w-r-rlaunch0092']=280,
		['w-r-rlaunch0093']=281,
		['w-r-rlaunch0090']=282,
		['w-r-rlaunch0091']=283,
		['w-r-rlaunch0094']=284,
		['w-r-rlaunch0097']=285,
		['w-r-rlaunch0098']=286,
		['w-r-rlaunch0095']=287,
		['w-r-rlaunch0096']=288,
	},
};
