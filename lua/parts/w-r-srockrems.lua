--GENERATED ON: 05/12/2024 07:01:01


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['w-r-srockrems'] = {
	TEX_WIDTH=2032,
	TEX_HEIGHT=1056,
	FRAME_COUNT=72,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0826771653543307,0),
		[3]=v2fM(0,165354330708661,0),
		[4]=v2fM(0,248031496062992,0),
		[5]=v2fM(0,330708661417323,0),
		[6]=v2fM(0,413385826771654,0),
		[7]=v2fM(0,0,166666666666667),
		[8]=v2fM(0,0826771653543307,0,166666666666667),
		[9]=v2fM(0,165354330708661,0,166666666666667),
		[10]=v2fM(0,248031496062992,0,166666666666667),
		[11]=v2fM(0,330708661417323,0,166666666666667),
		[12]=v2fM(0,413385826771654,0,166666666666667),
		[13]=v2fM(0,0,333333333333333),
		[14]=v2fM(0,0826771653543307,0,333333333333333),
		[15]=v2fM(0,165354330708661,0,333333333333333),
		[16]=v2fM(0,248031496062992,0,333333333333333),
		[17]=v2fM(0,330708661417323,0,333333333333333),
		[18]=v2fM(0,413385826771654,0,333333333333333),
		[19]=v2fM(0,0,5),
		[20]=v2fM(0,0826771653543307,0,5),
		[21]=v2fM(0,165354330708661,0,5),
		[22]=v2fM(0,248031496062992,0,5),
		[23]=v2fM(0,330708661417323,0,5),
		[24]=v2fM(0,413385826771654,0,5),
		[25]=v2fM(0,0,666666666666667),
		[26]=v2fM(0,0826771653543307,0,666666666666667),
		[27]=v2fM(0,165354330708661,0,666666666666667),
		[28]=v2fM(0,248031496062992,0,666666666666667),
		[29]=v2fM(0,330708661417323,0,666666666666667),
		[30]=v2fM(0,413385826771654,0,666666666666667),
		[31]=v2fM(0,503937007874016,0),
		[32]=v2fM(0,586614173228346,0),
		[33]=v2fM(0,669291338582677,0),
		[34]=v2fM(0,751968503937008,0),
		[35]=v2fM(0,834645669291339,0),
		[36]=v2fM(0,917322834645669,0),
		[37]=v2fM(0,503937007874016,0,166666666666667),
		[38]=v2fM(0,586614173228346,0,166666666666667),
		[39]=v2fM(0,669291338582677,0,166666666666667),
		[40]=v2fM(0,751968503937008,0,166666666666667),
		[41]=v2fM(0,834645669291339,0,166666666666667),
		[42]=v2fM(0,917322834645669,0,166666666666667),
		[43]=v2fM(0,503937007874016,0,333333333333333),
		[44]=v2fM(0,586614173228346,0,333333333333333),
		[45]=v2fM(0,669291338582677,0,333333333333333),
		[46]=v2fM(0,751968503937008,0,333333333333333),
		[47]=v2fM(0,834645669291339,0,333333333333333),
		[48]=v2fM(0,917322834645669,0,333333333333333),
		[49]=v2fM(0,503937007874016,0,5),
		[50]=v2fM(0,586614173228346,0,5),
		[51]=v2fM(0,669291338582677,0,5),
		[52]=v2fM(0,751968503937008,0,5),
		[53]=v2fM(0,834645669291339,0,5),
		[54]=v2fM(0,917322834645669,0,5),
		[55]=v2fM(0,503937007874016,0,666666666666667),
		[56]=v2fM(0,586614173228346,0,666666666666667),
		[57]=v2fM(0,669291338582677,0,666666666666667),
		[58]=v2fM(0,751968503937008,0,666666666666667),
		[59]=v2fM(0,834645669291339,0,666666666666667),
		[60]=v2fM(0,917322834645669,0,666666666666667),
		[61]=v2fM(0,0,833333333333333),
		[62]=v2fM(0,0826771653543307,0,833333333333333),
		[63]=v2fM(0,165354330708661,0,833333333333333),
		[64]=v2fM(0,248031496062992,0,833333333333333),
		[65]=v2fM(0,330708661417323,0,833333333333333),
		[66]=v2fM(0,413385826771654,0,833333333333333),
		[67]=v2fM(0,496062992125984,0,833333333333333),
		[68]=v2fM(0,578740157480315,0,833333333333333),
		[69]=v2fM(0,661417322834646,0,833333333333333),
		[70]=v2fM(0,744094488188976,0,833333333333333),
		[71]=v2fM(0,826771653543307,0,833333333333333),
		[72]=v2fM(0,909448818897638,0,833333333333333),
	},
	FRAME_SIZES={
		[1]={WIDTH=168,HEIGHT=176},
		[2]={WIDTH=168,HEIGHT=176},
		[3]={WIDTH=168,HEIGHT=176},
		[4]={WIDTH=168,HEIGHT=176},
		[5]={WIDTH=168,HEIGHT=176},
		[6]={WIDTH=168,HEIGHT=176},
		[7]={WIDTH=168,HEIGHT=176},
		[8]={WIDTH=168,HEIGHT=176},
		[9]={WIDTH=168,HEIGHT=176},
		[10]={WIDTH=168,HEIGHT=176},
		[11]={WIDTH=168,HEIGHT=176},
		[12]={WIDTH=168,HEIGHT=176},
		[13]={WIDTH=168,HEIGHT=176},
		[14]={WIDTH=168,HEIGHT=176},
		[15]={WIDTH=168,HEIGHT=176},
		[16]={WIDTH=168,HEIGHT=176},
		[17]={WIDTH=168,HEIGHT=176},
		[18]={WIDTH=168,HEIGHT=176},
		[19]={WIDTH=168,HEIGHT=176},
		[20]={WIDTH=168,HEIGHT=176},
		[21]={WIDTH=168,HEIGHT=176},
		[22]={WIDTH=168,HEIGHT=176},
		[23]={WIDTH=168,HEIGHT=176},
		[24]={WIDTH=168,HEIGHT=176},
		[25]={WIDTH=168,HEIGHT=176},
		[26]={WIDTH=168,HEIGHT=176},
		[27]={WIDTH=168,HEIGHT=176},
		[28]={WIDTH=168,HEIGHT=176},
		[29]={WIDTH=168,HEIGHT=176},
		[30]={WIDTH=168,HEIGHT=176},
		[31]={WIDTH=168,HEIGHT=176},
		[32]={WIDTH=168,HEIGHT=176},
		[33]={WIDTH=168,HEIGHT=176},
		[34]={WIDTH=168,HEIGHT=176},
		[35]={WIDTH=168,HEIGHT=176},
		[36]={WIDTH=168,HEIGHT=176},
		[37]={WIDTH=168,HEIGHT=176},
		[38]={WIDTH=168,HEIGHT=176},
		[39]={WIDTH=168,HEIGHT=176},
		[40]={WIDTH=168,HEIGHT=176},
		[41]={WIDTH=168,HEIGHT=176},
		[42]={WIDTH=168,HEIGHT=176},
		[43]={WIDTH=168,HEIGHT=176},
		[44]={WIDTH=168,HEIGHT=176},
		[45]={WIDTH=168,HEIGHT=176},
		[46]={WIDTH=168,HEIGHT=176},
		[47]={WIDTH=168,HEIGHT=176},
		[48]={WIDTH=168,HEIGHT=176},
		[49]={WIDTH=168,HEIGHT=176},
		[50]={WIDTH=168,HEIGHT=176},
		[51]={WIDTH=168,HEIGHT=176},
		[52]={WIDTH=168,HEIGHT=176},
		[53]={WIDTH=168,HEIGHT=176},
		[54]={WIDTH=168,HEIGHT=176},
		[55]={WIDTH=168,HEIGHT=176},
		[56]={WIDTH=168,HEIGHT=176},
		[57]={WIDTH=168,HEIGHT=176},
		[58]={WIDTH=168,HEIGHT=176},
		[59]={WIDTH=168,HEIGHT=176},
		[60]={WIDTH=168,HEIGHT=176},
		[61]={WIDTH=168,HEIGHT=176},
		[62]={WIDTH=168,HEIGHT=176},
		[63]={WIDTH=168,HEIGHT=176},
		[64]={WIDTH=168,HEIGHT=176},
		[65]={WIDTH=168,HEIGHT=176},
		[66]={WIDTH=168,HEIGHT=176},
		[67]={WIDTH=168,HEIGHT=176},
		[68]={WIDTH=168,HEIGHT=176},
		[69]={WIDTH=168,HEIGHT=176},
		[70]={WIDTH=168,HEIGHT=176},
		[71]={WIDTH=168,HEIGHT=176},
		[72]={WIDTH=168,HEIGHT=176},
	},
	FRAME_INDEXS={
		['w-r-srockrem0047s']=1,
		['w-r-srockrem0048s']=2,
		['w-r-srockrem0045s']=3,
		['w-r-srockrem0046s']=4,
		['w-r-srockrem0049s']=5,
		['w-r-srockrem0052s']=6,
		['w-r-srockrem0053s']=7,
		['w-r-srockrem0050s']=8,
		['w-r-srockrem0051s']=9,
		['w-r-srockrem0038s']=10,
		['w-r-srockrem0039s']=11,
		['w-r-srockrem0036s']=12,
		['w-r-srockrem0037s']=13,
		['w-r-srockrem0040s']=14,
		['w-r-srockrem0043s']=15,
		['w-r-srockrem0044s']=16,
		['w-r-srockrem0041s']=17,
		['w-r-srockrem0042s']=18,
		['w-r-srockrem0065s']=19,
		['w-r-srockrem0066s']=20,
		['w-r-srockrem0063s']=21,
		['w-r-srockrem0064s']=22,
		['w-r-srockrem0067s']=23,
		['w-r-srockrem0070s']=24,
		['w-r-srockrem0071s']=25,
		['w-r-srockrem0068s']=26,
		['w-r-srockrem0069s']=27,
		['w-r-srockrem0056s']=28,
		['w-r-srockrem0057s']=29,
		['w-r-srockrem0054s']=30,
		['w-r-srockrem0055s']=31,
		['w-r-srockrem0058s']=32,
		['w-r-srockrem0061s']=33,
		['w-r-srockrem0062s']=34,
		['w-r-srockrem0059s']=35,
		['w-r-srockrem0060s']=36,
		['w-r-srockrem0011s']=37,
		['w-r-srockrem0012s']=38,
		['w-r-srockrem0009s']=39,
		['w-r-srockrem0010s']=40,
		['w-r-srockrem0013s']=41,
		['w-r-srockrem0016s']=42,
		['w-r-srockrem0017s']=43,
		['w-r-srockrem0014s']=44,
		['w-r-srockrem0015s']=45,
		['w-r-srockrem0002s']=46,
		['w-r-srockrem0003s']=47,
		['w-r-srockrem0000s']=48,
		['w-r-srockrem0001s']=49,
		['w-r-srockrem0004s']=50,
		['w-r-srockrem0007s']=51,
		['w-r-srockrem0008s']=52,
		['w-r-srockrem0005s']=53,
		['w-r-srockrem0006s']=54,
		['w-r-srockrem0029s']=55,
		['w-r-srockrem0030s']=56,
		['w-r-srockrem0027s']=57,
		['w-r-srockrem0028s']=58,
		['w-r-srockrem0031s']=59,
		['w-r-srockrem0034s']=60,
		['w-r-srockrem0035s']=61,
		['w-r-srockrem0032s']=62,
		['w-r-srockrem0033s']=63,
		['w-r-srockrem0020s']=64,
		['w-r-srockrem0021s']=65,
		['w-r-srockrem0018s']=66,
		['w-r-srockrem0019s']=67,
		['w-r-srockrem0022s']=68,
		['w-r-srockrem0025s']=69,
		['w-r-srockrem0026s']=70,
		['w-r-srockrem0023s']=71,
		['w-r-srockrem0024s']=72,
	},
};
