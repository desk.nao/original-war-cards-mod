--GENERATED ON: 05/12/2024 07:01:02


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['w-u-doubles'] = {
	TEX_WIDTH=1984,
	TEX_HEIGHT=1014,
	FRAME_COUNT=288,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0403225806451613,0),
		[3]=v2fM(0,0806451612903226,0),
		[4]=v2fM(0,120967741935484,0),
		[5]=v2fM(0,161290322580645,0),
		[6]=v2fM(0,201612903225806,0),
		[7]=v2fM(0,241935483870968,0),
		[8]=v2fM(0,282258064516129,0),
		[9]=v2fM(0,32258064516129,0),
		[10]=v2fM(0,362903225806452,0),
		[11]=v2fM(0,403225806451613,0),
		[12]=v2fM(0,443548387096774,0),
		[13]=v2fM(0,0,0769230769230769),
		[14]=v2fM(0,0403225806451613,0,0769230769230769),
		[15]=v2fM(0,0806451612903226,0,0769230769230769),
		[16]=v2fM(0,120967741935484,0,0769230769230769),
		[17]=v2fM(0,161290322580645,0,0769230769230769),
		[18]=v2fM(0,201612903225806,0,0769230769230769),
		[19]=v2fM(0,241935483870968,0,0769230769230769),
		[20]=v2fM(0,282258064516129,0,0769230769230769),
		[21]=v2fM(0,32258064516129,0,0769230769230769),
		[22]=v2fM(0,362903225806452,0,0769230769230769),
		[23]=v2fM(0,403225806451613,0,0769230769230769),
		[24]=v2fM(0,443548387096774,0,0769230769230769),
		[25]=v2fM(0,0,153846153846154),
		[26]=v2fM(0,0403225806451613,0,153846153846154),
		[27]=v2fM(0,0806451612903226,0,153846153846154),
		[28]=v2fM(0,120967741935484,0,153846153846154),
		[29]=v2fM(0,161290322580645,0,153846153846154),
		[30]=v2fM(0,201612903225806,0,153846153846154),
		[31]=v2fM(0,241935483870968,0,153846153846154),
		[32]=v2fM(0,282258064516129,0,153846153846154),
		[33]=v2fM(0,32258064516129,0,153846153846154),
		[34]=v2fM(0,362903225806452,0,153846153846154),
		[35]=v2fM(0,403225806451613,0,153846153846154),
		[36]=v2fM(0,443548387096774,0,153846153846154),
		[37]=v2fM(0,0,230769230769231),
		[38]=v2fM(0,0403225806451613,0,230769230769231),
		[39]=v2fM(0,0806451612903226,0,230769230769231),
		[40]=v2fM(0,120967741935484,0,230769230769231),
		[41]=v2fM(0,161290322580645,0,230769230769231),
		[42]=v2fM(0,201612903225806,0,230769230769231),
		[43]=v2fM(0,241935483870968,0,230769230769231),
		[44]=v2fM(0,282258064516129,0,230769230769231),
		[45]=v2fM(0,32258064516129,0,230769230769231),
		[46]=v2fM(0,362903225806452,0,230769230769231),
		[47]=v2fM(0,403225806451613,0,230769230769231),
		[48]=v2fM(0,443548387096774,0,230769230769231),
		[49]=v2fM(0,0,307692307692308),
		[50]=v2fM(0,0403225806451613,0,307692307692308),
		[51]=v2fM(0,0806451612903226,0,307692307692308),
		[52]=v2fM(0,120967741935484,0,307692307692308),
		[53]=v2fM(0,161290322580645,0,307692307692308),
		[54]=v2fM(0,201612903225806,0,307692307692308),
		[55]=v2fM(0,241935483870968,0,307692307692308),
		[56]=v2fM(0,282258064516129,0,307692307692308),
		[57]=v2fM(0,32258064516129,0,307692307692308),
		[58]=v2fM(0,362903225806452,0,307692307692308),
		[59]=v2fM(0,403225806451613,0,307692307692308),
		[60]=v2fM(0,443548387096774,0,307692307692308),
		[61]=v2fM(0,0,384615384615385),
		[62]=v2fM(0,0403225806451613,0,384615384615385),
		[63]=v2fM(0,0806451612903226,0,384615384615385),
		[64]=v2fM(0,120967741935484,0,384615384615385),
		[65]=v2fM(0,161290322580645,0,384615384615385),
		[66]=v2fM(0,201612903225806,0,384615384615385),
		[67]=v2fM(0,241935483870968,0,384615384615385),
		[68]=v2fM(0,282258064516129,0,384615384615385),
		[69]=v2fM(0,32258064516129,0,384615384615385),
		[70]=v2fM(0,362903225806452,0,384615384615385),
		[71]=v2fM(0,403225806451613,0,384615384615385),
		[72]=v2fM(0,443548387096774,0,384615384615385),
		[73]=v2fM(0,0,461538461538462),
		[74]=v2fM(0,0403225806451613,0,461538461538462),
		[75]=v2fM(0,0806451612903226,0,461538461538462),
		[76]=v2fM(0,120967741935484,0,461538461538462),
		[77]=v2fM(0,161290322580645,0,461538461538462),
		[78]=v2fM(0,201612903225806,0,461538461538462),
		[79]=v2fM(0,241935483870968,0,461538461538462),
		[80]=v2fM(0,282258064516129,0,461538461538462),
		[81]=v2fM(0,32258064516129,0,461538461538462),
		[82]=v2fM(0,362903225806452,0,461538461538462),
		[83]=v2fM(0,403225806451613,0,461538461538462),
		[84]=v2fM(0,443548387096774,0,461538461538462),
		[85]=v2fM(0,0,538461538461538),
		[86]=v2fM(0,0403225806451613,0,538461538461538),
		[87]=v2fM(0,0806451612903226,0,538461538461538),
		[88]=v2fM(0,120967741935484,0,538461538461538),
		[89]=v2fM(0,161290322580645,0,538461538461538),
		[90]=v2fM(0,201612903225806,0,538461538461538),
		[91]=v2fM(0,241935483870968,0,538461538461538),
		[92]=v2fM(0,282258064516129,0,538461538461538),
		[93]=v2fM(0,32258064516129,0,538461538461538),
		[94]=v2fM(0,362903225806452,0,538461538461538),
		[95]=v2fM(0,403225806451613,0,538461538461538),
		[96]=v2fM(0,443548387096774,0,538461538461538),
		[97]=v2fM(0,0,615384615384615),
		[98]=v2fM(0,0403225806451613,0,615384615384615),
		[99]=v2fM(0,0806451612903226,0,615384615384615),
		[100]=v2fM(0,120967741935484,0,615384615384615),
		[101]=v2fM(0,161290322580645,0,615384615384615),
		[102]=v2fM(0,201612903225806,0,615384615384615),
		[103]=v2fM(0,241935483870968,0,615384615384615),
		[104]=v2fM(0,282258064516129,0,615384615384615),
		[105]=v2fM(0,32258064516129,0,615384615384615),
		[106]=v2fM(0,362903225806452,0,615384615384615),
		[107]=v2fM(0,403225806451613,0,615384615384615),
		[108]=v2fM(0,443548387096774,0,615384615384615),
		[109]=v2fM(0,0,692307692307692),
		[110]=v2fM(0,0403225806451613,0,692307692307692),
		[111]=v2fM(0,0806451612903226,0,692307692307692),
		[112]=v2fM(0,120967741935484,0,692307692307692),
		[113]=v2fM(0,161290322580645,0,692307692307692),
		[114]=v2fM(0,201612903225806,0,692307692307692),
		[115]=v2fM(0,241935483870968,0,692307692307692),
		[116]=v2fM(0,282258064516129,0,692307692307692),
		[117]=v2fM(0,32258064516129,0,692307692307692),
		[118]=v2fM(0,362903225806452,0,692307692307692),
		[119]=v2fM(0,403225806451613,0,692307692307692),
		[120]=v2fM(0,443548387096774,0,692307692307692),
		[121]=v2fM(0,0,769230769230769),
		[122]=v2fM(0,0403225806451613,0,769230769230769),
		[123]=v2fM(0,0806451612903226,0,769230769230769),
		[124]=v2fM(0,120967741935484,0,769230769230769),
		[125]=v2fM(0,161290322580645,0,769230769230769),
		[126]=v2fM(0,201612903225806,0,769230769230769),
		[127]=v2fM(0,241935483870968,0,769230769230769),
		[128]=v2fM(0,282258064516129,0,769230769230769),
		[129]=v2fM(0,32258064516129,0,769230769230769),
		[130]=v2fM(0,362903225806452,0,769230769230769),
		[131]=v2fM(0,403225806451613,0,769230769230769),
		[132]=v2fM(0,443548387096774,0,769230769230769),
		[133]=v2fM(0,0,846153846153846),
		[134]=v2fM(0,0403225806451613,0,846153846153846),
		[135]=v2fM(0,0806451612903226,0,846153846153846),
		[136]=v2fM(0,120967741935484,0,846153846153846),
		[137]=v2fM(0,161290322580645,0,846153846153846),
		[138]=v2fM(0,201612903225806,0,846153846153846),
		[139]=v2fM(0,241935483870968,0,846153846153846),
		[140]=v2fM(0,282258064516129,0,846153846153846),
		[141]=v2fM(0,32258064516129,0,846153846153846),
		[142]=v2fM(0,362903225806452,0,846153846153846),
		[143]=v2fM(0,403225806451613,0,846153846153846),
		[144]=v2fM(0,443548387096774,0,846153846153846),
		[145]=v2fM(0,0,923076923076923),
		[146]=v2fM(0,0403225806451613,0,923076923076923),
		[147]=v2fM(0,0806451612903226,0,923076923076923),
		[148]=v2fM(0,120967741935484,0,923076923076923),
		[149]=v2fM(0,161290322580645,0,923076923076923),
		[150]=v2fM(0,201612903225806,0,923076923076923),
		[151]=v2fM(0,241935483870968,0,923076923076923),
		[152]=v2fM(0,282258064516129,0,923076923076923),
		[153]=v2fM(0,32258064516129,0,923076923076923),
		[154]=v2fM(0,362903225806452,0,923076923076923),
		[155]=v2fM(0,403225806451613,0,923076923076923),
		[156]=v2fM(0,443548387096774,0,923076923076923),
		[157]=v2fM(0,516129032258065,0),
		[158]=v2fM(0,556451612903226,0),
		[159]=v2fM(0,596774193548387,0),
		[160]=v2fM(0,637096774193548,0),
		[161]=v2fM(0,67741935483871,0),
		[162]=v2fM(0,717741935483871,0),
		[163]=v2fM(0,758064516129032,0),
		[164]=v2fM(0,798387096774194,0),
		[165]=v2fM(0,838709677419355,0),
		[166]=v2fM(0,879032258064516,0),
		[167]=v2fM(0,919354838709677,0),
		[168]=v2fM(0,959677419354839,0),
		[169]=v2fM(0,516129032258065,0,0769230769230769),
		[170]=v2fM(0,556451612903226,0,0769230769230769),
		[171]=v2fM(0,596774193548387,0,0769230769230769),
		[172]=v2fM(0,637096774193548,0,0769230769230769),
		[173]=v2fM(0,67741935483871,0,0769230769230769),
		[174]=v2fM(0,717741935483871,0,0769230769230769),
		[175]=v2fM(0,758064516129032,0,0769230769230769),
		[176]=v2fM(0,798387096774194,0,0769230769230769),
		[177]=v2fM(0,838709677419355,0,0769230769230769),
		[178]=v2fM(0,879032258064516,0,0769230769230769),
		[179]=v2fM(0,919354838709677,0,0769230769230769),
		[180]=v2fM(0,959677419354839,0,0769230769230769),
		[181]=v2fM(0,516129032258065,0,153846153846154),
		[182]=v2fM(0,556451612903226,0,153846153846154),
		[183]=v2fM(0,596774193548387,0,153846153846154),
		[184]=v2fM(0,637096774193548,0,153846153846154),
		[185]=v2fM(0,67741935483871,0,153846153846154),
		[186]=v2fM(0,717741935483871,0,153846153846154),
		[187]=v2fM(0,758064516129032,0,153846153846154),
		[188]=v2fM(0,798387096774194,0,153846153846154),
		[189]=v2fM(0,838709677419355,0,153846153846154),
		[190]=v2fM(0,879032258064516,0,153846153846154),
		[191]=v2fM(0,919354838709677,0,153846153846154),
		[192]=v2fM(0,959677419354839,0,153846153846154),
		[193]=v2fM(0,516129032258065,0,230769230769231),
		[194]=v2fM(0,556451612903226,0,230769230769231),
		[195]=v2fM(0,596774193548387,0,230769230769231),
		[196]=v2fM(0,637096774193548,0,230769230769231),
		[197]=v2fM(0,67741935483871,0,230769230769231),
		[198]=v2fM(0,717741935483871,0,230769230769231),
		[199]=v2fM(0,758064516129032,0,230769230769231),
		[200]=v2fM(0,798387096774194,0,230769230769231),
		[201]=v2fM(0,838709677419355,0,230769230769231),
		[202]=v2fM(0,879032258064516,0,230769230769231),
		[203]=v2fM(0,919354838709677,0,230769230769231),
		[204]=v2fM(0,959677419354839,0,230769230769231),
		[205]=v2fM(0,516129032258065,0,307692307692308),
		[206]=v2fM(0,556451612903226,0,307692307692308),
		[207]=v2fM(0,596774193548387,0,307692307692308),
		[208]=v2fM(0,637096774193548,0,307692307692308),
		[209]=v2fM(0,67741935483871,0,307692307692308),
		[210]=v2fM(0,717741935483871,0,307692307692308),
		[211]=v2fM(0,758064516129032,0,307692307692308),
		[212]=v2fM(0,798387096774194,0,307692307692308),
		[213]=v2fM(0,838709677419355,0,307692307692308),
		[214]=v2fM(0,879032258064516,0,307692307692308),
		[215]=v2fM(0,919354838709677,0,307692307692308),
		[216]=v2fM(0,959677419354839,0,307692307692308),
		[217]=v2fM(0,516129032258065,0,384615384615385),
		[218]=v2fM(0,556451612903226,0,384615384615385),
		[219]=v2fM(0,596774193548387,0,384615384615385),
		[220]=v2fM(0,637096774193548,0,384615384615385),
		[221]=v2fM(0,67741935483871,0,384615384615385),
		[222]=v2fM(0,717741935483871,0,384615384615385),
		[223]=v2fM(0,758064516129032,0,384615384615385),
		[224]=v2fM(0,798387096774194,0,384615384615385),
		[225]=v2fM(0,838709677419355,0,384615384615385),
		[226]=v2fM(0,879032258064516,0,384615384615385),
		[227]=v2fM(0,919354838709677,0,384615384615385),
		[228]=v2fM(0,959677419354839,0,384615384615385),
		[229]=v2fM(0,516129032258065,0,461538461538462),
		[230]=v2fM(0,556451612903226,0,461538461538462),
		[231]=v2fM(0,596774193548387,0,461538461538462),
		[232]=v2fM(0,637096774193548,0,461538461538462),
		[233]=v2fM(0,67741935483871,0,461538461538462),
		[234]=v2fM(0,717741935483871,0,461538461538462),
		[235]=v2fM(0,758064516129032,0,461538461538462),
		[236]=v2fM(0,798387096774194,0,461538461538462),
		[237]=v2fM(0,838709677419355,0,461538461538462),
		[238]=v2fM(0,879032258064516,0,461538461538462),
		[239]=v2fM(0,919354838709677,0,461538461538462),
		[240]=v2fM(0,959677419354839,0,461538461538462),
		[241]=v2fM(0,516129032258065,0,538461538461538),
		[242]=v2fM(0,556451612903226,0,538461538461538),
		[243]=v2fM(0,596774193548387,0,538461538461538),
		[244]=v2fM(0,637096774193548,0,538461538461538),
		[245]=v2fM(0,67741935483871,0,538461538461538),
		[246]=v2fM(0,717741935483871,0,538461538461538),
		[247]=v2fM(0,758064516129032,0,538461538461538),
		[248]=v2fM(0,798387096774194,0,538461538461538),
		[249]=v2fM(0,838709677419355,0,538461538461538),
		[250]=v2fM(0,879032258064516,0,538461538461538),
		[251]=v2fM(0,919354838709677,0,538461538461538),
		[252]=v2fM(0,959677419354839,0,538461538461538),
		[253]=v2fM(0,516129032258065,0,615384615384615),
		[254]=v2fM(0,556451612903226,0,615384615384615),
		[255]=v2fM(0,596774193548387,0,615384615384615),
		[256]=v2fM(0,637096774193548,0,615384615384615),
		[257]=v2fM(0,67741935483871,0,615384615384615),
		[258]=v2fM(0,717741935483871,0,615384615384615),
		[259]=v2fM(0,758064516129032,0,615384615384615),
		[260]=v2fM(0,798387096774194,0,615384615384615),
		[261]=v2fM(0,838709677419355,0,615384615384615),
		[262]=v2fM(0,879032258064516,0,615384615384615),
		[263]=v2fM(0,919354838709677,0,615384615384615),
		[264]=v2fM(0,959677419354839,0,615384615384615),
		[265]=v2fM(0,516129032258065,0,692307692307692),
		[266]=v2fM(0,556451612903226,0,692307692307692),
		[267]=v2fM(0,596774193548387,0,692307692307692),
		[268]=v2fM(0,637096774193548,0,692307692307692),
		[269]=v2fM(0,67741935483871,0,692307692307692),
		[270]=v2fM(0,717741935483871,0,692307692307692),
		[271]=v2fM(0,758064516129032,0,692307692307692),
		[272]=v2fM(0,798387096774194,0,692307692307692),
		[273]=v2fM(0,838709677419355,0,692307692307692),
		[274]=v2fM(0,879032258064516,0,692307692307692),
		[275]=v2fM(0,919354838709677,0,692307692307692),
		[276]=v2fM(0,959677419354839,0,692307692307692),
		[277]=v2fM(0,516129032258065,0,769230769230769),
		[278]=v2fM(0,556451612903226,0,769230769230769),
		[279]=v2fM(0,596774193548387,0,769230769230769),
		[280]=v2fM(0,637096774193548,0,769230769230769),
		[281]=v2fM(0,67741935483871,0,769230769230769),
		[282]=v2fM(0,717741935483871,0,769230769230769),
		[283]=v2fM(0,758064516129032,0,769230769230769),
		[284]=v2fM(0,798387096774194,0,769230769230769),
		[285]=v2fM(0,838709677419355,0,769230769230769),
		[286]=v2fM(0,879032258064516,0,769230769230769),
		[287]=v2fM(0,919354838709677,0,769230769230769),
		[288]=v2fM(0,959677419354839,0,769230769230769),
	},
	FRAME_SIZES={
		[1]={WIDTH=80,HEIGHT=78},
		[2]={WIDTH=80,HEIGHT=78},
		[3]={WIDTH=80,HEIGHT=78},
		[4]={WIDTH=80,HEIGHT=78},
		[5]={WIDTH=80,HEIGHT=78},
		[6]={WIDTH=80,HEIGHT=78},
		[7]={WIDTH=80,HEIGHT=78},
		[8]={WIDTH=80,HEIGHT=78},
		[9]={WIDTH=80,HEIGHT=78},
		[10]={WIDTH=80,HEIGHT=78},
		[11]={WIDTH=80,HEIGHT=78},
		[12]={WIDTH=80,HEIGHT=78},
		[13]={WIDTH=80,HEIGHT=78},
		[14]={WIDTH=80,HEIGHT=78},
		[15]={WIDTH=80,HEIGHT=78},
		[16]={WIDTH=80,HEIGHT=78},
		[17]={WIDTH=80,HEIGHT=78},
		[18]={WIDTH=80,HEIGHT=78},
		[19]={WIDTH=80,HEIGHT=78},
		[20]={WIDTH=80,HEIGHT=78},
		[21]={WIDTH=80,HEIGHT=78},
		[22]={WIDTH=80,HEIGHT=78},
		[23]={WIDTH=80,HEIGHT=78},
		[24]={WIDTH=80,HEIGHT=78},
		[25]={WIDTH=80,HEIGHT=78},
		[26]={WIDTH=80,HEIGHT=78},
		[27]={WIDTH=80,HEIGHT=78},
		[28]={WIDTH=80,HEIGHT=78},
		[29]={WIDTH=80,HEIGHT=78},
		[30]={WIDTH=80,HEIGHT=78},
		[31]={WIDTH=80,HEIGHT=78},
		[32]={WIDTH=80,HEIGHT=78},
		[33]={WIDTH=80,HEIGHT=78},
		[34]={WIDTH=80,HEIGHT=78},
		[35]={WIDTH=80,HEIGHT=78},
		[36]={WIDTH=80,HEIGHT=78},
		[37]={WIDTH=80,HEIGHT=78},
		[38]={WIDTH=80,HEIGHT=78},
		[39]={WIDTH=80,HEIGHT=78},
		[40]={WIDTH=80,HEIGHT=78},
		[41]={WIDTH=80,HEIGHT=78},
		[42]={WIDTH=80,HEIGHT=78},
		[43]={WIDTH=80,HEIGHT=78},
		[44]={WIDTH=80,HEIGHT=78},
		[45]={WIDTH=80,HEIGHT=78},
		[46]={WIDTH=80,HEIGHT=78},
		[47]={WIDTH=80,HEIGHT=78},
		[48]={WIDTH=80,HEIGHT=78},
		[49]={WIDTH=80,HEIGHT=78},
		[50]={WIDTH=80,HEIGHT=78},
		[51]={WIDTH=80,HEIGHT=78},
		[52]={WIDTH=80,HEIGHT=78},
		[53]={WIDTH=80,HEIGHT=78},
		[54]={WIDTH=80,HEIGHT=78},
		[55]={WIDTH=80,HEIGHT=78},
		[56]={WIDTH=80,HEIGHT=78},
		[57]={WIDTH=80,HEIGHT=78},
		[58]={WIDTH=80,HEIGHT=78},
		[59]={WIDTH=80,HEIGHT=78},
		[60]={WIDTH=80,HEIGHT=78},
		[61]={WIDTH=80,HEIGHT=78},
		[62]={WIDTH=80,HEIGHT=78},
		[63]={WIDTH=80,HEIGHT=78},
		[64]={WIDTH=80,HEIGHT=78},
		[65]={WIDTH=80,HEIGHT=78},
		[66]={WIDTH=80,HEIGHT=78},
		[67]={WIDTH=80,HEIGHT=78},
		[68]={WIDTH=80,HEIGHT=78},
		[69]={WIDTH=80,HEIGHT=78},
		[70]={WIDTH=80,HEIGHT=78},
		[71]={WIDTH=80,HEIGHT=78},
		[72]={WIDTH=80,HEIGHT=78},
		[73]={WIDTH=80,HEIGHT=78},
		[74]={WIDTH=80,HEIGHT=78},
		[75]={WIDTH=80,HEIGHT=78},
		[76]={WIDTH=80,HEIGHT=78},
		[77]={WIDTH=80,HEIGHT=78},
		[78]={WIDTH=80,HEIGHT=78},
		[79]={WIDTH=80,HEIGHT=78},
		[80]={WIDTH=80,HEIGHT=78},
		[81]={WIDTH=80,HEIGHT=78},
		[82]={WIDTH=80,HEIGHT=78},
		[83]={WIDTH=80,HEIGHT=78},
		[84]={WIDTH=80,HEIGHT=78},
		[85]={WIDTH=80,HEIGHT=78},
		[86]={WIDTH=80,HEIGHT=78},
		[87]={WIDTH=80,HEIGHT=78},
		[88]={WIDTH=80,HEIGHT=78},
		[89]={WIDTH=80,HEIGHT=78},
		[90]={WIDTH=80,HEIGHT=78},
		[91]={WIDTH=80,HEIGHT=78},
		[92]={WIDTH=80,HEIGHT=78},
		[93]={WIDTH=80,HEIGHT=78},
		[94]={WIDTH=80,HEIGHT=78},
		[95]={WIDTH=80,HEIGHT=78},
		[96]={WIDTH=80,HEIGHT=78},
		[97]={WIDTH=80,HEIGHT=78},
		[98]={WIDTH=80,HEIGHT=78},
		[99]={WIDTH=80,HEIGHT=78},
		[100]={WIDTH=80,HEIGHT=78},
		[101]={WIDTH=80,HEIGHT=78},
		[102]={WIDTH=80,HEIGHT=78},
		[103]={WIDTH=80,HEIGHT=78},
		[104]={WIDTH=80,HEIGHT=78},
		[105]={WIDTH=80,HEIGHT=78},
		[106]={WIDTH=80,HEIGHT=78},
		[107]={WIDTH=80,HEIGHT=78},
		[108]={WIDTH=80,HEIGHT=78},
		[109]={WIDTH=80,HEIGHT=78},
		[110]={WIDTH=80,HEIGHT=78},
		[111]={WIDTH=80,HEIGHT=78},
		[112]={WIDTH=80,HEIGHT=78},
		[113]={WIDTH=80,HEIGHT=78},
		[114]={WIDTH=80,HEIGHT=78},
		[115]={WIDTH=80,HEIGHT=78},
		[116]={WIDTH=80,HEIGHT=78},
		[117]={WIDTH=80,HEIGHT=78},
		[118]={WIDTH=80,HEIGHT=78},
		[119]={WIDTH=80,HEIGHT=78},
		[120]={WIDTH=80,HEIGHT=78},
		[121]={WIDTH=80,HEIGHT=78},
		[122]={WIDTH=80,HEIGHT=78},
		[123]={WIDTH=80,HEIGHT=78},
		[124]={WIDTH=80,HEIGHT=78},
		[125]={WIDTH=80,HEIGHT=78},
		[126]={WIDTH=80,HEIGHT=78},
		[127]={WIDTH=80,HEIGHT=78},
		[128]={WIDTH=80,HEIGHT=78},
		[129]={WIDTH=80,HEIGHT=78},
		[130]={WIDTH=80,HEIGHT=78},
		[131]={WIDTH=80,HEIGHT=78},
		[132]={WIDTH=80,HEIGHT=78},
		[133]={WIDTH=80,HEIGHT=78},
		[134]={WIDTH=80,HEIGHT=78},
		[135]={WIDTH=80,HEIGHT=78},
		[136]={WIDTH=80,HEIGHT=78},
		[137]={WIDTH=80,HEIGHT=78},
		[138]={WIDTH=80,HEIGHT=78},
		[139]={WIDTH=80,HEIGHT=78},
		[140]={WIDTH=80,HEIGHT=78},
		[141]={WIDTH=80,HEIGHT=78},
		[142]={WIDTH=80,HEIGHT=78},
		[143]={WIDTH=80,HEIGHT=78},
		[144]={WIDTH=80,HEIGHT=78},
		[145]={WIDTH=80,HEIGHT=78},
		[146]={WIDTH=80,HEIGHT=78},
		[147]={WIDTH=80,HEIGHT=78},
		[148]={WIDTH=80,HEIGHT=78},
		[149]={WIDTH=80,HEIGHT=78},
		[150]={WIDTH=80,HEIGHT=78},
		[151]={WIDTH=80,HEIGHT=78},
		[152]={WIDTH=80,HEIGHT=78},
		[153]={WIDTH=80,HEIGHT=78},
		[154]={WIDTH=80,HEIGHT=78},
		[155]={WIDTH=80,HEIGHT=78},
		[156]={WIDTH=80,HEIGHT=78},
		[157]={WIDTH=80,HEIGHT=78},
		[158]={WIDTH=80,HEIGHT=78},
		[159]={WIDTH=80,HEIGHT=78},
		[160]={WIDTH=80,HEIGHT=78},
		[161]={WIDTH=80,HEIGHT=78},
		[162]={WIDTH=80,HEIGHT=78},
		[163]={WIDTH=80,HEIGHT=78},
		[164]={WIDTH=80,HEIGHT=78},
		[165]={WIDTH=80,HEIGHT=78},
		[166]={WIDTH=80,HEIGHT=78},
		[167]={WIDTH=80,HEIGHT=78},
		[168]={WIDTH=80,HEIGHT=78},
		[169]={WIDTH=80,HEIGHT=78},
		[170]={WIDTH=80,HEIGHT=78},
		[171]={WIDTH=80,HEIGHT=78},
		[172]={WIDTH=80,HEIGHT=78},
		[173]={WIDTH=80,HEIGHT=78},
		[174]={WIDTH=80,HEIGHT=78},
		[175]={WIDTH=80,HEIGHT=78},
		[176]={WIDTH=80,HEIGHT=78},
		[177]={WIDTH=80,HEIGHT=78},
		[178]={WIDTH=80,HEIGHT=78},
		[179]={WIDTH=80,HEIGHT=78},
		[180]={WIDTH=80,HEIGHT=78},
		[181]={WIDTH=80,HEIGHT=78},
		[182]={WIDTH=80,HEIGHT=78},
		[183]={WIDTH=80,HEIGHT=78},
		[184]={WIDTH=80,HEIGHT=78},
		[185]={WIDTH=80,HEIGHT=78},
		[186]={WIDTH=80,HEIGHT=78},
		[187]={WIDTH=80,HEIGHT=78},
		[188]={WIDTH=80,HEIGHT=78},
		[189]={WIDTH=80,HEIGHT=78},
		[190]={WIDTH=80,HEIGHT=78},
		[191]={WIDTH=80,HEIGHT=78},
		[192]={WIDTH=80,HEIGHT=78},
		[193]={WIDTH=80,HEIGHT=78},
		[194]={WIDTH=80,HEIGHT=78},
		[195]={WIDTH=80,HEIGHT=78},
		[196]={WIDTH=80,HEIGHT=78},
		[197]={WIDTH=80,HEIGHT=78},
		[198]={WIDTH=80,HEIGHT=78},
		[199]={WIDTH=80,HEIGHT=78},
		[200]={WIDTH=80,HEIGHT=78},
		[201]={WIDTH=80,HEIGHT=78},
		[202]={WIDTH=80,HEIGHT=78},
		[203]={WIDTH=80,HEIGHT=78},
		[204]={WIDTH=80,HEIGHT=78},
		[205]={WIDTH=80,HEIGHT=78},
		[206]={WIDTH=80,HEIGHT=78},
		[207]={WIDTH=80,HEIGHT=78},
		[208]={WIDTH=80,HEIGHT=78},
		[209]={WIDTH=80,HEIGHT=78},
		[210]={WIDTH=80,HEIGHT=78},
		[211]={WIDTH=80,HEIGHT=78},
		[212]={WIDTH=80,HEIGHT=78},
		[213]={WIDTH=80,HEIGHT=78},
		[214]={WIDTH=80,HEIGHT=78},
		[215]={WIDTH=80,HEIGHT=78},
		[216]={WIDTH=80,HEIGHT=78},
		[217]={WIDTH=80,HEIGHT=78},
		[218]={WIDTH=80,HEIGHT=78},
		[219]={WIDTH=80,HEIGHT=78},
		[220]={WIDTH=80,HEIGHT=78},
		[221]={WIDTH=80,HEIGHT=78},
		[222]={WIDTH=80,HEIGHT=78},
		[223]={WIDTH=80,HEIGHT=78},
		[224]={WIDTH=80,HEIGHT=78},
		[225]={WIDTH=80,HEIGHT=78},
		[226]={WIDTH=80,HEIGHT=78},
		[227]={WIDTH=80,HEIGHT=78},
		[228]={WIDTH=80,HEIGHT=78},
		[229]={WIDTH=80,HEIGHT=78},
		[230]={WIDTH=80,HEIGHT=78},
		[231]={WIDTH=80,HEIGHT=78},
		[232]={WIDTH=80,HEIGHT=78},
		[233]={WIDTH=80,HEIGHT=78},
		[234]={WIDTH=80,HEIGHT=78},
		[235]={WIDTH=80,HEIGHT=78},
		[236]={WIDTH=80,HEIGHT=78},
		[237]={WIDTH=80,HEIGHT=78},
		[238]={WIDTH=80,HEIGHT=78},
		[239]={WIDTH=80,HEIGHT=78},
		[240]={WIDTH=80,HEIGHT=78},
		[241]={WIDTH=80,HEIGHT=78},
		[242]={WIDTH=80,HEIGHT=78},
		[243]={WIDTH=80,HEIGHT=78},
		[244]={WIDTH=80,HEIGHT=78},
		[245]={WIDTH=80,HEIGHT=78},
		[246]={WIDTH=80,HEIGHT=78},
		[247]={WIDTH=80,HEIGHT=78},
		[248]={WIDTH=80,HEIGHT=78},
		[249]={WIDTH=80,HEIGHT=78},
		[250]={WIDTH=80,HEIGHT=78},
		[251]={WIDTH=80,HEIGHT=78},
		[252]={WIDTH=80,HEIGHT=78},
		[253]={WIDTH=80,HEIGHT=78},
		[254]={WIDTH=80,HEIGHT=78},
		[255]={WIDTH=80,HEIGHT=78},
		[256]={WIDTH=80,HEIGHT=78},
		[257]={WIDTH=80,HEIGHT=78},
		[258]={WIDTH=80,HEIGHT=78},
		[259]={WIDTH=80,HEIGHT=78},
		[260]={WIDTH=80,HEIGHT=78},
		[261]={WIDTH=80,HEIGHT=78},
		[262]={WIDTH=80,HEIGHT=78},
		[263]={WIDTH=80,HEIGHT=78},
		[264]={WIDTH=80,HEIGHT=78},
		[265]={WIDTH=80,HEIGHT=78},
		[266]={WIDTH=80,HEIGHT=78},
		[267]={WIDTH=80,HEIGHT=78},
		[268]={WIDTH=80,HEIGHT=78},
		[269]={WIDTH=80,HEIGHT=78},
		[270]={WIDTH=80,HEIGHT=78},
		[271]={WIDTH=80,HEIGHT=78},
		[272]={WIDTH=80,HEIGHT=78},
		[273]={WIDTH=80,HEIGHT=78},
		[274]={WIDTH=80,HEIGHT=78},
		[275]={WIDTH=80,HEIGHT=78},
		[276]={WIDTH=80,HEIGHT=78},
		[277]={WIDTH=80,HEIGHT=78},
		[278]={WIDTH=80,HEIGHT=78},
		[279]={WIDTH=80,HEIGHT=78},
		[280]={WIDTH=80,HEIGHT=78},
		[281]={WIDTH=80,HEIGHT=78},
		[282]={WIDTH=80,HEIGHT=78},
		[283]={WIDTH=80,HEIGHT=78},
		[284]={WIDTH=80,HEIGHT=78},
		[285]={WIDTH=80,HEIGHT=78},
		[286]={WIDTH=80,HEIGHT=78},
		[287]={WIDTH=80,HEIGHT=78},
		[288]={WIDTH=80,HEIGHT=78},
	},
	FRAME_INDEXS={
		['w-u-double0191s']=1,
		['w-u-double0192s']=2,
		['w-u-double0189s']=3,
		['w-u-double0190s']=4,
		['w-u-double0193s']=5,
		['w-u-double0196s']=6,
		['w-u-double0197s']=7,
		['w-u-double0194s']=8,
		['w-u-double0195s']=9,
		['w-u-double0182s']=10,
		['w-u-double0183s']=11,
		['w-u-double0180s']=12,
		['w-u-double0181s']=13,
		['w-u-double0184s']=14,
		['w-u-double0187s']=15,
		['w-u-double0188s']=16,
		['w-u-double0185s']=17,
		['w-u-double0186s']=18,
		['w-u-double0209s']=19,
		['w-u-double0210s']=20,
		['w-u-double0207s']=21,
		['w-u-double0208s']=22,
		['w-u-double0211s']=23,
		['w-u-double0214s']=24,
		['w-u-double0215s']=25,
		['w-u-double0212s']=26,
		['w-u-double0213s']=27,
		['w-u-double0200s']=28,
		['w-u-double0201s']=29,
		['w-u-double0198s']=30,
		['w-u-double0199s']=31,
		['w-u-double0202s']=32,
		['w-u-double0205s']=33,
		['w-u-double0206s']=34,
		['w-u-double0203s']=35,
		['w-u-double0204s']=36,
		['w-u-double0155s']=37,
		['w-u-double0156s']=38,
		['w-u-double0153s']=39,
		['w-u-double0154s']=40,
		['w-u-double0157s']=41,
		['w-u-double0160s']=42,
		['w-u-double0161s']=43,
		['w-u-double0158s']=44,
		['w-u-double0159s']=45,
		['w-u-double0146s']=46,
		['w-u-double0147s']=47,
		['w-u-double0144s']=48,
		['w-u-double0145s']=49,
		['w-u-double0148s']=50,
		['w-u-double0151s']=51,
		['w-u-double0152s']=52,
		['w-u-double0149s']=53,
		['w-u-double0150s']=54,
		['w-u-double0173s']=55,
		['w-u-double0174s']=56,
		['w-u-double0171s']=57,
		['w-u-double0172s']=58,
		['w-u-double0175s']=59,
		['w-u-double0178s']=60,
		['w-u-double0179s']=61,
		['w-u-double0176s']=62,
		['w-u-double0177s']=63,
		['w-u-double0164s']=64,
		['w-u-double0165s']=65,
		['w-u-double0162s']=66,
		['w-u-double0163s']=67,
		['w-u-double0166s']=68,
		['w-u-double0169s']=69,
		['w-u-double0170s']=70,
		['w-u-double0167s']=71,
		['w-u-double0168s']=72,
		['w-u-double0263s']=73,
		['w-u-double0264s']=74,
		['w-u-double0261s']=75,
		['w-u-double0262s']=76,
		['w-u-double0265s']=77,
		['w-u-double0268s']=78,
		['w-u-double0269s']=79,
		['w-u-double0266s']=80,
		['w-u-double0267s']=81,
		['w-u-double0254s']=82,
		['w-u-double0255s']=83,
		['w-u-double0252s']=84,
		['w-u-double0253s']=85,
		['w-u-double0256s']=86,
		['w-u-double0259s']=87,
		['w-u-double0260s']=88,
		['w-u-double0257s']=89,
		['w-u-double0258s']=90,
		['w-u-double0281s']=91,
		['w-u-double0282s']=92,
		['w-u-double0279s']=93,
		['w-u-double0280s']=94,
		['w-u-double0283s']=95,
		['w-u-double0286s']=96,
		['w-u-double0287s']=97,
		['w-u-double0284s']=98,
		['w-u-double0285s']=99,
		['w-u-double0272s']=100,
		['w-u-double0273s']=101,
		['w-u-double0270s']=102,
		['w-u-double0271s']=103,
		['w-u-double0274s']=104,
		['w-u-double0277s']=105,
		['w-u-double0278s']=106,
		['w-u-double0275s']=107,
		['w-u-double0276s']=108,
		['w-u-double0227s']=109,
		['w-u-double0228s']=110,
		['w-u-double0225s']=111,
		['w-u-double0226s']=112,
		['w-u-double0229s']=113,
		['w-u-double0232s']=114,
		['w-u-double0233s']=115,
		['w-u-double0230s']=116,
		['w-u-double0231s']=117,
		['w-u-double0218s']=118,
		['w-u-double0219s']=119,
		['w-u-double0216s']=120,
		['w-u-double0217s']=121,
		['w-u-double0220s']=122,
		['w-u-double0223s']=123,
		['w-u-double0224s']=124,
		['w-u-double0221s']=125,
		['w-u-double0222s']=126,
		['w-u-double0245s']=127,
		['w-u-double0246s']=128,
		['w-u-double0243s']=129,
		['w-u-double0244s']=130,
		['w-u-double0247s']=131,
		['w-u-double0250s']=132,
		['w-u-double0251s']=133,
		['w-u-double0248s']=134,
		['w-u-double0249s']=135,
		['w-u-double0236s']=136,
		['w-u-double0237s']=137,
		['w-u-double0234s']=138,
		['w-u-double0235s']=139,
		['w-u-double0238s']=140,
		['w-u-double0241s']=141,
		['w-u-double0242s']=142,
		['w-u-double0239s']=143,
		['w-u-double0240s']=144,
		['w-u-double0047s']=145,
		['w-u-double0048s']=146,
		['w-u-double0045s']=147,
		['w-u-double0046s']=148,
		['w-u-double0049s']=149,
		['w-u-double0052s']=150,
		['w-u-double0053s']=151,
		['w-u-double0050s']=152,
		['w-u-double0051s']=153,
		['w-u-double0038s']=154,
		['w-u-double0039s']=155,
		['w-u-double0036s']=156,
		['w-u-double0037s']=157,
		['w-u-double0040s']=158,
		['w-u-double0043s']=159,
		['w-u-double0044s']=160,
		['w-u-double0041s']=161,
		['w-u-double0042s']=162,
		['w-u-double0065s']=163,
		['w-u-double0066s']=164,
		['w-u-double0063s']=165,
		['w-u-double0064s']=166,
		['w-u-double0067s']=167,
		['w-u-double0070s']=168,
		['w-u-double0071s']=169,
		['w-u-double0068s']=170,
		['w-u-double0069s']=171,
		['w-u-double0056s']=172,
		['w-u-double0057s']=173,
		['w-u-double0054s']=174,
		['w-u-double0055s']=175,
		['w-u-double0058s']=176,
		['w-u-double0061s']=177,
		['w-u-double0062s']=178,
		['w-u-double0059s']=179,
		['w-u-double0060s']=180,
		['w-u-double0011s']=181,
		['w-u-double0012s']=182,
		['w-u-double0009s']=183,
		['w-u-double0010s']=184,
		['w-u-double0013s']=185,
		['w-u-double0016s']=186,
		['w-u-double0017s']=187,
		['w-u-double0014s']=188,
		['w-u-double0015s']=189,
		['w-u-double0002s']=190,
		['w-u-double0003s']=191,
		['w-u-double0000s']=192,
		['w-u-double0001s']=193,
		['w-u-double0004s']=194,
		['w-u-double0007s']=195,
		['w-u-double0008s']=196,
		['w-u-double0005s']=197,
		['w-u-double0006s']=198,
		['w-u-double0029s']=199,
		['w-u-double0030s']=200,
		['w-u-double0027s']=201,
		['w-u-double0028s']=202,
		['w-u-double0031s']=203,
		['w-u-double0034s']=204,
		['w-u-double0035s']=205,
		['w-u-double0032s']=206,
		['w-u-double0033s']=207,
		['w-u-double0020s']=208,
		['w-u-double0021s']=209,
		['w-u-double0018s']=210,
		['w-u-double0019s']=211,
		['w-u-double0022s']=212,
		['w-u-double0025s']=213,
		['w-u-double0026s']=214,
		['w-u-double0023s']=215,
		['w-u-double0024s']=216,
		['w-u-double0119s']=217,
		['w-u-double0120s']=218,
		['w-u-double0117s']=219,
		['w-u-double0118s']=220,
		['w-u-double0121s']=221,
		['w-u-double0124s']=222,
		['w-u-double0125s']=223,
		['w-u-double0122s']=224,
		['w-u-double0123s']=225,
		['w-u-double0110s']=226,
		['w-u-double0111s']=227,
		['w-u-double0108s']=228,
		['w-u-double0109s']=229,
		['w-u-double0112s']=230,
		['w-u-double0115s']=231,
		['w-u-double0116s']=232,
		['w-u-double0113s']=233,
		['w-u-double0114s']=234,
		['w-u-double0137s']=235,
		['w-u-double0138s']=236,
		['w-u-double0135s']=237,
		['w-u-double0136s']=238,
		['w-u-double0139s']=239,
		['w-u-double0142s']=240,
		['w-u-double0143s']=241,
		['w-u-double0140s']=242,
		['w-u-double0141s']=243,
		['w-u-double0128s']=244,
		['w-u-double0129s']=245,
		['w-u-double0126s']=246,
		['w-u-double0127s']=247,
		['w-u-double0130s']=248,
		['w-u-double0133s']=249,
		['w-u-double0134s']=250,
		['w-u-double0131s']=251,
		['w-u-double0132s']=252,
		['w-u-double0083s']=253,
		['w-u-double0084s']=254,
		['w-u-double0081s']=255,
		['w-u-double0082s']=256,
		['w-u-double0085s']=257,
		['w-u-double0088s']=258,
		['w-u-double0089s']=259,
		['w-u-double0086s']=260,
		['w-u-double0087s']=261,
		['w-u-double0074s']=262,
		['w-u-double0075s']=263,
		['w-u-double0072s']=264,
		['w-u-double0073s']=265,
		['w-u-double0076s']=266,
		['w-u-double0079s']=267,
		['w-u-double0080s']=268,
		['w-u-double0077s']=269,
		['w-u-double0078s']=270,
		['w-u-double0101s']=271,
		['w-u-double0102s']=272,
		['w-u-double0099s']=273,
		['w-u-double0100s']=274,
		['w-u-double0103s']=275,
		['w-u-double0106s']=276,
		['w-u-double0107s']=277,
		['w-u-double0104s']=278,
		['w-u-double0105s']=279,
		['w-u-double0092s']=280,
		['w-u-double0093s']=281,
		['w-u-double0090s']=282,
		['w-u-double0091s']=283,
		['w-u-double0094s']=284,
		['w-u-double0097s']=285,
		['w-u-double0098s']=286,
		['w-u-double0095s']=287,
		['w-u-double0096s']=288,
	},
};
