--GENERATED ON: 05/12/2024 07:01:05


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['w-u-heavys'] = {
	TEX_WIDTH=1984,
	TEX_HEIGHT=1012,
	FRAME_COUNT=216,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0483870967741935,0),
		[3]=v2fM(0,0967741935483871,0),
		[4]=v2fM(0,145161290322581,0),
		[5]=v2fM(0,193548387096774,0),
		[6]=v2fM(0,241935483870968,0),
		[7]=v2fM(0,290322580645161,0),
		[8]=v2fM(0,338709677419355,0),
		[9]=v2fM(0,387096774193548,0),
		[10]=v2fM(0,435483870967742,0),
		[11]=v2fM(0,0,0909090909090909),
		[12]=v2fM(0,0483870967741935,0,0909090909090909),
		[13]=v2fM(0,0967741935483871,0,0909090909090909),
		[14]=v2fM(0,145161290322581,0,0909090909090909),
		[15]=v2fM(0,193548387096774,0,0909090909090909),
		[16]=v2fM(0,241935483870968,0,0909090909090909),
		[17]=v2fM(0,290322580645161,0,0909090909090909),
		[18]=v2fM(0,338709677419355,0,0909090909090909),
		[19]=v2fM(0,387096774193548,0,0909090909090909),
		[20]=v2fM(0,435483870967742,0,0909090909090909),
		[21]=v2fM(0,0,181818181818182),
		[22]=v2fM(0,0483870967741935,0,181818181818182),
		[23]=v2fM(0,0967741935483871,0,181818181818182),
		[24]=v2fM(0,145161290322581,0,181818181818182),
		[25]=v2fM(0,193548387096774,0,181818181818182),
		[26]=v2fM(0,241935483870968,0,181818181818182),
		[27]=v2fM(0,290322580645161,0,181818181818182),
		[28]=v2fM(0,338709677419355,0,181818181818182),
		[29]=v2fM(0,387096774193548,0,181818181818182),
		[30]=v2fM(0,435483870967742,0,181818181818182),
		[31]=v2fM(0,0,272727272727273),
		[32]=v2fM(0,0483870967741935,0,272727272727273),
		[33]=v2fM(0,0967741935483871,0,272727272727273),
		[34]=v2fM(0,145161290322581,0,272727272727273),
		[35]=v2fM(0,193548387096774,0,272727272727273),
		[36]=v2fM(0,241935483870968,0,272727272727273),
		[37]=v2fM(0,290322580645161,0,272727272727273),
		[38]=v2fM(0,338709677419355,0,272727272727273),
		[39]=v2fM(0,387096774193548,0,272727272727273),
		[40]=v2fM(0,435483870967742,0,272727272727273),
		[41]=v2fM(0,0,363636363636364),
		[42]=v2fM(0,0483870967741935,0,363636363636364),
		[43]=v2fM(0,0967741935483871,0,363636363636364),
		[44]=v2fM(0,145161290322581,0,363636363636364),
		[45]=v2fM(0,193548387096774,0,363636363636364),
		[46]=v2fM(0,241935483870968,0,363636363636364),
		[47]=v2fM(0,290322580645161,0,363636363636364),
		[48]=v2fM(0,338709677419355,0,363636363636364),
		[49]=v2fM(0,387096774193548,0,363636363636364),
		[50]=v2fM(0,435483870967742,0,363636363636364),
		[51]=v2fM(0,0,454545454545455),
		[52]=v2fM(0,0483870967741935,0,454545454545455),
		[53]=v2fM(0,0967741935483871,0,454545454545455),
		[54]=v2fM(0,145161290322581,0,454545454545455),
		[55]=v2fM(0,193548387096774,0,454545454545455),
		[56]=v2fM(0,241935483870968,0,454545454545455),
		[57]=v2fM(0,290322580645161,0,454545454545455),
		[58]=v2fM(0,338709677419355,0,454545454545455),
		[59]=v2fM(0,387096774193548,0,454545454545455),
		[60]=v2fM(0,435483870967742,0,454545454545455),
		[61]=v2fM(0,0,545454545454545),
		[62]=v2fM(0,0483870967741935,0,545454545454545),
		[63]=v2fM(0,0967741935483871,0,545454545454545),
		[64]=v2fM(0,145161290322581,0,545454545454545),
		[65]=v2fM(0,193548387096774,0,545454545454545),
		[66]=v2fM(0,241935483870968,0,545454545454545),
		[67]=v2fM(0,290322580645161,0,545454545454545),
		[68]=v2fM(0,338709677419355,0,545454545454545),
		[69]=v2fM(0,387096774193548,0,545454545454545),
		[70]=v2fM(0,435483870967742,0,545454545454545),
		[71]=v2fM(0,0,636363636363636),
		[72]=v2fM(0,0483870967741935,0,636363636363636),
		[73]=v2fM(0,0967741935483871,0,636363636363636),
		[74]=v2fM(0,145161290322581,0,636363636363636),
		[75]=v2fM(0,193548387096774,0,636363636363636),
		[76]=v2fM(0,241935483870968,0,636363636363636),
		[77]=v2fM(0,290322580645161,0,636363636363636),
		[78]=v2fM(0,338709677419355,0,636363636363636),
		[79]=v2fM(0,387096774193548,0,636363636363636),
		[80]=v2fM(0,435483870967742,0,636363636363636),
		[81]=v2fM(0,0,727272727272727),
		[82]=v2fM(0,0483870967741935,0,727272727272727),
		[83]=v2fM(0,0967741935483871,0,727272727272727),
		[84]=v2fM(0,145161290322581,0,727272727272727),
		[85]=v2fM(0,193548387096774,0,727272727272727),
		[86]=v2fM(0,241935483870968,0,727272727272727),
		[87]=v2fM(0,290322580645161,0,727272727272727),
		[88]=v2fM(0,338709677419355,0,727272727272727),
		[89]=v2fM(0,387096774193548,0,727272727272727),
		[90]=v2fM(0,435483870967742,0,727272727272727),
		[91]=v2fM(0,0,818181818181818),
		[92]=v2fM(0,0483870967741935,0,818181818181818),
		[93]=v2fM(0,0967741935483871,0,818181818181818),
		[94]=v2fM(0,145161290322581,0,818181818181818),
		[95]=v2fM(0,193548387096774,0,818181818181818),
		[96]=v2fM(0,241935483870968,0,818181818181818),
		[97]=v2fM(0,290322580645161,0,818181818181818),
		[98]=v2fM(0,338709677419355,0,818181818181818),
		[99]=v2fM(0,387096774193548,0,818181818181818),
		[100]=v2fM(0,435483870967742,0,818181818181818),
		[101]=v2fM(0,0,909090909090909),
		[102]=v2fM(0,0483870967741935,0,909090909090909),
		[103]=v2fM(0,0967741935483871,0,909090909090909),
		[104]=v2fM(0,145161290322581,0,909090909090909),
		[105]=v2fM(0,193548387096774,0,909090909090909),
		[106]=v2fM(0,241935483870968,0,909090909090909),
		[107]=v2fM(0,290322580645161,0,909090909090909),
		[108]=v2fM(0,338709677419355,0,909090909090909),
		[109]=v2fM(0,387096774193548,0,909090909090909),
		[110]=v2fM(0,435483870967742,0,909090909090909),
		[111]=v2fM(0,516129032258065,0),
		[112]=v2fM(0,564516129032258,0),
		[113]=v2fM(0,612903225806452,0),
		[114]=v2fM(0,661290322580645,0),
		[115]=v2fM(0,709677419354839,0),
		[116]=v2fM(0,758064516129032,0),
		[117]=v2fM(0,806451612903226,0),
		[118]=v2fM(0,854838709677419,0),
		[119]=v2fM(0,903225806451613,0),
		[120]=v2fM(0,951612903225806,0),
		[121]=v2fM(0,516129032258065,0,0909090909090909),
		[122]=v2fM(0,564516129032258,0,0909090909090909),
		[123]=v2fM(0,612903225806452,0,0909090909090909),
		[124]=v2fM(0,661290322580645,0,0909090909090909),
		[125]=v2fM(0,709677419354839,0,0909090909090909),
		[126]=v2fM(0,758064516129032,0,0909090909090909),
		[127]=v2fM(0,806451612903226,0,0909090909090909),
		[128]=v2fM(0,854838709677419,0,0909090909090909),
		[129]=v2fM(0,903225806451613,0,0909090909090909),
		[130]=v2fM(0,951612903225806,0,0909090909090909),
		[131]=v2fM(0,516129032258065,0,181818181818182),
		[132]=v2fM(0,564516129032258,0,181818181818182),
		[133]=v2fM(0,612903225806452,0,181818181818182),
		[134]=v2fM(0,661290322580645,0,181818181818182),
		[135]=v2fM(0,709677419354839,0,181818181818182),
		[136]=v2fM(0,758064516129032,0,181818181818182),
		[137]=v2fM(0,806451612903226,0,181818181818182),
		[138]=v2fM(0,854838709677419,0,181818181818182),
		[139]=v2fM(0,903225806451613,0,181818181818182),
		[140]=v2fM(0,951612903225806,0,181818181818182),
		[141]=v2fM(0,516129032258065,0,272727272727273),
		[142]=v2fM(0,564516129032258,0,272727272727273),
		[143]=v2fM(0,612903225806452,0,272727272727273),
		[144]=v2fM(0,661290322580645,0,272727272727273),
		[145]=v2fM(0,709677419354839,0,272727272727273),
		[146]=v2fM(0,758064516129032,0,272727272727273),
		[147]=v2fM(0,806451612903226,0,272727272727273),
		[148]=v2fM(0,854838709677419,0,272727272727273),
		[149]=v2fM(0,903225806451613,0,272727272727273),
		[150]=v2fM(0,951612903225806,0,272727272727273),
		[151]=v2fM(0,516129032258065,0,363636363636364),
		[152]=v2fM(0,564516129032258,0,363636363636364),
		[153]=v2fM(0,612903225806452,0,363636363636364),
		[154]=v2fM(0,661290322580645,0,363636363636364),
		[155]=v2fM(0,709677419354839,0,363636363636364),
		[156]=v2fM(0,758064516129032,0,363636363636364),
		[157]=v2fM(0,806451612903226,0,363636363636364),
		[158]=v2fM(0,854838709677419,0,363636363636364),
		[159]=v2fM(0,903225806451613,0,363636363636364),
		[160]=v2fM(0,951612903225806,0,363636363636364),
		[161]=v2fM(0,516129032258065,0,454545454545455),
		[162]=v2fM(0,564516129032258,0,454545454545455),
		[163]=v2fM(0,612903225806452,0,454545454545455),
		[164]=v2fM(0,661290322580645,0,454545454545455),
		[165]=v2fM(0,709677419354839,0,454545454545455),
		[166]=v2fM(0,758064516129032,0,454545454545455),
		[167]=v2fM(0,806451612903226,0,454545454545455),
		[168]=v2fM(0,854838709677419,0,454545454545455),
		[169]=v2fM(0,903225806451613,0,454545454545455),
		[170]=v2fM(0,951612903225806,0,454545454545455),
		[171]=v2fM(0,516129032258065,0,545454545454545),
		[172]=v2fM(0,564516129032258,0,545454545454545),
		[173]=v2fM(0,612903225806452,0,545454545454545),
		[174]=v2fM(0,661290322580645,0,545454545454545),
		[175]=v2fM(0,709677419354839,0,545454545454545),
		[176]=v2fM(0,758064516129032,0,545454545454545),
		[177]=v2fM(0,806451612903226,0,545454545454545),
		[178]=v2fM(0,854838709677419,0,545454545454545),
		[179]=v2fM(0,903225806451613,0,545454545454545),
		[180]=v2fM(0,951612903225806,0,545454545454545),
		[181]=v2fM(0,516129032258065,0,636363636363636),
		[182]=v2fM(0,564516129032258,0,636363636363636),
		[183]=v2fM(0,612903225806452,0,636363636363636),
		[184]=v2fM(0,661290322580645,0,636363636363636),
		[185]=v2fM(0,709677419354839,0,636363636363636),
		[186]=v2fM(0,758064516129032,0,636363636363636),
		[187]=v2fM(0,806451612903226,0,636363636363636),
		[188]=v2fM(0,854838709677419,0,636363636363636),
		[189]=v2fM(0,903225806451613,0,636363636363636),
		[190]=v2fM(0,951612903225806,0,636363636363636),
		[191]=v2fM(0,516129032258065,0,727272727272727),
		[192]=v2fM(0,564516129032258,0,727272727272727),
		[193]=v2fM(0,612903225806452,0,727272727272727),
		[194]=v2fM(0,661290322580645,0,727272727272727),
		[195]=v2fM(0,709677419354839,0,727272727272727),
		[196]=v2fM(0,758064516129032,0,727272727272727),
		[197]=v2fM(0,806451612903226,0,727272727272727),
		[198]=v2fM(0,854838709677419,0,727272727272727),
		[199]=v2fM(0,903225806451613,0,727272727272727),
		[200]=v2fM(0,951612903225806,0,727272727272727),
		[201]=v2fM(0,516129032258065,0,818181818181818),
		[202]=v2fM(0,564516129032258,0,818181818181818),
		[203]=v2fM(0,612903225806452,0,818181818181818),
		[204]=v2fM(0,661290322580645,0,818181818181818),
		[205]=v2fM(0,709677419354839,0,818181818181818),
		[206]=v2fM(0,758064516129032,0,818181818181818),
		[207]=v2fM(0,806451612903226,0,818181818181818),
		[208]=v2fM(0,854838709677419,0,818181818181818),
		[209]=v2fM(0,903225806451613,0,818181818181818),
		[210]=v2fM(0,951612903225806,0,818181818181818),
		[211]=v2fM(0,516129032258065,0,909090909090909),
		[212]=v2fM(0,564516129032258,0,909090909090909),
		[213]=v2fM(0,612903225806452,0,909090909090909),
		[214]=v2fM(0,661290322580645,0,909090909090909),
		[215]=v2fM(0,709677419354839,0,909090909090909),
		[216]=v2fM(0,758064516129032,0,909090909090909),
	},
	FRAME_SIZES={
		[1]={WIDTH=96,HEIGHT=92},
		[2]={WIDTH=96,HEIGHT=92},
		[3]={WIDTH=96,HEIGHT=92},
		[4]={WIDTH=96,HEIGHT=92},
		[5]={WIDTH=96,HEIGHT=92},
		[6]={WIDTH=96,HEIGHT=92},
		[7]={WIDTH=96,HEIGHT=92},
		[8]={WIDTH=96,HEIGHT=92},
		[9]={WIDTH=96,HEIGHT=92},
		[10]={WIDTH=96,HEIGHT=92},
		[11]={WIDTH=96,HEIGHT=92},
		[12]={WIDTH=96,HEIGHT=92},
		[13]={WIDTH=96,HEIGHT=92},
		[14]={WIDTH=96,HEIGHT=92},
		[15]={WIDTH=96,HEIGHT=92},
		[16]={WIDTH=96,HEIGHT=92},
		[17]={WIDTH=96,HEIGHT=92},
		[18]={WIDTH=96,HEIGHT=92},
		[19]={WIDTH=96,HEIGHT=92},
		[20]={WIDTH=96,HEIGHT=92},
		[21]={WIDTH=96,HEIGHT=92},
		[22]={WIDTH=96,HEIGHT=92},
		[23]={WIDTH=96,HEIGHT=92},
		[24]={WIDTH=96,HEIGHT=92},
		[25]={WIDTH=96,HEIGHT=92},
		[26]={WIDTH=96,HEIGHT=92},
		[27]={WIDTH=96,HEIGHT=92},
		[28]={WIDTH=96,HEIGHT=92},
		[29]={WIDTH=96,HEIGHT=92},
		[30]={WIDTH=96,HEIGHT=92},
		[31]={WIDTH=96,HEIGHT=92},
		[32]={WIDTH=96,HEIGHT=92},
		[33]={WIDTH=96,HEIGHT=92},
		[34]={WIDTH=96,HEIGHT=92},
		[35]={WIDTH=96,HEIGHT=92},
		[36]={WIDTH=96,HEIGHT=92},
		[37]={WIDTH=96,HEIGHT=92},
		[38]={WIDTH=96,HEIGHT=92},
		[39]={WIDTH=96,HEIGHT=92},
		[40]={WIDTH=96,HEIGHT=92},
		[41]={WIDTH=96,HEIGHT=92},
		[42]={WIDTH=96,HEIGHT=92},
		[43]={WIDTH=96,HEIGHT=92},
		[44]={WIDTH=96,HEIGHT=92},
		[45]={WIDTH=96,HEIGHT=92},
		[46]={WIDTH=96,HEIGHT=92},
		[47]={WIDTH=96,HEIGHT=92},
		[48]={WIDTH=96,HEIGHT=92},
		[49]={WIDTH=96,HEIGHT=92},
		[50]={WIDTH=96,HEIGHT=92},
		[51]={WIDTH=96,HEIGHT=92},
		[52]={WIDTH=96,HEIGHT=92},
		[53]={WIDTH=96,HEIGHT=92},
		[54]={WIDTH=96,HEIGHT=92},
		[55]={WIDTH=96,HEIGHT=92},
		[56]={WIDTH=96,HEIGHT=92},
		[57]={WIDTH=96,HEIGHT=92},
		[58]={WIDTH=96,HEIGHT=92},
		[59]={WIDTH=96,HEIGHT=92},
		[60]={WIDTH=96,HEIGHT=92},
		[61]={WIDTH=96,HEIGHT=92},
		[62]={WIDTH=96,HEIGHT=92},
		[63]={WIDTH=96,HEIGHT=92},
		[64]={WIDTH=96,HEIGHT=92},
		[65]={WIDTH=96,HEIGHT=92},
		[66]={WIDTH=96,HEIGHT=92},
		[67]={WIDTH=96,HEIGHT=92},
		[68]={WIDTH=96,HEIGHT=92},
		[69]={WIDTH=96,HEIGHT=92},
		[70]={WIDTH=96,HEIGHT=92},
		[71]={WIDTH=96,HEIGHT=92},
		[72]={WIDTH=96,HEIGHT=92},
		[73]={WIDTH=96,HEIGHT=92},
		[74]={WIDTH=96,HEIGHT=92},
		[75]={WIDTH=96,HEIGHT=92},
		[76]={WIDTH=96,HEIGHT=92},
		[77]={WIDTH=96,HEIGHT=92},
		[78]={WIDTH=96,HEIGHT=92},
		[79]={WIDTH=96,HEIGHT=92},
		[80]={WIDTH=96,HEIGHT=92},
		[81]={WIDTH=96,HEIGHT=92},
		[82]={WIDTH=96,HEIGHT=92},
		[83]={WIDTH=96,HEIGHT=92},
		[84]={WIDTH=96,HEIGHT=92},
		[85]={WIDTH=96,HEIGHT=92},
		[86]={WIDTH=96,HEIGHT=92},
		[87]={WIDTH=96,HEIGHT=92},
		[88]={WIDTH=96,HEIGHT=92},
		[89]={WIDTH=96,HEIGHT=92},
		[90]={WIDTH=96,HEIGHT=92},
		[91]={WIDTH=96,HEIGHT=92},
		[92]={WIDTH=96,HEIGHT=92},
		[93]={WIDTH=96,HEIGHT=92},
		[94]={WIDTH=96,HEIGHT=92},
		[95]={WIDTH=96,HEIGHT=92},
		[96]={WIDTH=96,HEIGHT=92},
		[97]={WIDTH=96,HEIGHT=92},
		[98]={WIDTH=96,HEIGHT=92},
		[99]={WIDTH=96,HEIGHT=92},
		[100]={WIDTH=96,HEIGHT=92},
		[101]={WIDTH=96,HEIGHT=92},
		[102]={WIDTH=96,HEIGHT=92},
		[103]={WIDTH=96,HEIGHT=92},
		[104]={WIDTH=96,HEIGHT=92},
		[105]={WIDTH=96,HEIGHT=92},
		[106]={WIDTH=96,HEIGHT=92},
		[107]={WIDTH=96,HEIGHT=92},
		[108]={WIDTH=96,HEIGHT=92},
		[109]={WIDTH=96,HEIGHT=92},
		[110]={WIDTH=96,HEIGHT=92},
		[111]={WIDTH=96,HEIGHT=92},
		[112]={WIDTH=96,HEIGHT=92},
		[113]={WIDTH=96,HEIGHT=92},
		[114]={WIDTH=96,HEIGHT=92},
		[115]={WIDTH=96,HEIGHT=92},
		[116]={WIDTH=96,HEIGHT=92},
		[117]={WIDTH=96,HEIGHT=92},
		[118]={WIDTH=96,HEIGHT=92},
		[119]={WIDTH=96,HEIGHT=92},
		[120]={WIDTH=96,HEIGHT=92},
		[121]={WIDTH=96,HEIGHT=92},
		[122]={WIDTH=96,HEIGHT=92},
		[123]={WIDTH=96,HEIGHT=92},
		[124]={WIDTH=96,HEIGHT=92},
		[125]={WIDTH=96,HEIGHT=92},
		[126]={WIDTH=96,HEIGHT=92},
		[127]={WIDTH=96,HEIGHT=92},
		[128]={WIDTH=96,HEIGHT=92},
		[129]={WIDTH=96,HEIGHT=92},
		[130]={WIDTH=96,HEIGHT=92},
		[131]={WIDTH=96,HEIGHT=92},
		[132]={WIDTH=96,HEIGHT=92},
		[133]={WIDTH=96,HEIGHT=92},
		[134]={WIDTH=96,HEIGHT=92},
		[135]={WIDTH=96,HEIGHT=92},
		[136]={WIDTH=96,HEIGHT=92},
		[137]={WIDTH=96,HEIGHT=92},
		[138]={WIDTH=96,HEIGHT=92},
		[139]={WIDTH=96,HEIGHT=92},
		[140]={WIDTH=96,HEIGHT=92},
		[141]={WIDTH=96,HEIGHT=92},
		[142]={WIDTH=96,HEIGHT=92},
		[143]={WIDTH=96,HEIGHT=92},
		[144]={WIDTH=96,HEIGHT=92},
		[145]={WIDTH=96,HEIGHT=92},
		[146]={WIDTH=96,HEIGHT=92},
		[147]={WIDTH=96,HEIGHT=92},
		[148]={WIDTH=96,HEIGHT=92},
		[149]={WIDTH=96,HEIGHT=92},
		[150]={WIDTH=96,HEIGHT=92},
		[151]={WIDTH=96,HEIGHT=92},
		[152]={WIDTH=96,HEIGHT=92},
		[153]={WIDTH=96,HEIGHT=92},
		[154]={WIDTH=96,HEIGHT=92},
		[155]={WIDTH=96,HEIGHT=92},
		[156]={WIDTH=96,HEIGHT=92},
		[157]={WIDTH=96,HEIGHT=92},
		[158]={WIDTH=96,HEIGHT=92},
		[159]={WIDTH=96,HEIGHT=92},
		[160]={WIDTH=96,HEIGHT=92},
		[161]={WIDTH=96,HEIGHT=92},
		[162]={WIDTH=96,HEIGHT=92},
		[163]={WIDTH=96,HEIGHT=92},
		[164]={WIDTH=96,HEIGHT=92},
		[165]={WIDTH=96,HEIGHT=92},
		[166]={WIDTH=96,HEIGHT=92},
		[167]={WIDTH=96,HEIGHT=92},
		[168]={WIDTH=96,HEIGHT=92},
		[169]={WIDTH=96,HEIGHT=92},
		[170]={WIDTH=96,HEIGHT=92},
		[171]={WIDTH=96,HEIGHT=92},
		[172]={WIDTH=96,HEIGHT=92},
		[173]={WIDTH=96,HEIGHT=92},
		[174]={WIDTH=96,HEIGHT=92},
		[175]={WIDTH=96,HEIGHT=92},
		[176]={WIDTH=96,HEIGHT=92},
		[177]={WIDTH=96,HEIGHT=92},
		[178]={WIDTH=96,HEIGHT=92},
		[179]={WIDTH=96,HEIGHT=92},
		[180]={WIDTH=96,HEIGHT=92},
		[181]={WIDTH=96,HEIGHT=92},
		[182]={WIDTH=96,HEIGHT=92},
		[183]={WIDTH=96,HEIGHT=92},
		[184]={WIDTH=96,HEIGHT=92},
		[185]={WIDTH=96,HEIGHT=92},
		[186]={WIDTH=96,HEIGHT=92},
		[187]={WIDTH=96,HEIGHT=92},
		[188]={WIDTH=96,HEIGHT=92},
		[189]={WIDTH=96,HEIGHT=92},
		[190]={WIDTH=96,HEIGHT=92},
		[191]={WIDTH=96,HEIGHT=92},
		[192]={WIDTH=96,HEIGHT=92},
		[193]={WIDTH=96,HEIGHT=92},
		[194]={WIDTH=96,HEIGHT=92},
		[195]={WIDTH=96,HEIGHT=92},
		[196]={WIDTH=96,HEIGHT=92},
		[197]={WIDTH=96,HEIGHT=92},
		[198]={WIDTH=96,HEIGHT=92},
		[199]={WIDTH=96,HEIGHT=92},
		[200]={WIDTH=96,HEIGHT=92},
		[201]={WIDTH=96,HEIGHT=92},
		[202]={WIDTH=96,HEIGHT=92},
		[203]={WIDTH=96,HEIGHT=92},
		[204]={WIDTH=96,HEIGHT=92},
		[205]={WIDTH=96,HEIGHT=92},
		[206]={WIDTH=96,HEIGHT=92},
		[207]={WIDTH=96,HEIGHT=92},
		[208]={WIDTH=96,HEIGHT=92},
		[209]={WIDTH=96,HEIGHT=92},
		[210]={WIDTH=96,HEIGHT=92},
		[211]={WIDTH=96,HEIGHT=92},
		[212]={WIDTH=96,HEIGHT=92},
		[213]={WIDTH=96,HEIGHT=92},
		[214]={WIDTH=96,HEIGHT=92},
		[215]={WIDTH=96,HEIGHT=92},
		[216]={WIDTH=96,HEIGHT=92},
	},
	FRAME_INDEXS={
		['w-u-heavy0144s']=1,
		['w-u-heavy0143s']=2,
		['w-u-heavy0142s']=3,
		['w-u-heavy0147s']=4,
		['w-u-heavy0146s']=5,
		['w-u-heavy0145s']=6,
		['w-u-heavy0141s']=7,
		['w-u-heavy0137s']=8,
		['w-u-heavy0136s']=9,
		['w-u-heavy0135s']=10,
		['w-u-heavy0140s']=11,
		['w-u-heavy0139s']=12,
		['w-u-heavy0138s']=13,
		['w-u-heavy0148s']=14,
		['w-u-heavy0158s']=15,
		['w-u-heavy0157s']=16,
		['w-u-heavy0156s']=17,
		['w-u-heavy0161s']=18,
		['w-u-heavy0160s']=19,
		['w-u-heavy0159s']=20,
		['w-u-heavy0155s']=21,
		['w-u-heavy0151s']=22,
		['w-u-heavy0150s']=23,
		['w-u-heavy0149s']=24,
		['w-u-heavy0154s']=25,
		['w-u-heavy0153s']=26,
		['w-u-heavy0152s']=27,
		['w-u-heavy0117s']=28,
		['w-u-heavy0116s']=29,
		['w-u-heavy0115s']=30,
		['w-u-heavy0120s']=31,
		['w-u-heavy0119s']=32,
		['w-u-heavy0118s']=33,
		['w-u-heavy0114s']=34,
		['w-u-heavy0110s']=35,
		['w-u-heavy0109s']=36,
		['w-u-heavy0108s']=37,
		['w-u-heavy0113s']=38,
		['w-u-heavy0112s']=39,
		['w-u-heavy0111s']=40,
		['w-u-heavy0121s']=41,
		['w-u-heavy0131s']=42,
		['w-u-heavy0130s']=43,
		['w-u-heavy0129s']=44,
		['w-u-heavy0134s']=45,
		['w-u-heavy0133s']=46,
		['w-u-heavy0132s']=47,
		['w-u-heavy0128s']=48,
		['w-u-heavy0124s']=49,
		['w-u-heavy0123s']=50,
		['w-u-heavy0122s']=51,
		['w-u-heavy0127s']=52,
		['w-u-heavy0126s']=53,
		['w-u-heavy0125s']=54,
		['w-u-heavy0198s']=55,
		['w-u-heavy0197s']=56,
		['w-u-heavy0196s']=57,
		['w-u-heavy0201s']=58,
		['w-u-heavy0200s']=59,
		['w-u-heavy0199s']=60,
		['w-u-heavy0195s']=61,
		['w-u-heavy0191s']=62,
		['w-u-heavy0190s']=63,
		['w-u-heavy0189s']=64,
		['w-u-heavy0194s']=65,
		['w-u-heavy0193s']=66,
		['w-u-heavy0192s']=67,
		['w-u-heavy0202s']=68,
		['w-u-heavy0212s']=69,
		['w-u-heavy0211s']=70,
		['w-u-heavy0210s']=71,
		['w-u-heavy0215s']=72,
		['w-u-heavy0214s']=73,
		['w-u-heavy0213s']=74,
		['w-u-heavy0209s']=75,
		['w-u-heavy0205s']=76,
		['w-u-heavy0204s']=77,
		['w-u-heavy0203s']=78,
		['w-u-heavy0208s']=79,
		['w-u-heavy0207s']=80,
		['w-u-heavy0206s']=81,
		['w-u-heavy0171s']=82,
		['w-u-heavy0170s']=83,
		['w-u-heavy0169s']=84,
		['w-u-heavy0174s']=85,
		['w-u-heavy0173s']=86,
		['w-u-heavy0172s']=87,
		['w-u-heavy0168s']=88,
		['w-u-heavy0164s']=89,
		['w-u-heavy0163s']=90,
		['w-u-heavy0162s']=91,
		['w-u-heavy0167s']=92,
		['w-u-heavy0166s']=93,
		['w-u-heavy0165s']=94,
		['w-u-heavy0175s']=95,
		['w-u-heavy0185s']=96,
		['w-u-heavy0184s']=97,
		['w-u-heavy0183s']=98,
		['w-u-heavy0188s']=99,
		['w-u-heavy0187s']=100,
		['w-u-heavy0186s']=101,
		['w-u-heavy0182s']=102,
		['w-u-heavy0178s']=103,
		['w-u-heavy0177s']=104,
		['w-u-heavy0176s']=105,
		['w-u-heavy0181s']=106,
		['w-u-heavy0180s']=107,
		['w-u-heavy0179s']=108,
		['w-u-heavy0036s']=109,
		['w-u-heavy0035s']=110,
		['w-u-heavy0034s']=111,
		['w-u-heavy0039s']=112,
		['w-u-heavy0038s']=113,
		['w-u-heavy0037s']=114,
		['w-u-heavy0033s']=115,
		['w-u-heavy0029s']=116,
		['w-u-heavy0028s']=117,
		['w-u-heavy0027s']=118,
		['w-u-heavy0032s']=119,
		['w-u-heavy0031s']=120,
		['w-u-heavy0030s']=121,
		['w-u-heavy0040s']=122,
		['w-u-heavy0050s']=123,
		['w-u-heavy0049s']=124,
		['w-u-heavy0048s']=125,
		['w-u-heavy0053s']=126,
		['w-u-heavy0052s']=127,
		['w-u-heavy0051s']=128,
		['w-u-heavy0047s']=129,
		['w-u-heavy0043s']=130,
		['w-u-heavy0042s']=131,
		['w-u-heavy0041s']=132,
		['w-u-heavy0046s']=133,
		['w-u-heavy0045s']=134,
		['w-u-heavy0044s']=135,
		['w-u-heavy0009s']=136,
		['w-u-heavy0008s']=137,
		['w-u-heavy0007s']=138,
		['w-u-heavy0012s']=139,
		['w-u-heavy0011s']=140,
		['w-u-heavy0010s']=141,
		['w-u-heavy0006s']=142,
		['w-u-heavy0002s']=143,
		['w-u-heavy0001s']=144,
		['w-u-heavy0000s']=145,
		['w-u-heavy0005s']=146,
		['w-u-heavy0004s']=147,
		['w-u-heavy0003s']=148,
		['w-u-heavy0013s']=149,
		['w-u-heavy0023s']=150,
		['w-u-heavy0022s']=151,
		['w-u-heavy0021s']=152,
		['w-u-heavy0026s']=153,
		['w-u-heavy0025s']=154,
		['w-u-heavy0024s']=155,
		['w-u-heavy0020s']=156,
		['w-u-heavy0016s']=157,
		['w-u-heavy0015s']=158,
		['w-u-heavy0014s']=159,
		['w-u-heavy0019s']=160,
		['w-u-heavy0018s']=161,
		['w-u-heavy0017s']=162,
		['w-u-heavy0090s']=163,
		['w-u-heavy0089s']=164,
		['w-u-heavy0088s']=165,
		['w-u-heavy0093s']=166,
		['w-u-heavy0092s']=167,
		['w-u-heavy0091s']=168,
		['w-u-heavy0087s']=169,
		['w-u-heavy0083s']=170,
		['w-u-heavy0082s']=171,
		['w-u-heavy0081s']=172,
		['w-u-heavy0086s']=173,
		['w-u-heavy0085s']=174,
		['w-u-heavy0084s']=175,
		['w-u-heavy0094s']=176,
		['w-u-heavy0104s']=177,
		['w-u-heavy0103s']=178,
		['w-u-heavy0102s']=179,
		['w-u-heavy0107s']=180,
		['w-u-heavy0106s']=181,
		['w-u-heavy0105s']=182,
		['w-u-heavy0101s']=183,
		['w-u-heavy0097s']=184,
		['w-u-heavy0096s']=185,
		['w-u-heavy0095s']=186,
		['w-u-heavy0100s']=187,
		['w-u-heavy0099s']=188,
		['w-u-heavy0098s']=189,
		['w-u-heavy0063s']=190,
		['w-u-heavy0062s']=191,
		['w-u-heavy0061s']=192,
		['w-u-heavy0066s']=193,
		['w-u-heavy0065s']=194,
		['w-u-heavy0064s']=195,
		['w-u-heavy0060s']=196,
		['w-u-heavy0056s']=197,
		['w-u-heavy0055s']=198,
		['w-u-heavy0054s']=199,
		['w-u-heavy0059s']=200,
		['w-u-heavy0058s']=201,
		['w-u-heavy0057s']=202,
		['w-u-heavy0067s']=203,
		['w-u-heavy0077s']=204,
		['w-u-heavy0076s']=205,
		['w-u-heavy0075s']=206,
		['w-u-heavy0080s']=207,
		['w-u-heavy0079s']=208,
		['w-u-heavy0078s']=209,
		['w-u-heavy0074s']=210,
		['w-u-heavy0070s']=211,
		['w-u-heavy0069s']=212,
		['w-u-heavy0068s']=213,
		['w-u-heavy0073s']=214,
		['w-u-heavy0072s']=215,
		['w-u-heavy0071s']=216,
	},
};
