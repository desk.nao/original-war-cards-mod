--GENERATED ON: 05/12/2024 07:01:08


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['w-u-rockets'] = {
	TEX_WIDTH=1012,
	TEX_HEIGHT=256,
	FRAME_COUNT=72,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0454545454545455,0),
		[3]=v2fM(0,0909090909090909,0),
		[4]=v2fM(0,136363636363636,0),
		[5]=v2fM(0,181818181818182,0),
		[6]=v2fM(0,227272727272727,0),
		[7]=v2fM(0,272727272727273,0),
		[8]=v2fM(0,318181818181818,0),
		[9]=v2fM(0,363636363636364,0),
		[10]=v2fM(0,409090909090909,0),
		[11]=v2fM(0,454545454545455,0),
		[12]=v2fM(0,5,0),
		[13]=v2fM(0,545454545454545,0),
		[14]=v2fM(0,590909090909091,0),
		[15]=v2fM(0,636363636363636,0),
		[16]=v2fM(0,681818181818182,0),
		[17]=v2fM(0,727272727272727,0),
		[18]=v2fM(0,772727272727273,0),
		[19]=v2fM(0,818181818181818,0),
		[20]=v2fM(0,863636363636364,0),
		[21]=v2fM(0,909090909090909,0),
		[22]=v2fM(0,954545454545455,0),
		[23]=v2fM(0,0,25),
		[24]=v2fM(0,0454545454545455,0,25),
		[25]=v2fM(0,0909090909090909,0,25),
		[26]=v2fM(0,136363636363636,0,25),
		[27]=v2fM(0,181818181818182,0,25),
		[28]=v2fM(0,227272727272727,0,25),
		[29]=v2fM(0,272727272727273,0,25),
		[30]=v2fM(0,318181818181818,0,25),
		[31]=v2fM(0,363636363636364,0,25),
		[32]=v2fM(0,409090909090909,0,25),
		[33]=v2fM(0,454545454545455,0,25),
		[34]=v2fM(0,5,0,25),
		[35]=v2fM(0,545454545454545,0,25),
		[36]=v2fM(0,590909090909091,0,25),
		[37]=v2fM(0,636363636363636,0,25),
		[38]=v2fM(0,681818181818182,0,25),
		[39]=v2fM(0,727272727272727,0,25),
		[40]=v2fM(0,772727272727273,0,25),
		[41]=v2fM(0,818181818181818,0,25),
		[42]=v2fM(0,863636363636364,0,25),
		[43]=v2fM(0,909090909090909,0,25),
		[44]=v2fM(0,954545454545455,0,25),
		[45]=v2fM(0,0,5),
		[46]=v2fM(0,0454545454545455,0,5),
		[47]=v2fM(0,0909090909090909,0,5),
		[48]=v2fM(0,136363636363636,0,5),
		[49]=v2fM(0,181818181818182,0,5),
		[50]=v2fM(0,227272727272727,0,5),
		[51]=v2fM(0,272727272727273,0,5),
		[52]=v2fM(0,318181818181818,0,5),
		[53]=v2fM(0,363636363636364,0,5),
		[54]=v2fM(0,409090909090909,0,5),
		[55]=v2fM(0,454545454545455,0,5),
		[56]=v2fM(0,5,0,5),
		[57]=v2fM(0,545454545454545,0,5),
		[58]=v2fM(0,590909090909091,0,5),
		[59]=v2fM(0,636363636363636,0,5),
		[60]=v2fM(0,681818181818182,0,5),
		[61]=v2fM(0,727272727272727,0,5),
		[62]=v2fM(0,772727272727273,0,5),
		[63]=v2fM(0,818181818181818,0,5),
		[64]=v2fM(0,863636363636364,0,5),
		[65]=v2fM(0,909090909090909,0,5),
		[66]=v2fM(0,954545454545455,0,5),
		[67]=v2fM(0,0,75),
		[68]=v2fM(0,0454545454545455,0,75),
		[69]=v2fM(0,0909090909090909,0,75),
		[70]=v2fM(0,136363636363636,0,75),
		[71]=v2fM(0,181818181818182,0,75),
		[72]=v2fM(0,227272727272727,0,75),
	},
	FRAME_SIZES={
		[1]={WIDTH=46,HEIGHT=64},
		[2]={WIDTH=46,HEIGHT=64},
		[3]={WIDTH=46,HEIGHT=64},
		[4]={WIDTH=46,HEIGHT=64},
		[5]={WIDTH=46,HEIGHT=64},
		[6]={WIDTH=46,HEIGHT=64},
		[7]={WIDTH=46,HEIGHT=64},
		[8]={WIDTH=46,HEIGHT=64},
		[9]={WIDTH=46,HEIGHT=64},
		[10]={WIDTH=46,HEIGHT=64},
		[11]={WIDTH=46,HEIGHT=64},
		[12]={WIDTH=46,HEIGHT=64},
		[13]={WIDTH=46,HEIGHT=64},
		[14]={WIDTH=46,HEIGHT=64},
		[15]={WIDTH=46,HEIGHT=64},
		[16]={WIDTH=46,HEIGHT=64},
		[17]={WIDTH=46,HEIGHT=64},
		[18]={WIDTH=46,HEIGHT=64},
		[19]={WIDTH=46,HEIGHT=64},
		[20]={WIDTH=46,HEIGHT=64},
		[21]={WIDTH=46,HEIGHT=64},
		[22]={WIDTH=46,HEIGHT=64},
		[23]={WIDTH=46,HEIGHT=64},
		[24]={WIDTH=46,HEIGHT=64},
		[25]={WIDTH=46,HEIGHT=64},
		[26]={WIDTH=46,HEIGHT=64},
		[27]={WIDTH=46,HEIGHT=64},
		[28]={WIDTH=46,HEIGHT=64},
		[29]={WIDTH=46,HEIGHT=64},
		[30]={WIDTH=46,HEIGHT=64},
		[31]={WIDTH=46,HEIGHT=64},
		[32]={WIDTH=46,HEIGHT=64},
		[33]={WIDTH=46,HEIGHT=64},
		[34]={WIDTH=46,HEIGHT=64},
		[35]={WIDTH=46,HEIGHT=64},
		[36]={WIDTH=46,HEIGHT=64},
		[37]={WIDTH=46,HEIGHT=64},
		[38]={WIDTH=46,HEIGHT=64},
		[39]={WIDTH=46,HEIGHT=64},
		[40]={WIDTH=46,HEIGHT=64},
		[41]={WIDTH=46,HEIGHT=64},
		[42]={WIDTH=46,HEIGHT=64},
		[43]={WIDTH=46,HEIGHT=64},
		[44]={WIDTH=46,HEIGHT=64},
		[45]={WIDTH=46,HEIGHT=64},
		[46]={WIDTH=46,HEIGHT=64},
		[47]={WIDTH=46,HEIGHT=64},
		[48]={WIDTH=46,HEIGHT=64},
		[49]={WIDTH=46,HEIGHT=64},
		[50]={WIDTH=46,HEIGHT=64},
		[51]={WIDTH=46,HEIGHT=64},
		[52]={WIDTH=46,HEIGHT=64},
		[53]={WIDTH=46,HEIGHT=64},
		[54]={WIDTH=46,HEIGHT=64},
		[55]={WIDTH=46,HEIGHT=64},
		[56]={WIDTH=46,HEIGHT=64},
		[57]={WIDTH=46,HEIGHT=64},
		[58]={WIDTH=46,HEIGHT=64},
		[59]={WIDTH=46,HEIGHT=64},
		[60]={WIDTH=46,HEIGHT=64},
		[61]={WIDTH=46,HEIGHT=64},
		[62]={WIDTH=46,HEIGHT=64},
		[63]={WIDTH=46,HEIGHT=64},
		[64]={WIDTH=46,HEIGHT=64},
		[65]={WIDTH=46,HEIGHT=64},
		[66]={WIDTH=46,HEIGHT=64},
		[67]={WIDTH=46,HEIGHT=64},
		[68]={WIDTH=46,HEIGHT=64},
		[69]={WIDTH=46,HEIGHT=64},
		[70]={WIDTH=46,HEIGHT=64},
		[71]={WIDTH=46,HEIGHT=64},
		[72]={WIDTH=46,HEIGHT=64},
	},
	FRAME_INDEXS={
		['w-u-rocket0047s']=1,
		['w-u-rocket0048s']=2,
		['w-u-rocket0045s']=3,
		['w-u-rocket0046s']=4,
		['w-u-rocket0049s']=5,
		['w-u-rocket0052s']=6,
		['w-u-rocket0053s']=7,
		['w-u-rocket0050s']=8,
		['w-u-rocket0051s']=9,
		['w-u-rocket0038s']=10,
		['w-u-rocket0039s']=11,
		['w-u-rocket0036s']=12,
		['w-u-rocket0037s']=13,
		['w-u-rocket0040s']=14,
		['w-u-rocket0043s']=15,
		['w-u-rocket0044s']=16,
		['w-u-rocket0041s']=17,
		['w-u-rocket0042s']=18,
		['w-u-rocket0065s']=19,
		['w-u-rocket0066s']=20,
		['w-u-rocket0063s']=21,
		['w-u-rocket0064s']=22,
		['w-u-rocket0067s']=23,
		['w-u-rocket0070s']=24,
		['w-u-rocket0071s']=25,
		['w-u-rocket0068s']=26,
		['w-u-rocket0069s']=27,
		['w-u-rocket0056s']=28,
		['w-u-rocket0057s']=29,
		['w-u-rocket0054s']=30,
		['w-u-rocket0055s']=31,
		['w-u-rocket0058s']=32,
		['w-u-rocket0061s']=33,
		['w-u-rocket0062s']=34,
		['w-u-rocket0059s']=35,
		['w-u-rocket0060s']=36,
		['w-u-rocket0011s']=37,
		['w-u-rocket0012s']=38,
		['w-u-rocket0009s']=39,
		['w-u-rocket0010s']=40,
		['w-u-rocket0013s']=41,
		['w-u-rocket0016s']=42,
		['w-u-rocket0017s']=43,
		['w-u-rocket0014s']=44,
		['w-u-rocket0015s']=45,
		['w-u-rocket0002s']=46,
		['w-u-rocket0003s']=47,
		['w-u-rocket0000s']=48,
		['w-u-rocket0001s']=49,
		['w-u-rocket0004s']=50,
		['w-u-rocket0007s']=51,
		['w-u-rocket0008s']=52,
		['w-u-rocket0005s']=53,
		['w-u-rocket0006s']=54,
		['w-u-rocket0029s']=55,
		['w-u-rocket0030s']=56,
		['w-u-rocket0027s']=57,
		['w-u-rocket0028s']=58,
		['w-u-rocket0031s']=59,
		['w-u-rocket0034s']=60,
		['w-u-rocket0035s']=61,
		['w-u-rocket0032s']=62,
		['w-u-rocket0033s']=63,
		['w-u-rocket0020s']=64,
		['w-u-rocket0021s']=65,
		['w-u-rocket0018s']=66,
		['w-u-rocket0019s']=67,
		['w-u-rocket0022s']=68,
		['w-u-rocket0025s']=69,
		['w-u-rocket0026s']=70,
		['w-u-rocket0023s']=71,
		['w-u-rocket0024s']=72,
	},
};
