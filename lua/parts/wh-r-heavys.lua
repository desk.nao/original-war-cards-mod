--GENERATED ON: 05/12/2024 07:01:15


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['wh-r-heavys'] = {
	TEX_WIDTH=1012,
	TEX_HEIGHT=108,
	FRAME_COUNT=108,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0217391304347826,0),
		[3]=v2fM(0,0434782608695652,0),
		[4]=v2fM(0,0652173913043478,0),
		[5]=v2fM(0,0869565217391304,0),
		[6]=v2fM(0,108695652173913,0),
		[7]=v2fM(0,130434782608696,0),
		[8]=v2fM(0,152173913043478,0),
		[9]=v2fM(0,173913043478261,0),
		[10]=v2fM(0,195652173913043,0),
		[11]=v2fM(0,217391304347826,0),
		[12]=v2fM(0,239130434782609,0),
		[13]=v2fM(0,260869565217391,0),
		[14]=v2fM(0,282608695652174,0),
		[15]=v2fM(0,304347826086957,0),
		[16]=v2fM(0,326086956521739,0),
		[17]=v2fM(0,347826086956522,0),
		[18]=v2fM(0,369565217391304,0),
		[19]=v2fM(0,391304347826087,0),
		[20]=v2fM(0,41304347826087,0),
		[21]=v2fM(0,434782608695652,0),
		[22]=v2fM(0,456521739130435,0),
		[23]=v2fM(0,478260869565217,0),
		[24]=v2fM(0,5,0),
		[25]=v2fM(0,521739130434783,0),
		[26]=v2fM(0,543478260869565,0),
		[27]=v2fM(0,565217391304348,0),
		[28]=v2fM(0,58695652173913,0),
		[29]=v2fM(0,608695652173913,0),
		[30]=v2fM(0,630434782608696,0),
		[31]=v2fM(0,652173913043478,0),
		[32]=v2fM(0,673913043478261,0),
		[33]=v2fM(0,695652173913043,0),
		[34]=v2fM(0,717391304347826,0),
		[35]=v2fM(0,739130434782609,0),
		[36]=v2fM(0,760869565217391,0),
		[37]=v2fM(0,782608695652174,0),
		[38]=v2fM(0,804347826086957,0),
		[39]=v2fM(0,826086956521739,0),
		[40]=v2fM(0,847826086956522,0),
		[41]=v2fM(0,869565217391304,0),
		[42]=v2fM(0,891304347826087,0),
		[43]=v2fM(0,91304347826087,0),
		[44]=v2fM(0,934782608695652,0),
		[45]=v2fM(0,956521739130435,0),
		[46]=v2fM(0,978260869565217,0),
		[47]=v2fM(0,0,333333333333333),
		[48]=v2fM(0,0217391304347826,0,333333333333333),
		[49]=v2fM(0,0434782608695652,0,333333333333333),
		[50]=v2fM(0,0652173913043478,0,333333333333333),
		[51]=v2fM(0,0869565217391304,0,333333333333333),
		[52]=v2fM(0,108695652173913,0,333333333333333),
		[53]=v2fM(0,130434782608696,0,333333333333333),
		[54]=v2fM(0,152173913043478,0,333333333333333),
		[55]=v2fM(0,173913043478261,0,333333333333333),
		[56]=v2fM(0,195652173913043,0,333333333333333),
		[57]=v2fM(0,217391304347826,0,333333333333333),
		[58]=v2fM(0,239130434782609,0,333333333333333),
		[59]=v2fM(0,260869565217391,0,333333333333333),
		[60]=v2fM(0,282608695652174,0,333333333333333),
		[61]=v2fM(0,304347826086957,0,333333333333333),
		[62]=v2fM(0,326086956521739,0,333333333333333),
		[63]=v2fM(0,347826086956522,0,333333333333333),
		[64]=v2fM(0,369565217391304,0,333333333333333),
		[65]=v2fM(0,391304347826087,0,333333333333333),
		[66]=v2fM(0,41304347826087,0,333333333333333),
		[67]=v2fM(0,434782608695652,0,333333333333333),
		[68]=v2fM(0,456521739130435,0,333333333333333),
		[69]=v2fM(0,478260869565217,0,333333333333333),
		[70]=v2fM(0,5,0,333333333333333),
		[71]=v2fM(0,521739130434783,0,333333333333333),
		[72]=v2fM(0,543478260869565,0,333333333333333),
		[73]=v2fM(0,565217391304348,0,333333333333333),
		[74]=v2fM(0,58695652173913,0,333333333333333),
		[75]=v2fM(0,608695652173913,0,333333333333333),
		[76]=v2fM(0,630434782608696,0,333333333333333),
		[77]=v2fM(0,652173913043478,0,333333333333333),
		[78]=v2fM(0,673913043478261,0,333333333333333),
		[79]=v2fM(0,695652173913043,0,333333333333333),
		[80]=v2fM(0,717391304347826,0,333333333333333),
		[81]=v2fM(0,739130434782609,0,333333333333333),
		[82]=v2fM(0,760869565217391,0,333333333333333),
		[83]=v2fM(0,782608695652174,0,333333333333333),
		[84]=v2fM(0,804347826086957,0,333333333333333),
		[85]=v2fM(0,826086956521739,0,333333333333333),
		[86]=v2fM(0,847826086956522,0,333333333333333),
		[87]=v2fM(0,869565217391304,0,333333333333333),
		[88]=v2fM(0,891304347826087,0,333333333333333),
		[89]=v2fM(0,91304347826087,0,333333333333333),
		[90]=v2fM(0,934782608695652,0,333333333333333),
		[91]=v2fM(0,956521739130435,0,333333333333333),
		[92]=v2fM(0,978260869565217,0,333333333333333),
		[93]=v2fM(0,0,666666666666667),
		[94]=v2fM(0,0217391304347826,0,666666666666667),
		[95]=v2fM(0,0434782608695652,0,666666666666667),
		[96]=v2fM(0,0652173913043478,0,666666666666667),
		[97]=v2fM(0,0869565217391304,0,666666666666667),
		[98]=v2fM(0,108695652173913,0,666666666666667),
		[99]=v2fM(0,130434782608696,0,666666666666667),
		[100]=v2fM(0,152173913043478,0,666666666666667),
		[101]=v2fM(0,173913043478261,0,666666666666667),
		[102]=v2fM(0,195652173913043,0,666666666666667),
		[103]=v2fM(0,217391304347826,0,666666666666667),
		[104]=v2fM(0,239130434782609,0,666666666666667),
		[105]=v2fM(0,260869565217391,0,666666666666667),
		[106]=v2fM(0,282608695652174,0,666666666666667),
		[107]=v2fM(0,304347826086957,0,666666666666667),
		[108]=v2fM(0,326086956521739,0,666666666666667),
	},
	FRAME_SIZES={
		[1]={WIDTH=22,HEIGHT=36},
		[2]={WIDTH=22,HEIGHT=36},
		[3]={WIDTH=22,HEIGHT=36},
		[4]={WIDTH=22,HEIGHT=36},
		[5]={WIDTH=22,HEIGHT=36},
		[6]={WIDTH=22,HEIGHT=36},
		[7]={WIDTH=22,HEIGHT=36},
		[8]={WIDTH=22,HEIGHT=36},
		[9]={WIDTH=22,HEIGHT=36},
		[10]={WIDTH=22,HEIGHT=36},
		[11]={WIDTH=22,HEIGHT=36},
		[12]={WIDTH=22,HEIGHT=36},
		[13]={WIDTH=22,HEIGHT=36},
		[14]={WIDTH=22,HEIGHT=36},
		[15]={WIDTH=22,HEIGHT=36},
		[16]={WIDTH=22,HEIGHT=36},
		[17]={WIDTH=22,HEIGHT=36},
		[18]={WIDTH=22,HEIGHT=36},
		[19]={WIDTH=22,HEIGHT=36},
		[20]={WIDTH=22,HEIGHT=36},
		[21]={WIDTH=22,HEIGHT=36},
		[22]={WIDTH=22,HEIGHT=36},
		[23]={WIDTH=22,HEIGHT=36},
		[24]={WIDTH=22,HEIGHT=36},
		[25]={WIDTH=22,HEIGHT=36},
		[26]={WIDTH=22,HEIGHT=36},
		[27]={WIDTH=22,HEIGHT=36},
		[28]={WIDTH=22,HEIGHT=36},
		[29]={WIDTH=22,HEIGHT=36},
		[30]={WIDTH=22,HEIGHT=36},
		[31]={WIDTH=22,HEIGHT=36},
		[32]={WIDTH=22,HEIGHT=36},
		[33]={WIDTH=22,HEIGHT=36},
		[34]={WIDTH=22,HEIGHT=36},
		[35]={WIDTH=22,HEIGHT=36},
		[36]={WIDTH=22,HEIGHT=36},
		[37]={WIDTH=22,HEIGHT=36},
		[38]={WIDTH=22,HEIGHT=36},
		[39]={WIDTH=22,HEIGHT=36},
		[40]={WIDTH=22,HEIGHT=36},
		[41]={WIDTH=22,HEIGHT=36},
		[42]={WIDTH=22,HEIGHT=36},
		[43]={WIDTH=22,HEIGHT=36},
		[44]={WIDTH=22,HEIGHT=36},
		[45]={WIDTH=22,HEIGHT=36},
		[46]={WIDTH=22,HEIGHT=36},
		[47]={WIDTH=22,HEIGHT=36},
		[48]={WIDTH=22,HEIGHT=36},
		[49]={WIDTH=22,HEIGHT=36},
		[50]={WIDTH=22,HEIGHT=36},
		[51]={WIDTH=22,HEIGHT=36},
		[52]={WIDTH=22,HEIGHT=36},
		[53]={WIDTH=22,HEIGHT=36},
		[54]={WIDTH=22,HEIGHT=36},
		[55]={WIDTH=22,HEIGHT=36},
		[56]={WIDTH=22,HEIGHT=36},
		[57]={WIDTH=22,HEIGHT=36},
		[58]={WIDTH=22,HEIGHT=36},
		[59]={WIDTH=22,HEIGHT=36},
		[60]={WIDTH=22,HEIGHT=36},
		[61]={WIDTH=22,HEIGHT=36},
		[62]={WIDTH=22,HEIGHT=36},
		[63]={WIDTH=22,HEIGHT=36},
		[64]={WIDTH=22,HEIGHT=36},
		[65]={WIDTH=22,HEIGHT=36},
		[66]={WIDTH=22,HEIGHT=36},
		[67]={WIDTH=22,HEIGHT=36},
		[68]={WIDTH=22,HEIGHT=36},
		[69]={WIDTH=22,HEIGHT=36},
		[70]={WIDTH=22,HEIGHT=36},
		[71]={WIDTH=22,HEIGHT=36},
		[72]={WIDTH=22,HEIGHT=36},
		[73]={WIDTH=22,HEIGHT=36},
		[74]={WIDTH=22,HEIGHT=36},
		[75]={WIDTH=22,HEIGHT=36},
		[76]={WIDTH=22,HEIGHT=36},
		[77]={WIDTH=22,HEIGHT=36},
		[78]={WIDTH=22,HEIGHT=36},
		[79]={WIDTH=22,HEIGHT=36},
		[80]={WIDTH=22,HEIGHT=36},
		[81]={WIDTH=22,HEIGHT=36},
		[82]={WIDTH=22,HEIGHT=36},
		[83]={WIDTH=22,HEIGHT=36},
		[84]={WIDTH=22,HEIGHT=36},
		[85]={WIDTH=22,HEIGHT=36},
		[86]={WIDTH=22,HEIGHT=36},
		[87]={WIDTH=22,HEIGHT=36},
		[88]={WIDTH=22,HEIGHT=36},
		[89]={WIDTH=22,HEIGHT=36},
		[90]={WIDTH=22,HEIGHT=36},
		[91]={WIDTH=22,HEIGHT=36},
		[92]={WIDTH=22,HEIGHT=36},
		[93]={WIDTH=22,HEIGHT=36},
		[94]={WIDTH=22,HEIGHT=36},
		[95]={WIDTH=22,HEIGHT=36},
		[96]={WIDTH=22,HEIGHT=36},
		[97]={WIDTH=22,HEIGHT=36},
		[98]={WIDTH=22,HEIGHT=36},
		[99]={WIDTH=22,HEIGHT=36},
		[100]={WIDTH=22,HEIGHT=36},
		[101]={WIDTH=22,HEIGHT=36},
		[102]={WIDTH=22,HEIGHT=36},
		[103]={WIDTH=22,HEIGHT=36},
		[104]={WIDTH=22,HEIGHT=36},
		[105]={WIDTH=22,HEIGHT=36},
		[106]={WIDTH=22,HEIGHT=36},
		[107]={WIDTH=22,HEIGHT=36},
		[108]={WIDTH=22,HEIGHT=36},
	},
	FRAME_INDEXS={
		['wh-r-heavy0071s']=1,
		['wh-r-heavy0072s']=2,
		['wh-r-heavy0073s']=3,
		['wh-r-heavy0068s']=4,
		['wh-r-heavy0069s']=5,
		['wh-r-heavy0070s']=6,
		['wh-r-heavy0074s']=7,
		['wh-r-heavy0078s']=8,
		['wh-r-heavy0079s']=9,
		['wh-r-heavy0080s']=10,
		['wh-r-heavy0075s']=11,
		['wh-r-heavy0076s']=12,
		['wh-r-heavy0077s']=13,
		['wh-r-heavy0067s']=14,
		['wh-r-heavy0057s']=15,
		['wh-r-heavy0058s']=16,
		['wh-r-heavy0059s']=17,
		['wh-r-heavy0054s']=18,
		['wh-r-heavy0055s']=19,
		['wh-r-heavy0056s']=20,
		['wh-r-heavy0060s']=21,
		['wh-r-heavy0064s']=22,
		['wh-r-heavy0065s']=23,
		['wh-r-heavy0066s']=24,
		['wh-r-heavy0061s']=25,
		['wh-r-heavy0062s']=26,
		['wh-r-heavy0063s']=27,
		['wh-r-heavy0098s']=28,
		['wh-r-heavy0099s']=29,
		['wh-r-heavy0100s']=30,
		['wh-r-heavy0095s']=31,
		['wh-r-heavy0096s']=32,
		['wh-r-heavy0097s']=33,
		['wh-r-heavy0101s']=34,
		['wh-r-heavy0105s']=35,
		['wh-r-heavy0106s']=36,
		['wh-r-heavy0107s']=37,
		['wh-r-heavy0102s']=38,
		['wh-r-heavy0103s']=39,
		['wh-r-heavy0104s']=40,
		['wh-r-heavy0094s']=41,
		['wh-r-heavy0084s']=42,
		['wh-r-heavy0085s']=43,
		['wh-r-heavy0086s']=44,
		['wh-r-heavy0081s']=45,
		['wh-r-heavy0082s']=46,
		['wh-r-heavy0083s']=47,
		['wh-r-heavy0087s']=48,
		['wh-r-heavy0091s']=49,
		['wh-r-heavy0092s']=50,
		['wh-r-heavy0093s']=51,
		['wh-r-heavy0088s']=52,
		['wh-r-heavy0089s']=53,
		['wh-r-heavy0090s']=54,
		['wh-r-heavy0017s']=55,
		['wh-r-heavy0018s']=56,
		['wh-r-heavy0019s']=57,
		['wh-r-heavy0014s']=58,
		['wh-r-heavy0015s']=59,
		['wh-r-heavy0016s']=60,
		['wh-r-heavy0020s']=61,
		['wh-r-heavy0024s']=62,
		['wh-r-heavy0025s']=63,
		['wh-r-heavy0026s']=64,
		['wh-r-heavy0021s']=65,
		['wh-r-heavy0022s']=66,
		['wh-r-heavy0023s']=67,
		['wh-r-heavy0013s']=68,
		['wh-r-heavy0003s']=69,
		['wh-r-heavy0004s']=70,
		['wh-r-heavy0005s']=71,
		['wh-r-heavy0000s']=72,
		['wh-r-heavy0001s']=73,
		['wh-r-heavy0002s']=74,
		['wh-r-heavy0006s']=75,
		['wh-r-heavy0010s']=76,
		['wh-r-heavy0011s']=77,
		['wh-r-heavy0012s']=78,
		['wh-r-heavy0007s']=79,
		['wh-r-heavy0008s']=80,
		['wh-r-heavy0009s']=81,
		['wh-r-heavy0044s']=82,
		['wh-r-heavy0045s']=83,
		['wh-r-heavy0046s']=84,
		['wh-r-heavy0041s']=85,
		['wh-r-heavy0042s']=86,
		['wh-r-heavy0043s']=87,
		['wh-r-heavy0047s']=88,
		['wh-r-heavy0051s']=89,
		['wh-r-heavy0052s']=90,
		['wh-r-heavy0053s']=91,
		['wh-r-heavy0048s']=92,
		['wh-r-heavy0049s']=93,
		['wh-r-heavy0050s']=94,
		['wh-r-heavy0040s']=95,
		['wh-r-heavy0030s']=96,
		['wh-r-heavy0031s']=97,
		['wh-r-heavy0032s']=98,
		['wh-r-heavy0027s']=99,
		['wh-r-heavy0028s']=100,
		['wh-r-heavy0029s']=101,
		['wh-r-heavy0033s']=102,
		['wh-r-heavy0037s']=103,
		['wh-r-heavy0038s']=104,
		['wh-r-heavy0039s']=105,
		['wh-r-heavy0034s']=106,
		['wh-r-heavy0035s']=107,
		['wh-r-heavy0036s']=108,
	},
};
