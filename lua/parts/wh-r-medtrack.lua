--GENERATED ON: 05/12/2024 07:01:16


if (SPRITE_DATA == nil) then SPRITE_DATA = {}; end;

SPRITE_DATA['wh-r-medtrack'] = {
	TEX_WIDTH=1020,
	TEX_HEIGHT=1020,
	FRAME_COUNT=216,
	FRAME_COORDS={
		[1]=v2fM(0,0),
		[2]=v2fM(0,0666666666666667,0),
		[3]=v2fM(0,133333333333333,0),
		[4]=v2fM(0,2,0),
		[5]=v2fM(0,266666666666667,0),
		[6]=v2fM(0,333333333333333,0),
		[7]=v2fM(0,4,0),
		[8]=v2fM(0,466666666666667,0),
		[9]=v2fM(0,533333333333333,0),
		[10]=v2fM(0,6,0),
		[11]=v2fM(0,666666666666667,0),
		[12]=v2fM(0,733333333333333,0),
		[13]=v2fM(0,8,0),
		[14]=v2fM(0,866666666666667,0),
		[15]=v2fM(0,933333333333333,0),
		[16]=v2fM(0,0,0666666666666667),
		[17]=v2fM(0,0666666666666667,0,0666666666666667),
		[18]=v2fM(0,133333333333333,0,0666666666666667),
		[19]=v2fM(0,2,0,0666666666666667),
		[20]=v2fM(0,266666666666667,0,0666666666666667),
		[21]=v2fM(0,333333333333333,0,0666666666666667),
		[22]=v2fM(0,4,0,0666666666666667),
		[23]=v2fM(0,466666666666667,0,0666666666666667),
		[24]=v2fM(0,533333333333333,0,0666666666666667),
		[25]=v2fM(0,6,0,0666666666666667),
		[26]=v2fM(0,666666666666667,0,0666666666666667),
		[27]=v2fM(0,733333333333333,0,0666666666666667),
		[28]=v2fM(0,8,0,0666666666666667),
		[29]=v2fM(0,866666666666667,0,0666666666666667),
		[30]=v2fM(0,933333333333333,0,0666666666666667),
		[31]=v2fM(0,0,133333333333333),
		[32]=v2fM(0,0666666666666667,0,133333333333333),
		[33]=v2fM(0,133333333333333,0,133333333333333),
		[34]=v2fM(0,2,0,133333333333333),
		[35]=v2fM(0,266666666666667,0,133333333333333),
		[36]=v2fM(0,333333333333333,0,133333333333333),
		[37]=v2fM(0,4,0,133333333333333),
		[38]=v2fM(0,466666666666667,0,133333333333333),
		[39]=v2fM(0,533333333333333,0,133333333333333),
		[40]=v2fM(0,6,0,133333333333333),
		[41]=v2fM(0,666666666666667,0,133333333333333),
		[42]=v2fM(0,733333333333333,0,133333333333333),
		[43]=v2fM(0,8,0,133333333333333),
		[44]=v2fM(0,866666666666667,0,133333333333333),
		[45]=v2fM(0,933333333333333,0,133333333333333),
		[46]=v2fM(0,0,2),
		[47]=v2fM(0,0666666666666667,0,2),
		[48]=v2fM(0,133333333333333,0,2),
		[49]=v2fM(0,2,0,2),
		[50]=v2fM(0,266666666666667,0,2),
		[51]=v2fM(0,333333333333333,0,2),
		[52]=v2fM(0,4,0,2),
		[53]=v2fM(0,466666666666667,0,2),
		[54]=v2fM(0,533333333333333,0,2),
		[55]=v2fM(0,6,0,2),
		[56]=v2fM(0,666666666666667,0,2),
		[57]=v2fM(0,733333333333333,0,2),
		[58]=v2fM(0,8,0,2),
		[59]=v2fM(0,866666666666667,0,2),
		[60]=v2fM(0,933333333333333,0,2),
		[61]=v2fM(0,0,266666666666667),
		[62]=v2fM(0,0666666666666667,0,266666666666667),
		[63]=v2fM(0,133333333333333,0,266666666666667),
		[64]=v2fM(0,2,0,266666666666667),
		[65]=v2fM(0,266666666666667,0,266666666666667),
		[66]=v2fM(0,333333333333333,0,266666666666667),
		[67]=v2fM(0,4,0,266666666666667),
		[68]=v2fM(0,466666666666667,0,266666666666667),
		[69]=v2fM(0,533333333333333,0,266666666666667),
		[70]=v2fM(0,6,0,266666666666667),
		[71]=v2fM(0,666666666666667,0,266666666666667),
		[72]=v2fM(0,733333333333333,0,266666666666667),
		[73]=v2fM(0,8,0,266666666666667),
		[74]=v2fM(0,866666666666667,0,266666666666667),
		[75]=v2fM(0,933333333333333,0,266666666666667),
		[76]=v2fM(0,0,333333333333333),
		[77]=v2fM(0,0666666666666667,0,333333333333333),
		[78]=v2fM(0,133333333333333,0,333333333333333),
		[79]=v2fM(0,2,0,333333333333333),
		[80]=v2fM(0,266666666666667,0,333333333333333),
		[81]=v2fM(0,333333333333333,0,333333333333333),
		[82]=v2fM(0,4,0,333333333333333),
		[83]=v2fM(0,466666666666667,0,333333333333333),
		[84]=v2fM(0,533333333333333,0,333333333333333),
		[85]=v2fM(0,6,0,333333333333333),
		[86]=v2fM(0,666666666666667,0,333333333333333),
		[87]=v2fM(0,733333333333333,0,333333333333333),
		[88]=v2fM(0,8,0,333333333333333),
		[89]=v2fM(0,866666666666667,0,333333333333333),
		[90]=v2fM(0,933333333333333,0,333333333333333),
		[91]=v2fM(0,0,4),
		[92]=v2fM(0,0666666666666667,0,4),
		[93]=v2fM(0,133333333333333,0,4),
		[94]=v2fM(0,2,0,4),
		[95]=v2fM(0,266666666666667,0,4),
		[96]=v2fM(0,333333333333333,0,4),
		[97]=v2fM(0,4,0,4),
		[98]=v2fM(0,466666666666667,0,4),
		[99]=v2fM(0,533333333333333,0,4),
		[100]=v2fM(0,6,0,4),
		[101]=v2fM(0,666666666666667,0,4),
		[102]=v2fM(0,733333333333333,0,4),
		[103]=v2fM(0,8,0,4),
		[104]=v2fM(0,866666666666667,0,4),
		[105]=v2fM(0,933333333333333,0,4),
		[106]=v2fM(0,0,466666666666667),
		[107]=v2fM(0,0666666666666667,0,466666666666667),
		[108]=v2fM(0,133333333333333,0,466666666666667),
		[109]=v2fM(0,2,0,466666666666667),
		[110]=v2fM(0,266666666666667,0,466666666666667),
		[111]=v2fM(0,333333333333333,0,466666666666667),
		[112]=v2fM(0,4,0,466666666666667),
		[113]=v2fM(0,466666666666667,0,466666666666667),
		[114]=v2fM(0,533333333333333,0,466666666666667),
		[115]=v2fM(0,6,0,466666666666667),
		[116]=v2fM(0,666666666666667,0,466666666666667),
		[117]=v2fM(0,733333333333333,0,466666666666667),
		[118]=v2fM(0,8,0,466666666666667),
		[119]=v2fM(0,866666666666667,0,466666666666667),
		[120]=v2fM(0,933333333333333,0,466666666666667),
		[121]=v2fM(0,0,533333333333333),
		[122]=v2fM(0,0666666666666667,0,533333333333333),
		[123]=v2fM(0,133333333333333,0,533333333333333),
		[124]=v2fM(0,2,0,533333333333333),
		[125]=v2fM(0,266666666666667,0,533333333333333),
		[126]=v2fM(0,333333333333333,0,533333333333333),
		[127]=v2fM(0,4,0,533333333333333),
		[128]=v2fM(0,466666666666667,0,533333333333333),
		[129]=v2fM(0,533333333333333,0,533333333333333),
		[130]=v2fM(0,6,0,533333333333333),
		[131]=v2fM(0,666666666666667,0,533333333333333),
		[132]=v2fM(0,733333333333333,0,533333333333333),
		[133]=v2fM(0,8,0,533333333333333),
		[134]=v2fM(0,866666666666667,0,533333333333333),
		[135]=v2fM(0,933333333333333,0,533333333333333),
		[136]=v2fM(0,0,6),
		[137]=v2fM(0,0666666666666667,0,6),
		[138]=v2fM(0,133333333333333,0,6),
		[139]=v2fM(0,2,0,6),
		[140]=v2fM(0,266666666666667,0,6),
		[141]=v2fM(0,333333333333333,0,6),
		[142]=v2fM(0,4,0,6),
		[143]=v2fM(0,466666666666667,0,6),
		[144]=v2fM(0,533333333333333,0,6),
		[145]=v2fM(0,6,0,6),
		[146]=v2fM(0,666666666666667,0,6),
		[147]=v2fM(0,733333333333333,0,6),
		[148]=v2fM(0,8,0,6),
		[149]=v2fM(0,866666666666667,0,6),
		[150]=v2fM(0,933333333333333,0,6),
		[151]=v2fM(0,0,666666666666667),
		[152]=v2fM(0,0666666666666667,0,666666666666667),
		[153]=v2fM(0,133333333333333,0,666666666666667),
		[154]=v2fM(0,2,0,666666666666667),
		[155]=v2fM(0,266666666666667,0,666666666666667),
		[156]=v2fM(0,333333333333333,0,666666666666667),
		[157]=v2fM(0,4,0,666666666666667),
		[158]=v2fM(0,466666666666667,0,666666666666667),
		[159]=v2fM(0,533333333333333,0,666666666666667),
		[160]=v2fM(0,6,0,666666666666667),
		[161]=v2fM(0,666666666666667,0,666666666666667),
		[162]=v2fM(0,733333333333333,0,666666666666667),
		[163]=v2fM(0,8,0,666666666666667),
		[164]=v2fM(0,866666666666667,0,666666666666667),
		[165]=v2fM(0,933333333333333,0,666666666666667),
		[166]=v2fM(0,0,733333333333333),
		[167]=v2fM(0,0666666666666667,0,733333333333333),
		[168]=v2fM(0,133333333333333,0,733333333333333),
		[169]=v2fM(0,2,0,733333333333333),
		[170]=v2fM(0,266666666666667,0,733333333333333),
		[171]=v2fM(0,333333333333333,0,733333333333333),
		[172]=v2fM(0,4,0,733333333333333),
		[173]=v2fM(0,466666666666667,0,733333333333333),
		[174]=v2fM(0,533333333333333,0,733333333333333),
		[175]=v2fM(0,6,0,733333333333333),
		[176]=v2fM(0,666666666666667,0,733333333333333),
		[177]=v2fM(0,733333333333333,0,733333333333333),
		[178]=v2fM(0,8,0,733333333333333),
		[179]=v2fM(0,866666666666667,0,733333333333333),
		[180]=v2fM(0,933333333333333,0,733333333333333),
		[181]=v2fM(0,0,8),
		[182]=v2fM(0,0666666666666667,0,8),
		[183]=v2fM(0,133333333333333,0,8),
		[184]=v2fM(0,2,0,8),
		[185]=v2fM(0,266666666666667,0,8),
		[186]=v2fM(0,333333333333333,0,8),
		[187]=v2fM(0,4,0,8),
		[188]=v2fM(0,466666666666667,0,8),
		[189]=v2fM(0,533333333333333,0,8),
		[190]=v2fM(0,6,0,8),
		[191]=v2fM(0,666666666666667,0,8),
		[192]=v2fM(0,733333333333333,0,8),
		[193]=v2fM(0,8,0,8),
		[194]=v2fM(0,866666666666667,0,8),
		[195]=v2fM(0,933333333333333,0,8),
		[196]=v2fM(0,0,866666666666667),
		[197]=v2fM(0,0666666666666667,0,866666666666667),
		[198]=v2fM(0,133333333333333,0,866666666666667),
		[199]=v2fM(0,2,0,866666666666667),
		[200]=v2fM(0,266666666666667,0,866666666666667),
		[201]=v2fM(0,333333333333333,0,866666666666667),
		[202]=v2fM(0,4,0,866666666666667),
		[203]=v2fM(0,466666666666667,0,866666666666667),
		[204]=v2fM(0,533333333333333,0,866666666666667),
		[205]=v2fM(0,6,0,866666666666667),
		[206]=v2fM(0,666666666666667,0,866666666666667),
		[207]=v2fM(0,733333333333333,0,866666666666667),
		[208]=v2fM(0,8,0,866666666666667),
		[209]=v2fM(0,866666666666667,0,866666666666667),
		[210]=v2fM(0,933333333333333,0,866666666666667),
		[211]=v2fM(0,0,933333333333333),
		[212]=v2fM(0,0666666666666667,0,933333333333333),
		[213]=v2fM(0,133333333333333,0,933333333333333),
		[214]=v2fM(0,2,0,933333333333333),
		[215]=v2fM(0,266666666666667,0,933333333333333),
		[216]=v2fM(0,333333333333333,0,933333333333333),
	},
	FRAME_SIZES={
		[1]={WIDTH=68,HEIGHT=68},
		[2]={WIDTH=68,HEIGHT=68},
		[3]={WIDTH=68,HEIGHT=68},
		[4]={WIDTH=68,HEIGHT=68},
		[5]={WIDTH=68,HEIGHT=68},
		[6]={WIDTH=68,HEIGHT=68},
		[7]={WIDTH=68,HEIGHT=68},
		[8]={WIDTH=68,HEIGHT=68},
		[9]={WIDTH=68,HEIGHT=68},
		[10]={WIDTH=68,HEIGHT=68},
		[11]={WIDTH=68,HEIGHT=68},
		[12]={WIDTH=68,HEIGHT=68},
		[13]={WIDTH=68,HEIGHT=68},
		[14]={WIDTH=68,HEIGHT=68},
		[15]={WIDTH=68,HEIGHT=68},
		[16]={WIDTH=68,HEIGHT=68},
		[17]={WIDTH=68,HEIGHT=68},
		[18]={WIDTH=68,HEIGHT=68},
		[19]={WIDTH=68,HEIGHT=68},
		[20]={WIDTH=68,HEIGHT=68},
		[21]={WIDTH=68,HEIGHT=68},
		[22]={WIDTH=68,HEIGHT=68},
		[23]={WIDTH=68,HEIGHT=68},
		[24]={WIDTH=68,HEIGHT=68},
		[25]={WIDTH=68,HEIGHT=68},
		[26]={WIDTH=68,HEIGHT=68},
		[27]={WIDTH=68,HEIGHT=68},
		[28]={WIDTH=68,HEIGHT=68},
		[29]={WIDTH=68,HEIGHT=68},
		[30]={WIDTH=68,HEIGHT=68},
		[31]={WIDTH=68,HEIGHT=68},
		[32]={WIDTH=68,HEIGHT=68},
		[33]={WIDTH=68,HEIGHT=68},
		[34]={WIDTH=68,HEIGHT=68},
		[35]={WIDTH=68,HEIGHT=68},
		[36]={WIDTH=68,HEIGHT=68},
		[37]={WIDTH=68,HEIGHT=68},
		[38]={WIDTH=68,HEIGHT=68},
		[39]={WIDTH=68,HEIGHT=68},
		[40]={WIDTH=68,HEIGHT=68},
		[41]={WIDTH=68,HEIGHT=68},
		[42]={WIDTH=68,HEIGHT=68},
		[43]={WIDTH=68,HEIGHT=68},
		[44]={WIDTH=68,HEIGHT=68},
		[45]={WIDTH=68,HEIGHT=68},
		[46]={WIDTH=68,HEIGHT=68},
		[47]={WIDTH=68,HEIGHT=68},
		[48]={WIDTH=68,HEIGHT=68},
		[49]={WIDTH=68,HEIGHT=68},
		[50]={WIDTH=68,HEIGHT=68},
		[51]={WIDTH=68,HEIGHT=68},
		[52]={WIDTH=68,HEIGHT=68},
		[53]={WIDTH=68,HEIGHT=68},
		[54]={WIDTH=68,HEIGHT=68},
		[55]={WIDTH=68,HEIGHT=68},
		[56]={WIDTH=68,HEIGHT=68},
		[57]={WIDTH=68,HEIGHT=68},
		[58]={WIDTH=68,HEIGHT=68},
		[59]={WIDTH=68,HEIGHT=68},
		[60]={WIDTH=68,HEIGHT=68},
		[61]={WIDTH=68,HEIGHT=68},
		[62]={WIDTH=68,HEIGHT=68},
		[63]={WIDTH=68,HEIGHT=68},
		[64]={WIDTH=68,HEIGHT=68},
		[65]={WIDTH=68,HEIGHT=68},
		[66]={WIDTH=68,HEIGHT=68},
		[67]={WIDTH=68,HEIGHT=68},
		[68]={WIDTH=68,HEIGHT=68},
		[69]={WIDTH=68,HEIGHT=68},
		[70]={WIDTH=68,HEIGHT=68},
		[71]={WIDTH=68,HEIGHT=68},
		[72]={WIDTH=68,HEIGHT=68},
		[73]={WIDTH=68,HEIGHT=68},
		[74]={WIDTH=68,HEIGHT=68},
		[75]={WIDTH=68,HEIGHT=68},
		[76]={WIDTH=68,HEIGHT=68},
		[77]={WIDTH=68,HEIGHT=68},
		[78]={WIDTH=68,HEIGHT=68},
		[79]={WIDTH=68,HEIGHT=68},
		[80]={WIDTH=68,HEIGHT=68},
		[81]={WIDTH=68,HEIGHT=68},
		[82]={WIDTH=68,HEIGHT=68},
		[83]={WIDTH=68,HEIGHT=68},
		[84]={WIDTH=68,HEIGHT=68},
		[85]={WIDTH=68,HEIGHT=68},
		[86]={WIDTH=68,HEIGHT=68},
		[87]={WIDTH=68,HEIGHT=68},
		[88]={WIDTH=68,HEIGHT=68},
		[89]={WIDTH=68,HEIGHT=68},
		[90]={WIDTH=68,HEIGHT=68},
		[91]={WIDTH=68,HEIGHT=68},
		[92]={WIDTH=68,HEIGHT=68},
		[93]={WIDTH=68,HEIGHT=68},
		[94]={WIDTH=68,HEIGHT=68},
		[95]={WIDTH=68,HEIGHT=68},
		[96]={WIDTH=68,HEIGHT=68},
		[97]={WIDTH=68,HEIGHT=68},
		[98]={WIDTH=68,HEIGHT=68},
		[99]={WIDTH=68,HEIGHT=68},
		[100]={WIDTH=68,HEIGHT=68},
		[101]={WIDTH=68,HEIGHT=68},
		[102]={WIDTH=68,HEIGHT=68},
		[103]={WIDTH=68,HEIGHT=68},
		[104]={WIDTH=68,HEIGHT=68},
		[105]={WIDTH=68,HEIGHT=68},
		[106]={WIDTH=68,HEIGHT=68},
		[107]={WIDTH=68,HEIGHT=68},
		[108]={WIDTH=68,HEIGHT=68},
		[109]={WIDTH=68,HEIGHT=68},
		[110]={WIDTH=68,HEIGHT=68},
		[111]={WIDTH=68,HEIGHT=68},
		[112]={WIDTH=68,HEIGHT=68},
		[113]={WIDTH=68,HEIGHT=68},
		[114]={WIDTH=68,HEIGHT=68},
		[115]={WIDTH=68,HEIGHT=68},
		[116]={WIDTH=68,HEIGHT=68},
		[117]={WIDTH=68,HEIGHT=68},
		[118]={WIDTH=68,HEIGHT=68},
		[119]={WIDTH=68,HEIGHT=68},
		[120]={WIDTH=68,HEIGHT=68},
		[121]={WIDTH=68,HEIGHT=68},
		[122]={WIDTH=68,HEIGHT=68},
		[123]={WIDTH=68,HEIGHT=68},
		[124]={WIDTH=68,HEIGHT=68},
		[125]={WIDTH=68,HEIGHT=68},
		[126]={WIDTH=68,HEIGHT=68},
		[127]={WIDTH=68,HEIGHT=68},
		[128]={WIDTH=68,HEIGHT=68},
		[129]={WIDTH=68,HEIGHT=68},
		[130]={WIDTH=68,HEIGHT=68},
		[131]={WIDTH=68,HEIGHT=68},
		[132]={WIDTH=68,HEIGHT=68},
		[133]={WIDTH=68,HEIGHT=68},
		[134]={WIDTH=68,HEIGHT=68},
		[135]={WIDTH=68,HEIGHT=68},
		[136]={WIDTH=68,HEIGHT=68},
		[137]={WIDTH=68,HEIGHT=68},
		[138]={WIDTH=68,HEIGHT=68},
		[139]={WIDTH=68,HEIGHT=68},
		[140]={WIDTH=68,HEIGHT=68},
		[141]={WIDTH=68,HEIGHT=68},
		[142]={WIDTH=68,HEIGHT=68},
		[143]={WIDTH=68,HEIGHT=68},
		[144]={WIDTH=68,HEIGHT=68},
		[145]={WIDTH=68,HEIGHT=68},
		[146]={WIDTH=68,HEIGHT=68},
		[147]={WIDTH=68,HEIGHT=68},
		[148]={WIDTH=68,HEIGHT=68},
		[149]={WIDTH=68,HEIGHT=68},
		[150]={WIDTH=68,HEIGHT=68},
		[151]={WIDTH=68,HEIGHT=68},
		[152]={WIDTH=68,HEIGHT=68},
		[153]={WIDTH=68,HEIGHT=68},
		[154]={WIDTH=68,HEIGHT=68},
		[155]={WIDTH=68,HEIGHT=68},
		[156]={WIDTH=68,HEIGHT=68},
		[157]={WIDTH=68,HEIGHT=68},
		[158]={WIDTH=68,HEIGHT=68},
		[159]={WIDTH=68,HEIGHT=68},
		[160]={WIDTH=68,HEIGHT=68},
		[161]={WIDTH=68,HEIGHT=68},
		[162]={WIDTH=68,HEIGHT=68},
		[163]={WIDTH=68,HEIGHT=68},
		[164]={WIDTH=68,HEIGHT=68},
		[165]={WIDTH=68,HEIGHT=68},
		[166]={WIDTH=68,HEIGHT=68},
		[167]={WIDTH=68,HEIGHT=68},
		[168]={WIDTH=68,HEIGHT=68},
		[169]={WIDTH=68,HEIGHT=68},
		[170]={WIDTH=68,HEIGHT=68},
		[171]={WIDTH=68,HEIGHT=68},
		[172]={WIDTH=68,HEIGHT=68},
		[173]={WIDTH=68,HEIGHT=68},
		[174]={WIDTH=68,HEIGHT=68},
		[175]={WIDTH=68,HEIGHT=68},
		[176]={WIDTH=68,HEIGHT=68},
		[177]={WIDTH=68,HEIGHT=68},
		[178]={WIDTH=68,HEIGHT=68},
		[179]={WIDTH=68,HEIGHT=68},
		[180]={WIDTH=68,HEIGHT=68},
		[181]={WIDTH=68,HEIGHT=68},
		[182]={WIDTH=68,HEIGHT=68},
		[183]={WIDTH=68,HEIGHT=68},
		[184]={WIDTH=68,HEIGHT=68},
		[185]={WIDTH=68,HEIGHT=68},
		[186]={WIDTH=68,HEIGHT=68},
		[187]={WIDTH=68,HEIGHT=68},
		[188]={WIDTH=68,HEIGHT=68},
		[189]={WIDTH=68,HEIGHT=68},
		[190]={WIDTH=68,HEIGHT=68},
		[191]={WIDTH=68,HEIGHT=68},
		[192]={WIDTH=68,HEIGHT=68},
		[193]={WIDTH=68,HEIGHT=68},
		[194]={WIDTH=68,HEIGHT=68},
		[195]={WIDTH=68,HEIGHT=68},
		[196]={WIDTH=68,HEIGHT=68},
		[197]={WIDTH=68,HEIGHT=68},
		[198]={WIDTH=68,HEIGHT=68},
		[199]={WIDTH=68,HEIGHT=68},
		[200]={WIDTH=68,HEIGHT=68},
		[201]={WIDTH=68,HEIGHT=68},
		[202]={WIDTH=68,HEIGHT=68},
		[203]={WIDTH=68,HEIGHT=68},
		[204]={WIDTH=68,HEIGHT=68},
		[205]={WIDTH=68,HEIGHT=68},
		[206]={WIDTH=68,HEIGHT=68},
		[207]={WIDTH=68,HEIGHT=68},
		[208]={WIDTH=68,HEIGHT=68},
		[209]={WIDTH=68,HEIGHT=68},
		[210]={WIDTH=68,HEIGHT=68},
		[211]={WIDTH=68,HEIGHT=68},
		[212]={WIDTH=68,HEIGHT=68},
		[213]={WIDTH=68,HEIGHT=68},
		[214]={WIDTH=68,HEIGHT=68},
		[215]={WIDTH=68,HEIGHT=68},
		[216]={WIDTH=68,HEIGHT=68},
	},
	FRAME_INDEXS={
		['wh-r-medtrack0144']=1,
		['wh-r-medtrack0143']=2,
		['wh-r-medtrack0142']=3,
		['wh-r-medtrack0147']=4,
		['wh-r-medtrack0146']=5,
		['wh-r-medtrack0145']=6,
		['wh-r-medtrack0141']=7,
		['wh-r-medtrack0137']=8,
		['wh-r-medtrack0136']=9,
		['wh-r-medtrack0135']=10,
		['wh-r-medtrack0140']=11,
		['wh-r-medtrack0139']=12,
		['wh-r-medtrack0138']=13,
		['wh-r-medtrack0148']=14,
		['wh-r-medtrack0158']=15,
		['wh-r-medtrack0157']=16,
		['wh-r-medtrack0156']=17,
		['wh-r-medtrack0161']=18,
		['wh-r-medtrack0160']=19,
		['wh-r-medtrack0159']=20,
		['wh-r-medtrack0155']=21,
		['wh-r-medtrack0151']=22,
		['wh-r-medtrack0150']=23,
		['wh-r-medtrack0149']=24,
		['wh-r-medtrack0154']=25,
		['wh-r-medtrack0153']=26,
		['wh-r-medtrack0152']=27,
		['wh-r-medtrack0117']=28,
		['wh-r-medtrack0116']=29,
		['wh-r-medtrack0115']=30,
		['wh-r-medtrack0120']=31,
		['wh-r-medtrack0119']=32,
		['wh-r-medtrack0118']=33,
		['wh-r-medtrack0114']=34,
		['wh-r-medtrack0110']=35,
		['wh-r-medtrack0109']=36,
		['wh-r-medtrack0108']=37,
		['wh-r-medtrack0113']=38,
		['wh-r-medtrack0112']=39,
		['wh-r-medtrack0111']=40,
		['wh-r-medtrack0121']=41,
		['wh-r-medtrack0131']=42,
		['wh-r-medtrack0130']=43,
		['wh-r-medtrack0129']=44,
		['wh-r-medtrack0134']=45,
		['wh-r-medtrack0133']=46,
		['wh-r-medtrack0132']=47,
		['wh-r-medtrack0128']=48,
		['wh-r-medtrack0124']=49,
		['wh-r-medtrack0123']=50,
		['wh-r-medtrack0122']=51,
		['wh-r-medtrack0127']=52,
		['wh-r-medtrack0126']=53,
		['wh-r-medtrack0125']=54,
		['wh-r-medtrack0198']=55,
		['wh-r-medtrack0197']=56,
		['wh-r-medtrack0196']=57,
		['wh-r-medtrack0201']=58,
		['wh-r-medtrack0200']=59,
		['wh-r-medtrack0199']=60,
		['wh-r-medtrack0195']=61,
		['wh-r-medtrack0191']=62,
		['wh-r-medtrack0190']=63,
		['wh-r-medtrack0189']=64,
		['wh-r-medtrack0194']=65,
		['wh-r-medtrack0193']=66,
		['wh-r-medtrack0192']=67,
		['wh-r-medtrack0202']=68,
		['wh-r-medtrack0212']=69,
		['wh-r-medtrack0211']=70,
		['wh-r-medtrack0210']=71,
		['wh-r-medtrack0215']=72,
		['wh-r-medtrack0214']=73,
		['wh-r-medtrack0213']=74,
		['wh-r-medtrack0209']=75,
		['wh-r-medtrack0205']=76,
		['wh-r-medtrack0204']=77,
		['wh-r-medtrack0203']=78,
		['wh-r-medtrack0208']=79,
		['wh-r-medtrack0207']=80,
		['wh-r-medtrack0206']=81,
		['wh-r-medtrack0171']=82,
		['wh-r-medtrack0170']=83,
		['wh-r-medtrack0169']=84,
		['wh-r-medtrack0174']=85,
		['wh-r-medtrack0173']=86,
		['wh-r-medtrack0172']=87,
		['wh-r-medtrack0168']=88,
		['wh-r-medtrack0164']=89,
		['wh-r-medtrack0163']=90,
		['wh-r-medtrack0162']=91,
		['wh-r-medtrack0167']=92,
		['wh-r-medtrack0166']=93,
		['wh-r-medtrack0165']=94,
		['wh-r-medtrack0175']=95,
		['wh-r-medtrack0185']=96,
		['wh-r-medtrack0184']=97,
		['wh-r-medtrack0183']=98,
		['wh-r-medtrack0188']=99,
		['wh-r-medtrack0187']=100,
		['wh-r-medtrack0186']=101,
		['wh-r-medtrack0182']=102,
		['wh-r-medtrack0178']=103,
		['wh-r-medtrack0177']=104,
		['wh-r-medtrack0176']=105,
		['wh-r-medtrack0181']=106,
		['wh-r-medtrack0180']=107,
		['wh-r-medtrack0179']=108,
		['wh-r-medtrack0036']=109,
		['wh-r-medtrack0035']=110,
		['wh-r-medtrack0034']=111,
		['wh-r-medtrack0039']=112,
		['wh-r-medtrack0038']=113,
		['wh-r-medtrack0037']=114,
		['wh-r-medtrack0033']=115,
		['wh-r-medtrack0029']=116,
		['wh-r-medtrack0028']=117,
		['wh-r-medtrack0027']=118,
		['wh-r-medtrack0032']=119,
		['wh-r-medtrack0031']=120,
		['wh-r-medtrack0030']=121,
		['wh-r-medtrack0040']=122,
		['wh-r-medtrack0050']=123,
		['wh-r-medtrack0049']=124,
		['wh-r-medtrack0048']=125,
		['wh-r-medtrack0053']=126,
		['wh-r-medtrack0052']=127,
		['wh-r-medtrack0051']=128,
		['wh-r-medtrack0047']=129,
		['wh-r-medtrack0043']=130,
		['wh-r-medtrack0042']=131,
		['wh-r-medtrack0041']=132,
		['wh-r-medtrack0046']=133,
		['wh-r-medtrack0045']=134,
		['wh-r-medtrack0044']=135,
		['wh-r-medtrack0009']=136,
		['wh-r-medtrack0008']=137,
		['wh-r-medtrack0007']=138,
		['wh-r-medtrack0012']=139,
		['wh-r-medtrack0011']=140,
		['wh-r-medtrack0010']=141,
		['wh-r-medtrack0006']=142,
		['wh-r-medtrack0002']=143,
		['wh-r-medtrack0001']=144,
		['wh-r-medtrack0000']=145,
		['wh-r-medtrack0005']=146,
		['wh-r-medtrack0004']=147,
		['wh-r-medtrack0003']=148,
		['wh-r-medtrack0013']=149,
		['wh-r-medtrack0023']=150,
		['wh-r-medtrack0022']=151,
		['wh-r-medtrack0021']=152,
		['wh-r-medtrack0026']=153,
		['wh-r-medtrack0025']=154,
		['wh-r-medtrack0024']=155,
		['wh-r-medtrack0020']=156,
		['wh-r-medtrack0016']=157,
		['wh-r-medtrack0015']=158,
		['wh-r-medtrack0014']=159,
		['wh-r-medtrack0019']=160,
		['wh-r-medtrack0018']=161,
		['wh-r-medtrack0017']=162,
		['wh-r-medtrack0090']=163,
		['wh-r-medtrack0089']=164,
		['wh-r-medtrack0088']=165,
		['wh-r-medtrack0093']=166,
		['wh-r-medtrack0092']=167,
		['wh-r-medtrack0091']=168,
		['wh-r-medtrack0087']=169,
		['wh-r-medtrack0083']=170,
		['wh-r-medtrack0082']=171,
		['wh-r-medtrack0081']=172,
		['wh-r-medtrack0086']=173,
		['wh-r-medtrack0085']=174,
		['wh-r-medtrack0084']=175,
		['wh-r-medtrack0094']=176,
		['wh-r-medtrack0104']=177,
		['wh-r-medtrack0103']=178,
		['wh-r-medtrack0102']=179,
		['wh-r-medtrack0107']=180,
		['wh-r-medtrack0106']=181,
		['wh-r-medtrack0105']=182,
		['wh-r-medtrack0101']=183,
		['wh-r-medtrack0097']=184,
		['wh-r-medtrack0096']=185,
		['wh-r-medtrack0095']=186,
		['wh-r-medtrack0100']=187,
		['wh-r-medtrack0099']=188,
		['wh-r-medtrack0098']=189,
		['wh-r-medtrack0063']=190,
		['wh-r-medtrack0062']=191,
		['wh-r-medtrack0061']=192,
		['wh-r-medtrack0066']=193,
		['wh-r-medtrack0065']=194,
		['wh-r-medtrack0064']=195,
		['wh-r-medtrack0060']=196,
		['wh-r-medtrack0056']=197,
		['wh-r-medtrack0055']=198,
		['wh-r-medtrack0054']=199,
		['wh-r-medtrack0059']=200,
		['wh-r-medtrack0058']=201,
		['wh-r-medtrack0057']=202,
		['wh-r-medtrack0067']=203,
		['wh-r-medtrack0077']=204,
		['wh-r-medtrack0076']=205,
		['wh-r-medtrack0075']=206,
		['wh-r-medtrack0080']=207,
		['wh-r-medtrack0079']=208,
		['wh-r-medtrack0078']=209,
		['wh-r-medtrack0074']=210,
		['wh-r-medtrack0070']=211,
		['wh-r-medtrack0069']=212,
		['wh-r-medtrack0068']=213,
		['wh-r-medtrack0073']=214,
		['wh-r-medtrack0072']=215,
		['wh-r-medtrack0071']=216,
	},
};
