function vector2fMake(X,Y)
	return {[1]=X,[2]=Y};
end;

function vector3fMake(X,Y,Z)
	return {[1]=X,[2]=Y,[3]=Z};
end;

function v2fM(X,Y)
	return {[1]=X,[2]=Y};
end;

function v3fM(X,Y,Z)
	return {[1]=X,[2]=Y,[3]=Z};
end;

function vector2fAdd(A,B)
	return {[1]=A[1]+B[1],[2]=A[2]+B[2]};
end;

function vector2fSubtract(A,B)
	return {[1]=A[1]-B[1],[2]=A[2]-B[2]};
end;

function vector2fMultiply(A,B)
	return {[1]=A[1]*B[1],[2]=A[2]*B[2]};
end;

function vector2fScale(A,SCALE)
	return {[1]=A[1]*SCALE,[2]=A[2]*SCALE};
end;

function vector3fAdd(A,B)
	return {[1]=A[1]+B[1],[2]=A[2]+B[2],[3]=A[3]+B[3]};
end;

function vector3fSubtract(A,B)
	return {[1]=A[1]-B[1],[2]=A[2]-B[2],[3]=A[3]-B[3]};
end;

function vector3fMultiply(A,B)
	return {[1]=A[1]*B[1],[2]=A[2]*B[2],[3]=A[3]*B[3]};
end;

function vector3fScale(A,SCALE)
	return {[1]=A[1]*SCALE,[2]=A[2]*SCALE,[3]=A[3]*SCALE};
end;

function vector2fSame(A,B)
	return (A[1]==B[1]) and (A[2]==B[2]);
end;

function vector3fSame(A,B)
	return (A[1]==B[1]) and (A[2]==B[2]) and (A[3]==B[3]);
end;

function vector2fFloor(A)
	return {[1]=math.floor(A[1]),[2]=math.floor(A[2])};
end;

function vector3fFloor(A)
	return {[1]=math.floor(A[1]),[2]=math.floor(A[2]),[3]=math.floor(A[3])};
end;

function vector2fInvert(A)
	return {[1]=-A[1],[2]=-A[2]};
end;

function vector3fInvert(A)
	return {[1]=-A[1],[2]=-A[2],[3]=-A[3]};
end;