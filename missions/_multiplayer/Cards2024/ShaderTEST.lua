shaderCreate = function()
	local self = {}

	self.COLOUR_TEXTURE = loadOGLTexture('b-a-armoury_00000.png',true,false,false,false)
	self.HEIGHT_TEXTURE = loadOGLTexture('b-a-armoury_00000z.png',true,false,false,false)
	self.COLOUR_TEXTURE2 = loadOGLTexture('b-a-armoury_00001.png',true,false,false,false)
	self.HEIGHT_TEXTURE2 = loadOGLTexture('b-a-armoury_00001z.png',true,false,false,false)

	self.MOUSEOVER = getElementEX(nil,anchorLT,XYWH(0,0,0,0),false,{nomouseevent=true})
	self.SCREEN_FBO = fboclass.make(200,200,true,false,0,false)

	self.GMZ_Shader = loadGLSL('gpu_quad')

	setVisible(self.MOUSEOVER,true)

	self.startFBO = function()

		self.SCREEN_FBO:doBegin();
		OGL_BEGIN();
			OGL_GLENABLE(GL_BLEND);
			OGL_QUAD_BEGIN2D(self.SCREEN_FBO.W,self.SCREEN_FBO.H);

				--self.GMZ_Shader:setUniform1I(self.GMZ_Shader.zpos,40);
				self.GMZ_Shader.zpos = self.GMZ_Shader:getLocation('zpos')
				self.GMZ_Shader:use();
				self.GMZ_Shader:setUniform1I(self.GMZ_Shader.zpos,40);
				OGL_ACTIVE_TEXTURE(GL_TEXTURE0);
				self.COLOUR_TEXTURE:bind();
				OGL_ACTIVE_TEXTURE(GL_TEXTURE1);
				self.HEIGHT_TEXTURE:bind();
				OGL_QUAD_RENDER(0,0,200,200,0,0,1,1);
	   
				OGL_ACTIVE_TEXTURE(GL_TEXTURE1);
				self.HEIGHT_TEXTURE2:bind();
				OGL_ACTIVE_TEXTURE(GL_TEXTURE0);
				self.COLOUR_TEXTURE2:bind();
				OGL_QUAD_RENDER(0,0,200,200,0,0,1,1);

			OGL_QUAD_END2D();
			OGL_GLDISABLE(GL_BLEND);
		OGL_END();
		self.SCREEN_FBO:doEnd();

		SGUI_settextureid(self.MOUSEOVER.ID,self.SCREEN_FBO:getTextureID(),200,200,200,200)
		setWH(self.MOUSEOVER,self.COLOUR_TEXTURE.W,self.COLOUR_TEXTURE.H)

	end

	return self
end

shader = shaderCreate()

regTickCallback('shader.startFBO()')